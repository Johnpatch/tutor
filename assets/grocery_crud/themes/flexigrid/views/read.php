<?php

	$this->set_css($this->default_theme_path.'/flexigrid/css/flexigrid.css');
	$this->set_js_lib($this->default_theme_path.'/flexigrid/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/flexigrid/js/flexigrid-edit.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div class="mDiv">
		<div class="ftitle">
			<div class='ftitle-left'>
				<?php echo $this->l('list_record'); ?> <?php echo $subject?>
			</div>
			<div class='clear'></div>
		</div>
		<div title="<?php echo $this->l('minimize_maximize');?>" class="ptogtitle">
			<span></span>
		</div>
	</div>
<div id='main-table-box'>
	<?php echo form_open( $read_url, 'method="post" id="crudForm"  enctype="multipart/form-data"'); ?>
	<div class='form-div'>
		<?php
		$counter = 0;
			foreach($fields as $field)
			{
				if($field->field_name != "created"){
				$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
				$counter++;
		?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
					<?php if($field->field_name != 'attach' && $field->field_name != "time_slots") { ?>
					<?php echo $input_fields[$field->field_name]->input?>
					<?php } else if($field->field_name == "time_slots"){ 
						$time_str = (array)$input_fields['created'];
						$time_str_index = strip_tags($time_str['input']);
						$value = $input_fields[$field->field_name]->input;
						$value = strip_tags($value);
						$temp = explode('"',$value);
						$time_slot_list = explode(',',$temp[0]);
						$time_zone = $this->timezone[$time_str_index];
						
						$userTimezone = new DateTimeZone($time_zone);
						$gmtTimezone = date_create("now",timezone_open("UTC"));
						$time_str = "";
						foreach($time_slot_list as $k => $v){
							if(!empty($v)){
								$time_slot = explode("-",$v);	
								$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone('UTC'));
								$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone('UTC'));
								$offset = timezone_offset_get($userTimezone,$gmtTimezone);
								$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
								$myDateTime_start->add($myInterval);
								$myDateTime_end->add($myInterval);
								
								$result = $myDateTime_start->format('H')."-".$myDateTime_end->format('H');
								if($k != 0)
									$time_str .= ",";
								$time_str .= $result;
							}	
						}
						echo '<div id="field-time_slots" class="readonly_label">'.$time_str.'</div>';
					} else{ 
						$temp = strip_tags($input_fields[$field->field_name]->input);?>
					<?php if($subject == "Instant Bids"){ ?>
					<script>
						$.ajax({
			                method: "POST",
			                url: "../../tutor/get_attach_list_web",
			                data: { attach:'<?php echo $temp;?>' },
			                success: function(data){
			                    var obj = JSON.parse(data);
			                    $('#attach_input_box').html(obj.html);
			                }
			            });
					</script>
					<?php } else if( $subject == "Instant Question"){ ?>
						<script>
						$.ajax({
			                method: "POST",
			                url: "../../student/get_attach_list_web",
			                data: { attach:'<?php echo $temp;?>' },
			                success: function(data){
			                    var obj = JSON.parse(data);
			                    $('#attach_input_box').html(obj.html);
			                }
			            });
					</script>
					<?php } else{ ?>
					<a href="<?php echo base_url()."assets/uploads/instant_bids/".$temp;?>" download><?php echo $temp;?></a>	
					<?php } } ?>
				</div>
				<div class='clear'></div>
			</div>
		<?php }}?>
		<?php if(!empty($hidden_fields)){?>
		<!-- Start of hidden inputs -->
			<?php
				foreach($hidden_fields as $hidden_field){
					echo $hidden_field->input;
				}
			?>
		<!-- End of hidden inputs -->
		<?php }?>
		<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
		<div id='report-error' class='report-div error'></div>
		<div id='report-success' class='report-div success'></div>
	</div>
	<div class="pDiv">
		<div class='form-button-box'>
			<input type='button' value='<?php echo $this->l('form_back_to_list'); ?>' class="btn btn-large back-to-list" id="cancel-button" />
		</div>
		<div class='form-button-box'>
			<div class='small-loading' id='FormLoading'><?php echo $this->l('form_update_loading'); ?></div>
		</div>
		<div class='clear'></div>
	</div>
	<?php echo form_close(); ?>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
</script>
