<?php

	$this->set_css($this->default_theme_path.'/flexigrid/css/flexigrid.css');

    $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.form.min.js');
	$this->set_js_config($this->default_theme_path.'/flexigrid/js/flexigrid-edit.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
	$is_parent = $is_child = 0;
?>
<script src="<?php echo URL_ADMIN_JS;?>jquery-1.11.1.min.js"></script>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div class="mDiv">
		<div class="ftitle">
			<div class='ftitle-left'>
				<?php echo $this->l('form_edit'); ?> <?php echo $subject?>
			</div>
			<div class='clear'></div>
		</div>
		<div title="<?php echo $this->l('minimize_maximize');?>" class="ptogtitle">
			<span></span>
		</div>
	</div>
<div id='main-table-box'>
	<?php echo form_open( $update_url, 'method="post" id="crudForm"  enctype="multipart/form-data"'); ?>
	<div class='form-div'>
		<?php
		$counter = 0;
		$is_parent = 0;
			foreach($fields as $field)
			{
				if($field->field_name != "created"){
				$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
				$counter++;
		?>
			<?php if($field->field_name != 'child' && $field->field_name != 'parent'){ ?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<?php if($field->field_name != "collab_id"){ ?>
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
				<?php if($field->field_name != "time_slots") { ?>
					<?php echo $input_fields[$field->field_name]->input;?>
				<?php } else { 
					$time_str = (array)$input_fields['created'];
					$time_str = explode("selected='selected' >",$time_str['input']);
					if(isset($time_str[1])){
						$time_str = explode("</option>",$time_str[1]);
						$time_str_index = strip_tags($time_str[0]);	
						$time_zone = $this->timezone[$time_str_index];
					}
					else{
						$time_zone = "UTC";
					}
					$value = $input_fields[$field->field_name]->input;
					$value = explode('value="',$value);
					$temp = explode('"',$value[1]);
					$time_slot_list = explode(',',$temp[0]);
					
					$userTimezone = new DateTimeZone($time_zone);
					$gmtTimezone = date_create("now",timezone_open("UTC"));
					$time_str = "";
					foreach($time_slot_list as $k => $v){
						if(!empty($v)){
							$time_slot = explode("-",$v);	
							$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone('UTC'));
							$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone('UTC'));
							$offset = timezone_offset_get($userTimezone,$gmtTimezone);
							$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
							$myDateTime_start->add($myInterval);
							$myDateTime_end->add($myInterval);
							
							$result = $myDateTime_start->format('H')."-".$myDateTime_end->format('H');
							if($k != 0)
								$time_str .= ",";
							$time_str .= $result;
						}	
					}
					echo '<input type="text" name="time_slots" value="'.$time_str.'" placeholder="Example format 6-7,13-14,14-16,20.30-21.30">';
				} ?>
				</div>
				<div class='clear'></div>
				<?php } else { 
				$cur_id = explode('delete/',$delete_url);?>
					<div class="form-display-as-box">
						<input  id="regenerate_collab" type='button' value='Regenerate Collab ID' class="btn btn-large" style="border: 2px solid #14bdee;margin-top:-8px;" onclick="generate_collab(<?php echo $cur_id[1];?>)"/>
					</div>
					<div class="form-input-box" id="collab_id_input_box">
						<input id="field-collab_id" class="form-control" type="text" value="" maxlength="255">
					</div>
				<?php } ?>
			</div>
			<?php } else if($field->field_name == 'child') { 
				$is_parent = 1;?>
				<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box" style="display: none;">
				</div>
			<?php } else if($field->field_name == 'parent'){ 
				$is_child = 1;?>
		<?php }}}?>
		<?php if(!empty($hidden_fields)){?>
		<!-- Start of hidden inputs -->
			<?php
				foreach($hidden_fields as $hidden_field){
					echo $hidden_field->input;
				}
			?>
		<!-- End of hidden inputs -->
		<?php }?>
		<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
		<div id='report-error' class='report-div error'></div>
		<div id='report-success' class='report-div success'></div>
	</div>
	<div class="pDiv">
		<div class='form-button-box'>
			<input  id="form-button-save" type='submit' value='<?php echo $this->l('form_update_changes'); ?>' class="btn btn-large"/>
		</div>
<?php 	if(!$this->unset_back_to_list) { ?>
		<div class='form-button-box'>
			<input type='button' value='<?php echo $this->l('form_update_and_go_back'); ?>' id="save-and-go-back-button" class="btn btn-large"/>
		</div>
		<div class='form-button-box'>
			<input type='button' value='<?php echo $this->l('form_cancel'); ?>' class="btn btn-large" id="cancel-button" />
		</div>
<?php 	} ?>
		<div class='form-button-box'>
			<div class='small-loading' id='FormLoading'><?php echo $this->l('form_update_loading'); ?></div>
		</div>
		<div class='clear'></div>
	</div>
	
	<?php echo form_close(); ?>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
	var is_parent = "<?php echo $is_parent;?>";
	var is_child = "<?php echo $is_child;?>";
	if(is_parent == 1){
		$.ajax({
	        url:"<?php echo URL_STUDENT_SELECT_CHILD1; ?>",
	        data:{email:$('#field-email').val()},
	        type:"post", 
	        success :function(data){
				var obj = JSON.parse(data);
				var html = "";
				for(var i = 0;i<obj.list.length;i++){
					html += '<div class="form-field-box">';
					html += '<div class="form-display-as-box">Child Student Email ID '+parseInt(i+1)+':</div>';
					html += '<div class="form-input-box"><input class="form-control" name="child_email'+i+'" type="text" value="'+obj.list[i].email+'" maxlength="100"></div>';
					html += '</div>';
				}
				html += '<div class="form-field-box">';
				html += '<div class="form-display-as-box">Child Student Email ID '+parseInt(i+1)+':</div>';
				html += '<div class="form-input-box"><input class="form-control" name="child_email'+i+'" type="text" value="" maxlength="100"></div>';
				html += '</div>';
				html += '<input type="hidden" name="update_child"/>';
				$('#child_field_box').append(html);
				$('#child_field_box').css('display','block');
	        }
	    })	
	}
	if(is_child == 1){
		$('#parent_field_box').css('display','block');
		$.ajax({
	        url:"<?php echo URL_STUDENT_GET_PARENT; ?>",
	        data:{email:$('#field-email').val()},
	        type:"post", 
	        success :function(data){
				var obj = JSON.parse(data);
				var html = "";
				console.log(obj);
				if(obj.info != null){
					html += '<div class="form-field-box">';
					html += '<div class="form-display-as-box">Parent Email:</div>';
					html += '<div class="form-input-box"><input class="form-control" name="parent_email" type="text" value="'+obj.info.email+'" maxlength="100"></div>';
					html += '</div>';
					html += '<input type="hidden" name="update_parent"/>';
				} else{
					html += '<div class="form-field-box">';
					html += '<div class="form-display-as-box">Parent Email:</div>';
					html += '<div class="form-input-box"><input class="form-control" name="parent_email" type="text" value="" maxlength="100"></div>';
					html += '</div>';
					html += '<input type="hidden" name="update_parent"/>';
				}
				$('#parent_field_box').append(html);
	        }
	    })	
	}
	
	function generate_collab(id){
		$.ajax({
	        url:"<?php echo URL_STUDENT_REGENERATE_COLLAB; ?>",
	        data:{id:id},
	        type:"post", 
	        success :function(data){
				var obj = JSON.parse(data);
				$('#field-collab_id').val(obj.collab_id);
	        }
	    })	
	}
</script>