<?php 

	$column_width = (int)(80/count($columns));
	$name_field = "";
	$timezone = "";
	if(!empty($list)){
?>
<style>
	a.fa-mixcloud[href='#']{
		display: block;
	}
</style>
<div class="bDiv" >
		<table cellspacing="0" cellpadding="0" border="0" id="flex1">
		<thead>
			<tr class='hDiv'>
				<?php foreach($columns as $column){
					if($column->display_as != "Created by" && $column->display_as != "Tutor id" && $column->display_as != "Student id" && $column->display_as != "Created"){
					?>
				<th width='<?php echo $column_width?>%'>
					<div class="text-left field-sorting <?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?><?php echo $order_by[1]?><?php }?>" 
						rel='<?php echo $column->field_name?>'>
						<?php echo $column->display_as?>
					</div>
				</th>
				<?php } elseif($column->display_as == "Tutor id" || $column->display_as == "Student id" || $column->display_as == "Created"){$timezone=$column->field_name;} else{ $name_field = $column->field_name;} }?>
				<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
				<th align="left" abbr="tools" axis="col1" class="" width='20%'>
					<div class="text-right">
						<?php echo $this->l('list_actions'); ?>
					</div>
				</th>
				<?php }?>
			</tr>
		</thead>		
		<tbody>
<?php 

foreach($list as $num_row => $row){ 
$a = (array)$row;
if(!isset($row->collab_id) || !isset($row->status) ||(isset($row->collab_id) && $row->status == ucfirst($GLOBALS['params'][0]))){
if(isset($row->start_date)){
	$row->start_date = str_replace('/','-',$row->start_date);
	$row->start_date = date($_SESSION['date_format'],strtotime($row->start_date));	
}
if(isset($row->iq_date)){
	$row->iq_date = str_replace('/','-',$row->iq_date);
	$row->iq_date = date($_SESSION['date_format'],strtotime($row->iq_date));	
}
if(isset($row->date)){
	$row->date = str_replace('/','-',$row->date);
	$row->date = date($_SESSION['date_format'],strtotime($row->date));	
}
$is_exist_collab = 0;
if(isset($row->open_collab_id) && $row->open_collab_id == $_SESSION['collab_id']){
	$is_exist_collab = 1;
}
if(isset($row->collab_id))
	$timezone = $_SESSION['time_zone'];
?>        
		<tr  <?php if($num_row % 2 == 1){?>class="erow"<?php }?>>
			<?php 
			$myDateTime_start = $myDateTime_end = "";
			$book_time = "";
			foreach($columns as $column){
				$a = (array)$column;
				if($column->field_name == "start_date" || $column->field_name == "iq_date" || $column->field_name == "date"){
					$book_time = $row->{$column->field_name};
					$book_time = date("Y-m-d",strtotime($book_time));
				}
				else if($column->field_name == "time_slot" || $column->field_name == "iq_time_slot"){
					if(!empty($timezone)){
						if(is_numeric($timezone) == 1)
							$index = $timezone;
						else
							$index = $row->$timezone;
						$time_zone = $this->timezone[$index];
						$time = $row->{$column->field_name};
						if(!empty($time)){
							$time_slot = explode("-",$time);
							$userTimezone = new DateTimeZone($time_zone);
							$gmtTimezone = date_create("now",timezone_open("UTC"));
							$myDateTime_start = new DateTime(date('Y-m-d',strtotime($book_time))." ".$time_slot[0].":00:00", new DateTimeZone('UTC'));
							$myDateTime_end = new DateTime(date('Y-m-d',strtotime($book_time))." ".$time_slot[1].":00:00", new DateTimeZone('UTC'));
							
							$offset = timezone_offset_get($userTimezone,$gmtTimezone);
							
							$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
							$myDateTime_start->add($myInterval);
							$myDateTime_end->add($myInterval);
							
						}
					}
				}
			}
			foreach($columns as $column){
				$b = (array)$column;
				if($column->display_as != "Created by" && $column->display_as != "Tutor id" && $column->display_as != "Student id" && $column->display_as != "Created"){
					
					if($column->display_as == 'Student Name' || $column->display_as == 'Student'){	
						$name = $row->$name_field;
						$name = explode('---',$name);
						if($name[0] == "Show Name"){
						?>
							<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
				<div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
			</td>			
						<?php } else { ?>
							<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
				<div class='text-left'><?php echo "S".$name[1]; ?></div>
			</td>			
						<?php }
					}elseif($column->field_name == "time_slot" || $column->field_name == "iq_time_slot" || $column->field_name == "start_date" || $column->field_name == "iq_date" || $column->field_name == "date"){ ?>
						<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
							<div class='text-left'>
								<?php
									if(!empty($timezone)){
										if(is_numeric($timezone) == 1)
											$index = $timezone;
										else
											$index = $row->$timezone;
										$time_zone = $this->timezone[$index];
										$time = $row->{$column->field_name};
										if(!empty($time)){
											if($column->field_name == "start_date" || $column->field_name == "iq_date" || $column->field_name == "date"){
												if(!empty($myDateTime_start))
													$result = $myDateTime_start->format($_SESSION['date_format']);
												else
													$result = '';
												echo $result != '' ? $result : '&nbsp;' ;		
											} else{
												if(!empty($myDateTime_start))
													$result = $myDateTime_start->format('H')."-".$myDateTime_end->format('H');
												else
													$result = '';
												echo $result != '' ? $result : '&nbsp;' ;	
											}
											
										}
										else
											echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ;
									}else{
									 	echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ;
									}
								?>
							</div>
						</td>		
					<?php } else if($column->field_name == "time_slots"){ ?>
						<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
							<div class='text-left'>
								<?php
									if(!empty($row->$timezone)){
										if(is_numeric($timezone) == 1)
											$index = $timezone;
										else
											$index = $row->$timezone;
										$time_zone = $this->timezone[$index];
										$time = $row->{$column->field_name};
										$time = explode(",",$time);
										$userTimezone = new DateTimeZone($time_zone);
										$gmtTimezone = date_create("now",timezone_open("UTC"));
										$time_str = "";
										foreach($time as $k => $v){
											if(!empty($v)){
												$time_slot = explode("-",$v);	
												$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone('UTC'));
												$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone('UTC'));
												$offset = timezone_offset_get($userTimezone,$gmtTimezone);
												$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
												$myDateTime_start->add($myInterval);
												$myDateTime_end->add($myInterval);
												
												$result = $myDateTime_start->format('H')."-".$myDateTime_end->format('H');
												if($k != 0)
													$time_str .= ",";
												$time_str .= $result;
											}	
										}
										echo $time_str;
									}else{
									 	echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ;
									}
								?>
							</div>
						</td>	
					<?php }elseif($column->field_name == "tutor_gdrive_url" || $column->field_name == "student_gdrive_url"){ ?>
						<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
							<div class='text-left'><?php if($row->{$column->field_name} != ''){ ?><a href="<?php echo str_replace("...","",$row->{$column->field_name}); ?>" target="_blank">GDrive Url</a><?php } ?></div>
						</td>
						
					<?php }elseif($column->field_name == "tutor_id1" || $column->field_name == "student_id1" || $column->field_name == "parent_id1"){ ?>
						<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
							<div class='text-left'>
							<?php if($row->{$column->field_name} != ''){ 
							$slug = prepare_slug($row->{$column->field_name}, 'slug1', 'users');?>
							<a href="<?php echo site_url('user-profile/'.$slug);?>"><?php echo $row->{$column->field_name}; ?></a>
							<?php } ?></div>
						</td>
						
					<?php }else{ ?>
			<td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
				<div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
			</td>
			<?php } } }?>
			<?php 
			if((!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)) && $is_exist_collab == 0){?>
			<td align="left" width='20%'>
				<div class='tools'>				
					<?php if(!$unset_delete){
						if(isset($row->status) && $row->status != "Session Initiated"){?>
                    	<a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row" >
                    			<span class='delete-icon'></span>
                    	</a>
                    <?php } else { ?>
                    	<a href='<?php echo $row->delete_url?>' title='Cancel' class="delete-row" >
                    			<span class='delete-icon'></span>
                    	</a>
                    <?php }}?>
                    <?php if(!$unset_edit){?>
						<a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button"><span class='edit-icon'></span></a>
					<?php }?>
					<?php if(!$unset_read){?>
						<a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button"><span class='read-icon'></span></a>
					<?php }?>
					<?php 
					if(isset($row->status) && ($row->status == "Session Initiated" || $row->status == "Running")){
						$today = date('Y-m-d');
						$s_d = strtotime(str_replace('/','-',$row->start_date));
						$e_d = strtotime($row->end_date);
						$c_d = strtotime($today);
						$dayWeek = strtoupper(date('D'));
						$is_holiday = strpos($row->days_off,$dayWeek);
						//if($s_d <= $c_d && $e_d >= $c_d && $is_holiday == false){
							$cur_time 	= (float)date('H.i');
							$time 	  	= explode('-', str_replace(' ', '', $row->time_slot));
							$start_time = date('H:i', strtotime(number_format($time[0],2)));
							$end_time   = date('H:i', strtotime(number_format($time[1],2)));
							$certain_mins_before_start_time = (float)date('H.i', strtotime($start_time.' -'.$this->enable_initiate_session_option_before_mins.' minutes'));
							$certain_mins_end_time = (float)date('H.i', strtotime($end_time));
							//if($cur_time >= $certain_mins_before_start_time && $cur_time <= $certain_mins_end_time){
								
								if(!empty($row->action_urls)){
									foreach($row->action_urls as $action_unique_id => $action_url){ 
										$action = $actions[$action_unique_id];
								?>
										<a href="#" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>" onclick="join_meeting(<?php echo $row->booking_id;?>,'<?php echo $action_url; ?>')"><?php 
											if(!empty($action->image_url))
											{
												?><img src="<?php echo $action->image_url; ?>" alt="<?php echo $action->label?>" /><?php 	
											}
										?></a>		
								<?php }
								}
							//}
						//}
					}
					else {
						if(!empty($row->action_urls)){
							foreach($row->action_urls as $action_unique_id => $action_url){ 
								$action = $actions[$action_unique_id];
						?>
								<a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>"><?php 
									if(!empty($action->image_url))
									{
										?><img src="<?php echo $action->image_url; ?>" alt="<?php echo $action->label?>" /><?php 	
									}
								?></a>		
						<?php }
						}
					}
					?>					
                    <div class='clear'></div>
				</div>
			</td>
			<?php }?>
		</tr>
<?php }} ?>       
<script>
	function join_meeting(booking_id,zoom_url){
		$.ajax({
            method: "POST",
            url: "<?php echo URL_BOOKING_RUN_STUDENT; ?>",
            data: { booking_id:booking_id },
            success: function(data){
            	var obj = JSON.parse(data);
            	if(obj.is_open == 1)
            		window.open(obj.url);
            	window.open(zoom_url);
            }
        });
	}
</script>
		</tbody>
		</table>
	</div>
<?php }else{?>
	<br/>
	&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $this->l('list_no_items'); ?>
	<br/>
	<br/>
<?php }?>	
