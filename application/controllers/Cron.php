<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        
    }
    
    public function index(){
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$user_list = $this->base_model->get_user_list_iq_online();
		if(!empty($user_list)){
			foreach($user_list as $key => $value){
				$time_zone = $time_zone_list[$value->time_zone];
				$userTimezone = new DateTimeZone($time_zone);
				$gmtTimezone = date_create("now",timezone_open("UTC"));
				
				$myDateTime = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone($time_zone));
				
				$offset = timezone_offset_get($userTimezone,$gmtTimezone);
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime->add($myInterval);
				if($myDateTime->format("Y-m-d H:i:s") >= $myDateTime->format("Y-m-d 00:00:00") && $myDateTime->format("Y-m-d H:i:s") <= $myDateTime->format("Y-m-d 01:00:00"))
					$this->base_model->format_iq_live($value->id);
			}
		}
	}

}