<?php defined('BASEPATH') OR exit('No direct script access allowed');

 /* 
    | -----------------------------------------------------
    | PRODUCT NAME:     MENORAH TUTORS SYSTEM
    | -----------------------------------------------------
    | AUTHOR:           DIGITAL VIDHYA TEAM
    | -----------------------------------------------------
    | EMAIL:            digitalvidhya4u@gmail.com
    | -----------------------------------------------------
    | COPYRIGHTS:       RESERVED BY DIGITAL VIDHYA
    | -----------------------------------------------------
    | DATE:				05-06-2017
    | ------------------------------------------------------
    | WEBSITE:          http://digitalvidhya.com
    |                   http://codecanyon.net/user/digitalvidhya
    | -----------------------------------------------------
    |
    | MODULE:             REST CONTROLLER
    | -----------------------------------------------------
    | This is Orders module controller file.
    | -----------------------------------------------------
    */
require APPPATH.'/libraries/REST_Controller.php';
require __DIR__.'../../libraries/jwt/autoload.php';
header('Access-Control-Allow-Origin: *');

use \Firebase\JWT\JWT;
class Service extends REST_Controller 
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
		 
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language','global_helper'));
		$this->load->model(array('base_model', 'home_model','service_model'));
		$this->load->model(array('tutor/tutor_model','student/student_model'));
		
		$this->load->model('ion_auth_model');
		
		$this->load->helper('security');
		
		/* Loading Language Files */
		$map = $this->config->item('languages');
		if (is_array($this->response->lang)) {
			$this->load->language('auth', $map[$this->response->lang[1]]);
			$this->load->language('ion_auth', $map[$this->response->lang[1]]);
		}
        
        /* Configure limits on our controller methods. Ensure
		you have created the 'limits' table and enabled 'limits'
        within application/config/rest.php	*/
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		
		/* Check Whether User logged in and is a member */
		// $this->phrases = $this->config->item('words');	
		
		date_default_timezone_set($this->config->item('site_settings')->time_zone);
    }

	 //User Authentication
    function authenticateUser($identity = NULL, $password = NULL)
    {
        if ($identity == "" || $password == "") {
            $this->response(array(
                'status' => 'Please provide Email and Password'
            ), 400);
        }
        
        return $this->ion_auth->login($identity, $password, false);
    }
	
    // GET CATEGORIES
	function get_categories_post()
	{

		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$from_limit = $request->from_limit;
		$to_limit = 20;
		$categories = $this->base_model->get_query_result('SELECT * FROM '.TBL_CATEGORIES.' WHERE is_parent = "1" AND status = "1" LIMIT '.$from_limit.','.$to_limit.'');
		$this->serviceResponse($categories,'Categories',1);
	}

	// GET COURSES
	function get_courses_post()
	{
		$postdata = file_get_contents("php://input");
    	$request = json_decode($postdata);

		$category_id = $request->category_id;
		$courses = $this->base_model->fetch_records_from('categories',array('categories'=>$category_id));

		$this->serviceResponse($courses,'Courses',1);
	}

	// GET ALL COURSES

	function get_all_courses_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$from_limit = $request->from_limit;
		$to_limit = 20;
		$categories = $this->base_model->get_query_result('SELECT * FROM '.TBL_CATEGORIES.' WHERE is_parent = "0" AND status = "1" LIMIT '.$from_limit.','.$to_limit.'');
		$this->serviceResponse($categories,'Courses',1);


		/*$courses = $this->base_model->fetch_records_from('categories',array('is_parent'=>0));
		$this->serviceResponse($courses,'Courses',1);	*/
	}
	function get_all_tutors_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$from_limit = $request->from_limit;
		$to_limit = 20;
		$type = 3;
		$categories = $this->base_model->get_query_result('SELECT * FROM '.TBL_USERS.' WHERE user_belongs_group="3" AND active="1" LIMIT '.$from_limit.','.$to_limit.'');
		$this->serviceResponse($categories,'Tutors',1);


		/*$courses = $this->base_model->fetch_records_from('categories',array('is_parent'=>0));
		$this->serviceResponse($courses,'Courses',1);	*/
	}
	function get_all_leads_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$from_limit = $request->from_limit;
		// $from_limit = 0;
		// 
		$to_limit = 10;
		$data=array();
		
		//$student_id = 213;
		// $student_id = '161';//$request->user_id;
		
		$data = $this->base_model->get_query_result("SELECT sl.*,tt.teaching_type,l.location_name,c.name as course_name FROM pre_student_leads sl INNER JOIN pre_teaching_types tt ON sl.teaching_type_id=tt.id INNER JOIN pre_locations l ON sl.location_id=l.id INNER JOIN pre_categories c ON sl.course_id=c.id ORDER BY sl.id DESC LIMIT ".$from_limit.",".$to_limit."");

		
		$this->serviceResponse($data,'Student Leads',1);
	}

	// GET LEADS
	function check_viewed_lead_post()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		$user_id = $request->user_id;
		$lead_id = $request->lead_id;

		if($this->home_model->is_already_viewed_the_lead($user_id,'student_leads',$lead_id)){
			$response = get_languageword('Tutor already viewed the lead');
			$status   = 1;
		}else{
			$response = get_languageword('Tutor not viewed the lead');
			$status   = 0;
		}
		

		$this->serviceResponse(array(),$response,$status);
	}

	//GET TEACHING TYPES
	function get_teaching_types_post()
	{
		$teaching_types = $this->base_model->fetch_records_from('teaching_types',array('status'=>1));

		$this->serviceResponse($teaching_types,'Teaching types',1);
	}



	// GET PACKAGES
	function get_packages_post()
	{
		// Package for : Tutor/Student
		$package_for = $this->post('package_for');
		$packages = $this->base_model->fetch_records_from('packages',array('package_for'=>$package_for,'status'=>'Active'));

		$this->serviceResponse($packages,'Packages',1);
	}
	
	function profile_upload_post()
	{
		// UPLOAD IMAGE
		$image 	= $_FILES['userfile']['name'];
		$user_id = $this->post('user_id');
		//Upload User Photo
		if (!empty($_FILES['userfile']['name'])) 
		{
			//$ext = pathinfo($image, PATHINFO_EXTENSION);
			$ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
			$file_name = $user_id.'.'.$ext;
			$config['upload_path'] 		= './assets/uploads/profiles/';
			$config['allowed_types'] 	= '*';
			$config['overwrite'] 		= true;
			$config['file_name']        = $file_name;
			$config['max_size']             = '1024000';
			$this->load->library('upload', $config);
			
			if($this->upload->do_upload())
			{
				$inputdata['photo']		= $file_name;
				//$this->create_thumbnail($config['upload_path'].$config['file_name'],'./assets/uploads/profiles/thumbs/'.$config['file_name'], 200, 200);
				$this->base_model->update_operation($inputdata, 'users', array('id' => $user_id));
				$result = $this->base_model->fetch_records_from('users',array('id'=>$user_id));
				//$result = array();
				$response = 'Image uploaded successfully';
				$status = 1;
			}else{
				$data = $this->upload->display_errors();
				$result = array();
				$response = 'Image upload failed';
				$status = 0;
				$this->serviceResponse($data,$response,$status);
			}
		}else{
			$result = array();
			$response = 'Please select image';
			$status = 0;
		}
			$this->serviceResponse($result,$response,$status);
		
	}
	

	// ADD TUTOR COURSE

	function add_tutor_course_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		//check  for the course already having 
		$course_id 			= $request->course_id;
		$user_id 			= $request->user_id;
		$tutor_course_id 	= $request->id;

		$input_data['tutor_id'] 		= $request->user_id;
		$input_data['course_id'] 		= $request->course_id;
		$input_data['duration_value'] 	= $request->duration_value;
		$input_data['duration_type'] 	= $request->duration_type;
		$input_data['fee'] 				= $request->fee;
		$input_data['content'] 			= $request->content;
		$input_data['time_slots'] 		= $request->time_slots;
		$input_data['days_off'] 		= $request->days_off;
		$input_data['status'] 			= 1;
		$input_data['sort_order'] 		= $request->sort_order;
		$input_data['created_at'] 		= date('Y-m-d h:i:s');
		
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$tutor_details = $this->base_model->get_user_info($user_id);
		$time_zone = $time_zone_list[$tutor_details->time_zone];
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$time_slot_list = explode(",",$input_data['time_slots']);
		$time_slot_str = "";
		foreach($time_slot_list as $k => $v){
			if(!empty($v)){
				$time_slot = explode("-",$v);
				
				$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
				$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
				$offset = timezone_offset_get($userTimezone,$gmtTimezone);
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->sub($myInterval);
				$myDateTime_end->sub($myInterval);
				$result = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
				if($k != 0)
					$time_slot_str .= ",";
				$time_slot_str .= $result;
			}
		}
		
		$input_data['time_slots'] = $time_slot_str;

		if($tutor_course_id!=''){

			$where['id'] = $tutor_course_id;
			if($this->base_model->update_operation($input_data,'tutor_courses',$where)){
				$response = get_languageword('course updated successfully');
				$status = 1;
			}else{
				$response = get_languageword('course not updated');
				$status = 0;
			}
		}else{
		
			$result = $this->base_model->fetch_records_from('tutor_courses',array('tutor_id'=>$user_id,'course_id'=>$course_id,'status'=>'Active'));
			
			
			if(empty($result)){

				if($this->base_model->insert_operation($input_data,'tutor_courses')){
					$response = get_languageword('course added successfully');
					$status = 1;
				}else{
					$response = get_languageword('course not added');
					$status = 0;
				}

			}else{
				$response = get_languageword('course already added');
				$status   = 0;
			}

		}

		$this->serviceResponse(array(),$response,$status);
	}

	// GET BOOKINGS OF TUTORS

	function get_tutor_bookings_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$user_id 	= $request->user_id;
		$from_limit = $request->from_limit;
		$booking_type = $request->type;
       
        $status_condition = '';
		if($booking_type!='all'){
			$status_condition = ' AND b.status="'.$booking_type.'"';
		}
		if($booking_type == "session_initiated")
			$status_condition .= ' AND b.preferred_location= "online-zoom" ';

		//$from_limit = 0;
		$to_limit = 20;

		// $user_id 	= $this->post('user_id');
		/*if($booking_type != "session_initiated")
			$bookings 	= $this->base_model->get_query_result('SELECT b.*,u.username as student_name,u.phone,u.email,u.device_id,u.photo as student_photo,c.name as course_name FROM '.TBL_BOOKINGS.' b,'.TBL_USERS.' u,'.TBL_CATEGORIES.' c WHERE b.student_id=u.id AND b.course_id=c.id AND  b.tutor_id="'.$user_id.'" '.$status_condition.' LIMIT '.$from_limit.','.$to_limit.'');
		else*/
			$bookings 	= $this->base_model->get_query_result('SELECT b.*,u.username AS student_name,u.phone,u.email,u.device_id,u.photo AS student_photo,c. NAME AS course_name,d.tutor_zoom_link,d.student_zoom_link FROM '.TBL_BOOKINGS.' b INNER JOIN	'.TBL_USERS.' u on b.student_id = u.id INNER JOIN	'.TBL_CATEGORIES.' c on b.course_id = c.id LEFT JOIN '.TBL_ZOOM_SESSION.' d on b.booking_id = d.booking_id WHERE b.tutor_id = "'.$user_id.'" '.$status_condition.' LIMIT '.$from_limit.','.$to_limit.'');
		$tutor_details = $this->base_model->get_user_info($user_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$tutor_details->time_zone];	
		foreach($bookings as $key => $value){
			if($value->tutor_zoom_link != ""){
				$today = date('Y-m-d');
				$s_d = strtotime(str_replace('/','-',$value->start_date));
				$e_d = strtotime($value->end_date);
				$c_d = strtotime($today);
				$dayWeek = strtoupper(date('D'));
				$is_holiday = strpos($value->days_off,$dayWeek);
				if($s_d <= $c_d && $e_d >= $c_d && $is_holiday == false){
					$cur_time 	= (float)date('H.i');
					$time 	  	= explode('-', str_replace(' ', '', $value->time_slot));
					$start_time = date('H:i', strtotime(number_format($time[0],2)));
					$end_time   = date('H:i', strtotime(number_format($time[1],2)));
					$certain_mins_before_start_time = (float)date('H.i', strtotime($start_time.' -'.$this->config->item('site_settings')->enable_initiate_session_option_before_mins.' minutes'));
					$certain_mins_end_time = (float)date('H.i', strtotime($end_time));
					if($cur_time >= $certain_mins_before_start_time && $cur_time <= $certain_mins_end_time){
						
					}else{
						$bookings[$key]->student_zoom_link = "";
					}
				} else{
					$bookings[$key]->student_zoom_link = "";
				}
			}
			$t = strtotime($value->start_date);
			$t1 = strtotime($value->end_date);
			$time_slot = explode("-",$value->time_slot);
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
			$myDateTime_end = new DateTime(date('Y-m-d',$t1)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->add($myInterval);
			$myDateTime_end->add($myInterval);
			$bookings[$key]->start_date = $myDateTime_start->format("Y-m-d");
			$bookings[$key]->end_date = $myDateTime_end->format("Y-m-d");
			$bookings[$key]->time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		}
		$this->serviceResponse($bookings,'Tutor Bookings',1); 
	}

	function get_tutor_blogs_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		// $user_id 	= $this->post('user_id');

		
		//$languages = $this->base_model->fetch_records_from(TBL_BLOGS,array('status'=>'Active'));
		$from_limit = $request->from_limit;
		$to_limit	= 20;
		$blogs = $this->base_model->get_query_result('SELECT b.*,u.username as author_name,u.photo as author_photo FROM '.TBL_BLOGS.' b,'.TBL_PREFIX.'users u where u.id=b.tutor_id and  b.admin_approved = "Yes" LIMIT '.$from_limit.','.$to_limit.''); 
		
		//$blogs = $this->base_model->get_query_result("SELECT * FROM '.TBL_BLOGS.'");
		
		$this->serviceResponse($blogs,'Tutor Blogs',1); 
	}

	
	function booking_status_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		$booking_id = $request->booking_id;
		$user_id	= $request->user_id;
		// $booking_id = $this->post('booking_id');
		// $user_id	= $this->post('user_id');
		// $device_id = $request->device_id;

		$booking_det = $this->base_model->fetch_records_from('bookings', array('booking_id' => $booking_id));
		if(!empty($booking_det)) {

				$booking_det = $booking_det[0];

				if($booking_det->tutor_id != $user_id) {

					$response = get_languageword('not_authorized');
					$status   = 0; 	
					$this->serviceResponse(array(),$response,$status);
    			
				}

				
				$booking_status = $booking_det->status;
				$updated_by = getUserType($booking_det->updated_by);

				// if(in_array($booking_status, array('pending', 'approved', 'session_initiated', 'running', 'completed'))) {
				// 		$crud->required_fields(array('status'));
				// }

				if($booking_status == "pending") {

					$status_array = array('approved' => get_languageword('approve'), 'cancelled_before_course_started' => get_languageword('cancel'));
				}

				if($booking_status == "approved") {
					//echo $booking_status; die();
					//$status_array = array('cancelled_before_course_started' => get_languageword('cancel'));
					$status_array = array('session_initiated' => get_languageword('initiate_session'), 'cancelled_before_course_started' => get_languageword('cancel'));

					/*$today = date('Y-m-d');

					if((strtotime($booking_det->start_date) <= strtotime($today)) && (strtotime($today) <= strtotime($booking_det->end_date))) {

						// echo "Inner Time"; 

						$cur_time 	= (float)date('H.i');
						$time_slot 	= str_replace(':', '.', $booking_det->time_slot);
						$time 	  	= explode('-', str_replace(' ', '', $time_slot));
						$start_time = date('H:i', strtotime(number_format($time[0],2)));
						$end_time   = date('H:i', strtotime(number_format($time[1],2)));

						$certain_mins_before_start_time = (float)date('H.i', strtotime($start_time.' -'.$this->config->item('site_settings')->enable_initiate_session_option_before_mins.' minutes'));

						$certain_mins_before_end_time 	= (float)date('H.i', strtotime($end_time.' -'.$this->config->item('site_settings')->enable_course_completed_option_before_mins.' minutes'));
						// echo "<pre>";
						// echo $cur_time.'<br>';
						// echo $certain_mins_before_start_time.'<br>';
						// echo $certain_mins_before_end_time.'<br>';
						if($cur_time <= $certain_mins_before_start_time && $cur_time <= $certain_mins_before_end_time) {
							// echo "Condiion sat"; die();
							$status_array = array('session_initiated' => get_languageword('initiate_session'), 'cancelled_before_course_started' => get_languageword('cancel'));
						}
						// else{
						// 	echo "Not satis"; die();
						// }
					}*/
				}

				if($booking_status == "session_initiated") {

					$status_array = array('cancelled_before_course_started' => get_languageword('cancel'));
					// $crud->field_type('status', 'dropdown', $status);
				}

				if($booking_status == "running") {

					$status_array = array('cancelled_when_course_running' => get_languageword('cancel'));

					$today = date('Y-m-d');

					if(strtotime($today) >= strtotime($booking_det->start_date)) {

						$status_array = array('completed' => get_languageword('course_completed'), 'cancelled_when_course_running' => get_languageword('cancel'));
					}

					// $crud->field_type('status', 'dropdown', $status);

				}

				if($booking_status == "completed") {

					$status_array = array('called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

					// $crud->field_type('status', 'dropdown', $status);

				}

				if($booking_status == "called_for_admin_intervention" && $updated_by == "student") {

					if($booking_det->prev_status == "pending")
						$status_array['approved'] = get_languageword('approve');
					else if($booking_det->prev_status == "approved")
						$status_array['cancelled_before_course_started'] = get_languageword('cancel');
					else if($booking_det->prev_status == "running") {
						$status_array['running'] = get_languageword('continue_course');
						$status_array['cancelled_when_course_running'] = get_languageword('cancel');
					}
					else if($booking_det->prev_status == "cancelled_when_course_running") {
						$status_array['running'] = get_languageword('continue_course');
					}
					else if($booking_det->prev_status == "completed") {
						$status_array['running'] = get_languageword('continue_course');
						$status_array['cancelled_when_course_running'] = get_languageword('cancel');
					}

					// $crud->required_fields(array('status'));
					// $crud->field_type('status', 'dropdown', $status);

				} else if($booking_status == "called_for_admin_intervention" && ($updated_by == "tutor"  || $updated_by == "admin")) {
					$status_array = array();
					// $crud->edit_fields('status_desc', 'updated_at');
				}


				if($booking_status == "cancelled_when_course_running" && $updated_by == "student") {

					// $crud->required_fields(array('status'));

					$status_array = array('called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

					// $crud->field_type('status', 'dropdown', $status);
				}


				if($booking_status == "cancelled_after_course_completed" && $updated_by == "student") {

					// $crud->required_fields(array('status'));

					$status_array = array('called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

					// $crud->field_type('status', 'dropdown', $status);
				}

				if($booking_status == "closed" || $booking_status == "cancelled_before_course_started" || ($booking_status == "cancelled_when_course_running" && $updated_by == "tutor") || ($booking_status == "cancelled_after_course_completed" && $updated_by == "tutor")) {

					// $crud->edit_fields('status_desc', 'updated_at');
					$status_array =  array();

				}
					$data = array();
				if(!empty($status_array)){
					foreach ($status_array as $key => $value) {
						$tmp = array(
							'status_key'=>$key,
							'status_value'=>$value
						);

						array_push($data, $tmp);
					}
				}
				$this->serviceResponse($data,'Status fields',1);

			}else{
				$response = get_languageword('Booking details not found');
				$status = 0;
				$this->serviceResponse(array(),$response,$status);
			}


	}

	// UPDATE BOOKING STATUS

	function update_booking_status_post()
	{
		$postdata 		= file_get_contents("php://input");
		$request 		= json_decode($postdata);
		$booking_status = $request->status;
		$prev_status 	= $request->prev_status;
		$updated_by 	= $request->updated_by;
		$updated_at 	= date('Y-m-d H:i:s');
		$booking_id		= $request->booking_id;
		$device_id = "";
		if(isset($request->device_id))
			$device_id      = $request->device_id;
		if($booking_status == 'completed'){
			$booking_info = (array)$this->student_model->get_booking_detail1($booking_id);
			if($booking_info['preferred_location'] == 'online-zoom' && !empty($booking_info['iq_id'])){
				$this->student_model->remove_new_pending_iq($booking_info);
			}
		}
		if($booking_status == 'approved' ){
			$booking_info = (array)$this->tutor_model->get_booking_detail($booking_id);
			if($booking_info['preferred_location'] != 'record'){
				$session_exist = (array)$this->tutor_model->get_session_exist($booking_info['time_slot'],$booking_info['start_date'],$booking_info['end_date'],$booking_info['tutor_id'],$booking_info['tution'],$booking_info['course_id'],$booking_info['booking_id'],$booking_info['preferred_location']);
				if($session_exist['cnt'] > 0){
					$response = 'There is an existing session already booked within the same timings.';
					$status   = 0;
					$this->serviceResponse(array(),$response,$status);
					return;
				}
			} else{
				/*if(!empty($this->input->post('record_attach_link'))){
					$this->tutor_model->update_record_attach($booking_info['iq_id'],$this->input->post('record_attach_link'));
					$sub = "Record Attach Link";
					$msg = "This is <a href='".$this->input->post('record_attach_link')."'>Record Link.</a>";
					$student_email = $this->tutor_model->get_user_email($booking_info['student_id']);
					$email_settings = (array)$this->config->item('email_settings');
					sendEmail($email_settings['webmail']['User Name'], $student_email, $sub, $msg);
				}*/
			}
		}
		if($booking_status == "session_initiated"){
			$booking_info = (array)$this->tutor_model->get_booking_detail($booking_id);
			$session_exist = (array)$this->tutor_model->get_session_exist($booking_info['time_slot'],$booking_info['start_date'],$booking_info['end_date'],$booking_info['tutor_id'],$booking_info['tution'],$booking_info['course_id'],$booking_info['booking_id'],$booking_info['preferred_location']);
			$my_info = (array)$this->tutor_model->get_tutor_info($booking_info['tutor_id']);
			if($session_exist['cnt'] > 0){
				$response = 'There is an existing session already booked within the same timings.';
				$status   = 0;
				$this->serviceResponse(array(),$response,$status);
				return;
			} 
			/*else if($my_info['zoom_id'] != "Yes" && $booking_info['preferred_location'] == "online-zoom"){
				$response = 'You are not authorized to schedule zoom meeting. Contact Admin.';
				$status   = 0;
				$this->serviceResponse(array(),$response,$status);
				return;
			}*/
			else if($session_exist['cnt'] == 0 && $booking_info['preferred_location'] == "online-zoom"){
				if($booking_info['tution'] == "Share"){
					$credit = $this->tutor_model->get_credit_value($booking_info['tutor_id'],$booking_info['course_id']);
					$student_cnt = $this->tutor_model->get_same_course_time_cnt($booking_info);
					$each_credit = ceil($credit/($student_cnt+1));
					$rest = $each_credit % 10;
					if($rest != 0)
						$each_credit = (int)(floor($each_credit/10)+1)*10;
					$this->tutor_model->update_credits($booking_info,$each_credit);
				}
				/*$create_result = $this->create_meeting($booking_info);
				if($create_result == 2){
					$response = 'There is no available ZOOM api keys';
					$status   = 0;
					$this->serviceResponse(array(),$response,$status);
					return;
				}*/
				$this->create_calendar_event($booking_info);
			}
		}
		if($booking_status == "closed" || $booking_status == "completed"){
			$booking_info = (array)$this->tutor_model->get_booking_detail($booking_id);
			if($booking_info['status'] == 'session_initiated' && ($booking_status == "closed" || $booking_status == "completed")){
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '29'));
				if(!empty($email_tpl)) {
					$email_tpl = $email_tpl[0];
					if(!empty($email_tpl->from_email)) {
						$from = $email_tpl->from_email;
					} else {
						$from 	= get_system_settings('Portal_Email');
					}
					$to = $this->tutor_model->get_user_email($booking_info['student_id']);
					$parent_detail = $this->home_model->get_parent_details($to);
					if(!empty($parent_detail) && !empty($parent_detail->email))
						$to 	= $to.",".$parent_detail->email;
					else
						$to 	= $to;

					if(!empty($email_tpl->template_subject)) {
						$sub = $email_tpl->template_subject;
					} else {
						$sub = get_languageword("Session Cancellation By Tutor");
					}
					$course_name = $this->base_model->fetch_value('categories', 'name', array('id' => $booking_info['course_id']));
					if(!empty($email_tpl->template_content)) {
						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
						$site_title = $this->config->item('site_settings')->site_title;
						$student_name = $this->tutor_model->get_student_name($booking_info['student_id']);
						$original_vars  = array($logo_img, $site_title, $student_name, $this->ion_auth->get_user_name(), $course_name, $booking_info['start_date']." & ".$booking_info['time_slot'], $booking_info['preferred_location'], '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);
						$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_STUDENT_NAME_', '_TUTOR_NAME_', '_COURSE_NAME_', '_DATE_TIME_', '_LOCATION_', '_LOGIN_LINK_', '_SITE_TITLE_');
						$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);
					} else{
						$msg = "";
					}
					sendEmail($from, $to, $sub, $msg);
				}
			}
			if($booking_info['status'] == 'pending' || $booking_info['status'] == 'running' || $booking_info['status'] == 'session_initiated'){
				$this->delete_google_calendar_event($booking_info);
				if($booking_info['tution'] == 'Share'){
					$this->base_model->delete_same_records($booking_info,$booking_status);
				}
			}
		}
		$update_data['status'] 		= $booking_status;
		$update_data['prev_status'] = $prev_status;
		$update_data['updated_by'] 	= $updated_by;
		$update_data['updated_at'] 	= $updated_at;
		$where['booking_id']		= $booking_id;

		if($this->base_model->update_operation($update_data,'bookings',$where)){
			$message = get_languageword('booking status has been changed to ').$booking_status;
			if($device_id!=''){
				$this->sendPushNotification($device_id,get_languageword('booking status'),$message);
			}
			$response = 'booking status updated successfully';
			$status   = 1;
		}else{
			$response = 'unable to update status';
			$status   = 0;
		}

		$this->serviceResponse(array(),$response,$status);
	}
	
	//GET TUTOR COURSES
	function get_tutor_courses1_post(){
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$avail_time_slots = array();
		$course_id = $request->course_id;
		$tutor_id = $request->user_id;
		$selected_date = $request->start_date;
		$tution = $request->tution;

		if(empty($course_id) || empty($tutor_id) || empty($selected_date) || empty($tution)) {
			echo ''; die();
		}

		$row =  $this->home_model->get_tutor_course_details1($course_id, $tutor_id);

		if(empty($row)) {
			echo NULL; die();
		}

		$tutor_time_slots = explode(',', $row->time_slots);
		
		$booked_slots = $this->home_model->get_booked_slots($tutor_id, $row->course_id, $selected_date,$tution);

		if(!empty($booked_slots)) {

			foreach ($tutor_time_slots as $slot) {
				$available = 1;
				foreach($booked_slots as $booked_slot){
					if($booked_slot['slot'] == $slot){
						if($booked_slot['tution'] == 'One on One')
							$available = 0;
						else{
							if($tution == 'One on One')
								$available = 0;
						}
					}
				}
				if($available == 1)
					$avail_time_slots[] = $slot;
			}

		} else {

			$avail_time_slots = $tutor_time_slots;
		}
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		
		$user_details = $this->base_model->get_user_info($request->my_id);
		if(!empty($user_details->time_zone))
			$time_zone = $time_zone_list[$user_details->time_zone];
		else
			$time_zone = "UTC";
		foreach($avail_time_slots as $key => $value){
			$time_slot = explode("-",$value);
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone("UTC"));
			$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone("UTC"));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->add($myInterval);
			$myDateTime_end->add($myInterval);
			$avail_time_slots[$key] = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		}
		$this->serviceResponse($avail_time_slots,'Tutor Courses',1); 
		
	}
	function get_tutor_courses_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$user_id 	= $request->user_id;
		

		if (!isset($request->from_limit) || $request->from_limit=='no_limit') {

			$from_limit = 0;
			$to_limit = 1000;

		} else {

			$from_limit = $request->from_limit;
			$to_limit = 20;
		}

		$courses 	= $this->base_model->get_query_result('SELECT tc.*,c.name as course_name,c.image as course_image,c.slug as course_slug FROM '.TBL_TUTOR_COURSES.' tc,'.TBL_CATEGORIES.' c WHERE tc.course_id=c.id AND tc.tutor_id="'.$user_id.'" AND tc.status=1 AND c.status=1 ORDER BY tc.sort_order ASC LIMIT '.$from_limit.','.$to_limit.''); 
		$tutor_details = $this->base_model->get_user_info($user_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$tutor_details->time_zone];
		
		foreach($courses as $key => $value){
			$time_slot_list = explode(",",$value->time_slots);
			$time_slot_str = "";
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			foreach($time_slot_list as $k => $v){
				if(!empty($v)){
					$time_slot = explode("-",$v);
					$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
					$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
					
					$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
					$myDateTime_start->add($myInterval);
					$myDateTime_end->add($myInterval);
					$result = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
					if($k != 0)
						$time_slot_str .= ",";
					$time_slot_str .= $result;
				}
			}
			$courses[$key]->time_slots = $time_slot_str;
		}
		$this->serviceResponse($courses,'Tutor Courses',1); 
	}
	function get_tutor_courses2_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$user_id 	= $request->user_id;
		

		if (!isset($request->from_limit) || $request->from_limit=='no_limit') {

			$from_limit = 0;
			$to_limit = 1000;

		} else {

			$from_limit = $request->from_limit;
			$to_limit = 20;
		}

		$courses 	= $this->base_model->get_query_result('SELECT tc.*,c.name as course_name,c.image as course_image,c.slug as course_slug FROM '.TBL_TUTOR_COURSES.' tc,'.TBL_CATEGORIES.' c WHERE tc.course_id=c.id AND tc.tutor_id="'.$user_id.'" AND tc.status=1 AND c.status=1 ORDER BY tc.sort_order ASC LIMIT '.$from_limit.','.$to_limit.''); 
		/*$tutor_details = $this->base_model->get_user_info($request->my_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$tutor_details->time_zone];
		
		foreach($courses as $key => $value){
			$time_slot_list = explode(",",$value->time_slots);
			$time_slot_str = "";
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			foreach($time_slot_list as $k => $v){
				if(!empty($v)){
					$time_slot = explode("-",$v);
					$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
					$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
					
					$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
					$myDateTime_start->add($myInterval);
					$myDateTime_end->add($myInterval);
					$result = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
					if($k != 0)
						$time_slot_str .= ",";
					$time_slot_str .= $result;
				}
			}
			$courses[$key]->time_slots = $time_slot_str;
		}*/
		$this->serviceResponse($courses,'Tutor Courses',1); 
	}
	
	
	
	//STUDENT send message to tutor
	function stu_send_message_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		
		$data = array();
		$response='';
		$status=0;
		
		$student_id = $request->student_id;
		$tutor_id = $request->tutor_id;
		
		$credits_for_sending_message = $this->config->item('site_settings')->credits_for_sending_message;
		
		//Check Whether student is premium user or not
		if(!is_premium($student_id)) 
		{
			$response = get_languageword('please_become_premium_member_to_send_message_to_tutor');
			$status=0;
			$this->serviceResponse($data,$response,$status); 
		}
		
		
		//Check If student has sufficient credits to send message to tutor
		if(!is_eligible_to_make_booking($student_id, $credits_for_sending_message)) 
		{
			$response = get_languageword("you_do_not_have_enough_credits_to_send_message_to_the_tutor_Please_get_required_credits_here");
			$status=0;
			$this->serviceResponse($data,$response,$status);
		}
		
		$course_name = $this->base_model->fetch_value('categories', 'name', array('id' => $request->course_id));
		
		
		$inputdata['from_user_id'] 	= $student_id;
		$inputdata['name'] 			= $request->name;
		$inputdata['course_slug']	= $course_name;
		$inputdata['email'] 		= $request->email;
		$inputdata['phone'] 		= $request->phone;
		$inputdata['message'] 		= $request->message;
		
		$to_user_type   = "Tutor";
		$inputdata['to_user_id']   = $request->tutor_id;
		
		$inputdata['created_at']	= date('Y-m-d H:i:s');
		$inputdata['updated_at']	= $inputdata['created_at'];
		
		$ref = $this->base_model->insert_operation($inputdata, 'messages');
		
		if($ref) 
		{
			//Send message details to Tutor Email
			//Email Alert to Tutor - Start
			//Get Send Message Email Template
			
			$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '17'));
			
			$tutor_rec = getUserRec($inputdata['to_user_id']);
			
			$from 	= $inputdata['email'];
			$to 	= $tutor_rec->email;
			$sub 	= get_languageword("Message Received From Student");
			
			$msg 	= '<p>
						'.get_languageword('Hi ').$tutor_rec->username.',</p>
					<p>
						'.get_languageword('You got a message from Student Below are the details').'</p>
					<p>
						<strong>'.get_languageword('name').':</strong> '.$inputdata['name'].'</p>
					<p>
						<strong>'.get_languageword('email').':</strong> '.$inputdata['email'].'</p>
					<p>
						<strong>'.get_languageword('phone').':</strong> '.$inputdata['phone'].'</p>
					<p>
						<strong>'.get_languageword('course_seeking').':</strong> '.$inputdata['course_slug'].'</p>
					<p>
						<strong>'.get_languageword('message').':</strong> '.$inputdata['message'].'</p>
					<p>
						&nbsp;</p>
					';
			$msg 	.= "<p>".get_languageword('Thank you')."</p>";
			
			if(!empty($email_tpl)) 
			{
				$email_tpl = $email_tpl[0];
				
				if(!empty($email_tpl->from_email)) 
				{
					$from = $email_tpl->from_email;
				}
				
				if(!empty($email_tpl->template_subject)) 
				{
					$sub = $email_tpl->template_subject.get_languageword(' Student');
				}
				
				if(!empty($email_tpl->template_content)) 
				{
					$msg = "";
					$original_vars  = array($tutor_rec->username, get_languageword('Student'), $inputdata['name'], $inputdata['email'], $inputdata['phone'], $inputdata['course_slug'], $inputdata['message']);
					$temp_vars		= array('___TO_NAME___','___USER_TYPE___','___NAME___', '___EMAIL___', '___PHONE___', '___COURSE___', '___MESSAGE___');
					$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);
				}
			}
			
			if(sendEmail($from, $to, $sub, $msg)) 
			{
				//Log Credits transaction data & update user net credits - Start
				
				$per_credit_value = $this->config->item('site_settings')->per_credit_value;
				
				$log_data = array(
					'user_id' => $inputdata['from_user_id'],
					'credits' => $credits_for_sending_message,
					'per_credit_value' => $per_credit_value,
					'action'  => 'debited',
					'purpose' => 'For Sending Message To Tutor "'.$tutor_rec->username.'" ',
					'date_of_action	' => date('Y-m-d H:i:s'),
					'reference_table' => 'messages',
					'reference_id' => $ref,
				);

				log_user_credits_transaction($log_data);

				update_user_credits($inputdata['from_user_id'], $credits_for_sending_message, 'debit');
				//Log Credits transaction data & update user net credits - End
				
				$response = get_languageword('Your message sent to Tutor successfully');
				$status=1;
				
				$data = $this->base_model->fetch_records_from('users',array('id'=>$student_id));
				$this->serviceResponse($data,$response,$status);
			} 
			else 
			{
				$response = get_languageword('Your message not sent due to some technical issue Please send message after some time Thankyou');
				$status=0;
				$this->serviceResponse($data,$response,$status);
			}
		} 
		else 
		{
			$response = get_languageword("message_not_sent_please_try_again");
			$status=0;
			$this->serviceResponse($data,$response,$status);
		}
	}
	
	//Get Tutor Reviews
	function get_tutor_reviews_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		$tutor_id 	= $request->tutor_id;
		
		$data 	= array();
		$response='';
		$status=0;
		
		$query = "select  u.username as student_name,u.gender,u.photo,c.name as course,tr.rating,tr.comments,tr.created_at as posted_on from ".$this->db->dbprefix('tutor_reviews')." tr inner join ".$this->db->dbprefix('users')." u on tr.student_id = u.id inner join ".$this->db->dbprefix('categories')." c on tr.course_id=c.id where tr.tutor_id=".$tutor_id." and tr.status='Approved' ORDER by rating DESC ";//limit 0,5
     
		$data = $this->base_model->get_query_result($query);
		if(empty($data)) 
		{
			$response='No Reviews';
			$status=0;
		}
		else
		{
			$response='Tutor Reviews';
			$status=1;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//DELETE TUTOR COURSE
	function delete_tutor_course_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		$user_id 	= $request->user_id;
		$id 		= $request->id;
		
		if ($this->base_model->delete_record_new('tutor_courses', array('tutor_id' => $user_id,'id'=>$id))) 
		{
			$status	  = 1;
			$response = get_languageword('deleted_successfully');
		}
		else
		{
			$status	  = 0;
			$response = get_languageword('not_deleted');
		}
		$this->serviceResponse(array(),$response,$status); 
	}
	
	//TUTOR MANAGE LOCATIONS
	function get_tutor_locations_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		$tutor_id 	= $request->user_id;
		// $tutor_id   = $this->post('user_id');
		
		// $locations = array();
		
		$parentLocationDetails = $this->db->select('id AS parentLocation_id, location_name AS parentLocation_name')->get_where($this->db->dbprefix( 'locations' ), array('parent_location_id' => 0, 'status' => 'Active'))->result();
		
		// foreach ($variable as $key => $value) {
		// 	# code...
		// }

		foreach($parentLocationDetails as $index => $p) 
		{
			$query = "SELECT l . * ,(
					SELECT count( * )
					FROM ".$this->db->dbprefix( 'tutor_locations' )." tl,
					 ".$this->db->dbprefix( 'users' )." u,
					 ".$this->db->dbprefix( 'users_groups' )." ug
					WHERE (tl.location_id = l.id OR 
					u.location_id = l.id) 
					AND ug.group_id = 3
					AND ug.user_id = u.id
					AND u.id = tl.tutor_id
					AND u.active = 1
					AND tl.status = '1'
					) AS no_of_tutors,(SELECT tc.location_id  from ".$this->db->dbprefix('tutor_locations')." tc WHERE tc.tutor_id=".$tutor_id." AND tc.location_id=l.id AND tc.status=1) as checked_id
					FROM ".$this->db->dbprefix( 'locations' )." l
					WHERE l.parent_location_id = ".$p->parentLocation_id."
					AND l.status = 'active'";
			
			$childLocations = $this->db->query($query)->result();

			$parentLocationDetails[$index]->sub_locations = $childLocations;
			
			// $locations[$p->parentLocation_name] = $childLocations;		
		}
		$response = 'Tutor Locations';
		$this->serviceResponse($parentLocationDetails,$response,0);
	}
	
	// UPDATE TUTOR LOCATIONS

	function update_tutor_locations_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$user_id 	= $request->user_id;
		$locations 	= json_decode($request->locations);
		$update_data = array();
		$get_existing_locations  = $this->base_model->fetch_records_from('tutor_locations',array('tutor_id'=>$user_id));
		if(!empty($locations)){

			foreach ($locations as  $value) {
				$temp = array(
					'tutor_id'=>$user_id,
					'location_id'=>$value->location_id,
					'created_at'=>date('Y-m-d h:i:s')
				);

				array_push($update_data, $temp);
			}
		}

		if(!empty($get_existing_locations)){
			$where['tutor_id'] = $user_id;
			$this->base_model->delete_record_new('tutor_locations',$where);
			if(!empty($update_data)){
				$this->db->insert_batch('tutor_locations',$update_data);

			} 

		}else{
			if(!empty($update_data)){
				$this->db->insert_batch('tutor_locations',$update_data);
			} 
		}


		$response = get_languageword('Tutor locations updated successfully');
		

		$this->serviceResponse(array(),$response,1);
	}

	// TUTORS TEACHING 

	function get_tutors_teaching_types_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$user_id  = $request->user_id;
		// $user_id =  $this->post('user_id');

		$teaching_types = $this->base_model->get_query_result("SELECT tt.id, tt.teaching_type,CASE WHEN EXISTS (SELECT ttt.teaching_type_id FROM pre_tutor_teaching_types ttt WHERE ttt.teaching_type_id = tt.id AND ttt.tutor_id='".$user_id."') THEN true ELSE false END AS checked  FROM pre_teaching_types tt WHERE tt.status=1");

		$this->serviceResponse($teaching_types,'teaching types',1);
	}

	// UPDATE TUTOR TEACHING TYPES

	function update_tutor_teaching_types_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$user_id 	= $request->user_id;
		$types 	= json_decode($request->types);
		$update_data = array();
		$get_existing_teaching_types  = $this->base_model->fetch_records_from('tutor_teaching_types',array('tutor_id'=>$user_id));
		if(!empty($types)){

			foreach ($types as  $value) {
				$temp = array(
					'tutor_id'=>$user_id,
					'teaching_type_id'=>$value->teaching_id,
					'created_at'=>date('Y-m-d h:i:s')
				);

				array_push($update_data, $temp);
			}
		}

		if(!empty($get_existing_teaching_types)){
			$where['tutor_id'] = $user_id;
			$this->base_model->delete_record_new('tutor_teaching_types',$where);
			if(!empty($update_data)){
				$this->db->insert_batch('tutor_teaching_types',$update_data);

			} 

		}else{
			if(!empty($update_data)){
				$this->db->insert_batch('tutor_teaching_types',$update_data);
			} 
		}


		$response = get_languageword('Tutor teaching types updated successfully');
		

		$this->serviceResponse(array(),$response,1);
	}

	// Manage Profile
    function edit_profile_post()
    {
        if (!$this->post('first_name') || !$this->post('last_name') || !$this->post('phone') || !$this->post('address') || !$this->post('city') || !$this->post('landmark') || !$this->post('id')) {
            $response = 'All fields are mandatory';
			$this->serviceResponse($data = array(),$response,0);
        }
		
        $user_id  = $this->post('id');
        $username = strtolower($this->post('first_name')) . ' ' . strtolower($this->post('last_name'));
        $update_data     = array(
            'first_name' => $this->post('first_name'),
            'last_name' => $this->post('last_name'),
            'username' => $username,
            'address' => $this->post('address'),
            'phone' => $this->post('phone'),
            'city' => $this->post('city'),
            // 'state' => $this->post('state'),
            // 'pincode' => $this->post('pincode'),
            'landmark' => $this->post('landmark'),
            'updated_on' => date('Y-m-d')
        );
        $where['id'] = $this->post('id');
        if ($this->base_model->update_operation($update_data,TBL_USERS,$where)) {
            $response = 'Updated successfully';
            $this->serviceResponse($data = array(),$response,1);
        } else {
            $response = $this->ion_auth->errors();
            $this->serviceResponse($data = array(),$response,0);
        }
    }
	


    /*** Get Site Setting Details ****/
    function get_site_settings_post()
    {

    	/*$results =  $this->base_model->get_query_result('SELECT * FROM pre_system_settings_fields sf INNER JOIN pre_system_settings_types types on sf.type_id = types.type_id WHERE types.type_slug="SYSTEM_SETTINGS" || types.type_slug="PUSH_NOTIFICATION_SETTINGS"');

			$site_settings = array();
			foreach($results as $r) {
				$site_settings[strtolower($r->field_key)] =  $r->field_output_value;
			}


		$this->serviceResponse($site_settings,'Site Settings',1);*/
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$response='Site Details';


		$results =  $this->base_model->get_query_result('SELECT * FROM pre_system_settings_fields sf INNER JOIN pre_system_settings_types types on sf.type_id = types.type_id WHERE types.type_slug="SYSTEM_SETTINGS" || types.type_slug="PUSH_NOTIFICATION_SETTINGS"');

		$site_settings = array();
		foreach($results as $r) {
			$site_settings[strtolower($r->field_key)] =  $r->field_output_value;
		}
		

        if (!empty($site_settings)) 
		{
            $site_settings	=	$site_settings;
			$response		=	'Site details found'; 
			

			//languages
			$language_types = array();

			//student dashboard data
			//$dashboard_data = [];
		
			$data = array();
			
			$query  = "SELECT tds.column_name,c.code FROM information_schema.columns tds,pre_languages c WHERE table_name='".TBL_PREFIX."languagewords' AND tds.column_name!= 'lang_id' AND tds.column_name!= 'phrase_for' AND tds.column_name!= 'lang_key' AND c.name=tds.column_name "; 
			
			$languages = $this->db->query($query)->result();

			
			if(!empty($languages))
			{
				foreach($languages as $lang):
					
					$language='';
					$language=$lang->column_name;
					
					$data['language_name'] = $lang->column_name;
					$data['language_code'] = $lang->code;
					
					
					$languagewords = "select lang_key,$language from ".TBL_PREFIX."languagewords where phrase_for='App'";
					$languagewords = $this->db->query($languagewords)->result();
					
					$language_strings = array();
					foreach($languagewords as $key=>$val):
						$language_strings[str_replace("_", " ", $val->lang_key)] = $val->$language;
					endforeach;
					$data['language_strings'] = $language_strings;
					
					array_push($language_types,$data);
					unset($data);
				endforeach;
			}



			if(!empty($request)){
				$user_id 	= $request->user_id;
		 		$user_role  = $request->user_role;
		 		// $user_id = $this->post('user_id');
		 		// $user_role = $this->post('user_role');
		 		if($user_id>0){
		 			 if ($user_role == 2) {
		 				$dashboard_data = $this->student_model->get_student_dashboard_data($user_id);
		 			 } else if($user_role == 3){
		 				$dashboard_data = $this->tutor_model->get_tutor_dashboard_data($user_id);
		 			}
		 		}	
			}

        } 
		else 
		{
            $site_settings	=	array();
			$response		=	'Site details not found'; 
        }

		$data = array(
					'site_settings'=>$site_settings,
					'language_types'=>$language_types
					// 'dashboard_data'=>$dashboard_data
					);
		$this->serviceResponse($data,$response,1);
    }
	
	function test_settings_get()
	{
		$results =  $this->base_model->get_query_result('SELECT * FROM pre_system_settings_fields sf INNER JOIN pre_system_settings_types types on sf.type_id = types.type_id WHERE types.type_slug="SYSTEM_SETTINGS" || types.type_slug="PUSH_NOTIFICATION_SETTINGS"');

			$site_settings = array();
			foreach($results as $r) {
				$site_settings[strtolower($r->field_key)] =  $r->field_output_value;
			}


		$this->serviceResponse($site_settings,'Site Settings',1);
	}
    
    // GET PHONE CODES

    function get_phone_codes_post()
    {
    	$countries = $this->base_model->fetch_records_from('country');
		
    	$country_opts = array();

		foreach ($countries as $key => $value) {
			
			$temp = array(
					'id'=>$value->phonecode,
					'name'=>$value->nicename." +".$value->phonecode
				);
			array_push($country_opts, $temp);
		}
		array_push($country_opts, array('' => get_languageword('select_Phone_Code')));
		// $country_opts = array('' => get_languageword('select_Phone_Code'));

		$this->serviceResponse($country_opts,'Country Codes',1);
    }
	
	/**
	* Change Password
	* @author John Peter
	* @return
	*/
	/* function change_password_post()
	{
					
		$identity 							= $this->post('email');
		$change 							= $this->ion_auth->change_password(
		$identity, $this->post('current_password') , $this->post('new_password'));
		if ($change) {

				$response = strip_tags($this->ion_auth->messages());
				$status = 1;
		}
		else {
				
				$response = strip_tags($this->ion_auth->errors());
				$status = 0;
		}
		
		$this->serviceResponse($data = array(),$response,$status);
		
	} */
	
    
  
	// Dynamic Pages
	
	function pages_post()
	{
		$page_details	= $this->base_model->fetch_records_from('pages',array('status'=>'Active'));
		
		if(count($page_details)>0){
			$page_details = $page_details;
			$response		=	'Page Detials found';
		}else{
			$page_details = array();
			$response		=	'Page Detials found empty';
		}
		$this->serviceResponse($page_details,$response,1);
	}
	
	function get_languages_post()
	{
		$languages = $this->base_model->fetch_records_from(TBL_LANGUAGES,array('status'=>'Active'));
		if(count($languages)>0){
			$languages = $languages;
		}else{
			$languages = array();
		}
		$this->serviceResponse($languages,'Languages',1);
	}
	
	function get_language_strings_post()
	{
		$language_id = $this->post('language_id');
		if($language_id!='' || !is_numeric($language_id)){
			$language_id = 1;	
		}
		$phrases 	 = $this->base_model->run_query(
		"SELECT p.id,p.text, ml.text as existing_text FROM " . DBPREFIX.TBL_PHRASES
		 . " p," . DBPREFIX.TBL_MULTI_LANG . " ml WHERE ml.phrase_id=p.id 
		AND ml.lang_id=" . $language_id." AND p.phrase_type='app' AND ml.phrase_type='app'");
		$option_data = array(
            
        );
		if(count($phrases)>0){
			 foreach ($phrases as $key => $val) {
				$option_data[$val->text] = $val->existing_text;
			}
		}
		
		$this->serviceResponse($option_data,$this->phrases['could not found the record'],1);
		
	}

	/* SEARCH STUDENT LEADS */
    function search_student_leads_post()
	{
		$postdata = file_get_contents("php://input");

		$request = json_decode($postdata);
		$from_limit = $request->from_limit;
		$to_limit = 10;
		/* $course_slug = '';
		if(!empty($request->course_slug)){
			$course_slug 	= $request->course_slug;
		}

		$location_slug = '';
		if(!empty($request->location_slug)){
			$location_slug 			= $request->location_slug;
		}

		$teaching_type_slug = '';
		if(!empty($request->teaching_type_slug)){
			$teaching_type_slug 	= $request->teaching_type_slug;	
		}
		
		if(!empty($course_slug[0]) && $course_slug[0] == "by_location")
			$course_slug = '';
		if(!empty($course_slug[0]) && $course_slug[0] == "by_teaching_type") {
			$teaching_type_slug = $location_slug;
			$course_slug   = '';
			$location_slug = '';
		}

		$course_slug 	= str_replace('_', '-', $course_slug);
		$location_slug 	= str_replace('_', '-', $location_slug);
		$teaching_type_slug = str_replace('_', '-', $teaching_type_slug);

		$params = array('course_slug' 	=> $course_slug, 
						'location_slug' => $location_slug, 
						'teaching_type_slug' => $teaching_type_slug);

		$data = $this->home_model->get_student_leads($params); */
		
		$data = array();
		
		$course_cond 		= "";
        $location_cond 		= "";
        $teaching_type_cond = "";
		
		if(isset($request->courses))
		{
			$courses = $request->courses;
			if(!empty($courses))
			{
				$courses=implode(",",$courses);
				$course_cond = " AND sl.course_id IN (".$courses.") ";
			}
		}
		
		if(isset($request->locations))
		{
			$locations = $request->locations;
			if(!empty($locations))
			{
				$locations=implode(",",$locations);
				$location_cond = " AND sl.location_id IN (".$locations.") ";
			}
		}
		
		if(isset($request->teaching_types))
		{
			$teaching_types = $request->teaching_types;
			if(!empty($teaching_types))
			{
				$teaching_types=implode(",",$teaching_types);
				$teaching_type_cond = " AND sl.teaching_type_id IN (".$teaching_types.") ";
			}
		}
		
		$query = "SELECT u.*, sl.*, sl.id AS lead_id FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id INNER JOIN ".TBL_STUDENT_LEADS." sl ON sl.user_id=u.id WHERE u.active=1 AND u.visibility_in_search='1' AND u.availability_status='1' AND u.is_profile_update=1 AND ug.group_id=2 AND sl.status='opened' ".$course_cond." ".$location_cond." ".$teaching_type_cond." ORDER BY sl.id DESC LIMIT ".$from_limit.",".$to_limit." ";
		
		$data = $this->base_model->get_query_result($query);
		
		$this->serviceResponse($data,'Leads',1);
		
	}

	// LEAD DETAILS 

	function lead_details_post()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$student_lead_id 	= $request->lead_id;
		$student_slug 		= $request->slug;
		$stduent_details 	= $this->service_model->get_student_profile($student_slug,$student_lead_id,'105');

		$this->serviceResponse($stduent_details,'Lead details',1);
	}
 
 	function leads_filter_data_post()
 	{
 		/* //Course Options
		$courses = $this->home_model->get_courses();
		// $course_opts[''] = get_languageword('select');
		$show_records_count_in_search_filters = strip_tags($this->config->item('site_settings')->show_records_count_in_search_filters);
		$avail_records_cnt = "";
		$course_opts = array();
		if(!empty($courses)) {
			foreach ($courses as $key => $value) {
				if($show_records_count_in_search_filters == "Yes") {

					$avail_records_cnt = " (".count($this->home_model->get_student_leads(array('course_slug'=>$value->slug))).")";
				}

					$temp = array(
						'slug'=>$value->id,
						'name'=>$value->name.$avail_records_cnt
					);
				array_push($course_opts, $temp );	
			}
		}
		
		// courses options
		 $data['course_opts'] = $course_opts;
		// $data->course_opts = $course_opts;

 		//Location Options
		$locations = $this->home_model->get_locations(array('child' => true));
		 $location_opts = array();
		if(!empty($locations)) {
			foreach ($locations as $key => $value) {
				if($show_records_count_in_search_filters == "Yes") {

					$avail_records_cnt = " (".count($this->home_model->get_student_leads(array('location_slug'=>$value->slug))).")";
				}

				$temp = array(
						'slug'=>$value->slug,
						'name'=>$value->location_name.$avail_records_cnt
					);
				array_push($location_opts, $temp);	
				// $location_opts[$value->slug] = $value->location_name.$avail_records_cnt;
			}
		}
		$data['location_opts'] = $location_opts;
		// $data->location_opts = $location_opts;

 		//Teaching type Options
		$teaching_types = $this->home_model->get_teaching_types();
		 $teaching_type_opts = array();
		foreach ($teaching_types as $key => $value) {
			if($show_records_count_in_search_filters == "Yes") {

				$avail_records_cnt = " (".count($this->home_model->get_student_leads(array('teaching_type_slug'=>$value->slug))).")";
			}

			$temp = array(
						'slug'=>$value->slug,
						'name'=>$value->teaching_type.$avail_records_cnt
					);
				array_push($teaching_type_opts, $temp);
			// $teaching_type_opts[$value->slug] = $value->teaching_type.$avail_records_cnt;
		}

		$data['teaching_type_opts'] = $teaching_type_opts;
		// $data->teaching_type_opts = $teaching_type_opts

		// for($i=0;$i<=1;$)

		// $data = array(
		// 		'course_opts'=>$course_opts,
		// 		'teaching_type_opts'=>$teaching_type_opts,
		// 		'location_opts'=>$location_opts
		// 	);
		$this->serviceResponse($data,'Leads filter data',1); */
		
		$data=array();
		
		$postdata 	= file_get_contents("php://input");
 		$request 	= json_decode($postdata);
		
		//courses
		$query = "SELECT courses.id,courses.name,(SELECT COUNT(DISTINCT(sl.id)) FROM ".TBL_PREFIX."student_leads sl WHERE sl.course_id=courses.id AND sl.status='Opened') as avail_records_cnt  FROM ".TBL_CATEGORIES." courses WHERE courses.is_parent=0 AND courses.status=1";
		$courses = $this->base_model->get_query_result($query);
				
		
		//locations
		$query = "SELECT l.id,l.location_name,(SELECT COUNT(DISTINCT(sl.id)) FROM ".TBL_PREFIX."student_leads sl WHERE sl.location_id=l.id AND sl.status='Opened') as avail_records_cnt FROM ".TBL_LOCATIONS." l WHERE (l.status='Active' OR l.status=1) AND l.parent_location_id != 0 ORDER BY l.sort_order ASC ";	
		$locations=$this->base_model->get_query_result($query);
		
		
			
		//teaching types
		$query = "SELECT t.id,t.teaching_type,(SELECT COUNT(DISTINCT(sl.id)) FROM ".TBL_PREFIX."student_leads sl WHERE sl.teaching_type_id=t.id AND sl.status='Opened') as avail_records_cnt FROM ".TBL_TEACHING_TYPES." t WHERE t.status=1 ORDER BY t.sort_order ASC ";	
		$teaching_types=$this->base_model->get_query_result($query);
		
		$data = array($courses,$locations,$teaching_types);
		
		$this->serviceResponse($data,'Leads Filter Data',1);
 	}

 	// TUTOR DASHBOARD COUNT

 	function tutor_dashboard_post()
 	{
 		$postdata 	= file_get_contents("php://input");
 		$request 	= json_decode($postdata);
 		$user_id 	= $request->user_id;
 		// $user_id = $this->post('user_id');
 		$tutor_dashboard_data = $this->tutor_model->get_tutor_dashboard_data($user_id);
		$credit_info = $this->base_model->fetch_records_from('user_credit_transactions',array('user_id'=>$user_id));
		$tutor_dashboard_data['credit_history'] = count($credit_info);
		$admin_money_transactions  = $this->base_model->fetch_records_from('admin_money_transactions',array('user_id'=>$user_id));
		$tutor_dashboard_data['credit_claim'] = count($admin_money_transactions);
		$this->serviceResponse($tutor_dashboard_data,'tutor dashboard data',1);
 	}

 	// Tutor pending messages list
 	function get_tutor_chat_students_list_post()
 	{
 		$postdata 	= file_get_contents("php://input");
 		$request 	= json_decode($postdata);
 		$user_id 	= $request->user_id;
 		$result = array();
 		
 		$type = $request->type;
		if($type=='student'){
			$query = "SELECT DISTINCT from_id,u.username,u.email,u.photo,u.device_id,(SELECT count(*) FROM ".TBL_CHAT." cc WHERE cc.to_id=".$user_id." and cc.student_status=0 and c.from_id=cc.from_id) as messages_count,(SELECT message from ".TBL_CHAT." m where m.from_id=c.from_id order by m.id desc LIMIT 1) as message from ".TBL_CHAT." c,".TBL_USERS." u where c.to_id=".$user_id." and  u.id=c.from_id";

		}else{
			$query = "SELECT DISTINCT from_id,u.username,u.email,u.photo,u.device_id,(SELECT count(*) FROM ".TBL_CHAT." cc WHERE cc.to_id=".$user_id." and cc.tutor_status=0 and c.from_id=cc.from_id) as messages_count,(SELECT message from ".TBL_CHAT." m where m.from_id=c.from_id order by m.id desc LIMIT 1) as message from ".TBL_CHAT." c,".TBL_USERS." u where c.to_id=".$user_id." and  u.id=c.from_id";
		}

 		$data =  $this->base_model->get_query_result($query);
 		if(empty($data)) 
		{
			$response='tutor chat students list';
			$status=0;
		}
		else
		{
			
			$response='tutor chat students list';
			$status=1;
		}

		$this->serviceResponse($data,$response,$status);
 	}

 	// UPDATE MESSAGES READ STATUS

 	function update_chat_status_post(){
 		$postdata = file_get_contents("php://input");
 		$request  = json_decode($postdata);

 		/*$from_id = $request->from_id;
 		$to_id = $request->to_id;*/
 		
 		$type = $request->type;
		if($type=='student'){
			$from_id = $request->tutor_id;
			$to_id   = $request->student_id;
			$update_data['student_status'] = '1';

		}else{
			$from_id = $request->student_id;
			$to_id   = $request->tutor_id;
			$update_data['tutor_status'] = '1';
		}

 		//$update_data['status'] = 1;

 		$where = array('from_id'=>$from_id,'to_id'=>$to_id);


 		if($this->base_model->update_operation($update_data,'chat',$where)){
 			$response = 'status changed successfully';
 			$status = 1;
 		}else{
 			$response = 'status not changed successfully';
 			$status = 0;
 		}

 		$this->serviceResponse(array(),$response,$status);
 	}

 	function get_student_questions_tutors_list_post()
 	{
 		$postdata = file_get_contents("php://input");
 		$request  = json_decode($postdata);
 		$student_id = $request->user_id;
 		$from_limit = $request->from_limit;

 		// $student_id = $this->post('user_id');
 		// $from_limit = $this->post('from_limit');

 		
		$query = "SELECT q.question_id,q.question_text,u.id as user_id,u.username,u.photo,u.device_id,c.name,(select count(*) from ".TBL_BOOKING_ANSWERS." where question_id=q.question_id and student_status='0') as converation_count FROM ".TBL_BOOKING_QUESTIONS." q,".TBL_USERS." u,".TBL_BOOKINGS." b,".TBL_CATEGORIES." c  WHERE q.booking_id IN (SELECT booking_id from ".TBL_BOOKINGS." where student_id=".$student_id.") AND q.question_id IN (SELECT question_id from ".TBL_BOOKING_ANSWERS.") AND b.tutor_id=u.id AND b.booking_id=q.booking_id AND b.course_id=c.id GROUP by q.question_id LIMIT ".$from_limit.",10";
		 		
 		$data =  $this->base_model->get_query_result($query);
 		if(empty($data)) 
		{
			$response='student questions list';
			$status=0;
		}
		else
		{			
			$response='student questions list';
			$status=1;
		}

		$this->serviceResponse($data,$response,$status);

 	}

 	function get_tutor_questions_students_list_post()
 	{
 		$postdata = file_get_contents("php://input");
 		$request  = json_decode($postdata);
 		$tutor_id = $request->tutor_id;

 		$query = "SELECT q.question_id,q.question_text,u.id as user_id,u.username,u.photo,u.device_id,c.name FROM ".TBL_BOOKING_QUESTIONS." q,".TBL_USERS." u,".TBL_BOOKINGS." b,".TBL_CATEGORIES." c  WHERE q.booking_id IN (SELECT booking_id from ".TBL_BOOKINGS." where tutor_id=".$tutor_id.") AND (q.question_id NOT IN (SELECT question_id from ".TBL_BOOKING_ANSWERS.") || q.question_id IN (SELECT question_id from ".TBL_BOOKING_ANSWERS." WHERE tutor_status='0' and to_id=".$tutor_id." and from_id=u.id)) AND b.booking_id=q.booking_id AND u.id=b.student_id AND b.course_id=c.id GROUP by q.question_id";
 		//$this->serviceResponse(array(),$query,1);
 		$data =  $this->base_model->get_query_result($query);
 		if(empty($data)) 
		{
			$response='tutor questions students list';
			$status=0;
		}
		else
		{			
			$response='tutor questions students list';
			$status=1;
		}

		$this->serviceResponse($data,$response,$status);

 	}
	
	//STUDENT
	/***
	*student dashboard
	*return
	***/
	function student_dashboard_post()
 	{
 		$postdata 	= file_get_contents("php://input");
 		$request 	= json_decode($postdata);
 		$user_id 	= $request->user_id;
 		// $user_id = $this->post('user_id');
 		$student_dashboard_data = $this->student_model->get_student_dashboard_data($user_id);
		$courses_info = $this->base_model->get_query_result("SELECT cp.purchase_id,cp.max_downloads,cp.total_downloads,cp.paid_date,c.course_title,u.username as tutor_name FROM ".$this->db->dbprefix('course_purchases')."  cp INNER JOIN ".$this->db->dbprefix('tutor_selling_courses')." c ON cp.sc_id=c.sc_id AND cp.tutor_id=c.tutor_id INNER JOIN ".$this->db->dbprefix('users')." u ON cp.tutor_id=u.id WHERE cp.user_id=".$user_id." ");
		$student_dashboard_data['courses'] = count($courses_info);
		$query = "SELECT * FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id  LEFT JOIN ".TBL_PREFIX."student_fav_tutors f ON u.id=f.tutor_id AND f.student_id=".$user_id." WHERE u.active=1 AND u.visibility_in_search='1' AND u.is_profile_update=1 AND (u.parent_id=0 OR u.parent_id='') AND ug.group_id=3  GROUP BY u.id ";
		
		$data = $this->base_model->get_query_result($query);
		$student_dashboard_data['tutors'] = count($data);
		
		$data = $this->base_model->fetch_records_from('user_credit_transactions',array('user_id'=>$user_id));
		$student_dashboard_data['credits'] = count($data);
		
		$this->serviceResponse($student_dashboard_data,'student dashboard data',1);
 	}
		
	/***
	**STUDENT BOOKINGS
	**return 
	***/
	function get_student_bookings_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$user_id 	= $request->user_id;
		$booking_type = $request->type;
		// $user_id 	= $this->post('user_id');
		$from_limit = $request->from_limit;
		$status_condition = '';
		if($booking_type!='all'){
			$status_condition = ' AND b.status="'.$booking_type.'"';
		}
		if($booking_type == "session_initiated")
			$status_condition .= ' AND b.preferred_location= "online-zoom" ';
		$to_limit = 10;
		
		//$bookings 	= $this->base_model->get_query_result('SELECT b.*,u.username as tutor_name,u.photo as tutor_photo,u.phone,u.email,u.device_id,c.name as course_name FROM '.TBL_BOOKINGS.' b,'.TBL_USERS.' u,'.TBL_CATEGORIES.' c WHERE b.tutor_id=u.id AND b.course_id=c.id AND b.student_id="'.$user_id.'" '.$status_condition.' LIMIT '.$from_limit.','.$to_limit.''); 
		
		$bookings 	= $this->base_model->get_query_result('SELECT b.*,u.username AS student_name,u.photo as tutor_photo,u.phone,u.email,u.device_id,c.name as course_name ,d.tutor_zoom_link,d.student_zoom_link FROM '.TBL_BOOKINGS.' b INNER JOIN	'.TBL_USERS.' u on b.tutor_id = u.id INNER JOIN	'.TBL_CATEGORIES.' c on b.course_id = c.id LEFT JOIN '.TBL_ZOOM_SESSION.' d on b.booking_id = d.booking_id WHERE b.student_id = "'.$user_id.'" '.$status_condition.' LIMIT '.$from_limit.','.$to_limit.'');
		$student_details = $this->base_model->get_user_info($user_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$student_details->time_zone];
		foreach($bookings as $key => $value){
			if($value->student_zoom_link != ""){
				$today = date('Y-m-d');
				$s_d = strtotime(str_replace('/','-',$value->start_date));
				$e_d = strtotime($value->end_date);
				$c_d = strtotime($today);
				$dayWeek = strtoupper(date('D'));
				$is_holiday = strpos($value->days_off,$dayWeek);
				if($s_d <= $c_d && $e_d >= $c_d && $is_holiday == false){
					$cur_time 	= (float)date('H.i');
					$time 	  	= explode('-', str_replace(' ', '', $value->time_slot));
					$start_time = date('H:i', strtotime(number_format($time[0],2)));
					$end_time   = date('H:i', strtotime(number_format($time[1],2)));
					$certain_mins_before_start_time = (float)date('H.i', strtotime($start_time.' -'.$this->config->item('site_settings')->enable_initiate_session_option_before_mins.' minutes'));
					$certain_mins_end_time = (float)date('H.i', strtotime($end_time));
					if($cur_time >= $certain_mins_before_start_time && $cur_time <= $certain_mins_end_time){
						
					}else{
						$bookings[$key]->student_zoom_link = "";
					}
				} else{
					$bookings[$key]->student_zoom_link = "";
				}
			}
			
			$t = strtotime($value->start_date);
			$t1 = strtotime($value->end_date);
			$time_slot = explode("-",$value->time_slot);
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
			$myDateTime_end = new DateTime(date('Y-m-d',$t1)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->add($myInterval);
			$myDateTime_end->add($myInterval);
			$bookings[$key]->start_date = $myDateTime_start->format("Y-m-d");
			$bookings[$key]->end_date = $myDateTime_end->format("Y-m-d");
			$bookings[$key]->time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		}
		$this->serviceResponse($bookings,'Student Bookings',1); 
	}
	
	/***
	**GET STUDENT BOOKING STATUS
	**
	***/
	function get_student_booking_status_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		$booking_id = $request->booking_id;
		$user_id	= $request->user_id;
		
		$booking_det = $this->base_model->fetch_records_from('bookings', array('booking_id' => $booking_id));
		if(!empty($booking_det)) 
		{
			$booking_det = $booking_det[0];
			if($booking_det->student_id != $user_id) 
			{
				$response = get_languageword('not_authorized');
				$status   = 0; 	
				$this->serviceResponse(array(),$response,$status);
			}

			$booking_status = $booking_det->status;
			$updated_by = getUserType($booking_det->updated_by);

			if($booking_status == "pending") 
			{
				$status_array = array('cancelled_before_course_started' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
			}

			if($booking_status == "approved") 
			{
				$status_array = array('cancelled_before_course_started' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
			}
				
			if($booking_status == "session_initiated") 
			{
				$status_array = array('running' => get_languageword('start_course'),
				'cancelled_before_course_started'=>get_languageword('cancel'),
				'called_for_admin_intervention'=>get_languageword('claim_for_admin_intervention')
				);
			}

			if($booking_status == "running") 
			{
				$status_array = array('completed' => get_languageword('completed'),'cancelled_when_course_running' => get_languageword('cancel'),
				'called_for_admin_intervention'=>get_languageword('claim_for_admin_intervention'));
			}

			if($booking_status == "completed") 
			{
				$status_array = array('closed' => get_languageword('close'),
				'called_for_admin_intervention'=>get_languageword('claim_for_admin_intervention'));
			}

			$data = array();
			if(!empty($status_array)){
				foreach ($status_array as $key => $value) {
					$tmp = array(
						'status_key'=>$key,
						'status_value'=>$value
					);

					array_push($data, $tmp);
				}
			}
			$this->serviceResponse($data,'Status fields',1);
		} 
		else 
		{
			$response = get_languageword('Booking details not found');
			$status = 0;
			$this->serviceResponse(array(),$response,$status);
		}
	}
	
	/***
	**Get Languages - Profile
	***/
	function get_known_languages_post()
	{
		$lng_opts = $this->db->get_where('languages',array('status' => 'Active'))->result();
		$data['languages'] = $lng_opts;
		$countries = $this->base_model->fetch_records_from('country');
		$temp = (array)$countries[0];
		foreach ($countries as $key => $value) {
			$countries[$key]->code = $key.'_'.$value->phonecode;
		}
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone_object = array();
		foreach($time_zone_list as $key => $value){
			$time_zone_object[$key] = new stdClass();
			$time_zone_object[$key]->id = $key;
			$time_zone_object[$key]->name = $value;
		}
		$data['country'] = $countries;
		$data['timezone'] = $time_zone_object;
		// $options = array();
		// if(!empty($lng_opts))
		// {
		// 	foreach($lng_opts as $row):
		// 		$options[$row->name] = $row->name;
		// 	endforeach;
		// }
		$response='Languages';
		$this->serviceResponse($data,$response,0);
	}
	
	/***
	*UPDATE STUDENT PERSONAL INFO
	***/
	function update_stpersonal_details_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$user_details=array();
		
		$response='';
		$status=0;
		
		$user_data  = $request->userData;
		$user_id 	= $user_data->id;
		
		$first_name = ucfirst(strtolower($user_data->first_name));
		$last_name 	= ucfirst(strtolower($user_data->last_name));
		$username 	=  $first_name.' '.$last_name;
		
		$prev_username = $this->base_model->fetch_value('users', 'username', array('id' => $user_id));
		
		//If user updates the username
		if($prev_username != $username) {
			$slug = prepare_slug($username, 'slug', 'users');
			$inputdata['slug'] =  $slug;
		}
		
		$inputdata['first_name']= $first_name;
		$inputdata['last_name'] = $last_name;
		$inputdata['username'] 	= $username;
		$inputdata['gender'] 	= $user_data->gender;
		$inputdata['dob'] 		= date('Y-m-d',strtotime($user_data->dob));
		$inputdata['website'] 	= $user_data->website;
		$inputdata['facebook'] 	= $user_data->facebook;
		$inputdata['twitter'] 	= $user_data->twitter;
		$inputdata['linkedin'] 	= $user_data->linkedin;
		$inputdata['is_profile_update'] 	= 1;
		$inputdata['time_zone'] = $user_data->time_zone;
		$inputdata['country'] = $user_data->country;
		
		//student parameters
		$inputdata['skype'] 	= $user_data->skype;
		//student parameters
		
		//tutor parameters
		$inputdata['paypal_email'] 		= $user_data->paypal_email;
		$inputdata['bank_ac_details'] 	= $user_data->bank_ac_details;
		//tutor parameters
		
		$language_of_teaching = json_decode($request->language_of_teaching);
		if(!empty($language_of_teaching))
		$inputdata['language_of_teaching'] = implode(',',$language_of_teaching);
	
		if($this->base_model->update_operation($inputdata,'users', array('id' => $user_id)))
		{
			$user_details = getUserRec($user_id);
			$response = get_languageword('updated_successfully');
			$status   = 1;
		}
		else
		{
			$response = get_languageword('not_updated');
			$status   = 0;
		}
		$this->serviceResponse($user_details,$response,$status);
	}
	
	/***
	*UPDATE STUDENT PROFILE INFO
	***/
	function update_stprofile_details_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$user_details=array();
		
		$response='';
		$status=0;
		
		$inputdata['profile'] 				= $request->profile;
		$inputdata['profile_page_title'] 	= $request->profile_page_title;
		$inputdata['seo_keywords'] 			= $request->seo_keywords;
		$inputdata['meta_desc'] 			= $request->meta_desc;
		$inputdata['willing_to_travel'] 	= $request->willing_to_travel;
		$inputdata['qualification'] 		= $request->qualification;
		$inputdata['own_vehicle'] 			= $request->own_vehicle;
		$inputdata['is_profile_update'] 	= 1;
		
		//tutor 
		if(isset($request->user_type) && $request->user_type=='tutor')
		{
			$inputdata['experience_desc'] 		= $request->experience_desc;
			$inputdata['teaching_experience'] 	= $request->teaching_experience;
			$inputdata['duration_of_experience'] = $request->duration_of_experience;
		}
		
		
		$user_id = $request->user_id;
		
		if($this->base_model->update_operation($inputdata,'users', array('id' => $user_id)))
		{
			$user_details = getUserRec($user_id);
			$response = get_languageword('updated_successfully');
			$status   = 1;
		}
		else
		{
			$response = get_languageword('not_updated');
			$status   = 0;
		}
		$this->serviceResponse($user_details,$response,$status);
	}
	
	/***
	*UPDATE TUTOR MANAGE PRIVACY SETTINGS
	***/
	function update_privacy_settings_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$user_details=array();
		
		$response='';
		$status=0;
		
		$inputdata['free_demo'] 			= $request->free_demo;
		$inputdata['visibility_in_search'] 	= $request->visibility_in_search;
		$inputdata['show_contact'] 			= $request->show_contact;
		$inputdata['availability_status'] 	= $request->availability_status;
		$inputdata['is_profile_update'] 	= 1;
		
		$user_id = $request->user_id;
		
		if($this->base_model->update_operation($inputdata,'users', array('id' => $user_id)))
		{
			$user_details = getUserRec($user_id);
			$response = get_languageword('updated_successfully');
			$status   = 1;
		}
		else
		{
			$response = get_languageword('not_updated');
			$status   = 0;
		}
		$this->serviceResponse($user_details,$response,$status);
	}
	
	/***
	**Get Countries-My Address
	***/
	function countries_post()
	{
		$countries = $this->base_model->fetch_records_from('country');
		/* $countries_opts = array('' => get_languageword('select_country'));
		if(!empty($countries))
		{
			foreach($countries as $country)
			{
				$countries_opts[$country->phonecode.'_'.$country->nicename]  = $country->nicename." +".$country->phonecode;
			}
		} */
		
		$response='Countries';
		$this->serviceResponse($countries,$response,0);
	}
	
	/***
	*UPDATE STUDENT CONTACT INFO
	***/
	function update_stcontact_details_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$user_details=array();
		
		$response='';
		$status=0;
		
		$inputdata['city'] 		= $request->city;
		$inputdata['land_mark'] = $request->land_mark;
		
		// $code_country = explode('_', $request->country);

		$inputdata['country'] 	 = $request->country;
		// $inputdata['phone_code'] = $code_country[0];

		$inputdata['pin_code'] 	= $request->pin_code;
		$inputdata['phone'] 	= $request->phone;	
		$inputdata['is_profile_update'] 	= 1;
		
		if($request->academic_class=='yes')
		$inputdata['academic_class'] 	 = 'yes';
		else
		$inputdata['academic_class'] 	 = 'no';

	
		if($request->non_academic_class=='yes')
		$inputdata['non_academic_class'] = 'yes';
		else
		$inputdata['non_academic_class'] = 'no';	
		
		$user_id = $request->user_id;
		
		if($this->base_model->update_operation($inputdata,'users', array('id' => $user_id)))
		{
			$user_details = getUserRec($user_id);
			$response = get_languageword('updated_successfully');
			$status   = 1;
		}
		else
		{
			$response = get_languageword('not_updated');
			$status   = 0;
		}
		$this->serviceResponse($user_details,$response,$status);
	}
	
	//GET CHAT HISTORY
	function get_chat_history_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$from_id 	= $request->sender_id;
		$to_id 	 	= $request->reciever_id;
		
		$from_limit = $request->from_limit;
		$to_limit	= $request->to_limit;
		
		$data = $this->base_model->get_query_result("SELECT * FROM ".$this->db->dbprefix('chat')." WHERE (from_id=".$from_id." AND to_id=".$to_id.") OR (from_id=".$to_id." AND to_id=".$from_id.") ORDER BY id DESC LIMIT ".$from_limit.",".$to_limit." ");
		
		$this->serviceResponse($data,'Chat History',1);
	}
	
	//SEND MESSAGE
	function send_message_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request  	= json_decode($postdata);
		
		$data = array();
		$status=0;
		$response='';
		
		if($request->sender_id > 0 && $request->reciever_id  >0)
		{
			$data['from_id'] = $request->sender_id;
			$data['to_id']	 = $request->reciever_id;
			$data['message'] = $request->message;

			$data['datetime'] = date('Y-m-d H:i:s');
			$device_id = $request->device_id;
			if($request->type=='tutor'){
				$data['student_status'] 	= '0';
				$data['tutor_status'] 	 	= '1';
			}else{
				$data['tutor_status'] 	 	= '0';
				$data['student_status'] 	= '1';
			}
			$chat_id=$this->base_model->insert_operation_id($data,'chat');
			if($chat_id)
			{
				$data = $this->base_model->fetch_records_from('chat',array('id'=>$chat_id));
				if($device_id!=''){
					$this->sendPushNotification($device_id,'Chat Message',$request->message);	
				}
				
				
				$response='Message sent successfully';
				$status=1;
			}
			else
			{
				$response='Message not sent';
				$status=0;
			}
			$this->serviceResponse($data,$response,$status);
		}
		else
		{
			$response='Invalid Operation';
			$status=0;
			$this->serviceResponse(array(),$response,$status);
		}
	}
	
	//GET STUDENT TEACHING TYPES
	function get_student_teaching_types_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$user_id  = $request->user_id;
		
		$teaching_types = $this->base_model->get_query_result("SELECT tt.id, tt.teaching_type,CASE WHEN EXISTS (SELECT stt.teaching_type_id FROM pre_student_prefferd_teaching_types stt WHERE stt.teaching_type_id = tt.id AND stt.student_id=".$user_id.") THEN true ELSE false END AS checked  FROM pre_teaching_types tt WHERE tt.status=1");

		$this->serviceResponse($teaching_types,'teaching types',1);
	}
	
	//GET TUTORS FOR SELECTED COURSE
	function get_course_tutors_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$course_id= $request->course_id;
		
		$data = array();
		
		$adm_approval_cond="";
		if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'yes') == 0) 
		{
        	$adm_approval_cond .= " AND u.admin_approved = 'Yes' ";
        }
		
		
		$query = "SELECT u.* FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id INNER JOIN ".TBL_TUTOR_COURSES." tc ON tc.tutor_id=u.id AND tc.course_id=".$course_id." AND tc.status=1 WHERE u.active=1 AND u.visibility_in_search='1' AND u.is_profile_update=1 AND (u.parent_id=0 OR u.parent_id='') AND ug.group_id=3 ".$adm_approval_cond." GROUP BY u.id ORDER BY u.net_credits DESC ";
		//AND u.admin_approved='No'
		
		$data = $this->base_model->get_query_result($query);
		$this->serviceResponse($data,'course tutors',1);
	}
	
	//FIND TUTORS -
	function get_tutors_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		$student_id = $request->student_id;
		$from_limit = $request->from_limit;
		$to_limit = 20;
		$course_tbl_join		= "";
        $course_cond 			= "";
		if(isset($request->courses))
		{
			$courses = $request->courses;
			if(!empty($courses))
			{
				$courses=implode(",",$courses);
				$course_tbl_join = " INNER JOIN ".TBL_TUTOR_COURSES." tc ON tc.tutor_id=u.id ";
				$course_cond = " AND tc.course_id IN (".$courses.") AND tc.status=1 ";
			}
		}
		
		$location_tbl_join 		= "";
		$location_cond 			= "";
		if(isset($request->locations))
		{
			$locations = $request->locations;
			if(!empty($locations))
			{
				$locations=implode(",",$locations);
				$location_tbl_join = " INNER JOIN ".TBL_TUTOR_LOCATIONS." tl ON tl.tutor_id=u.id ";
				$location_cond = " AND tl.location_id IN (".$locations.")";
			}
		}
		
		$teaching_type_tbl_join = "";
		$teaching_type_cond 	= "";
		if(isset($request->teaching_types))
		{
			$teaching_types = $request->teaching_types;
			if(!empty($teaching_types))
			{
				$teaching_types=implode(",",$teaching_types);
				$teaching_type_tbl_join = " INNER JOIN ".TBL_TUTOR_TEACHING_TYPES." tt ON tt.tutor_id=u.id ";
				$teaching_type_cond = " AND tt.teaching_type_id IN (".$teaching_types.") ";
			}
		}
		 
		
		
		$data = array();
		
		$adm_approval_cond="";
		if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'Yes') == 0) 
		{
        	$adm_approval_cond .= " AND u.admin_approved = 'Yes' ";
        }
		
		
		$query = "SELECT u.*,f.fav_id FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id ".$course_tbl_join." ".$location_tbl_join." ".$teaching_type_tbl_join." LEFT JOIN ".TBL_PREFIX."student_fav_tutors f ON u.id=f.tutor_id AND f.student_id=".$student_id." WHERE u.active=1 AND u.visibility_in_search='1' AND u.is_profile_update=1 AND (u.parent_id=0 OR u.parent_id='') AND ug.group_id=3 ".$course_cond." ".$location_cond." ".$teaching_type_cond." ".$adm_approval_cond." GROUP BY u.id ORDER BY u.net_credits DESC LIMIT ".$from_limit.",".$to_limit."";
		
		$data = $this->base_model->get_query_result($query);
		$this->serviceResponse($data,'Tutors',1);
	}
	
	//STUDENT add/remove his fav tutor
	function add_fav_tutor_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		$data 		= array();
		
		$response='';
		$status=0;
		
		$student_id = $request->student_id;
		$tutor_id   = $request->tutor_id;
		
		$fav_id = $request->fav_id;
		if($fav_id > 0)
		{
			if($this->base_model->delete_record_new('student_fav_tutors', array('fav_id' => $fav_id)))
			{
				$response='Tutor removed from your favourites';
				$status=1;
			}
			else
			{
				$response='Tutor not removed from your favourites';
				$status=0;
			}
		} 
		else
		{
			$inputdata = array();
			$inputdata['student_id'] = $student_id;
			$inputdata['tutor_id'] 	 = $tutor_id;
			if($this->base_model->insert_operation($inputdata,'student_fav_tutors'))
			{
				$response='Tutor added to your favourites';
				$status=1;
			}
			else
			{
				$response='Tutor not added to your favourites';
				$status=0;
			}
		}
		
		$course_tbl_join		= "";
        $course_cond 			= "";
		if(isset($request->courses))
		{
			$courses = $request->courses;
			if(!empty($courses))
			{
				$courses=implode(",",$courses);
				$course_tbl_join = " INNER JOIN ".TBL_TUTOR_COURSES." tc ON tc.tutor_id=u.id ";
				$course_cond = " AND tc.course_id IN (".$courses.") AND tc.status=1 ";
			}
		}
		
		$location_tbl_join 		= "";
		$location_cond 			= "";
		if(isset($request->locations))
		{
			$locations = $request->locations;
			if(!empty($locations))
			{
				$locations=implode(",",$locations);
				$location_tbl_join = " INNER JOIN ".TBL_TUTOR_LOCATIONS." tl ON tl.tutor_id=u.id ";
				$location_cond = " AND tl.location_id IN (".$locations.") ";
			}
		}
		
		$teaching_type_tbl_join = "";
		$teaching_type_cond 	= "";
		if(isset($request->teaching_types))
		{
			$teaching_types = $request->teaching_types;
			if(!empty($teaching_types))
			{
				$teaching_types=implode(",",$teaching_types);
				$teaching_type_tbl_join = " INNER JOIN ".TBL_TUTOR_TEACHING_TYPES." tt ON tt.tutor_id=u.id ";
				$teaching_type_cond = " AND tt.teaching_type_id IN (".$teaching_types.") ";
			}
		}
		
		$adm_approval_cond="";
		if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'yes') == 0) 
		{
        	$adm_approval_cond .= " AND u.admin_approved = 'Yes' ";
        }
		
		$query = "SELECT u.*,f.fav_id FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id ".$course_tbl_join." ".$location_tbl_join." ".$teaching_type_tbl_join." LEFT JOIN ".TBL_PREFIX."student_fav_tutors f ON u.id=f.tutor_id AND f.student_id=".$student_id." WHERE u.active=1 AND u.visibility_in_search='1' AND u.is_profile_update=1 AND (u.parent_id=0 OR u.parent_id='') AND ug.group_id=3 ".$course_cond." ".$location_cond." ".$teaching_type_cond." ".$adm_approval_cond." GROUP BY u.id ORDER BY u.net_credits DESC ";
		
		$data = $this->base_model->get_query_result($query);
		$this->serviceResponse($data,$response,$status);
	}
	
	//GET student favourite tutors
	function get_student_fav_tutors_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$student_id = $request->student_id;
		
		$data = array();
		$response='';
		$status=0;
		
		$query = "SELECT f.*,u.id,u.photo,u.availability_status,u.username,u.free_demo,u.teaching_experience,u.duration_of_experience,u.qualification from ".TBL_PREFIX."student_fav_tutors f INNER JOIN ".TBL_PREFIX."users u ON f.tutor_id=u.id WHERE f.student_id=".$student_id."";
		
		$data = $this->base_model->get_query_result($query);
		if(!empty($data))
		{
			$response = 'Your favourite tutors';
			$status=1;
		}
		else
		{
			$response = 'No favourite tutors available';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	
	function remove_fav_tutor_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		$student_id = $request->student_id;
		$fav_id		= $request->fav_id;
		
		$data = array();
		$response='';
		$status=0;
		
		if($this->base_model->delete_record_new('student_fav_tutors', array('fav_id' => $fav_id)))
		{
			$response = 'Tutor removed from your favourites';
			$status=1;
		}
		else
		{
			$response = 'Tutor not removed from your favourites';
			$status=0;
		}
		
		$query = "SELECT f.*,u.id,u.photo,u.availability_status,u.username,u.free_demo,u.teaching_experience,u.duration_of_experience,u.qualification from ".TBL_PREFIX."student_fav_tutors f INNER JOIN ".TBL_PREFIX."users u ON f.tutor_id=u.id WHERE f.student_id=".$student_id."";
		
		$data = $this->base_model->get_query_result($query);
		$this->serviceResponse($data,$response,$status);
	}
	
	//GET TUTOR Profile
	function get_tutor_profile_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$tutor_id = $request->tutor_id;
		
		$data = array();
		$response='';
		$status=0;
		
		/* $adm_approval_cond="";
		if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'yes') == 0) {
        	$adm_approval_cond = ' AND u.admin_approved = "Yes" ';
        } */
		
		$tutor_info_query = "SELECT u.* FROM ".$this->db->dbprefix('users')." u WHERE u.id=".$tutor_id." ";

    	$tutor_details = $this->base_model->get_query_result($tutor_info_query);
		
		
		//Tutoring Courses
    	$tutor_courses_query = "SELECT GROUP_CONCAT(' ', courses.name) AS tutoring_courses FROM ".$this->db->dbprefix('tutor_courses')." tc INNER JOIN ".$this->db->dbprefix('categories')." courses ON courses.id=tc.course_id WHERE tc.tutor_id=".$tutor_id." AND tc.status=1 AND courses.status=1 ORDER BY tc.sort_order ASC";
    	$tutor_details[0]->tutoring_courses = $this->db->query($tutor_courses_query)->row()->tutoring_courses;
		
		//Tutoring Locations
    	$tutor_locations_query = "SELECT GROUP_CONCAT(' ', l.location_name) AS tutoring_locations FROM ".$this->db->dbprefix('tutor_locations')." tl INNER JOIN ".$this->db->dbprefix('locations')." l ON l.id=tl.location_id WHERE tl.tutor_id=".$tutor_id." AND tl.status=1 AND l.status=1 ORDER BY tl.sort_order ASC";
    	$tutor_details[0]->tutoring_locations = $this->db->query($tutor_locations_query)->row()->tutoring_locations;

    	$tutor_locations_query2 = "SELECT l.id,l.slug,l.location_name AS tutoring_locations FROM ".$this->db->dbprefix('tutor_locations')." tl INNER JOIN ".$this->db->dbprefix('locations')." l ON l.id=tl.location_id WHERE tl.tutor_id=".$tutor_id." AND tl.status=1 AND l.status=1 ORDER BY tl.sort_order ASC";
    	$tutor_details[0]->tutoring_locations_data = $this->base_model->get_query_result($tutor_locations_query2);



		
		//Tutor's Gallery
        $tutor_gallery_query = "SELECT image_title, image_name FROM ".$this->db->dbprefix('gallery')." WHERE user_id=".$tutor_id." AND image_status='Active' ORDER BY image_order ASC";
        $tutor_details[0]->tutor_gallery = $this->db->query($tutor_gallery_query)->result();
		
		//Tutor Experience
        $tutor_experience_query = "SELECT company, role, description, from_date, to_date FROM ".$this->db->dbprefix('users_experience')." WHERE user_id=".$tutor_id." ";
        $tutor_details[0]->tutor_experience = $this->db->query($tutor_experience_query)->result();
		
		
		
		$no_of_comments = tutorCommentNumber($tutor_id);
		$tutor_details[0]->no_of_reviews = $no_of_comments;
		
		$tutor_rating = tutorAvgRatingValue($tutor_id);
		$tutor_details[0]->tutor_rating = $tutor_rating;

    	$this->serviceResponse($tutor_details,'Tutor Details',1);
	}
	
	//UPDATE STUDENT TEACHING TYPES
	function update_stteaching_types_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$student_id = $request->student_id;
		
		$teaching_types = $request->teaching_types;
		
		$result=array();
		$data = array();
		$response='';
		$status=0;
		
		foreach($teaching_types as $teaching) 
		{
			$update_data=array();
			$update_data['teaching_type_id'] = $teaching->id;
			$update_data['student_id'] = $student_id;
			$update_data['created_at'] = date('Y-m-d H:i:s');
			array_push($data,$update_data);
			unset($update_data);
		}
		
		$existing_teaching = $this->base_model->fetch_records_from('student_prefferd_teaching_types',array('student_id'=>$student_id));
		if(!empty($existing_teaching))
		{
			$this->base_model->delete_record_new('student_prefferd_teaching_types', array('student_id' => $student_id));
		}
		
		if(!empty($data))
		{
			if($this->db->insert_batch( 'pre_student_prefferd_teaching_types',$data))
			{
				$response = 'teaching types updated';
				$status=1;
			}
			else
			{
				$response = 'teaching types not updated';
				$status=0;
			}
		}
		$this->serviceResponse($result,$response,$status);
	}
	
	//STUDENT MANAGE LOCATIONS
	function get_student_locations_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		$student_id = $request->user_id;
		
		$parentLocationDetails = $this->db->select('id AS parentLocation_id, location_name AS parentLocation_name')->get_where($this->db->dbprefix( 'locations' ), array('parent_location_id' => 0, 'status' => 'Active'))->result();
		
		foreach($parentLocationDetails as $index => $p) {
		
			$query = "SELECT l.*,(SELECT count(*) FROM ".$this->db->dbprefix('student_locations')." tl,".$this->db->dbprefix( 'users' )." u,".$this->db->dbprefix( 'users_groups' )." ug WHERE (tl.location_id = l.id OR u.location_id = l.id) AND ug.group_id = 2 AND ug.user_id = u.id AND u.id = tl.student_id AND u.active = 1 AND tl.status = '1') AS no_of_student,(SELECT sl.location_id  from ".$this->db->dbprefix('student_locations')." sl WHERE sl.student_id=".$student_id." AND sl.location_id=l.id AND sl.status=1) as checked_id FROM ".$this->db->dbprefix( 'locations' )." l WHERE l.parent_location_id = ".$p->parentLocation_id." AND l.status = 'active'";
			
			$childLocations = $this->db->query($query)->result();
			
			// $locations[$p->parentLocation_name] = $childLocations;
			
			$parentLocationDetails[$index]->sub_locations = $childLocations;		
		}
		
		$response = 'Student Locations';
		$this->serviceResponse($parentLocationDetails,$response,1);
	}
	
	// UPDATE STUDENT LOCATIONS
	function update_student_locations_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$user_id 	= $request->user_id;
		$locations 	= $request->locations;
		
		$response='';
		$status=0;
		
		$update_data = array();
		
		if(!empty($locations)){
			foreach ($locations as  $value) 
			{
				$temp=array();
				$temp = array(
					'student_id'=>$user_id,
					'location_id'=>$value->location_id,
					'created_at'=>date('Y-m-d h:i:s')
				);
				array_push($update_data,$temp);
				unset($temp);
			}
		}
		
		$get_existing_locations  = $this->base_model->fetch_records_from('student_locations',array('student_id'=>$user_id));

		if(!empty($get_existing_locations))
		{
			$where['student_id'] = $user_id;
			$this->base_model->delete_record_new('student_locations',$where);
		}
		
		
		if(!empty($update_data))
		{
			if($this->db->insert_batch('pre_student_locations',$update_data))
			{
				$response = 'Student locations updated';
				$status=1;
			}
			else
			{
				$response = 'Student locations not updated';
				$status=0;
			}
		} 
		$this->serviceResponse(array(),$response,$status);
	}
	
	//STUDENT MANAGE COURSES
	function get_student_courses_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		$student_id = $request->user_id;
		
		
		$result=array();
		
		$categories = $this->db->query("SELECT id AS parent_course_id,name AS parent_course_name FROM ".TBL_CATEGORIES." WHERE is_parent=1 AND status=1 ")->result();
		
		if(!empty($categories))
		{
			foreach($categories as $index=>$parent):
			
			$query = "SELECT c.id as child_course_id,c.name as child_course_name,(SELECT sc.course_id from ".$this->db->dbprefix('student_preffered_courses')." sc WHERE sc.student_id=".$student_id." AND sc.course_id=c.id AND sc.status=1) as checked_id FROM ".TBL_CATEGORIES." c INNER JOIN ".TBL_COURSE_CATEGORIES." cc on c.id=cc.course_id AND cc.category_id=".$parent->parent_course_id." WHERE c.is_parent=0 AND c.status=1";
			
			$courses = $this->base_model->get_query_result($query);
			
			$categories[$index]->courses = $courses;
			endforeach;
		}
		$result = $categories;
		
		$response = 'Student Courses';
		$this->serviceResponse($result,$response,1);
	}
	
	// UPDATE STUDENT COURSES
	function update_student_courses_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$user_id 	= $request->user_id;
		$courses 	= $request->courses;
		
		$response='';
		$status=0;
		
		$update_data = array();
		
		if(!empty($courses)){
			foreach ($courses as  $value) 
			{
				$temp=array();
				$temp = array(
					'student_id'=>$user_id,
					'course_id'=>$value->course_id,
					'status'=>1,
					'created_at'=>date('Y-m-d h:i:s')
				);
				array_push($update_data,$temp);
				unset($temp);
			}
		}
		
		$get_existing_courses = $this->base_model->fetch_records_from('student_preffered_courses',array('student_id'=>$user_id));

		if(!empty($get_existing_courses))
		{
			$where['student_id'] = $user_id;
			$this->base_model->delete_record_new('student_preffered_courses',$where);
		}
		
		
		if(!empty($update_data))
		{
			if($this->db->insert_batch('pre_student_preffered_courses',$update_data))
			{
				$response = 'Student courses updated';
				$status=1;
			}
			else
			{
				$response = 'Student courses not updated';
				$status=0;
			}
		} 
		$this->serviceResponse(array(),$response,$status);
	}
	
	
	//STUDENT - GET TUTORS FILTER DATA
	function get_filter_data_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request	= json_decode($postdata);
		
		$data=array();

		//courses
		$query = "SELECT courses.id,courses.name,(SELECT COUNT(DISTINCT(tc.id)) FROM ".TBL_PREFIX."tutor_courses tc WHERE tc.course_id=courses.id ) as avail_records_cnt  FROM ".TBL_CATEGORIES." courses WHERE courses.is_parent=0 AND courses.status=1";
		$courses = $this->base_model->get_query_result($query);
				
		
		//locations
		$query = "SELECT l.id,l.location_name,(SELECT COUNT(DISTINCT(tl.id)) FROM ".TBL_PREFIX."tutor_locations tl WHERE tl.location_id=l.id) as avail_records_cnt FROM ".TBL_LOCATIONS." l WHERE (l.status='Active' OR l.status=1) AND l.parent_location_id != 0 ORDER BY l.sort_order ASC ";	
		$locations=$this->base_model->get_query_result($query);
		
		
			
		//teaching types
		$query = "SELECT t.id,t.teaching_type,(SELECT COUNT(DISTINCT(tt.id)) FROM ".TBL_PREFIX."tutor_teaching_types tt WHERE tt.teaching_type_id=t.id) as avail_records_cnt FROM ".TBL_TEACHING_TYPES." t WHERE t.status=1 ORDER BY t.sort_order ASC ";	
		$teaching_types=$this->base_model->get_query_result($query);
		
		$data = array($courses,$locations,$teaching_types);
			
		$response = 'Filter Data';
		$this->serviceResponse($data,$response,1);
	}
	
	
	//SEND MESSAGE 
	function send_lead_message_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$student_id 	= $request->student_id;
		$student_slug 	= $request->student_slug;
		
		$lead_id 		= $request->lead_id;
		
		$tutor_id 	= $request->tutor_id;
		
		$student_details = $this->home_model->get_student_profile_app($student_slug,$lead_id,$tutor_id);
		
		
		if(!is_array($student_details) || empty($student_details)) 
		{
			$response=$student_details;
			$this->serviceResponse(array(),$response,$status);
		}
		
		$inputdata=array();
		$inputdata['from_user_id']=$tutor_id;
		$credits_for_sending_message = $this->config->item('site_settings')->credits_for_sending_message;
		
		//Check Whether tutor is premium user or not
		if(!is_premium($inputdata['from_user_id'])) 
		{
			$response=get_languageword('please_become_premium_member_to_send_message_to_student');
			$this->serviceResponse(array(),$response,$status);
		}
		
		
		$inputdata['name'] 			= $request->firstName;
		$inputdata['course_slug']	= $student_details[0]->lead_details[0]->course_name;
		$inputdata['email'] 		= $request->email;
		$inputdata['phone'] 		= $request->phone;
		$inputdata['message'] 		= $request->msg;
		
		$to_user_type   			= 'student';		
		$inputdata['to_user_id']    = $student_id;				
		$inputdata['created_at']	= date('Y-m-d H:i:s');
		$inputdata['updated_at']	= $inputdata['created_at'];
		
		$ref = $this->base_model->insert_operation($inputdata, 'messages');
		
		if($ref) 
		{
			//Get Send Message Email Template
			$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '17'));
			
			$student_rec = getUserRec($inputdata['to_user_id']);

			$from 	= $inputdata['email'];
			$parent_detail = $this->home_model->get_parent_details($student_rec->email);
			if(!empty($parent_detail) && !empty($parent_detail->email))
				$to 	= $student_rec->email.",".$parent_detail->email;
			else
				$to 	= $student_rec->email;

			$sub 	= get_languageword("Message Received From ")." ".get_languageword('tutor');
			
			$msg='';
			$msg.= '<p>'.get_languageword('Hi ').$student_rec->username.',</p><p>'.get_languageword('You got a message from Tutor Below are the details').'</p><p>
			<strong>'.get_languageword('name').':</strong> '.$inputdata['name'].'</p><p><strong>'.get_languageword('email').':</strong> '.$inputdata['email'].'</p><p><strong>'.get_languageword('phone').':</strong> '.$inputdata['phone'].'</p><p><strong>'.get_languageword('message').':</strong> '.$inputdata['message'].'</p><p>&nbsp;</p>';
			$msg .= "<p>".get_languageword('Thank you')."</p>";
			
			if(!empty($email_tpl)) 
			{
				$email_tpl = $email_tpl[0];
				if(!empty($email_tpl->from_email)) 
				{
					$from = $email_tpl->from_email;
				}
				if(!empty($email_tpl->template_subject)) 
				{
					$sub = $email_tpl->template_subject." ".get_languageword('Tutor');
				}
				if(!empty($email_tpl->template_content)) 
				{
					$msg = "";
					$original_vars  = array($student_rec->username, get_languageword('Tutor'), $inputdata['name'], $inputdata['email'], $inputdata['phone'], $inputdata['course_slug'], $inputdata['message']);
					$temp_vars		= array('___TO_NAME___','___USER_TYPE___','___NAME___', '___EMAIL___', '___PHONE___', '___COURSE___', '___MESSAGE___');
					$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);
				}
			}
			
			if(sendEmail($from, $to, $sub, $msg)) 
			{
				//Log Credits transaction data & update user net credits - Start
				$per_credit_value = $this->config->item('site_settings')->per_credit_value;
				
				$log_data = array(
					'user_id' => $inputdata['from_user_id'],
					'credits' => $credits_for_sending_message,
					'per_credit_value' => $per_credit_value,
					'action'  => 'debited',
					'purpose' => 'For Sending Message To Student "'.$student_slug.'" ',
					'date_of_action	' => date('Y-m-d H:i:s'),
					'reference_table' => 'messages',
					'reference_id' => $ref,
				);
				
				log_user_credits_transaction($log_data);

				update_user_credits($inputdata['from_user_id'], $credits_for_sending_message, 'debit');
				//Log Credits transaction data & update user net credits - End
				$response = get_languageword('Your message sent to Student successfully');
				$status=1;
				
				$data = $this->base_model->fetch_records_from('users',array('id'=>$tutor_id));
				$this->serviceResponse($data,$response,$status);
			}
			else
			{
				$response=get_languageword('Your message not sent due to some technical issue Please send message after some time Thankyou');
				$this->serviceResponse(array(),$response,$status);
			}
		}
		else
		{
			$response=get_languageword('Your message not sent due to some technical issue Please send message after some time Thankyou');
			$this->serviceResponse(array(),$response,$status);
		}
	}
	
	//TUTOR VIEW LEAD FIRST TIME
	function tutor_view_lead_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		
		$student_id 	= $request->student_id;
		$student_slug 	= $request->student_slug;
		
		$lead_id 		= $request->lead_id;
		
		$tutor_id 	= $request->tutor_id;
		$result = $this->home_model->view_lead_app($student_slug,$lead_id,$tutor_id);
		if($result->status==true)
		{
			$data = $this->base_model->fetch_records_from('users',array('id'=>$tutor_id));
			$response=$result->message;
			$status=1;
		}
		else
		{
			$response=$result->message;
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//GET STUDENT CREDIT TRANSACTION History
	function get_stcredit_trans_history_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		
		$student_id = $request->user_id;
		
		$data = $this->base_model->fetch_records_from('user_credit_transactions',array('user_id'=>$student_id));
		
		$this->serviceResponse($data,'Credit Transaction History',1);
	}
	
	//GET STUDENT COURSE PURCHASES
	function get_stcourse_purchases_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		
		$student_id = $request->user_id;
		
		$data = $this->base_model->get_query_result("SELECT cp.purchase_id,cp.max_downloads,cp.total_downloads,cp.paid_date,c.course_title,u.username as tutor_name FROM ".$this->db->dbprefix('course_purchases')."  cp INNER JOIN ".$this->db->dbprefix('tutor_selling_courses')." c ON cp.sc_id=c.sc_id AND cp.tutor_id=c.tutor_id INNER JOIN ".$this->db->dbprefix('users')." u ON cp.tutor_id=u.id WHERE cp.user_id=".$student_id." ");
		
		$this->serviceResponse($data,'Course Purchases',1);
	}
	
	//GET STUDENT LEADS
	function get_student_leads_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$from_limit = $request->from_limit;
		// $from_limit = 0;
		// 
		$to_limit = 10;
		$data=array();
		
		$student_id = $request->user_id;
		// $student_id = '161';//$request->user_id;
		
		$data = $this->base_model->get_query_result("SELECT sl.*,tt.teaching_type,l.location_name,c.name as course_name FROM ".TBL_PREFIX."student_leads sl INNER JOIN ".TBL_PREFIX."teaching_types tt ON sl.teaching_type_id=tt.id INNER JOIN ".TBL_PREFIX."locations l ON sl.location_id=l.id INNER JOIN ".TBL_PREFIX."categories c ON sl.course_id=c.id WHERE sl.user_id=".$student_id." ORDER BY sl.id DESC LIMIT ".$from_limit.",".$to_limit." ");
		
		$this->serviceResponse($data,'Student Leads',1);
	}
	
	//Student update his lead status
	function update_student_lead_status_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$student_id 	= $request->user_id;
		$lead_id 		= $request->lead_id;
		$lead_status 	= $request->lead_status;
		
		$update_data  = array();
		$update_data['status']  	= $lead_status;
		$update_data['updated_at'] = date('Y-m-d H:i:s'); 
		
		if($this->base_model->update_operation($update_data,'student_leads',array('id'=>$lead_id)))
		{
			$response='Lead status updated';
			$status=1;
		}
		else
		{
			$response='Lead status not updated';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//GET DATA FOR STUDENT TO POST LEAD
	function get_post_lead_data_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		
		
		//location options
		$locations = $this->home_model->get_locations($params = array('child' => 1));
		
		//Course Options
		$courses = $this->home_model->get_courses();
		
		//Teaching type Options
		$teaching_types = $this->base_model->fetch_records_from('teaching_types');
		
		$data['locations'] 		= $locations;
		$data['courses'] 		= $courses;
		$data['teaching_types'] = $teaching_types;
		
		$this->serviceResponse($data,'required data for post Lead',0);
	}
	
	function post_lead_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$lead_data=$request->lead_data;
		
		$input_data=array();
		$input_data['user_id'] 		= $request->user_id;
		
		$input_data['location_id']  = $lead_data->location_id;
		$input_data['course_id']    = $lead_data->course_id;
		$input_data['teaching_type_id']		= $lead_data->teaching_type_id;
		$input_data['priority_of_requirement']= $lead_data->priority_of_requirement;
		$input_data['present_status']		= $lead_data->present_status;
		$input_data['duration_needed']		= $lead_data->duration_needed;
		$input_data['budget']				= $lead_data->budget;
		if($lead_data->budget != '')
		{
			$input_data['budget_type']		= $lead_data->budget_type;
		}
		$input_data['title_of_requirement']	= $lead_data->title_of_requirement;
		$input_data['requirement_details']	= $lead_data->requirement_details;
		$input_data['created_at']=date("Y-m-d H:i:s");
		$input_data['updated_at']=$input_data['created_at'];
		
		
		$is_duplicate = $this->base_model->fetch_records_from('student_leads', array('location_id' => $input_data['location_id'], 'course_id' => $input_data['course_id'], 'teaching_type_id' => $input_data['teaching_type_id'], 'budget' => $input_data['budget'], 'budget_type' => $input_data['budget_type'], 'priority_of_requirement' => $input_data['priority_of_requirement'], 'status' => 'Opened','user_id'=>$input_data['user_id']));
		
		if(count($is_duplicate) > 0) 
		{
			$response='You have already posted this lead';
			$this->serviceResponse($data,$response,0);
		}
		
		if($this->base_model->insert_operation($input_data, 'student_leads'))
		{
			$response='Lead posted successfully';
			$status=1;
		}
		else
		{
			$response='Lead not posted';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	function change_password_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$password_data = $request->data;
		
		$current_password 	  = $password_data->current_password;
		$new_password 		  = $password_data->new_password;
		$new_confirm_password = $password_data->new_confirm_password;
		
		$identity = $request->email;
		$change = $this->ion_auth->change_password($identity, $current_password, $new_password);
		
		if($change)
		{
			$response='Password changed successfully';
			$status=1;
		}
		else
		{
			$response = strip_tags($this->ion_auth->errors());
			$status=0;
		}	
		$this->serviceResponse($data,$response,$status);
	}
	
	//BOOKINGS-QUESTIONNING 
	//GET QUESTIONS
	function get_booking_questions_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		// $request = (object) $this->post();
		$data=array();
		$response='';
		$status=0;
		
		$booking_id = $request->booking_id;
		
		$student_id = $request->student_id;
		$student_record = getUserRec($student_id);
		$img = URL_UPLOADS_PROFILES_STUDENT_MALE_DEFAULT_IMAGE;
		
		if($student_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$student_record->photo))
		{
			$img = base_url()."assets/uploads/profiles/thumbs/".$student_record->photo;
		}
		
		$records_from = $request->records_from;
		$limit = $request->limit;
		
		$query = "SELECT q.*,(SELECT COUNT(*) FROM ".TBL_PREFIX."booking_answers a WHERE q.question_id=a.question_id ) as chat_count FROM ".TBL_PREFIX."booking_questions q WHERE q.booking_id=".$booking_id." ORDER BY q.question_id DESC LIMIT ".$records_from.",".$limit." ";
		
		$data = $this->base_model->get_query_result($query);
		
		if(!empty($data))
		{
			
			//for student service start
			if(isset($request->tutor_id))
			{
				$tutor_id = $request->tutor_id;
				
				$tutor_record = getUserRec($tutor_id);
				$tutor_img = URL_UPLOADS_PROFILES_TUTOR_MALE_DEFAULT_IMAGE;
				
				if($tutor_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$tutor_record->photo))
				{
					$tutor_img = base_url()."assets/uploads/profiles/thumbs/".$tutor_record->photo;
				}
			}
			else
			{
				$tutor_img='';
			}
			
			//for student service end
		
			foreach($data as $r):
				$r->imageFullUrl = $img;
				$r->tutorImageFullUrl = $tutor_img;
			endforeach;
			
			$response='Questions found';
			$status=1;
		}
		else
		{
			$response='No questions found';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	function get_booking_questions1_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		// $request = (object) $this->post();
		$data=array();
		$response='';
		$status=0;
		
		$booking_id = $request->lead_id;
		
		$student_id = $request->student_id;
		$student_record = getUserRec($student_id);
		$img = URL_UPLOADS_PROFILES_STUDENT_MALE_DEFAULT_IMAGE;
		
		if($student_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$student_record->photo))
		{
			$img = base_url()."assets/uploads/profiles/thumbs/".$student_record->photo;
		}
		
		$records_from = $request->records_from;
		$limit = $request->limit;
		
		$query = "SELECT q.*,(SELECT COUNT(*) FROM ".TBL_PREFIX."instant_answers a WHERE q.question_id=a.question_id ) as chat_count FROM ".TBL_PREFIX."instant_questions q WHERE q.lead_id=".$booking_id." ORDER BY q.question_id DESC LIMIT ".$records_from.",".$limit." ";
		
		$data = $this->base_model->get_query_result($query);
		
		if(!empty($data))
		{
			
			//for student service start
			if(isset($request->tutor_id))
			{
				$tutor_id = $request->tutor_id;
				
				$tutor_record = getUserRec($tutor_id);
				$tutor_img = URL_UPLOADS_PROFILES_TUTOR_MALE_DEFAULT_IMAGE;
				
				if($tutor_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$tutor_record->photo))
				{
					$tutor_img = base_url()."assets/uploads/profiles/thumbs/".$tutor_record->photo;
				}
			}
			else
			{
				$tutor_img='';
			}
			
			//for student service end
		
			foreach($data as $r):
				$r->imageFullUrl = $img;
				$r->tutorImageFullUrl = $tutor_img;
			endforeach;
			
			$response='Questions found';
			$status=1;
		}
		else
		{
			$response='No questions found';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//BOOKINGS-QUESTIONNING 
	//GET QUESTION - CONVERSATION
	function get_question_conversation_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		// $request = (object) $this->post();
		$data=array();
		$response='';
		$status=0;
		
		$question_id = $request->question_id;
		$type = 0;
		if(isset($request->type))
			$type = $request->type;
		//$student_id  = $request->student_id;
		//$tutor_id 	 = $request->tutor_id;
		
		$from_limit = $request->from_limit;
		$to_limit   = $request->to_limit;
		
		$query = "";
		if($type == 0)
			$query = "SELECT a.*,f.photo as from_image,t.photo as to_image FROM ".TBL_PREFIX."booking_answers a INNER JOIN ".TBL_PREFIX."users f ON a.from_id=f.id INNER JOIN ".TBL_PREFIX."users t ON a.to_id=t.id WHERE a.question_id=".$question_id." ORDER BY a.answer_id DESC LIMIT ".$from_limit.",".$to_limit." ";
		else
			$query = "SELECT a.*,f.photo as from_image,t.photo as to_image FROM ".TBL_PREFIX."instant_answers a INNER JOIN ".TBL_PREFIX."users f ON a.from_id=f.id INNER JOIN ".TBL_PREFIX."users t ON a.to_id=t.id WHERE a.question_id=".$question_id." ORDER BY a.answer_id DESC LIMIT ".$from_limit.",".$to_limit." ";
		
		$data = $this->base_model->get_query_result($query);
		
		if(!empty($data))
		{
			$response='Conversations found';
			$status=1;
		}
		else
		{
			$response='No Conversations found';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//BOOKINGS-QUESTIONNING 
	//REPLY TO QUESTION
	function answer_to_question_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		// $request = (object) $this->post();
		$data=array();
		$response='';
		$status=0;
		
		$temp = array();
		$temp['question_id'] = $request->question_id;
		$temp['from_id'] 	 = $request->sender_id;
		$temp['to_id'] 		 = $request->receiver_id;
		$temp['message'] 	 = $request->message;
		if($request->type=='tutor'){
			$temp['student_status'] 	= '0';
			$temp['tutor_status'] 	 	= '1';
		}else{
			$temp['tutor_status'] 	 	= '0';
			$temp['student_status'] 	= '1';
		}
		
		$temp['datetime'] 	 = date('Y-m-d H:i:s');
		$device_id = $request->device_id;
		$chat_type = 0;
		if(isset($request->chat_type))
			$chat_type = $request->chat_type;
		if($chat_type == 0)
			$answer_id = $this->base_model->insert_operation_id($temp,'booking_answers');
		else
			$answer_id = $this->base_model->insert_operation_id($temp,'instant_answers');
		
		if($answer_id)
		{
			$img='';
			$user = getUserRec($request->sender_id);
			if(!empty($user))
			{
				if($user->user_belongs_group==2) 
				{
					$img = URL_UPLOADS_PROFILES_STUDENT_MALE_DEFAULT_IMAGE;
				}
				else
				{
					$img = URL_UPLOADS_PROFILES_TUTOR_MALE_DEFAULT_IMAGE;
				}
				
				if($user->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$user->photo))
				{
					$img = base_url()."assets/uploads/profiles/thumbs/".$user->photo;
				}
			}
			if($chat_type == 0)
				$query = "SELECT a.* FROM ".TBL_PREFIX."booking_answers a WHERE a.answer_id=".$answer_id." ";
			else
				$query = "SELECT a.* FROM ".TBL_PREFIX."instant_answers a WHERE a.answer_id=".$answer_id." ";
		
			$data = $this->base_model->get_query_result($query);
			
			if(!empty($data))
			{
				$data[0]->img = $img;

			}
			if($device_id!=''){
				$this->sendPushNotification($device_id,'Answer',$request->message);
			}
			
			$response='answer posted successfully';
			$status=1;
		}
		else
		{
			$response='Answer not posted';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}

	// UPDATE STATUS IN ANSWERS WHEN STUDENT VIEWS THE ANSWER FROM THE TUTOR
	function update_answer_status_post(){
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		$question_id = $request->question_id;
		$chat_type = 0;
		if(isset($request->chat_type))
			$chat_type = $request->chat_type;
		$type = $request->type;
		if($type=='student'){
			$from_id = $request->tutor_id;
			$to_id   = $request->student_id;
			$update_data['student_status'] = '1';
		}else{
			$from_id = $request->student_id;
			$to_id   = $request->tutor_id;
			$update_data['tutor_status'] = '1';
		}
		
		$where = array('question_id'=>$question_id,'from_id'=>$from_id,'to_id'=>$to_id);
		
		if($chat_type == 0 && $this->base_model->update_operation($update_data,'booking_answers',$where)){
			$response = 'viewed status changed successfully';
			$status  = 1;
		}else if($chat_type == 1 && $this->base_model->update_operation($update_data,'instant_answers',$where)){
			$response = 'viewed status changed successfully';
			$status  = 1;
		}
		else{
			$response = 'viewed status not changed successfully';
			$status  = 0;
		}  

		$this->serviceResponse(array(),$response,$status);
	}
	
	//BOOKINGS-QUESTIONNING 
	//POST QUESTION
	function add_question_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		// $request = (object) $this->post();
		$data=array();
		$response='';
		$status=0;
		$type = 0;
		if(isset($request->type))
			$type = $request->type;
		$temp = array();
		if($type == 0)
			$temp['booking_id'] 	= $request->booking_id;
		else
			$temp['lead_id'] 	= $request->booking_id;
		$temp['question_text']	= $request->question_text;
		$temp['datetime']		= date('Y-m-d H:i:s');
		
		if($type == 0)
			$question_id = $this->base_model->insert_operation_id($temp,'booking_questions');
		else
			$question_id = $this->base_model->insert_operation_id($temp,'instant_questions');
		
		if($question_id)
		{
			
			$student_id = $request->student_id;
			$student_record = getUserRec($student_id);
			$img = URL_UPLOADS_PROFILES_STUDENT_MALE_DEFAULT_IMAGE;
			
			if($student_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$student_record->photo))
			{
				$img = base_url()."assets/uploads/profiles/thumbs/".$student_record->photo;
			}
			if($type == 0)
				$query = "SELECT q.*,(SELECT COUNT(*) FROM ".TBL_PREFIX."booking_answers a WHERE q.question_id=a.question_id ) as chat_count FROM ".TBL_PREFIX."booking_questions q WHERE q.question_id=".$question_id." ";
			else
				$query = "SELECT q.*,(SELECT COUNT(*) FROM ".TBL_PREFIX."instant_answers a WHERE q.question_id=a.question_id ) as chat_count FROM ".TBL_PREFIX."instant_questions q WHERE q.question_id=".$question_id." ";
		
			$data = $this->base_model->get_query_result($query);
			
			if(!empty($data))
			{
				
				//for student service start
			if(isset($request->tutor_id))
			{
				$tutor_id = $request->tutor_id;

				$device_id = '';
				
				$tutor_record = getUserRec($tutor_id);
				$tutor_img = URL_UPLOADS_PROFILES_TUTOR_MALE_DEFAULT_IMAGE;
				$device_id = $tutor_record->device_id;
				if($tutor_record->photo != '' && file_exists('assets/uploads/profiles/thumbs/'.$tutor_record->photo))
				{

					$tutor_img = base_url()."assets/uploads/profiles/thumbs/".$tutor_record->photo;
				}
			}
			else
			{
				$tutor_img='';
			}
			
			//for student service end
			
				foreach($data as $d):
					$d->imageFullUrl = $img;
					$d->tutorImageFullUrl = $tutor_img;
				endforeach;
			}
			if($device_id!=''){
				$this->sendPushNotification($device_id,'Question',$request->question_text);	
			}
			
		
			$response='answer posted successfully';
			$status=1;
		}
		else
		{
			$response='Answer not posted';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	
	//from peter service
	function credit_conversion_request_post()
	{
		$postdata 	= file_get_contents("php://input");
		$request 	= json_decode($postdata);
		$user_id    = $request->user_id;
		// $user_id 	= $this->post('user_id');
		$admin_money_transactions  = $this->base_model->fetch_records_from('admin_money_transactions',array('user_id'=>$user_id));
		$this->serviceResponse($admin_money_transactions,'Convesrion Request',1);

	}
	
	// BOOK TUTOR

	function book_tutor_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$student_id 		= $request->student_id;
		$tutor_id   		= $request->tutor_id;
		$tutor_slug   		= $request->tutor_slug;
		$tution 			= $request->tution;
		$course_id			= $request->course_id;



		//Check Whether student is premium user or not
		if(!is_premium($student_id)) {

			$response = get_languageword('please_become_premium_member_to_book_tutor');
			$this->serviceResponse(array(),$response,0);

		}

		 $course_details = $this->home_model->get_tutor_course_details1($course_id, $tutor_id);

		 // $this->serviceResponse($course_details,$student_id,1);
		
		//Check whether Tutor teaches the course or not
		if(empty($course_details)) {

			$response = get_languageword('no_course_details_found');
			$this->serviceResponse(array(),$response,0);

		}
		$fee = $course_details->fee;


		//Check If student has sufficient credits to book tutor
		if(!is_eligible_to_make_booking($student_id, $fee)) {

			$response = get_languageword("you_do_not_have_enough_credits_to_book_the_tutor_Please_get_required_credits_here");
			$this->serviceResponse(array(),$response,0);
		}

		// $this->serviceResponse($course_details,$student_id,1);
		$temp = explode("-",$request->start_date);
		$start_date  			= $temp[2]."-".$temp[0]."-".$temp[1];
		$time_slot   			= $request->time_slot;
		
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$student_details = $this->base_model->get_user_info($student_id);
		if(!empty($student_details->time_zone))
			$time_zone = $time_zone_list[$student_details->time_zone];
		else
			$time_zone = "UTC";
		
		$t = strtotime($start_date);
		$time_slot = explode("-",$time_slot);
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
		
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->sub($myInterval);
		$myDateTime_end->sub($myInterval);
		$start_date = $myDateTime_start->format("Y-m-d");
		$time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");

		//Check If student already booked the tutor on the same slot and it is not yet approved by tutor
		if($this->home_model->is_already_booked_the_tutor($student_id, $tutor_id, $course_id, $start_date, $time_slot)) {
			$response = get_languageword('you_already_booked_this_tutor_and_your_course_not_yet_completed');
			$this->serviceResponse(array(),$response,0);
		}

		//Check If selected time-slot is available
		if(empty($course_details->time_slots) || !$this->home_model->is_time_slot_avail($tutor_id, $course_id, $start_date, $time_slot)) {

			$response = get_languageword('time_slot_not_available');
			$this->serviceResponse(array(),$response,0);
		}
		$content 				= $course_details->content;
		$duration_value 		= $course_details->duration_value;
		$duration_type 			= $course_details->duration_type;
		$per_credit_value 		= $course_details->per_credit_value;
		$days_off 				= $course_details->days_off;
		$preferred_location 	= $request->teaching_type;
		$message   				= $request->message;
		if($duration_type == "hours") {

			$formatted  = str_replace(':', '.', $time_slot);
			$time 	    = explode('-', str_replace(' ', '', $formatted));

			$start_time = number_format($time[0],2);
			$end_time   = number_format($time[1],2);
			if($end_time < $start_time)
				$end_time = (int)$end_time + 24;

			$total_time = $end_time - $start_time;

			if($total_time >= 1) {

				$days = round($duration_value / $total_time);

			} else {

				$total_time = (int)(explode('.', number_format($total_time,2))[1]);
				$days = round($duration_value / ($total_time/60));
			}

			$count_day = 0;
			$days_off_list = array();
			$end_date = $start_date;
			if(!empty($days_off)){
				$days_off_list = explode(",",$days_off);
				while($count_day < $days){
					$temp_time = strtotime($end_date.'+1 days');
					$week_day = strtoupper(date("D",$temp_time));
					if(!in_array($week_day,$days_off_list)){
						$count_day++;	
					}
					$end_date = date("Y-m-d", strtotime($end_date.'+1 days'));
				}	
			}
			else{
				$end_date = date("Y-m-d", strtotime($start_date.'+'.$days.' days'));
			}

		} else {

			$end_date = date("Y-m-d", strtotime($start_date.'+'.$duration_value.' '.$duration_type));
		}

		$end_date = date("Y-m-d", strtotime($end_date.'-1 days'));

		$admin_commission   	= get_system_settings('admin_commission_for_a_booking');
		$admin_commission_val   = round($fee * ($admin_commission / 100));

		$created_at   		= date('Y-m-d H:i:s');
		$updated_at   		= $created_at;
		$updated_by   		= $student_id;


		$inputdata	=	array(
								'student_id'			=> $student_id,
								'tutor_id'				=> $tutor_id,
								'course_id'				=> $course_id,
								'content'				=> $content,
								'duration_value'		=> $duration_value,
								'duration_type'			=> $duration_type,
								'fee'					=> $fee,
								'per_credit_value'		=> $per_credit_value,
								'start_date'			=> $start_date,
								'end_date'				=> $end_date,
								'time_slot'				=> $time_slot,
								'days_off'				=> $days_off,
								'preferred_location'	=> $preferred_location,
								'message'				=> $message,
								'admin_commission'		=> $admin_commission,
								'admin_commission_val'	=> $admin_commission_val,
								'created_at'			=> $created_at,
								'updated_at'			=> $updated_at,
								'updated_by'			=> $updated_by,
								'tution'				=> $tution
							);

		$ref = $this->base_model->insert_operation($inputdata, 'bookings');

		if($ref > 0) {

			//Log Credits transaction data & update user net credits - Start
			$log_data = array(
							'user_id' => $student_id,
							'credits' => $fee,
							'per_credit_value' => $per_credit_value,
							'action'  => 'debited',
							'purpose' => 'Slot booked with the Tutor "'.$tutor_slug.'" and Booking Id is '.$ref,
							'date_of_action	' => date('Y-m-d H:i:s'),
							'reference_table' => 'bookings',
							'reference_id' => $ref,
						);

			log_user_credits_transaction($log_data);

			update_user_credits($student_id, $fee, 'debit');
			//Log Credits transaction data & update user net credits - End


			//Email Alert to Tutor - Start
				//Get Tutor Booking Success Email Template
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '5'));

				if(!empty($email_tpl)) {

					$email_tpl = $email_tpl[0];

					$student_rec = getUserRec($student_id);
					$tutor_rec 	 = getUserRec($tutor_id);


					if(!empty($email_tpl->from_email)) {

						$from = $email_tpl->from_email;

					} else {

						$from 	= get_system_settings('Portal_Email');
					}

					$to 	= $tutor_rec->email;

					if(!empty($email_tpl->template_subject)) {

						$sub = $email_tpl->template_subject;

					} else {

						$sub = get_languageword("Booking Request From Student");
					}

					if(!empty($email_tpl->template_content)) {

						$original_vars  = array($tutor_rec->username, $student_rec->username, $course_slug, $start_date." & ".$time_slot, $preferred_location, '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>');
						$temp_vars		= array('___TUTOR_NAME___', '___STUDENT_NAME___', '___COURSE_NAME___', '___DATE_TIME___', '___LOCATION___', '___LOGINLINK___');
						$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

					} else {

						$msg = get_languageword('please')." <a href='".URL_AUTH_LOGIN."'> ".get_languageword('Login Here')."</a> ".get_languageword('to view the booking details');
						$msg .= "<p>".get_languageword('Thank you')."</p>";
					}

					sendEmail($from, $to, $sub, $msg);
				}
			//Email Alert to Tutor - End

			$response = get_languageword('your_slot_with_the_tutor_booked_successfully_Once_tutor_approved_your_booking and_initiated_the_session_you_can_start_the_course_on_the_booked_date');
			$this->serviceResponse(array(),$response,1);

		} else {

			$response = get_languageword('your_slot_with_the_tutor_not_booked_you_can_send_message_to_the_tutor');
			$this->serviceResponse(array(),$response,1);

		}

	}
	
	// REVIEWS OF TUTOR

	function reviews_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$tutor_id  = $request->tutor_id;
		// $tutor_id  = $this->post('tutor_id');

		$reviews = $this->base_model->get_query_result("SELECT u.username,u.photo,c.name,r.* FROM ".TBL_PREFIX."tutor_reviews r,".TBL_PREFIX."users u,".TBL_PREFIX."categories c,".TBL_PREFIX."bookings b WHERE r.tutor_id=".$tutor_id." and r.status='Approved' and r.student_id=u.id and r.booking_id=b.booking_id and b.course_id=c.id");
		$this->serviceResponse($reviews,'Reviews',1);
	}

	// TUTOR EXPERIENCE 

	function tutor_experience_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$user_id = $request->user_id;

		$experiences = $this->base_model->fetch_records_from('users_experience',array('user_id'=>$user_id));
		$this->serviceResponse($experiences,'Tutor Experience',1);  
	}
	
	// ADD OR EDIT EXPERIENCE

	function add_experience_post()
	{
		$postdata 		= file_get_contents("php://input");
		$request  		= json_decode($postdata);
		$user_id 		= $request->user_id;
		$record_id 		= $request->record_id;
		$company 		= $request->company;
		$role 			= $request->role;
		$description 	= $request->description;
		$from_date 		= $request->from_date;
		$to_date 		= $request->to_date;
		
		$inputdata['user_id'] 		= $user_id;
		$inputdata['company'] 		= $company;
		$inputdata['role'] 			= $role;
		$inputdata['description'] 	= $description;
		$inputdata['from_date'] 	= $from_date;
		$inputdata['to_date'] 		= $to_date;
		$inputdata['updated_at'] 	= date('Y-m-d h:i:s');
		
		if($record_id!=''){
			
			$where['record_id'] =   $record_id;

			if($this->base_model->update_operation($inputdata, 'users_experience',$where)){
					$response = get_languageword('user experience updated successfully');
					$status = 1;
			}else{
				$response = get_languageword('user experience not updated');
					$status = 0;
			}
		}else{

			$inputdata['created_at'] 	= date('Y-m-d h:i:s');
			if($this->base_model->insert_operation($inputdata, 'users_experience')){
				$response = get_languageword('user experience added successfully');
					$status = 1;
			}else{
				$response = get_languageword('user experience not added');
					$status = 0;
			}
		}

		$this->serviceResponse(array(),$response,$status);

	}
	
	
	function get_certificates_tutors_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$user_id = $request->user_id;
		$certificates = $this->base_model->fetch_records_from('certificates', array('certificate_for' => 'tutors', 'status' => 'Active'));
		$user_uploads = $this->base_model->fetch_records_from('users_certificates', array('user_id' => $user_id));
		$user_uploads_arr = array();
		if(!empty($user_uploads))
		{  
			// foreach($certificates as $index=>$value){
				foreach($user_uploads as $up)
				{
				
				// if($up->admin_certificate_id==$value->certificate_id){
					$user_uploads_arr[$up->admin_certificate_id] = base_url().'assets/uploads/certificates/'.$up->certificate_name;
					//$user_uploads_arr[$up->admin_certificate_id] = $up->certificate_name;
				// }
			// }
		  }
		}
		
		 $data = array(
			'certificates'=>$certificates,
			'user_uploads_arr'=>$user_uploads_arr
		); 
		
		$this->serviceResponse($data,'Certificate',1);
	}
	
	
	function certificates_upload_post()
	{
		
		if(!empty($_FILES['userfile']['name']))
		{
		
			$tmpFilePath = $_FILES['userfile']['tmp_name'];
			$ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
			$user_id = $this->post('user_id');
			$i = $this->post('certificate_id');
			if($i==0){
				$new_name = $user_id.'_'.str_replace(' ','_',rand().'_'.$_FILES['userfile']['name']);
				$user_image['certificate_type']	= 'other';
			}else{
				$new_name = $user_id.'_'.$i.'.'.$ext;
				$user_image['certificate_type']		= 'admin';
			}
			
			
			
			
			$filePath = './assets/uploads/certificates/'.$new_name;
			if(move_uploaded_file($tmpFilePath, $filePath))
			{
				/* if(in_array(strtolower($ext), array('jpg', 'png', 'gif', 'jpeg')))
				{
					$this->create_thumbnail($filePath,'./assets/uploads/certificates/thumbs/','40','40');
				} */
				
				$user_image['admin_certificate_id'] = $i;
				$user_image['user_id']				= $user_id;
				if(isset($this->config->item('site_settings')->need_admin_for_tutor) && $this->config->item('site_settings')->need_admin_for_tutor == 'yes')
				$user_image['admin_status']			= 'Pending';
				else
				$user_image['admin_status']			= 'Approved';
				
				$user_image['certificate_name']		= $new_name;
				$user_image['file_type']		= $ext;
				
				$existed = $this->base_model->fetch_records_from('users_certificates',
											array('admin_certificate_id'=>$i,
											'user_id'=>$user_id,'certificate_type'=>'admin'));
				if(count($existed)>0)
				{
					$whr['user_certificate_id']			= $existed[0]->user_certificate_id;
					$this->base_model->update_operation($user_image,'users_certificates',$whr);
				}
				else 
				{
				$this->base_model->insert_operation($user_image,'users_certificates');	
				}
				$data = array('path'=>base_url().'/assets/uploads/certificates/'.$new_name);
				$response = 'uploaded successfully';
				$status = 1;
			}else{
				$data = array('path'=>'');
				$response = 'Unable to upload';
				$status = 0;
			}
		}
				
			
			$this->serviceResponse($data,$response,$status);
	}
	
	// TEST FILE UPLOAD
	
	function test_file_upload_post()
	{
			$response = 'Saved '.$_FILES['userfile']['name'];
			$status = 1;
			$image 	= $_FILES['userfile']['name'];
			if(!empty($_FILES['userfile']['name'])){
						
					$ext = pathinfo($image, PATHINFO_EXTENSION);			
					//$file_name = rand() .'.'. $ext;
					$file_name = 'peterJohn' .'.'. $ext;
					$config['upload_path'] 		= 'assets/uploads/profiles/';//URL_PUBLIC_UPLOADS_PROFILES;
					$config['allowed_types'] 	= '*';
					$config['overwrite'] 		= true;
					$config['file_name']        = $file_name; 
					$config['max_size']             = '1024000';
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					if ($this->upload->do_upload()) {
						
						//$update_data['photo'] = $file_name;
						//$where['id'] 		= $id;
						//$this->base_model->update_operation($update_data,'users', $where);
						
						$status = 1;
						$response = 'User Details with image is uploaded';	
					}
					else {
						
						$status = 0;
						$response = strip_tags($this->upload->display_errors());	
					}
			}
			
			$this->serviceResponse(array('path'=>URL_PUBLIC_UPLOADS_PROFILES),$response,$status);
	}
	
	// send push notifications

	function sendPushNotification($device_id,$title,$message)
	{
		if($device_id=='' || $title=='' || $message==''){
			return false;
		}

		// SEND PUSH NOTIFICATION IF IT IS ENABLE
		if($this->config->item('site_settings')->one_signal_push_notifications=='yes'){
						
			$message = array(
						  "en" => $message,
						  "title" => $title,
						  "icon" => "myicon",
						  "sound"=>"default"
						);
						
			$data = array(
						"body" => $message,
						"title" => $title,
					);
					
			$this->load->library('OneSignalPush');	
			$gcpm = new OneSignalPush();
			$gcpm->setDevices($device_id);
			$res = $gcpm->send($message,$data);
			return $res;
		}else{
			return false;
		}
	}

	function test_course_post()
	{
		//$postdata = file_get_contents("php://input");
    	//$request = json_decode($postdata);

		// $category_id = $request->category_id;
		$category_id = $this->post('category_id');
		$courses = $this->base_model->fetch_records_from('categories',array('categories'=>$category_id));

		$this->serviceResponse($courses,'Courses',1);
	}
	
	
	// Common Response Method 
	 function serviceResponse($data=array(),$response,$status=0)
	 {
	 		$data = array('data'=>$data);
			$response = array('message'=>$response,'status'=>$status);
			$result = array();
			array_push($result,$data);
			array_push($result,array('response'=>$response));
			$this->response(json_decode(json_encode ($result), true), 200);	
	 }	
	 
	 //GET STUDENT LEADS
	function get_student_instant_questions_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$from_limit = $request->from_limit;
		// $from_limit = 0;
		// 
		$to_limit = 10;
		$data=array();
		
		$student_id = $request->user_id;
		
		$data = $this->base_model->get_query_result("SELECT sl.*,c.name as course_name, d.username as tutor_name, d.photo as tutor_photo FROM ".TBL_PREFIX."instant_bids sl INNER JOIN ".TBL_PREFIX."categories c ON sl.category=c.id INNER JOIN pre_users d on sl.tutor_id = d.id WHERE sl.student_id=".$student_id." and sl.`status` != 'complete' ORDER BY sl.id DESC LIMIT ".$from_limit.",".$to_limit." ");
		foreach($data as $key => $value){
			if(!empty($value->attach)){
				$temp = explode(",",$value->attach);
				foreach($temp as $k => $v){
					if(!empty($v)){
						$result = $this->base_model->get_iq_attach_list($v);
						$data[$key]->attach_list[$k] = $result;
					}
				}
			} else{
				$data[$key]->attach_list = '';
			}
		}
		$this->serviceResponse($data,'Student Instant Questions',1);
	}
	
	//Update IQ status
	function update_student_iq_status_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$student_id 	= $request->user_id;
		$lead_id 		= $request->lead_id;
		$lead_status 	= $request->lead_status;
		
		$update_data  = array();
		$update_data['status']  	= $lead_status;
		$update_data['updated_at'] = date('Y-m-d H:i:s'); 
		$update_data['updated_by'] = $student_id; 
		
		if($this->base_model->update_operation($update_data,'instant_bids',array('id'=>$lead_id)))
		{
			$response='My Instant Question status updated';
			$booking_info = (array)$this->student_model->get_booking_detail($lead_id);
			
			if($lead_status == 'approved'){
				$data['student_id'] = $booking_info['student_id'];
				$data['tutor_id'] = $booking_info['tutor_id'];
				$data['course_id'] = $this->student_model->get_course_iq_id($booking_info['category']);
				$data['content'] = $booking_info['title_of_requirement'];
				$data['duration_value'] = 1;
				$data['duration_type'] = 'hours';
				$data['fee'] = $booking_info['budget'];
				$data['per_credit_value'] = get_system_settings('per_credit_value');
				$data['start_date'] = $booking_info['iq_date'];
				$data['end_date'] = $booking_info['iq_date'];
				$data['iq_id'] = $booking_info['id'];
				if($booking_info['teaching_type_id'] == 'Online ZOOM'){
					$data['time_slot'] = $booking_info['iq_time_slot'];
					$data['preferred_location'] = 'online-zoom';
				}
				if($booking_info['teaching_type_id'] == 'Record')
					$data['preferred_location'] = 'record';
				$data['prev_status'] = $data['status'] = 'pending';
				$data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $student_id;
				$data['admin_commission'] = get_system_settings('admin_commission_for_a_booking');
				$data['admin_commission_val']   = round($data['fee'] * ($data['admin_commission'] / 100));
				$data['tution'] = 'One on One';
				$this->student_model->insert_iq_to_booking($data);	
				
				$device_ids = array();
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '28'));
				$email_tpl = $email_tpl[0];
				$sub = $msg = "";
				if(!empty($email_tpl)){
					$sub = $email_tpl->template_subject;
					$msg = $email_tpl->template_content;
				}
				$tutor_info = (array)$this->base_model->get_tutor_info($booking_info['tutor_id']);
				if(!empty($tutor_info['device_id'])){
					array_push($device_ids,$tutor_info['device_id']);
				}
				if(!empty($tutor_info['email'])){
					if(!empty($msg)){
						$student_name = $this->base_model->get_student_name($booking_info['student_id']);
						$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
						$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$tutor_info['username'], $student_name, $booking_info['iq_time_slot'], $booking_info['teaching_type_id'], $login_link,$this->config->item('site_title', 'ion_auth'));
						$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR__NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','IQ_TITLE');
						$content = str_replace($temp_vars, $original_vars, $msg);
						$email_settings = (array)$this->config->item('email_settings');
						sendEmail($email_settings['webmail']['User Name'], $tutor_info['email'], $sub, $content);
					}
				}
				if(count($device_ids) > 0){
					$this->load->library('OneSignalPush');
					$message = array(
					  "en" => "Student Accept Bid",
					  "title" => "Student Accept Bid",
					  "icon" => "myicon",
					  "sound"=>"default"
					);
					$data = array(
						"body" => "Student Accept Bid",
						"title" => "Student Accept Bid",
					);
					$gcpm = new OneSignalPush();
					$gcpm->setDevices($device_ids);
					$res = $gcpm->send($message,$data);
				}
			}
			$status=1;
		}
		else
		{
			$response='My Instant Question status not updated';
			$status=0;
		}
		$this->serviceResponse($data,$response,$status);
	}
	
	//GET DATA FOR STUDENT TO POST IQ
	function get_post_iq_data_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		//Course Options
		$categories = get_categories();
		$courses = $categories;
		
		$data['courses'] 		= $courses;
		$this->serviceResponse($data,'required data for post Lead',0);
	}
	
	function post_iq_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		$attachExist = $request->attachExist;
		$lead_data=$request->lead_data;
		$files = $request->filenames;
		
		$file_name = "";
		if(!empty($files)){
			foreach($files as $key => $value){
				if($key != 0)
					$file_name .= ",";
				$file_name .= $value->id;
			}
		}
		
		
		$input_data=array();
		$input_data['student_id'] 		= $request->user_id;
		$input_data['teaching_type_id']		= $lead_data->teaching_type_id;
		$input_data['priority_of_requirement']= $lead_data->priority_of_requirement;
		$input_data['category']    = $lead_data->category;
		$input_data['title_of_requirement']	= $lead_data->title_of_requirement;
		$input_data['budget']				= $lead_data->budget;
		$input_data['budget_type']		= $lead_data->budget_type;
		$input_data['tutor_rating']		= $lead_data->tutor_rating;
		$input_data['requirement_details']	= $lead_data->requirement_details;
		if(!empty($file_name))
			$input_data['attach'] = $file_name;
		$input_data['priority_date_time']	= $lead_data->priority_date." ".$lead_data->priority_time;
		if(!empty($lead_data->iq_date)){
			$student_details = $this->base_model->get_user_info($request->user_id);
			$time_zone_list = $this->base_model->get_timezone_list();
			$time_zone_list = explode(",",$time_zone_list->field_type_values);
			$time_zone = $time_zone_list[$student_details->time_zone];
			$t = strtotime($lead_data->iq_date);
			$time_slot = explode("-",$lead_data->iq_time_slot);
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
			$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->sub($myInterval);
			$myDateTime_end->sub($myInterval);
			
			$input_data['iq_date']	= $myDateTime_start->format("Y-m-d");
			$input_data['iq_time_slot']	= $myDateTime_start->format("H")."-".$myDateTime_end->format("H");		
		}
		$input_data['created_at']=date("Y-m-d H:i:s");
		$input_data['updated_at']=$input_data['created_at'];
		$input_data['created_by'] = $input_data['updated_by'] = $request->user_id;
		$input_data['status'] = 'new';
		$student_name = $this->base_model->get_student_name($input_data['student_id']);
		$tutor_list = $this->base_model->get_tutor_list_from_category($input_data['category']);
		
		$device_ids = array();
		$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '26'));
		$email_tpl = $email_tpl[0];
		$sub = $msg = "";
		if(!empty($email_tpl)){
			$sub = $email_tpl->template_subject;
			$msg = $email_tpl->template_content;
		}
		
		foreach($tutor_list as $k => $v){
			$input_data['tutor_id'] = $v->id;
			$this->base_model->insert_operation_id($input_data,'instant_bids').",";
			if(!empty($v->device_id)){
				array_push($device_ids,$v->device_id);
			}
			if(!empty($v->email)){
				if(!empty($msg)){
					$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
					$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
					$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$v->username, $student_name, $input_data['iq_time_slot'], $input_data['teaching_type_id'], $login_link,$this->config->item('site_title', 'ion_auth'));
					$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR_NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','TITLE');
					$content = str_replace($temp_vars, $original_vars, $msg);
					$email_settings = (array)$this->config->item('email_settings');
					sendEmail($email_settings['webmail']['User Name'], $v->email, $sub, $content);
				}
			}
		}
		
		if(count($device_ids) > 0){
			$this->load->library('OneSignalPush');
			$message = array(
			  "en" => "New IQ bid is added",
			  "title" => "New Instant Question Bid",
			  "icon" => "myicon",
			  "sound"=>"default"
			);
			
			$data = array(
				"body" => "New IQ bid is added",
				"title" => "New Instant Question Bid",
			);
			$gcpm = new OneSignalPush();
			$gcpm->setDevices($device_ids);
			$res = $gcpm->send($message,$data);
		}
		$response='Instant Question posted successfully';
		$status=1;
		$data['success'] = "success";
		
		$this->serviceResponse($data,$response,$status);
	}
	
	/* SEARCH STUDENT IQ BIDS */
    function search_student_iq_bids_post()
	{
		$postdata = file_get_contents("php://input");

		$request = json_decode($postdata);
		$from_limit = $request->from_limit;
		$to_limit = 10;
		
		$data = array();
        $tutor_id = $request->user_id;
		
		$query = "SELECT sl.*,c.name as course_name, d.username as student_name, d.photo FROM ".TBL_PREFIX."instant_bids sl INNER JOIN ".TBL_PREFIX."categories c ON sl.category=c.id INNER JOIN pre_users d on sl.student_id = d.id WHERE sl.tutor_id=".$tutor_id." and sl.`status` != 'complete' ORDER BY sl.id DESC LIMIT ".$from_limit.",".$to_limit;
		$data = $this->base_model->get_query_result($query);
		
		$this->serviceResponse($data,'Leads',1);
	}
	
	function instant_bid_details_post()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$student_lead_id 	= $request->lead_id;
		$stduent_details 	= $this->base_model->instant_bid_details($student_lead_id);
		$stduent_details->lead_id = $student_lead_id;
		if(!empty($stduent_details->attach)){
			$temp = explode(",",$stduent_details->attach);
			foreach($temp as $k => $v){
				if(!empty($v)){
					$result = $this->base_model->get_iq_attach_list($v);
					$stduent_details->attach_list[$k] = $result;
				}
			}
		} else{
			$stduent_details->attach_list = '';
		}
		$tutor_details = $this->base_model->get_user_info($stduent_details->tutor_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$tutor_details->time_zone];
		
		$t = strtotime($stduent_details->iq_date);
		$time_slot = explode("-",$stduent_details->iq_time_slot);
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
		
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->add($myInterval);
		$myDateTime_end->add($myInterval);
		$stduent_details->iq_date = $myDateTime_start->format("Y-m-d");
		$stduent_details->iq_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		$this->serviceResponse($stduent_details,'Instant Bid details',1);
	}
	
	function post_iq_bid_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		
		$lead_data=$request->lead_data;
		
		$input_data=array();
		if(!empty($request->tutor_budget))
			$input_data['budget'] = $request->tutor_budget;
		$input_data['updated_at'] = date("Y-m-d H:i:s");
		$input_data['updated_by'] = $request->user_id;
		
		$tutor_details = $this->base_model->get_user_info($request->user_id);
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$time_zone = $time_zone_list[$tutor_details->time_zone];
		$t = strtotime($lead_data->iq_date);
		$time_slot = explode("-",$lead_data->iq_time_slot);
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
		
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->sub($myInterval);
		$myDateTime_end->sub($myInterval);
			
		$input_data['iq_time_slot'] = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		$input_data['iq_date'] = $myDateTime_start->format("Y-m-d");
		
		$input_data['status'] = 'pending';
		$this->base_model->update_operation($input_data, 'instant_bids', array('id' => $lead_data->lead_id));
		
		$lead_info = $this->base_model->get_instant_bid($lead_data->lead_id);
		
		
		$device_ids = array();
		$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '27'));
		$email_tpl = $email_tpl[0];
		$sub = $msg = "";
		if(!empty($email_tpl)){
			$sub = $email_tpl->template_subject;
			$msg = $email_tpl->template_content;
		}
		$student_info = (array)$this->base_model->get_tutor_info($lead_info->student_id);
		if(!empty($student_info['device_id'])){
			array_push($device_ids,$student_info['device_id']);
			$parent_detail = $this->home_model->get_parent_details($student_info['email']);
			if(!empty($parent_detail) && !empty($parent_detail->device_id))
				array_push($device_ids,$parent_detail->device_id);
		}
		if(!empty($student_info['email'])){
			if(!empty($msg)){
				$tutor_name = $this->base_model->get_tutor_name($lead_info->tutor_id);
				$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
				$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
				
				$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$tutor_name,$student_info['username'],  $lead_info->iq_time_slot, $lead_info->teaching_type_id, $login_link,$this->config->item('site_title', 'ion_auth'));
				
				$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR_NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','IQ_TITLE');
				$content = str_replace($temp_vars, $original_vars, $msg);
				$email_settings = (array)$this->config->item('email_settings');
				//sendEmail($email_settings['webmail']['User Name'], $student_info['email'], $sub, $content);
			}
		}
		if(count($device_ids) > 0){
			$this->load->library('OneSignalPush');
			$message = array(
			  "en" => "Tutor Submit Bid",
			  "title" => "Tutor Submit Bid",
			  "icon" => "myicon",
			  "sound"=>"default"
			);
			$data = array(
				"body" => "Tutor Submit Bid",
				"title" => "Tutor Submit Bid",
			);
			$gcpm = new OneSignalPush();
			$gcpm->setDevices($device_ids);
			$res = $gcpm->send($message,$data);
		}
		
		$response='Instant Bid updated successfully';
		$status=1;
		$this->serviceResponse($data,$response,$status);
	}
	
	function create_meeting($booking_detail, $type = 0){
		$user_id = $booking_detail['tutor_id'];
		if($booking_detail['tution'] == 'Share'){
			$book_session_exist = (array)$this->tutor_model->get_same_book_session_exist($booking_detail['time_slot'],$booking_detail['start_date'],$booking_detail['end_date'],$user_id,$booking_detail['tution'],$booking_detail['course_id'],$booking_detail['booking_id'],$booking_detail['preferred_location']);
			if(!empty($book_session_exist)){
				$cnt = 0;
				foreach($book_session_exist as $key => $value){
					$alreay_zoom_exist = (array)$this->tutor_model->get_zoom_session_exist($value->booking_id);
					if(!empty($alreay_zoom_exist)){
						$alreay_zoom_exist['student_id'] = $booking_detail['student_id'];
						$alreay_zoom_exist['booking_id'] = $booking_detail['booking_id'];
						$this->tutor_model->insert_zoom_session($alreay_zoom_exist);
						$cnt = 1;
						break;
					}
				}
				if($cnt == 1){
					return 1;
				}
			}
		}
		$response = array();
		$data['tutor_id'] = $user_id;
		$data['student_id'] = $booking_detail['student_id'];
		if(isset($booking_detail['course_id']))
			$data['course_id'] = $booking_detail['course_id'];
		if(isset($booking_detail['category']))
			$data['course_id'] = $booking_detail['category'];
		if($type == 0){
			$data['start_date'] = $booking_detail['start_date'];
			$time 	  	= explode('-', str_replace(' ', '', $booking_detail['time_slot']));
			$start_time = date('H:i', strtotime(number_format($time[0],2)));
			$data['start_time'] = $start_time;
			$data['duration'] = $booking_detail['duration_value']." ".$booking_detail['duration_type'];	
			$data['booking_type'] = "booking";
			$data['booking_id'] = $booking_detail['booking_id'];
		} else if($type == 1){
			$time_info = explode(' ',$booking_detail['time_slot']);
			$data['start_date'] = $time_info[0];
			$time 	  	= explode('-', str_replace(' ', '', $time_info[1]));
			$start_time = date('H:i', strtotime(number_format($time[0],2)));
			$data['start_time'] = $start_time;
			$data['duration'] = ($time[1]-$time[0])." hour";	
			$data['booking_type'] = "instant";
			$data['booking_id'] = $booking_detail['id'];
		}
		
		$zoom_info = $this->tutor_model->get_zoom_info_list();
		$same_zoom_meeting_exist = $this->tutor_model->get_zoom_meeting_list($data['start_time'],$data['start_date'],$data['duration']);
		$zoom = array();
		if(empty($same_zoom_meeting_exist)){
			$zoom = $zoom_info[0];
		} else{
			foreach($zoom_info as $key => $value){
				$is_exist = 0;
				foreach($same_zoom_meeting_exist as $k => $v){
					if($value['zoom_id'] == $v['zoom_id'])
						$is_exist = 1;
				}
				if($is_exist == 0){
					$zoom = $value;
					break;
				}
			}
		}
		if(!empty($zoom)){
			$zoom_user_info = $this->getUsers($zoom['zoom_api_key'],$zoom['zoom_secret_key']);
			$zoom_user_id = $zoom_user_info['response']->users[0]->id;
			$zoom_user_token = $zoom_user_info['token'];
			
			$duration = ($time[1]-$time[0])*60;
			
			
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.zoom.us/v2/users/".$zoom_user_id."/meetings",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => '{"type":8,"start_time" : "'.$data['start_date'].'T'.$data['start_time'].'","duration" : "'.$duration.'","recurrence":{"type" : 1,"repeat_interval" : 2,"end_date_time" : "'.$booking_detail['end_date'].'T'.$time[1].':00:00Z" },"settings":{"host_video":"true","participant_video" : "true","mute_upon_entry" : "true"}}',
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer $zoom_user_token",
			    "Content-Type: application/json",
			    "cache-control: no-cache"
			  ),
			));
			$zoom_response = curl_exec($curl);
			$err = curl_error($curl);
			$zoom_created_meeting_info = json_decode($zoom_response);
			curl_close($curl);
			if($err){
				$response['status'] = "curl_error";
				echo json_encode($response);
				return; 
			}
			else{
				$data['zoom_meeting_id'] = $zoom_created_meeting_info->id;
				$data['tutor_zoom_link'] = $zoom_created_meeting_info->start_url;
				$data['student_zoom_link'] = $zoom_created_meeting_info->join_url;
			}
			$data['zoom_id'] = $zoom['zoom_id'];
			$this->tutor_model->insert_zoom_session($data);
		} else{
			return 2;
		}
		return 1;
	}
	
	public function getUsers ($api_key, $api_secret) {
	    //list users endpoint GET https://api.zoom.us/v2/users
		$ch = curl_init('https://api.zoom.us/v2/users');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  
	    // add token to the authorization header
	    $token = $this->generateJWT($api_key, $api_secret);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $token
		));
		$response = curl_exec($ch);
		$response = json_decode($response);
		$data['response'] = $response;
		$data['token'] = $token;
		return $data;
	}
	
	public function generateJWT ($api_key, $api_secret) {
		$key = $api_key;
		$secret = $api_secret;
		$token = array(
			"iss" => $key,
	        // The benefit of JWT is expiry tokens, we'll set this one to expire in 1 minute
			"exp" => time() + 36000
		);
		$token = JWT::encode($token, $secret);
		return $token;
	}
	
	
	function attach_upload_post()
	{
		$user_id = $this->post('user_id');	
		if (!empty($_FILES['userfile']['name'])) 
		{
			$ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
			if(empty($ext)){
				$temp = explode('/',$_FILES['userfile']['type']);
				if(!isset($temp[1]) && !empty($temp[1]))
					$ext = $temp[1];
			}
			$file_name = $user_id.'_'.date("YmdHis").'.'.$ext;
			$config['upload_path'] 		= './assets/uploads/instant_bids/';
			$config['allowed_types'] = '*';
			$config['overwrite'] 		= true;
			$config['file_name']        = $file_name;
			$config['max_size']             = '1024000';
			$this->load->library('upload', $config);
			
			if($this->upload->do_upload('userfile'))
			{
				$insert_id = $this->base_model->update_attach_name($file_name,$user_id);
				$result['filename'] = $file_name;
				$result['file_id'] = $insert_id;
				$response = 'AttachFile uploaded successfully';
				$status = 1;
			}else{
				$data = $this->upload->display_errors();
				$result['filename'] = "";
				$response = 'AttachFile upload failed';
				$status = 0;
				$this->serviceResponse($data,$response,$status);
			}
		}else{
			$result['filename'] = "";
			$response = 'Please select file';
			$status = 0;
		}
		$this->serviceResponse($result,$response,$status);
	}
	
	function attach_upload1_post()
	{
		$user_id = $this->post('user_id');
		if (!empty($_FILES['userfile']['name'])) 
		{
			$ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
			if(empty($ext)){
				$temp = explode('/',$_FILES['userfile']['type']);
				if(!isset($temp[1]) && !empty($temp[1]))
					$ext = $temp[1];
			}
			$file_name = $user_id.'_'.date("YmdHis").'.'.$ext;
			$config['upload_path'] 		= './assets/uploads/instant_bids/';
			$config['allowed_types'] = '*';
			$config['overwrite'] 		= true;
			$config['file_name']        = $file_name;
			$config['max_size']             = '1024000';
			$this->load->library('upload', $config);
			
			if($this->upload->do_upload('userfile'))
			{
				$insert_id = $this->base_model->update_attach_name($file_name,$user_id);
				$result['file_id'] = $insert_id;
				$result['filename'] = $file_name;
				$response = 'AttachFile uploaded successfully';
				$status = 1;
			}else{
				$data = $this->upload->display_errors();
				$result['filename'] = '';
				$response = 'AttachFile upload failed';
				$status = 0;
				$this->serviceResponse($data,$response,$status);
			}
		}else{
			$result['filename'] = '';
			$response = 'Please select file';
			$status = 0;
		}
		$this->serviceResponse($result,$response,$status);
	}
	
	function delete_attach_iq_post(){
		$postdata 	= file_get_contents("php://input");
		$request	=	json_decode($postdata);
		$id = $request->attach_id;
		$this->base_model->delete_attach_file($id);
		$result = array();
		$response = 'AttachFile deleted successfully';
		$status = 1;
		$this->serviceResponse($result,$response,$status);
	}
	
	function get_tutors_iq_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		
		$student_id = $request->student_id;
		$from_limit = $request->from_limit;
		$to_limit = 20;
		$course_tbl_join		= "";
        $course_cond 			= "";
		if(isset($request->courses))
		{
			$courses = $request->courses;
			if(!empty($courses))
			{
				$courses=implode(",",$courses);
				$course_tbl_join = " INNER JOIN ".TBL_TUTOR_COURSES." tc ON tc.tutor_id=u.id ";
				$course_cond = " AND tc.course_id IN (".$courses.") AND tc.status=1 ";
			}
		}
		
		$location_tbl_join 		= "";
		$location_cond 			= "";
		if(isset($request->locations))
		{
			$locations = $request->locations;
			if(!empty($locations))
			{
				$locations=implode(",",$locations);
				$location_tbl_join = " INNER JOIN ".TBL_TUTOR_LOCATIONS." tl ON tl.tutor_id=u.id ";
				$location_cond = " AND tl.location_id IN (".$locations.")";
			}
		}
		
		$teaching_type_tbl_join = "";
		$teaching_type_cond 	= "";
		if(isset($request->teaching_types))
		{
			$teaching_types = $request->teaching_types;
			if(!empty($teaching_types))
			{
				$teaching_types=implode(",",$teaching_types);
				$teaching_type_tbl_join = " INNER JOIN ".TBL_TUTOR_TEACHING_TYPES." tt ON tt.tutor_id=u.id ";
				$teaching_type_cond = " AND tt.teaching_type_id IN (".$teaching_types.") ";
			}
		}
		 
		
		
		$data = array();
		
		$adm_approval_cond="";
		if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'Yes') == 0) 
		{
        	$adm_approval_cond .= " AND u.admin_approved = 'Yes' ";
        }
		
		
		$query = "SELECT u.*,f.fav_id FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id ".$course_tbl_join." ".$location_tbl_join." ".$teaching_type_tbl_join." LEFT JOIN ".TBL_PREFIX."student_fav_tutors f ON u.id=f.tutor_id AND f.student_id=".$student_id." WHERE u.active=1 AND u.visibility_in_search='1' AND u.is_profile_update=1 AND u.iq_live = 'Yes' AND u.iq_live_indicator='ONLINE' AND (u.parent_id=0 OR u.parent_id='') AND ug.group_id=3 ".$course_cond." ".$location_cond." ".$teaching_type_cond." ".$adm_approval_cond." GROUP BY u.id ORDER BY u.net_credits DESC LIMIT ".$from_limit.",".$to_limit."";
		
		$data = $this->base_model->get_query_result($query);
		$cur_date = date("Y-m-d");
		foreach($data as $key => $value){
			$session_list = $this->base_model->get_zoom_seesion_list($value->id,$cur_date);
			if(empty($session_list))
				$data[$key]->cur_status = 'AVAILABLE';
			else{
				$cur_time = date("H");
				$is_exist = 0;
				foreach($session_list as $k => $v){
					$time_slot = $v->time_slot;
					$time_slot = explode("-",$time_slot);
					if($time_slot[0] <= $cur_time & $time_slot[1] >= $cur_time){
						$data[$key]->cur_status = 'BUSY';
						$is_exist = 1;
						break;
					}
				}
				if($is_exist == 0)
					$data[$key]->cur_status = 'AVAILABLE';
			}
		}
		$this->serviceResponse($data,'Tutors',1);
	}
	
	function post_iq_live_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);

		$data=array();
		$response='';
		$status=0;
		$attachExist = $request->attachExist;
		$lead_data=$request->lead_data;
		$files = $request->filenames;
		$tutor = $request->tutor;
		
		$file_name = "";
		if(!empty($files)){
			foreach($files as $key => $value){
				if($key != 0)
					$file_name .= ",";
				$file_name .= $value->id;
			}
		}
		
		$input_data=array();
		$input_data['student_id'] 		= $request->user_id;
		$input_data['teaching_type_id']		= $lead_data->teaching_type_id;
		$input_data['priority_of_requirement']= $lead_data->priority_of_requirement;
		$input_data['category']    = $lead_data->category;
		$input_data['title_of_requirement']	= $lead_data->title_of_requirement;
		$input_data['budget']				= $lead_data->budget;
		$input_data['budget_type']		= $lead_data->budget_type;
		$input_data['tutor_rating']		= $lead_data->tutor_rating;
		$input_data['requirement_details']	= $lead_data->requirement_details;
		if(!empty($file_name))
			$input_data['attach'] = $file_name;
		$input_data['priority_date_time']	= $lead_data->priority_date." ".$lead_data->priority_time;
		//$input_data['iq_date']	= $lead_data->iq_date;
		//$input_data['iq_time_slot']	= $lead_data->iq_time_slot;
		if(!empty($lead_data->iq_date)){
			$student_details = $this->base_model->get_user_info($request->user_id);
			$time_zone_list = $this->base_model->get_timezone_list();
			$time_zone_list = explode(",",$time_zone_list->field_type_values);
			$time_zone = $time_zone_list[$student_details->time_zone];
			$t = strtotime($lead_data->iq_date);
			$time_slot = explode("-",$lead_data->iq_time_slot);
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
			$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->sub($myInterval);
			$myDateTime_end->sub($myInterval);
			
			$input_data['iq_date']	= $myDateTime_start->format("Y-m-d");
			$input_data['iq_time_slot']	= $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
		}
			
			
		$input_data['created_at']=date("Y-m-d H:i:s");
		$input_data['updated_at']=$input_data['created_at'];
		$input_data['created_by'] = $input_data['updated_by'] = $request->user_id;
		$input_data['status'] = 'new';
		$student_name = $this->base_model->get_student_name($input_data['student_id']);
		
		$device_ids = array();
		$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '26'));
		$email_tpl = $email_tpl[0];
		$sub = $msg = "";
		if(!empty($email_tpl)){
			$sub = $email_tpl->template_subject;
			$msg = $email_tpl->template_content;
		}
		
		$input_data['tutor_id'] = $tutor->id;
		$this->base_model->insert_operation_id($input_data,'instant_bids').",";
		if(!empty($tutor->device_id)){
			array_push($device_ids,$tutor->device_id);
		}
		if(!empty($tutor->email)){
			if(!empty($msg)){
				$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
				$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
				$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$tutor->username, $student_name, $input_data['iq_time_slot'], $input_data['teaching_type_id'], $login_link,$this->config->item('site_title', 'ion_auth'));
				$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR_NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','TITLE');
				$content = str_replace($temp_vars, $original_vars, $msg);
				$email_settings = (array)$this->config->item('email_settings');
				sendEmail($email_settings['webmail']['User Name'], $tutor->email, $sub, $content);
			}
		}
				
		if(count($device_ids) > 0){
			$this->load->library('OneSignalPush');
			$message = array(
			  "en" => "New IQ Live bid is added",
			  "title" => "New IQ Live bid is added",
			  "icon" => "myicon",
			  "sound"=>"default"
			);
			
			$data = array(
				"body" => "New IQ Live bid is added",
				"title" => "New IQ Live bid is added",
			);
			$gcpm = new OneSignalPush();
			$gcpm->setDevices($device_ids);
			$res = $gcpm->send($message,$data);
		}
		$response='IQ Live posted successfully';
		$status=1;
		$data['success'] = "success";
		
		$this->serviceResponse($data,$response,$status);
	}
	
	function get_post_iq_live_data_post()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = array();
        $tutor_id = $request->user_id;
		$query = "SELECT iq_live_credit,iq_live_indicator FROM pre_users where id=".$tutor_id;
		$data = $this->base_model->get_query_result($query);
		$this->serviceResponse($data,'Leads',1);
	}
	
	function post_live_iq_post()
	{
		$postdata = file_get_contents("php://input");
		$request  = json_decode($postdata);
		$data=array();
		$response='';
		$status=0;
		$lead_data=$request->lead_data;
		$input_data=array();
		$user_id 		= $request->user_id;
		$input_data['iq_live_credit']		= $lead_data->iq_live_credit;
		$input_data['iq_live_indicator']= $lead_data->iq_live_indicator;
		$this->base_model->update_operation($input_data,'users',array('id'=>$user_id));
		
		$response='Successfully';
		$status=1;
		$data['success'] = "success";
		$this->serviceResponse($data,$response,$status);
	}
	
	function get_countries_post()
    {
    	$countries = $this->base_model->fetch_records_from('country');
    	$country_opts = array();
		foreach ($countries as $key => $value) {
			$temp = array(
				'id'=>$key."_".$value->phonecode,
				'name'=>$value->nicename
			);
			array_push($country_opts, $temp);
		}
		array_push($country_opts, array('' => get_languageword('select_Country')));
		$this->serviceResponse($country_opts,'Country',1);
    }
    
    function get_timezone_post()
    {
    	$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$country_opts = array();
		foreach($time_zone_list as $key => $value){
			$temp = array(
				'id' => $key,
				'name' => $value
			);
			array_push($country_opts,$temp);
		}
		array_push($country_opts, array('' => get_languageword('select_Timezone')));
		$this->serviceResponse($country_opts,'Timezone',1);
    }
    
    function create_calendar_event($booking_info){
		$this->load->model('googlecalendar');
		$student_info = $this->base_model->get_student_list($booking_info);
		$tutor_info = (array)$this->base_model->get_user_info($booking_info['tutor_id']);
		$course_name = (array)$this->base_model->get_course_name($booking_info['course_id']);
		$this->googlecalendar->addEvent('primary', $booking_info, $student_info, $tutor_info,$course_name['name']);	
	}
	
	public function delete_google_calendar_event($booking_info){
		$this->load->model('googlecalendar');
		$student_info = (array)$this->base_model->get_user_info($booking_info['student_id']);
		$tutor_info = (array)$this->base_model->get_user_info($booking_info['tutor_id']);
		$this->googlecalendar->deleteEvent('primary', $booking_info, $student_info, $tutor_info);
	}
}
