<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth');
        $this->load->model('googlecalendar');
    }

    public function addEvent()
    {
        $data = array();   
        $event = array(
            'summary'     => "Test",
            'start'       => "2019-03-27".'T'."09:00".':00+03:00',
            'end'         => "2019-03-27".'T'."11:00".':00+03:00',
            'description' => "Test Description",
        );
        $foo = $this->googlecalendar->addEvent('primary', $event);
        if ($foo->status == 'confirmed') {
            $data['message'] = '<div class="alert alert-success">Events created.</div>';
        }

        //$this->load->helper('form');
        //$this->load->view('addevent', $data);

    }

}