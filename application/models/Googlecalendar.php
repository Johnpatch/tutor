<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Googlecalendar extends CI_Model
{

    public function __construct()
    {

        parent::__construct();
        $this->load->library('session');
        $this->load->library('googleplus1');
        $this->load->model('tutor_model');
        $this->calendar = new Google_Service_Calendar($this->googleplus1->client());

    }


    public function isLogin()
    {
        $token = $this->session->userdata('google_calendar_access_token');
        if ($token) {
            $this->googleplus1->client->setAccessToken($token);
        }
        if ($this->googleplus1->isAccessTokenExpired()) {
            return false;
        }
        return $token;
    }

    public function loginUrl()
    {
        return $this->googleplus1->loginUrl();
    }


    public function login($code)
    {
        $login = $this->googleplus1->client->authenticate($code);
        if ($login) {
            $token = $this->googleplus1->client->getAccessToken();

            $this->session->set_userdata('google_calendar_access_token', $token);

            return true;

        }

    }


    public function getUserInfo()
    {

        return $this->googleplus1->getUser();

    }


    public function deleteEvent($calendarId = 'primary', $data,$student_info = NULL, $tutor_info = NULL)
    {
    	$time_slot = $data['time_slot'];
    	$time_slot = explode('-',$time_slot);
    	if(strlen($time_slot[0]) == 1)
    		$time_slot[0] = "0".$time_slot[0];
    	if(strlen($time_slot[1]) == 1)
    		$time_slot[1] = "0".$time_slot[1];
    		
    	$begin = new DateTime($data['start_date']);
    	$end = new DateTime($data['end_date']);
	    $date = $begin->format("Y-m-d");
		$end_date = $end->format("Y-m-d");
		
		while (strtotime($date) <= strtotime($end_date)){
	    	$start_time = $date."T".$time_slot[0].":00:00+00:00";
	    	$end_time = $date."T".$time_slot[1].":00:00+00:00";
	    	
	        $timeMin = date("c", strtotime($start_time));
	        $timeMax = date("c", strtotime($end_time));

	        $optParams = array(
	            'maxResults'   => 10,
	            'orderBy'      => 'startTime',
	            'singleEvents' => true,
	            'timeMin'      => $timeMin,
	            'timeMax'      => $timeMax,
	            'timeZone'     => 'UTC',
	        );

	        $results = $this->googlecalendar->calendar->events->listEvents($calendarId, $optParams);
	        foreach($results->getItems() as $item){
				$id = $item->getId();
				$temp = $this->googlecalendar->calendar->events->delete($calendarId,$id);
			}
			$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
    }


    public function addEvent($calendarId = 'primary', $data, $student_info, $tutor_info, $course_name)
    {	
    	$time_slot = $data['time_slot'];
    	$time_slot = explode('-',$time_slot);
    	if(strlen($time_slot[0]) == 1)
    		$time_slot[0] = "0".$time_slot[0];
    	if(strlen($time_slot[1]) == 1)
    		$time_slot[1] = "0".$time_slot[1];
    	$week_day_list = array(0=>"MON",1=>"TUE",2=>"WED",3=>"THU",4=>"FRI",5=>"SAT",6=>"SUN");
    	$days_off = explode(",",$data['days_off']);
    	
    	$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$tutor_time_zone = $time_zone_list[$tutor_info['time_zone']];
		$utc_time = new DateTime($data['start_date']." ".$time_slot[0].":00:00");
		$t = strtotime($data['start_date']);
		
		$userTimezone = new DateTimeZone($tutor_time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($tutor_time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($tutor_time_zone));
		
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->add($myInterval);
		$myDateTime_end->add($myInterval);
		
    	if(($offset < 0 && $myDateTime_start->format("H") > $time_slot[0]) || ($offset > 0 && $myDateTime_start->format("H") < $time_slot[0])){
			foreach($days_off as $key => $value){
				foreach($week_day_list as $k => $v){
					if($value == $v){
						if($offset < 0)
							$index = $k+1;
						if($offset > 0)
							$index = $k-1;
						if($index > 6)
							$index = 0;
						if($index < 0)
							$index = 6;
						$days_off[$key] = $week_day_list[$index];
					}
						
				}
			}
		}
    	
    	$begin = new DateTime($data['start_date']);
    	$end = new DateTime($data['end_date']);
	    $date = $begin->format("Y-m-d");
		$end_date = $end->format("Y-m-d");
		$attendess = array();
		foreach($student_info as $key => $value){
			$student_name = "";
			if($value->name_privacy == 'Show ID')
				$student_name = "S".$value->student_idd;
			else
				$student_name = $value->username;
			$temp = array('displayName' => $student_name,'email'=>$value->email);
			array_push($attendess,$temp);
		}
		
		while (strtotime($date) <= strtotime($end_date)){
			$dt = new DateTime($date);
			
			$myDateTime_temp = new DateTime($dt->format("Y-m-d")." "."00:00:00", new DateTimeZone($tutor_time_zone));
			
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_temp->add($myInterval);
		
		
	    	$start_time = $date."T".$time_slot[0].":00:00+00:00";
	    	$temp_date = "";
	    	if($time_slot[1] == "00")
	    		$temp_date = date("Y-m-d",strtotime($date.' +1 day'));
	    	if($temp_date != "")
	    		$end_time = $temp_date."T".$time_slot[1].":00:00+00:00";
	    	else
	    		$end_time = $date."T".$time_slot[1].":00:00+00:00";
	    	
	    	$cur_day = $dt->format('D');
	    	$is_rest = 0;
	    	foreach($days_off as $key => $value){
				if(!empty($value))
					if($value == strtoupper($cur_day)){
						$is_rest = 1;
						break;
					}
			}
			if($is_rest == 0){
				
				$description = "";
				//if($tutor_info['whiteboard'] == 'Yes' && $data['tution'] == 'One on One')
					//$description = "https://bitpaper.io/go/".str_replace(" ","",$course_name)."_".$myDateTime_temp->format("Ymd")."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$tutor_info['username'])."_".$myDateTime_temp->format("Ymd");
				/*else if(($tutor_info['gcal_hangouts'] == 'Yes' || $tutor_info['gcal_zoom'] == 'Yes') && $data['tution'] == 'One on One')
					$description = "";
				else if(($tutor_info['gcal_hangouts'] == 'Yes' || $tutor_info['gcal_zoom'] == 'Yes') && $data['tution'] == 'Share')
					$description = "";*/
				$description = str_replace(' ','',$description);
		        $event = new Google_Service_Calendar_Event(
		            array(
		                'summary'     => strip_tags($data['content'])." ".$myDateTime_temp->format("Ymd")."_".$myDateTime_start->format("H").$myDateTime_end->format("H"),
		                'description' => $description,
		                'start' => array(
						    'dateTime' => $start_time,
						    'timeZone' => 'UTC',
						),
						'end' => array(
						    'dateTime' => $end_time,
						    'timeZone' => 'UTC',
						),
						'htmlLink' => "https://zoom.us/j/142250337",
						'guestsCanInviteOthers' => false,
						'guestsCanSeeOtherGuests' => false,
		                'attendees'   => $attendess,
		                'sendUpdates' => 'all',
		                'conferenceData' => array(
		                	'createRequest' => array(
		                		'requestId' => '576235454'
		                	),
		                	/*'entryPoints' => array(
		                		'accessCode' => '576235454',
		                		'entryPointType' => 'video',
		                		'label' => 'zoom.us',
		                		'uri' => 'https://zoom.us/j/142250337',
		                	)*/
		                ),
		                'creator' => array(
		                	array('displayName' => $tutor_info['username']),
		                ),
		                'sendNotifications' => true,
		                'reminders' => array(
		                	'overrides' => array(
		                		array('method' => 'popup','minutes' => 30),
		                		array('method' => 'popup','minutes' => 15),
		                		array('method' => 'popup','minutes' => 5),
		                	),
		                	'useDefault' => FALSE
		                ),
		            )
		        );
		        $form = $this->calendar->events->insert($calendarId, $event);
		        //$this->tutor_model->set_whiteboard_url($data['booking_id'],$description);
			}
			$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
    }


}