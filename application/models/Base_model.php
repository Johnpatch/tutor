<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Base_Model extends CI_Model  
{
	var $numrows;

	function __construct()
	{
		parent::__construct();
	}


	//General database operations	
	function run_query( $query )
	{
		$rs = $this->db->query( $query );
		return $rs or die ('Error:' . mysql_error());
	}

	function get_query_result( $query )
	{
		return $this->db->query( $query )->result();
	}

	function get_query_row( $query )
	{
		return $this->db->query( $query )->row();
	}
	
	function count_records( $table, $condition = '' )
	{
		if( !(empty($condition)) )
		$this->db->where( $condition );
		$this->db->from( $this->db->dbprefix( $table ) );
		$reocrds = $this->db->count_all_results();
		//echo $this->db->last_query();
		return $reocrds;
	}
	
	function insert_operation( $inputdata, $table)
	{
		$result  = $this->db->insert($this->db->dbprefix($table),$inputdata);
		return $this->db->insert_id();
	}
	function insert_operation_id($inputdata, $table, $email = '')
    {
        $result = $this->db->insert($this->db->dbprefix($table), $inputdata);
        return $this->db->insert_id();
    }
    function update_attach_status($userid, $id_list){
		$this->db->query("UPDATE pre_attach_iq SET iq_id = '".$id_list."' WHERE user_id = ".$userid." and iq_id IS NULL");
	}
	function update_operation( $inputdata, $table, $where )
	{
		$result  = $this->db->update($this->db->dbprefix($table),$inputdata, $where);
		return $result;
	}

	function update_operation_in( $inputdata, $table, $column, $values )
	{
		$this->db->where_in($column, $values);
		$result  = $this->db->update($this->db->dbprefix($table),$inputdata);
		return $result;
	}
	
	function fetch_records_from( $table, $condition = '',$select = '*', $order_by = '', $like = '', $offset = '', $perpage = '' )
	{
		$this->db->start_cache();
			$this->db->select($select, FALSE);
			$this->db->from( $this->db->dbprefix( $table ) );
			if( !empty( $condition ) )
				$this->db->where( $condition );
			if( !empty( $like ) )
					$this->db->like( $like );
			if( !empty( $order_by ) )
				$this->db->order_by( $order_by );
		$this->db->stop_cache();
		$result = $this->db->get();
		$this->numrows = $this->db->affected_rows();
      //echo $this->numrows.'<br>';
		if( $perpage != '' )
		$this->db->limit($perpage, $offset);
		$result = $this->db->get();
		//print_r($result);die();
		$this->db->flush_cache();
		return $result->result();
	}
	
	function fetch_records_from_in($table, $column, $value, $select = '*', $order_by = '', $like = '')
	{
		$this->db->start_cache();
			$this->db->select($select, FALSE);
			$this->db->from( $this->db->dbprefix( $table ) );
			$this->db->where_in( $column, $value );
			if( !empty( $like ) )
					$this->db->like( $like );
			if( !empty( $order_by ) )
				$this->db->order_by( $order_by );
		$this->db->stop_cache();    
		$this->numrows = $this->db->count_all_results();
		
		$result = $this->db->get();
		$this->db->flush_cache();
		return $result->result();
	}
	
	function fetch_value($table, $column, $where)
	{
		$this->db->select($column, FALSE);
		$this->db->from( $this->db->dbprefix( $table ) );
		$this->db->where( $where );
		$this->db->limit(0, 1);
		$result = $this->db->get()->result();
		$str = '-';
		if(count($result))
		{
			foreach($result as $row)
			{
				$str = $row->$column;
			}
		}
		return $str;
	}
		
	function changestatus( $table, $inputdata, $where  )
	{
		$result = $this->db->update($this->db->dbprefix($table),$inputdata, $where);
		return $result;
	}
	
	function delete_record($table, $column, $ids)
	{	
		$this->db->where_in($column, $ids);
		$result = $this->db->delete( $table );
		return $result;
	}
	
	function delete_record_new($table, $condition)
	{
		$this->db->where($condition);
		$this->db->delete( $table );
		return TRUE;
	}
	
	function get_user_details($id)
	{
		$query = 'SELECT u.*,g.name group_name, g.id group_id FROM '.$this->db->dbprefix('users').' u INNER JOIN '.$this->db->dbprefix('users_groups').' ug ON u.id = ug.user_id INNER JOIN '.$this->db->dbprefix('groups').' g ON g.id = ug.group_id WHERE g.group_status = "Active" AND u.id = '.$id;
		return $this->db->query($query)->result();
	}
	////////////////////////////Data Tables//////////////////////////
	private function _get_datatables_query($table, $condition = array(), $columns = array(), $order = array())
	{		
		
		$this->db->start_cache();
		
		$this->db->select($columns);
		$this->db->from($table);
		$this->db->group_start();
		if(!empty($condition))
		{
			if(isset($condition['incondition']))
			{
				$this->db->where_in($condition['incondition']['name'], $condition['incondition']['hey_stack']);
				unset($condition['incondition']);
				$this->db->where( $condition );
			}
			else
			{
				$this->db->where( $condition );
			}
		}
		$this->db->group_end();
		
		if($_POST['search']['value'])
		$this->db->group_start();
			$i = 0;
			$column = array();			
			foreach ($columns as $item) 
			{
				if($_POST['search']['value'])
					($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
				$column[$i] = $item;
				$i++;
			}			
		if($_POST['search']['value'])
		$this->db->group_end();
	
		//Colums Searching Start
		$column_search = FALSE;
		$p = 0;
		foreach ($columns as $item) 
		{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '') $column_search = TRUE;
			$p++;
		}
		if($column_search == TRUE)
		{
			$this->db->group_start();
			$p = 0;		
			foreach ($columns as $item) 
			{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '')
			$this->db->where($item, $this->getStringBetween(urldecode($_POST['columns'][$p]['search']['value']), '^', '$'));
			$p++;
			}
			$this->db->group_end();
		}
		//Colums Searching End
	
		if(isset($_POST['order']))
		{
			if(isset($_POST['order'][0]))
			$this->db->order_by($column[$_POST['order'][0]['column']], $_POST['order'][0]['dir']);
		} 
		else if(count($order) > 0)
		{
			$order = $order;
			$this->db->order_by(key($order), $order[key($order)]);
		}		
		$this->db->stop_cache();	
	}
	
	function getStringBetween($str,$from,$to)
	{
		$sub = substr($str, strpos($str,$from)+strlen($from),strlen($str));
		return substr($sub,0,strpos($sub,$to));
	}
	
	private function _get_datatables_customquery($query, $columns = array(), $order = array())
	{	
		$i = 0;
		$column = array();
		$str = '';
		foreach ($columns as $item) 
		{
			if($_POST['search']['value'])
			($i===0) ? $str .= ' AND ('.$item . ' LIKE "%' . $_POST['search']['value'] . '%"' : $str .= ' OR '.$item . ' LIKE "%'.$_POST['search']['value'].'%"';
			$column[$i] = $item;
			$i++;
		}
		if($str != '')
			$str .= ')';
		
		//Colums Searching Start
		$column_search = FALSE;
		$p = 0;
		foreach ($columns as $item) 
		{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '') $column_search = TRUE;
			$p++;
		}
		if($column_search == TRUE)
		{
			$p = 0;		
			foreach ($columns as $item) 
			{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '')
				$str .= ' AND '.$item . ' = ' . $this->getStringBetween(urldecode($_POST['columns'][$p]['search']['value']), '^', '$');	
			$p++;
			}
		}
		//Colums Searching End
		
		 
		if(count($order) > 0)
		{
			$order = $order;
			$str .= ' ORDER BY tds.' . key($order) . ' ' . $order[key($order)];
		}
		elseif(isset($_POST['order']))
		{
			$str .= ' ORDER BY tds.' . $column[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'];
		}
		return 	$query . $str;	
	}
	
	private function _get_datatables_customquery_new($query, $columns = array(), $order = array())
	{	
		$i = 0;
		$column = array();
		$str = '';
		foreach ($columns as $item) 
		{
			if($_POST['search']['value'])
			($i===0) ? $str .= ' AND ('.$item . ' LIKE "%' . $_POST['search']['value'] . '%"' : $str .= ' OR '.$item . ' LIKE "%'.$_POST['search']['value'].'%"';
			$column[$i] = $item;
			$i++;
		}
		if($str != '')
			$str .= ')';
		
		//Colums Searching Start
		$column_search = FALSE;
		$p = 0;
		foreach ($columns as $item) 
		{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '') $column_search = TRUE;
			$p++;
		}
		if($column_search == TRUE)
		{
			$p = 0;		
			foreach ($columns as $item) 
			{
			if(isset($_POST['columns'][$p]['search']['value']) && $_POST['columns'][$p]['search']['value'] != '')
				$str .= ' AND '.$item . ' = ' . $this->getStringBetween(urldecode($_POST['columns'][$p]['search']['value']), '^', '$');	
			$p++;
			}
		}
		//Colums Searching End
		
		 
		if(count($order) > 0)
		{
			$order = $order;
			$str .= ' ORDER BY ' . key($order) . ' ' . $order[key($order)];
		}
		elseif(isset($_POST['order']))
		{
			$str .= ' ORDER BY ' . $column[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'];
		}
		return 	$query . $str;	
	}
	
	function get_datatables($table, $type = 'auto', $condition = array(), $columns = array(), $order = array())
	{
		
		if($type == 'custom')
		{
			$query_str = $this->_get_datatables_customquery($table, $columns, $order);
			
			$queryall = $this->db->query($query_str);
			$this->numrows = $this->db->affected_rows();
			if($_POST['length'] != -1)
			$query_str = $query_str . ' LIMIT '.$_POST['start'] .','. $_POST['length'];
		
			$query = $this->db->query($query_str);
		}
		else if($type == 'customnew')
		{
			$query_str = $this->_get_datatables_customquery_new($table, $columns, $order);
			//echo $query_str;die();
			$queryall = $this->db->query($query_str);
			$this->numrows = $this->db->affected_rows();
			if($_POST['length'] != -1)
			$query_str = $query_str . ' LIMIT '.$_POST['start'] .','. $_POST['length'];
		
			$query = $this->db->query($query_str);
		}
		else if($type == 'complex')
		{
			$this->_get_datatables_query_complex($table, $condition, $columns, $order);
			$queryall = $this->db->get();
			$this->numrows = $this->db->affected_rows();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			
		}
		else
		{
			$this->_get_datatables_query($table, $condition, $columns, $order);
			//neatPrint($columns);
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
		}		
		$this->db->flush_cache();
		
		return $query->result();
	}
	
	function count_filtered($table, $type = 'auto', $condition = array(), $columns = array(), $order = array())
	{
		if($type == 'custom')
		{
			$query_str = $this->_get_datatables_customquery($table, $condition, $columns, $order);
			$query = $this->db->query($query_str)->result();
		}
		elseif($type == 'complex')
		{
			$this->_get_datatables_query_complex($table, $condition, $columns, $order);
			$query = $this->db->get();
		}
		else
		{
			$this->_get_datatables_query($table, $condition, $columns, $order);
			$query = $this->db->get();
		}
		
		
		//echo $this->db->last_query();
		return $query->num_rows();
	}
	
	public function count_all($table, $condition = array(),$type='')
	{
		if($type=='complex') 
							return 0;
		else 
		{
		$this->db->from($table);
		if(!empty($condition))
			$this->db->where( $condition );
		return $this->db->count_all_results();
		}
	}
	////Data tables end	
	function fetch_records_from_query_object($query, $offset = '', $perpage = '')
	{
		$resultset = $this->db->query( $query );
		$this->numrows = $resultset->num_rows();
		if( $perpage != '' )
			$query = $query . ' limit ' . $offset . ',' . $perpage;
		$resultsetlimit = $this->db->query( $query );
		return $resultsetlimit->result();
	}
	
	/**
	 * Get Payment Gateways
	 *
	 * @access	public
	 * @param	void
	 * @return	mixed
	 */
	function get_payment_gateways($str = '', $status = '')
	{		
		
		$status_cond = "";

		if(!empty($status))
			$status_cond = " AND st2.status='".$status."'";

		$query = "select st2.* from " . $this->db->dbprefix('system_settings_types') . " st inner join ".$this->db->dbprefix('system_settings_types')." st2 on st.type_id = st2.parent_id 
		where st.type_slug = 'PAYMENT_SETTINGS' ".$status_cond." ".$str;
		$packages = $this->db->query($query)->result();
		return $packages;	
	}


	function get_page_about_us(){

		$pageAboutUs= $this->db->get_where($this->db->dbprefix('pages'), array('id' => '1'))->result();
		return $pageAboutUs;
	}	

	function get_page_how_it_works(){

		$pageHowItWorks = $this->db->get_where($this->db->dbprefix('pages'), array('id' => '2'))->result();
		return $pageHowItWorks;
	}	

	function get_page_terms_and_conditions(){

		$pageTermsAndCondtions = $this->db->get_where($this->db->dbprefix('pages'), array('id' => '3'))->result();
		return $pageTermsAndCondtions;
	}	
	
	function get_page_privacy_and_policy(){

		$pagePrivachAndPolicy = $this->db->get_where($this->db->dbprefix('pages'), array('id' => '4'))->result();
		return $pagePrivachAndPolicy;
	}	

	function get_page_by_title()
	{
		
		$pages = $this->db->get_where($this->db->dbprefix('pages'), array('id > '=> '4'))->result();

		return $pages;
	}

	function get_page_by_title_content($slug){

		$pageData = $this->db->get_where($this->db->dbprefix('pages'),array('slug'=>$slug))->result();
		return $pageData;
	}

	function get_usersCount(){

		$query ="SELECT COUNT( * ) AS total_users, COUNT( IF( ug.group_id =2, 1, NULL ) ) AS total_students, COUNT( IF( ug.group_id =3, 1, NULL ) ) AS total_tutors, COUNT( IF( ug.group_id =4, 1, NULL ) ) AS total_institutes, COUNT( IF( ug.group_id =5, 1, NULL ) ) AS total_parents FROM " .$this->db->dbprefix('users'). " u  INNER JOIN ". $this->db->dbprefix('users_groups') ." ug ON ug.user_id = u.id WHERE u.id !=1";

		$userCount = $this->db->query($query)->result();

		return $userCount;

	}


	function get_packages_subscriptions()
	{
		// $query = "SELECT p.id, p.package_name, p.package_cost, (

		// 		SELECT COUNT( * ) 
		// 		FROM ". $this->db->dbprefix('subscriptions'). "
		// 		WHERE package_id = p.id
		// 		) AS total_subscriptions, 
				
		// 		(SELECT SUM( package_cost ) FROM " . $this->db->dbprefix('subscriptions') . " WHERE package_id = p.id) AS total_payments
		// 			FROM " . $this->db->dbprefix('packages') . " p";

		$query = "SELECT
					p.id, p.package_name, p.package_cost ,
					(select count(*)  from pre_subscriptions where package_id =p.id ) as total_subscriptions,
					(select count(*) from pre_subscriptions where user_type = 'Student' AND package_id =p.id) as Students,
					(select count(*) from pre_subscriptions where user_type = 'Tutor' AND package_id =p.id) as Tutors,
					(select count(*) from pre_subscriptions where user_type = 'Institute' AND package_id =p.id) as Institutes,
					(select count(*) from pre_subscriptions where user_type = 'Parent' AND package_id =p.id) as Parents,
					(select sum(package_cost) from pre_subscriptions where package_id = p.id) as total_payments

					FROM 
					pre_packages p ";

		$result = $this->db->query($query)->result();
		return $result;
	}
	
	/***********************
	05-12-2018
	***********************/
	function admin_header_notifications()
	{
		$records = $this->base_model->get_query_result("SELECT n.*,u.username,u.user_belongs_group,u.photo,u.gender FROM pre_notifications n INNER JOIN pre_users u ON n.user_id=u.id WHERE n.admin_read=0 ORDER BY n.notification_id DESC ");

		return $records;

	}
	
	function get_tutor_list_from_category($category){
		$query = "select pre_users.id,pre_users.device_id,pre_users.email,pre_users.username from pre_course_categories
INNER JOIN pre_categories ON pre_categories.id = pre_course_categories.course_id
INNER JOIN pre_tutor_courses ON pre_tutor_courses.`status` = 1 and pre_tutor_courses.course_id = pre_categories.id
INNER JOIN pre_users ON pre_users.active = 1 and pre_users.id = pre_tutor_courses.tutor_id
WHERE pre_course_categories.category_id = ".$category." GROUP BY pre_users.id";
		$result = $this->db->query($query)->result();
		return $result;
	}	
	
	function get_student_name($userid){
		$query = "select * from pre_users where id = ".$userid;
		$result = $this->db->query($query)->row();
		return $result->username;
	}
	
	function get_teaching_type($id){
		$record = $this->db->query("SELECT * FROM pre_inst_bid_teaching_types WHERE id = ".$id)->row();
		return $record->teaching_type;
	}
	
	function instant_bid_details($bid_id){
    	$sql = "SELECT pre_instant_bids.*,pre_users.*,pre_users.photo as student_photo FROM pre_instant_bids inner join pre_users on pre_instant_bids.student_id = pre_users.id where pre_instant_bids.id = ".$bid_id;
    	return $this->db->query($sql)->row();
		
	}
	
	function get_tutor_info($userid){
		$sql = "SELECT * FROM pre_users where id = ".$userid;
		return $this->db->query($sql)->row();
	}
	
	function get_instant_bid($lead_id){
		$sql = "SELECT * FROM pre_instant_bids where id = ".$lead_id;
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function get_tutor_name($userid){
		$query = "select * from pre_users where id = ".$userid;
		$result = $this->db->query($query)->row();
		return $result->username;
	}
	
	function insert_attach_iq($userid, $filename){
		$query = "select count(*) as cnt from pre_attach_iq where user_id = ".$userid;
		$result = $this->db->query($query)->row();
		if($result->cnt == 0){
			$data['user_id'] = $userid;
			$data['filename'] = $filename;
			$this->db->insert("pre_attach_iq",$data);	
		} else{
			$sql = "UPDATE pre_attach_iq SET filename = '".$filename."' where user_id = ".$userid;
			$this->db->query($sql);
		}
	}
	
	function get_attach_file_name($userid){
		$sql = "SELECT * FROM pre_attach_iq WHERE user_id = ".$userid." and iq_id IS NULL ORDER BY id DESC";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function update_attach_name($filename, $id){
		$data['user_id'] = $id;
		$data['filename'] = $filename;
		$this->db->insert('attach_iq',$data);
		return $this->db->insert_id();
	}
	
	function delete_zoom_session($booking_id){
		$sql = "DELETE FROM pre_zoom_session WHERE booking_id = ".$booking_id."";
		$this->db->query($sql);
	}
	
	function delete_zoom_session1($booking_id){
		$result = $this->db->query("SELECT * FROM pre_zoom_session WHERE booking_id = ".$booking_id)->row();
		$sql = "UPDATE pre_zoom_session SET delete_indicator = 1 WHERE booking_id = ".$booking_id."";
		$this->db->query($sql);
		$cnt = $this->db->query("SELECT count(*) as cnt FROM pre_zoom_session WHERE zoom_meeting_id = ".$result->zoom_meeting_id." and (delete_indicator != 1 or delete_indicator IS NULL)")->row();
		$result->cnt_zoom = $cnt->cnt;
		return (array)$result;
	}
	
	function get_zoom_info($zoom_id){
		$result = $this->db->query("SELECT * FROM pre_zoom WHERE zoom_id = ".$zoom_id)->row();
		return (array)$result;
	}
	
	function delete_attach_file($id){
		$this->db->query("DELETE FROM pre_attach_iq WHERE id = ".$id);	
	}
	
	function get_iq_attach_list($id){
		$result = $this->db->query("SELECT * FROM pre_attach_iq WHERE id = ".$id)->row();
		return $result;
	}
	
	function get_user_info($id){
		$result = $this->db->query("SELECT * FROM pre_users where id = ".$id)->row();
		return $result;
	}
	
	function get_max_student_id(){
		$result = $this->db->query("select MAX(student_idd) as max from pre_users")->row();
		return $result->max;
	}
	
	function format_iq_live($userId){
		$this->db->query("UPDATE pre_users SET iq_live_indicator = 'OFFLINE' WHERE id = ".$userId);
	}
	
	function get_zoom_seesion_list($id,$cur_date){
		$sql = "SELECT * FROM pre_bookings where tutor_id = ".$id." and preferred_location = 'online-zoom' and (status='session_initiated' or status='running') and start_date <= '".$cur_date."' and end_date >= '".$cur_date."'";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_course_name($course_id){
		$sql = "SELECT * FROM pre_categories WHERE id = ".$course_id;
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function get_timezone_list(){
		$sql = "SELECT * FROM pre_system_settings_fields WHERE field_key = 'time_zone'";
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function get_user_list_iq_online(){
		$sql = "SELECT * FROM pre_users WHERE iq_live_indicator = 'ONLINE'";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_student_list($booking_info){
		if($booking_info['tution'] == 'Share'){
			$sql = "SELECT * FROM pre_bookings WHERE course_id =".$booking_info['course_id']." and tutor_id = ".$booking_info['tutor_id']." and start_date = '".$booking_info['start_date']."' and time_slot = '".$booking_info['time_slot']."' and preferred_location = '".$booking_info['preferred_location']."' and tution='Share'";
			$result = $this->db->query($sql)->result();
			$student_list = array();
			foreach($result as $key => $value){
				$this->db->query("UPDATE pre_bookings SET prev_status = '".$value->status."', status = 'session_initiated' WHERE booking_id = ".$value->booking_id);
				$result = $this->db->query("SELECT * FROM pre_users WHERE id = ".$value->student_id)->row();
				array_push($student_list,$result);
			}
			return $student_list;
		}else{
			$student_list[] = $this->db->query("SELECT * FROM pre_users WHERE id = ".$booking_info['student_id'])->row();
			return $student_list;
		}
	}
	
	function delete_same_records($booking_info, $status){
		$sql = "SELECT * FROM pre_bookings WHERE course_id =".$booking_info['course_id']." and tutor_id = ".$booking_info['tutor_id']." and start_date = '".$booking_info['start_date']."' and time_slot = '".$booking_info['time_slot']."' and preferred_location = '".$booking_info['preferred_location']."' and tution='Share'";
		$result = $this->db->query($sql)->result();
		foreach($result as $key => $value){
			if($status == 'delete')
				$this->db->query("DELETE FROM pre_bookings WHERE booking_id = ".$value->booking_id);
			else
				$this->db->query("UPDATE pre_bookings SET prev_status = '".$value->status."', status = '".$status."' WHERE booking_id = ".$value->booking_id);
		}
	}
	
	function get_today_events($user_id, $cur_date, $type){
		$sql = "";
		if($type == 'Student'){
			$sql = "select d.description,e.tutor_zoom_link,e.fluidmath_link,c.name as course_name,b.username,b.slug,b.time_zone,b.zoom_pim_url,b.zoom_joining_details,b.fluidmath_url,a.* from pre_bookings a INNER JOIN pre_users b ON a.tutor_id = b.id INNER JOIN pre_categories c ON a.course_id = c.id LEFT JOIN pre_zoom_details d on a.booking_id = d.booking_id and a.tutor_id = d.tutor_id and d.date = '".$cur_date."' LEFT JOIN pre_zoom_session e on a.booking_id = e.booking_id and e.student_id = ".$user_id." and e.start_date = '".$cur_date."' WHERE (a.student_id = ".$user_id." and (a.status = 'session_initiated' or a.status = 'running')) UNION select d.description,e.tutor_zoom_link,e.fluidmath_link,c.name as course_name,b.username,b.slug,b.time_zone,b.zoom_pim_url,b.zoom_joining_details,b.fluidmath_url,a.* from pre_bookings a INNER JOIN pre_users b ON a.tutor_id = b.id INNER JOIN pre_categories c ON a.course_id = c.id LEFT JOIN pre_zoom_details d on a.booking_id = d.booking_id and a.tutor_id = d.tutor_id and d.date = '".$cur_date."' LEFT JOIN pre_zoom_session e on a.booking_id = e.booking_id and e.student_id = ".$user_id." and e.start_date = '".$cur_date."' WHERE (a.tutor_id = ".$user_id." and (a.status = 'session_initiated' or a.status = 'running') and a.tution = 'Leave bank') GROUP BY b.username,a.tutor_id,a.content,a.start_date,a.end_date,a.time_slot";
		}else{
			$sql = "select d.description,e.tutor_zoom_link,e.fluidmath_link,c.name as course_name,b.username,b.slug,b.time_zone,f.zoom_pim_url,f.zoom_joining_details,f.fluidmath_url,a.* from pre_bookings a INNER JOIN pre_users b ON a.student_id = b.id INNER JOIN pre_users f on a.tutor_id = f.id INNER JOIN pre_categories c ON a.course_id = c.id LEFT JOIN pre_zoom_details d on a.booking_id = d.booking_id and a.tutor_id = d.tutor_id and d.date = '".$cur_date."' LEFT JOIN pre_zoom_session e on a.booking_id = e.booking_id and e.tutor_id = ".$user_id." and e.start_date = '".$cur_date."' WHERE a.tutor_id = ".$user_id." and (a.status = 'session_initiated' or a.status = 'running')";
		}
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_specific_event($user_id,$cur_date,$type,$booking_id){
		$sql = "select d.description,e.tutor_zoom_link,e.fluidmath_link,c.name as course_name,b.username,b.slug,b.time_zone,f.zoom_pim_url,f.zoom_joining_details,f.fluidmath_url,a.* from pre_bookings a INNER JOIN pre_users b ON a.student_id = b.id INNER JOIN pre_users f on a.tutor_id = f.id INNER JOIN pre_categories c ON a.course_id = c.id LEFT JOIN pre_zoom_details d on a.booking_id = d.booking_id and a.tutor_id = d.tutor_id and d.date = '".$cur_date."' LEFT JOIN pre_zoom_session e on a.booking_id = e.booking_id and e.tutor_id = ".$user_id." and e.start_date = '".$cur_date."' WHERE a.tutor_id = ".$user_id." and (a.status = 'session_initiated' or a.status = 'running') and a.booking_id = ".$booking_id;
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function update_zoom_details($tutor_id,$booking_id,$date,$value){
		$inputdata = array();
		$inputdata['tutor_id'] = $tutor_id;
		$inputdata['booking_id'] = $booking_id;
		$inputdata['date'] = $date;
		$inputdata['description'] = $value;
		$sql = "SELECT count(*) as cnt FROM pre_zoom_details WHERE tutor_id = ".$tutor_id." and booking_id = ".$booking_id." and date = '".$date."'";
		$result = $this->db->query($sql)->row();
		if(!empty($result->cnt)){
			$sql = "UPDATE pre_zoom_details SET description = '".$value."' WHERE tutor_id = ".$tutor_id." and booking_id = ".$booking_id." and date = '".$date."'";
			$this->db->query($sql);
		}else{
			$result = $this->db->insert($this->db->dbprefix('zoom_details'), $inputdata);
		}
	}
	
	function get_student_id($student_email){
		$sql = "SELECT id FROM pre_users WHERE email = '".$student_email."' and active = 1 and user_belongs_group = 2";
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function register_parent_child($parent_id, $student_id){
		if(!empty($parent_id) && !empty($student_id)){
			$sql = "SELECT count(*) as cnt FROM pre_childs WHERE parent_id = ".$parent_id." and student_id = ".$student_id;
			$result = $this->db->query($sql)->row();
			if($result->cnt == 0){
				$sql = "DELETE FROM pre_childs WHERE student_id = ".$student_id;
				$this->db->query($sql);
				$sql = "INSERT INTO pre_childs (parent_id,student_id) VALUES(".$parent_id.",".$student_id.")";
				$this->db->query($sql);		
			}
		}
	}
	
	function format_child_list($email){
		$sql = "SELECT id FROM pre_users WHERE email = '".$email."'";
		$result = $this->db->query($sql)->row();
		$sql = "DELETE FROM pre_childs WHERE parent_id = ".$result->id;
		$this->db->query($sql);
	}
	
	function register_child($parent_email,$student_email){
		$sql = "SELECT id FROM pre_users WHERE email = '".$parent_email."'";
		$parent = $this->db->query($sql)->row();
		
		$sql = "SELECT id FROM pre_users WHERE email = '".$student_email."' and active = 1 and user_belongs_group = 2";
		$student = $this->db->query($sql)->row();
		
		if(!empty($parent) && !empty($student)){
			$this->register_parent_child($parent->id,$student->id);
		}
	}
	
	function get_parent_status($user_id){
		if($this->session->userdata('user_belongs_group') == 2){
			$sql = "SELECT count(*) as cnt FROM pre_childs a INNER JOIN pre_users b ON a.parent_id = b.id and b.active = 1 WHERE a.student_id = ".$user_id;
			$result = $this->db->query($sql)->row();
			return $result->cnt;
			//return 1;
		}else if($this->session->userdata('user_belongs_group') == 5){
			return 0;
		}else{
			return 1;
		}
	}
	
	function get_user_info_by_email($email){
		$sql = "SELECT * FROM pre_users WHERE email = '".$email."'";
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function upate_privileges_chage($inputdata,$user_id){
		$this->db->update($this->db->dbprefix('users'),$inputdata, array('id'=>$user_id));
	}
	
	function register_parent($student_email,$parent_email){
		$sql = "SELECT id FROM pre_users WHERE email = '".$parent_email."' and active = 1 and user_belongs_group = 5";
		$parent = $this->db->query($sql)->row();
		
		$sql = "SELECT id FROM pre_users WHERE email = '".$student_email."' and active = 1 and user_belongs_group = 2";
		$student = $this->db->query($sql)->row();
		
		if(!empty($parent) && !empty($student)){
			$this->register_parent_child($parent->id,$student->id);
		}
	}
	
	function get_parent_id($student_id){
		$sql = "SELECT * from pre_childs WHERE student_id = ".$student_id;
		$result = $this->db->query($sql)->row();
		return $result->parent_id;
	}
	
	function insert_tutor_student($data){
		$sql = "SELECT count(*) as cnt from pre_tutor_student WHERE tutor_id1 = ".$data['tutor_id1']." and student_id1 = ".$data['student_id1'];
		$result = $this->db->query($sql)->row();
		if($result->cnt == 0){
			$this->db->insert($this->db->dbprefix('tutor_student'),$data);
		}
	}
	
	function get_zoom_details($date,$booking_id){
		$sql = "SELECT * FROM pre_zoom_session WHERE booking_id = ".$booking_id." and start_date = '".$date."'";
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function update_zoom_link($data){
		$sql = "SELECT count(*) as cnt FROM pre_zoom_session WHERE booking_id = ".$data['booking_id']." and start_date = '".$data['date']."'";
		$result = $this->db->query($sql)->row();
		if($result->cnt > 0){
			$sql = "UPDATE pre_zoom_session SET tutor_zoom_link = '".$data['tutor_zoom_link']."' WHERE booking_id = ".$data['booking_id']." and start_date = '".$data['date']."' and student_id = ".$data['student_id'];
		}else{
			$sql = "INSERT INTO pre_zoom_session (tutor_zoom_link,start_date,booking_id,tutor_id,student_id) VALUES('".$data['tutor_zoom_link']."','".$data['date']."',".$data['booking_id'].",".$this->ion_auth->get_user_id().",".$data['student_id'].")";
		}
			
		$this->db->query($sql);
	}
	
	function get_unique_id(){
		$result = $this->db->query("select MAX(collab_id) as max from pre_users")->row();
		if(empty($result->max))
			return 1000;
		else
			return $result->max+1;
	}
	
	function save_collab_id($uid,$collab){
		$result = $this->db->query("SELECT collab_id FROM pre_users where id = ".$uid)->row();
		$collab_id = $result->collab_id;
		if($collab_id != $collab){
			$this->db->query("UPDATE pre_users SET collab_id = ".$collab." WHERE id = ".$uid);
			return $collab;
		}else{
			$this->db->query("UPDATE pre_users SET collab_id = ".($collab+1)." WHERE id = ".$uid);
			return $collab+1;
		}
	}
	
	function get_collab_list($grade_year,$education_type,$myid){
		$sql = "SELECT * FROM pre_users where id != ".$myid." and grade_year = ".$grade_year." and education_type = ".$education_type." and collab_id != '' AND user_belongs_group = 2";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_collab_count($collab_id){
		$sql = "SELECT count(*) as cnt FROM pre_collabs where collab_id = ".$collab_id;
		$result = $this->db->query($sql)->row();
		return $result->cnt;
	}
	
	function get_student_collab_list($collab_id,$cur_date){
		$sql = "SELECT * FROM pre_collabs where collab_id = ".$collab_id." and date = '".$cur_date."'";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_id_from_collab_id($collab_id){
		$sql = "SELECT * FROM pre_users where collab_id = ".$collab_id;
		$result = $this->db->query($sql)->row();
		return $result->id;
	}
	
	function get_collab_detail($id){
		$sql = "SELECT * FROM pre_collabs where id = ".$id;
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function update_fluid_link($data){
		$sql = "SELECT count(*) as cnt FROM pre_zoom_session WHERE booking_id = ".$data['booking_id']." and start_date = '".$data['date']."'";
		$result = $this->db->query($sql)->row();
		if($result->cnt > 0){
			$sql = "UPDATE pre_zoom_session SET fluidmath_link = '".$data['fluidmath_link']."' WHERE booking_id = ".$data['booking_id']." and start_date = '".$data['date']."' and student_id = ".$data['student_id'];
		}else{
			$sql = "INSERT INTO pre_zoom_session (fluidmath_link,start_date,booking_id,tutor_id,student_id) VALUES('".$data['fluidmath_link']."','".$data['date']."',".$data['booking_id'].",".$this->ion_auth->get_user_id().",".$data['student_id'].")";
		}
			
		$this->db->query($sql);
	}
	
	function get_previous_events($from_date,$to_date){
		$sql = "SELECT C.name, D.username, D.slug, A.*,B.tutor_zoom_link,B.fluidmath_link FROM pre_bookings A LEFT JOIN pre_zoom_session B ON A.booking_id = B.booking_id LEFT JOIN pre_categories C ON A.course_id = C.id LEFT JOIN pre_users D on A.student_id = D.id WHERE A.tutor_id = ".$this->ion_auth->get_user_id()." AND A.start_date >= '".$from_date."' AND A.start_date <= '".$to_date."' AND (A.status = 'session_initiated' OR A.status = 'running')";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function get_previous_events1($from_date,$to_date,$userid){
		$sql = "SELECT C.name, D.username,D.slug, A.time_slot,A.tution,A.content,A.start_date as start_date1,A.end_date,A.iq_id, B.* FROM pre_bookings A LEFT JOIN pre_zoom_session B ON A.booking_id = B.booking_id LEFT JOIN pre_categories C ON A.course_id = C.id LEFT JOIN pre_users D on A.tutor_id = D.id WHERE A.student_id = ".$userid." AND A.start_date >= '".$from_date."' AND A.start_date <= '".$to_date."' AND A.tution != 'Leave bank' UNION SELECT C.name, D.username,D.slug, A.time_slot,A.tution,A.content,A.start_date as start_date1,A.end_date,A.iq_id, B.* FROM pre_bookings A LEFT JOIN pre_zoom_session B ON A.booking_id = B.booking_id LEFT JOIN pre_categories C ON A.course_id = C.id LEFT JOIN pre_users D on A.tutor_id = D.id WHERE A.tutor_id = ".$userid." AND A.start_date >= '".$from_date."' AND A.start_date <= '".$to_date."' AND A.tution = 'Leave bank' GROUP BY A.time_slot,A.tution,A.content ";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function update_online_status($userID){
		$sql = "UPDATE pre_users SET is_online='yes' WHERE id = ".$userID;
		$this->db->query($sql);
	}
	
	function get_change_event($booking_id,$time_slot,$cur_date){
		$sql = "SELECT * FROM pre_change_event WHERE booking_id = ".$booking_id." and org_date = '".$cur_date."' and org_time_slot ='".$time_slot."' ORDER BY id desc LIMIT 1";
		$result = $this->db->query($sql)->row();
		return $result;
	}
	
	function get_add_change_event($booking_id,$cur_date){
		$sql = "SELECT * FROM pre_change_event WHERE booking_id = ".$booking_id." and date = '".$cur_date."' and org_date != date and type != 'delete'";
		$result = $this->db->query($sql)->result();
		return $result;
	}
	
	function reg_change_event($data){
		$sql = "SELECT count(*) as cnt FROM pre_change_event WHERE booking_id = ".$data['booking_id']." and org_time_slot = '".$data['org_time_slot']."' and org_date = '".$data['org_date']."'";
		$result = $this->db->query($sql)->row();
		if($result->cnt == 0){
			$this->insert_operation($data,'change_event');
		}else{
			$sql = "UPDATE pre_change_event SET date = '".$data['date']."', time_slot = '".$data['time_slot']."', type = '".$data['type']."' WHERE booking_id = ".$data['booking_id'];
			$this->db->query($sql);
		}
	}
	
	function get_date_format(){
		$sql = "SELECT * FROM pre_system_settings_fields WHERE field_key = 'Date_Format'";
		$result = $this->db->query($sql)->row();
		return $result->field_output_value;
	}
}
?>