<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Googleplus1
{

    /**
     * Googleplus constructor.
     */
    public function __construct()
    {
		
        require APPPATH."third_party/google-api-php/src/Google/autoload.php";
        $this->client = new Google_Client();
        $CI =& get_instance();
		$CI->config->load('googleplus');
        $this->client->setApplicationName($CI->config->item('application_name', 'googleplus'));
        
        $this->client->setClientId($CI->config->item('client_id', 'googleplus'));
        $this->client->setClientSecret($CI->config->item('client_secret', 'googleplus'));
        $this->client->setRedirectUri($CI->config->item('redirect_uri', 'googleplus'));
        $this->client->addScope("https://www.googleapis.com/auth/calendar");
        $this->client->addScope('profile');
        
    }

    public function loginUrl()
    {

        return $this->client->createAuthUrl();

    }

    public function getAuthenticate()
    {

        return $this->client->authenticate();

    }

    public function getAccessToken()
    {

        return $this->client->getAccessToken();

    }

    public function setAccessToken()
    {

        return $this->client->setAccessToken();

    }

    public function revokeToken()
    {

        return $this->client->revokeToken();

    }

    public function client()
    {

        return $this->client;

    }

    public function getUser()
    {

        $google_ouath = new Google_Service_Oauth2($this->client);

        return (object)$google_ouath->userinfo->get();

    }

    public function isAccessTokenExpired()
    {

        return $this->client->isAccessTokenExpired();

    }

}