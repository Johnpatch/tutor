
<!-- Dashboard panel -->
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'privileges', 'id' => 'privileges', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('student/privileges',$attributes);?>
		
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('course_sell_purch') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Course sell purch:</label>
				</div>
			</div>	
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'course_sell_purch' );
						}
						elseif( isset($profile->course_sell_purch) && !isset($_POST['submitbutt']))
						{
							$val = $profile->course_sell_purch;
						}
						echo form_dropdown('course_sell_purch', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('course_sell_purch');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('iq_live') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Iq live:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'iq_live' );
						}
						elseif( isset($profile->iq_live) && !isset($_POST['submitbutt']))
						{
							$val = $profile->iq_live;
						}
						echo form_dropdown('iq_live', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('iq_live');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('book_edit_status') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Book edit status:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'book_edit_status' );
						}
						elseif( isset($profile->book_edit_status) && !isset($_POST['submitbutt']))
						{
							$val = $profile->book_edit_status;
						}
						echo form_dropdown('book_edit_status', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('book_edit_status');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('g_sessions') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">G sessions:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'g_sessions' );
						}
						elseif( isset($profile->g_sessions) && !isset($_POST['submitbutt']))
						{
							$val = $profile->g_sessions;
						}
						echo form_dropdown('g_sessions', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('g_sessions');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('admin_claims') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Admin claims:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'admin_claims' );
						}
						elseif( isset($profile->admin_claims) && !isset($_POST['submitbutt']))
						{
							$val = $profile->admin_claims;
						}
						echo form_dropdown('admin_claims', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('admin_claims');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('my_leads') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">My leads:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'my_leads' );
						}
						elseif( isset($profile->my_leads) && !isset($_POST['submitbutt']))
						{
							$val = $profile->my_leads;
						}
						echo form_dropdown('my_leads', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('my_leads');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('iq_post') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Iq post:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'iq_post' );
						}
						elseif( isset($profile->iq_post) && !isset($_POST['submitbutt']))
						{
							$val = $profile->iq_post;
						}
						echo form_dropdown('iq_post', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('iq_post');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('packages') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Packages:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'packages' );
						}
						elseif( isset($profile->packages) && !isset($_POST['submitbutt']))
						{
							$val = $profile->packages;
						}
						echo form_dropdown('packages', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('packages');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('manage') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Manage:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'manage' );
						}
						elseif( isset($profile->manage) && !isset($_POST['submitbutt']))
						{
							$val = $profile->manage;
						}
						echo form_dropdown('manage', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('manage');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('account') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Account:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'account' );
						}
						elseif( isset($profile->account) && !isset($_POST['submitbutt']))
						{
							$val = $profile->account;
						}
						echo form_dropdown('account', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('account');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('enrolled_courses') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Enrolled courses:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'enrolled_courses' );
						}
						elseif( isset($profile->enrolled_courses) && !isset($_POST['submitbutt']))
						{
							$val = $profile->enrolled_courses;
						}
						echo form_dropdown('enrolled_courses', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('enrolled_courses');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($this->session->userdata('user_belongs_group') == 5 && $this->session->userdata('collabs') == 'Yes'){ ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">My Collabs:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<div class="dark-picker dark-picker-bright">
						<?php 
						$val = '';
						if( isset($_POST['submitbutt']) )
						{
							$val = $this->input->post( 'collabs' );
						}
						elseif( isset($profile->collabs) && !isset($_POST['submitbutt']))
						{
							$val = $profile->collabs;
						}
						echo form_dropdown('collabs', array('No'=>'No', 'Yes'=>'Yes'), $val, 'class="select-picker" ');
						?>
						<?php echo form_error('collabs');?>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>

		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
</script>
<!-- Dashboard panel ends -->