<!-- Dashboard panel -->
<style>
	.collab_table{
		text-align: center;
	}
	.collab_table tr td,
	.collab_table tr th{
		border:1px solid black;
	}
</style>
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'my_new_collabs', 'id' => 'my_new_collabs', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('student/new_collab',$attributes);?>
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Reason for Collab:');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'reason' );
					}
					
					$element = array(
						'name'	=>	'reason',
						'id'	=>	'reason',
						'value'	=>	$val,
						'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword(''),
					);			
					echo form_input($element);
					?>
					<?php echo form_error('reason');?>
				</div>
			</div>
			
			<div class="col-sm-6 " id="iq_date_div">
				<div class="input-group ">
					<label><?php echo get_languageword('Date');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post('date' );
					}else{
						$val = $default_date;
					}
					
					$element = array(
						'name'	=>	'date',
						'id'	=>	'date',
						'value'	=>	$val,
						'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('2019-03-01'),
					);
					echo form_input($element);
					?>
					<?php echo form_error('date');?>
				</div>
			</div>
			
			<div class="col-sm-6 " id="iq_time_slot_div">
				<div class="input-group ">
					<label><?php echo get_languageword('TimeSlot');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'time_slot' );
					}
					$element = array(
						'name'	=>	'time_slot',
						'id'	=>	'time_slot',
						'value'	=>	$val,
						'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('Example format 6-7,13-14,14-16,20.30-21.30')
					);
					echo form_input($element);
					?>
					<?php echo form_error('time_slot');?>
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Credits');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'credits' );
					}
					$element = array(
						'name'	=>	'credits',
						'id'	=>	'credits',
						'value'	=>	$val,
						//'required' => 'required',
						'readonly' => 'readonly',
						'class' => 'form-control',
						'placeholder' => get_languageword('FREE'),
					);			
					echo form_input($element);
					?>
					<?php echo form_error('credits');?>
				</div>
			</div>
			
			<table class="col-sm-12 collab_table" style="border-collapse: collapse;">
				<thead>
					<tr>
						<td>Choose</td>
						<td>Collab ID</td>
						<td>Collab Count</td>
						<td>Status</td>
					</tr>
				</thead>
				<tbody>
				<?php
				if(!empty($collab_list)){
					foreach($collab_list as $key => $value){ ?>
						<tr>
							<td>
								<div class="checkbox">
									<label>
									<?php if($value->collab_status == "AVAILABLE") {?>
										<input type="checkbox" value="<?php echo $value->collab_id;?>" name="collabs_list[]">
									<?php } ?>
										<span class="checkbox-content">
											<span class="item-content"></span>
											<?php if($value->collab_status == "AVAILABLE") {?>
												<i aria-hidden="true" class="fa fa-check "></i>
											<?php } ?>
											<i class="check-square"></i>
										</span>
									</label>
								</div>
							</td>
							<td><?php echo $value->grade_year."-".$value->collab_id; ?></td>
							<td><?php echo $value->collab_count;?></td>
							<td><?php echo $value->collab_status;?></td>
						</tr>
				<?php } } ?>
				</tbody>
			</table>
				
			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>

		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
	$(function() {
       $("#date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
   });
	
</script>
<!-- Dashboard panel ends -->