<!-- Dashboard panel -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    	<form action="<?php echo site_url('student/index');?>" class="comment-form" method="post" >
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Child List</h4>
		      </div>
		      <div class="modal-body">
		        
		      </div>
		      <div class="modal-footer">
		        <button type="submit" class="btn btn-info" name="submitBtn" >OK</button>
		      </div>
		</form>
    </div>

  </div>
</div>

<div class="dashboard-panel">
	<?php echo $message;?>
	<a data-toggle="modal" data-target="#myModal" style="display: none;" id="child_modal"></a>
	<div class="row">
		<div class="col-md-4 pad10">
			<div class="dash-block d-block1">
				<h2><?php
				if($this->session->userdata('user_belongs_group') == 5)
					echo $child_details->net_credits;
				else
					echo $this->session->userdata('net_credits');
				?></h2>
                <p><?php echo get_languageword('Net Credits');?></p>
			</div>
		</div>	
		<div class="col-md-4 pad10">
			<div class="dash-block d-block2">
				<h2><?php echo $student_dashboard_data['total_bookings'];?><a class="pull-right" href="<?php echo base_url();?>enquiries"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Total Bookings');?></p>
			</div>
		</div>
		<div class="col-md-4 pad10">
			<div class="dash-block d-block3">
				<h2><?php echo $student_dashboard_data['pending_bookings']?><a class="pull-right" href="<?php echo base_url();?>enquiries/pending"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Bookings Pending');?>  </p>
			</div>
		</div>
		<div class="col-md-4 pad10">
			<div class="dash-block d-block4">
				<h2><?php echo $student_dashboard_data['completed_bookings'];?><a class="pull-right" href="<?php echo base_url();?>enquiries/completed"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Bookings Completd');?></p>
			</div>
		</div>
		<div class="col-md-4 pad10">
			<div class="dash-block d-block5">
				<h2><?php echo $student_dashboard_data['approved_bookings'];?><a class="pull-right" href="<?php echo base_url();?>enquiries/approved"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Bookings Approved');?></p>
			</div>
		</div>	
		<div class="col-md-4 pad10">
			<div class="dash-block d-block6">
				<h2><?php echo $student_dashboard_data['open_leads'];?><a class="pull-right" href="<?php echo base_url();?>student/student_leads"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Open Leads');?></p>
			</div>
		</div>
		
		<div class="col-md-4 pad10">
			<div class="dash-block d-block7">
				<h2><?php echo $student_dashboard_data['closed_leads'];?><a class="pull-right" href="<?php echo base_url();?>student/student_leads"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Closed Leads');?></p>
			</div>
		</div>

		<div class="col-md-4 pad10">
			<div class="dash-block d-block8">
				<h2><?php echo $student_dashboard_data['inst_enrolled'];?><a class="pull-right" href="<?php echo base_url();?>student/enrolled-courses"><?php echo get_languageword('View');?></a></h2>
				<p><?php echo get_languageword('Institue Enrolled Courses');?></p>
			</div>
		</div>

		
	</div>

</div>
<script src="<?php echo URL_ADMIN_JS;?>jquery-1.11.1.min.js"></script>
<script>
	var user_type = "<?php echo $_SESSION['user_belongs_group'];?>";
	var first_login = "<?php echo $_SESSION['first_login'];?>";
	if(user_type == "5" && first_login == "1"){
		$.ajax({
	        url:"<?php echo URL_STUDENT_SELECT_CHILD; ?>",
	        data:{
	        },
	        type:"post", 
	        success :function(data){
				var obj = JSON.parse(data);
				var html = '<div class="radio">';
				if(obj.status == true){
					$('#child_modal').trigger('click');
					for(var i = 0;i<obj.list.length;i++){
						html += '<label><input type="radio" value="'+obj.list[i].id+'" name="child"><span class="radio-content"><span class="item-content">'+obj.list[i].first_name+' '+obj.list[i].last_name+'</span><i aria-hidden="true" class="fa uncheck fa-circle-thin"></i><i aria-hidden="true" class="fa check fa-dot-circle-o"></i></span></label><br>';	
					}
					html += "</div>";
					$('#myModal .modal-body').append(html);
				}
	        }
	    })	
	}
</script>
<!-- Dashboard panel ends --> 