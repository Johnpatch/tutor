
<!-- Dashboard panel -->
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'new_instant_bids', 'id' => 'new_instant_bids', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('student/new_instant_bids',$attributes);?>
			<div class="col-sm-6 ">
				<div class="input-group ">
						<label><?php echo get_languageword('Preferred Teaching type');?> <sup>*</sup>:</label>
						<div class="dark-picker dark-picker-bright">
							<?php 
							$val = '';
							if( isset($_POST['submitbutt']) )
							{
								$val = $this->input->post( 'teaching_type_id' );
							}
							elseif( isset($profile->teaching_type_id) && !isset($_POST['submitbutt']))
							{
								$val = $profile->teaching_type_id;
							}
							echo form_dropdown('teaching_type_id', array('Online ZOOM'=>get_languageword('online ZOOM'), 'Record'=>get_languageword('Recorded')), $val, 'class="select-picker"  onchange="teaching_type_sel()" id="teaching_type"');
							?>
							<?php echo form_error('teaching_type_id');?>
						</div>
				</div>
			</div>
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Priority of Requirement');?><?php echo required_symbol();?>:</label>
					<div class="dark-picker dark-picker-bright">
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'priority_of_requirement' );
					}
					elseif( isset($profile->priority_of_requirement) && !isset($_POST['submitbutt']))
					{
						$val = $profile->priority_of_requirement;
					}
					echo form_dropdown('priority_of_requirement', array('Immediately' => get_languageword('Immediately'), 'By Date & Time' => get_languageword('By Date & Time')), $val, 'class="select-picker" onchange="priority_sel();" id="priority"');
					?>
					<?php echo form_error('priority_of_requirement');?>
				   </div>
				</div>
			</div>
			
			<div class="col-sm-6 " id="priority_date" style="display: none;">
				<div class="input-group ">
					<label><?php echo get_languageword('Date');?>:</label>
					<?php
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'date' );
					}
					elseif( isset($profile->date) && !isset($_POST['submitbutt']))
					{
						$val = $profile->date;
					}
					$element = array(
						'name'	=>	'date',
						'id'	=>	'date',
						'value'	=>	$val,
						//'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('2019-03-01'),
					);
					echo form_input($element);
					?>
					
				</div>
			</div>
			
			<div class="col-sm-6 " id="priority_time" style="display: none;">
				<div class="input-group ">
					<label><?php echo get_languageword('Time');?>:</label>
					<?php
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'time' );
					}
					elseif( isset($profile->time) && !isset($_POST['submitbutt']))
					{
						$val = $profile->time;
					}
					$element = array(
						'name'	=>	'time',
						'id'	=>	'time',
						'value'	=>	$val,
						//'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('15:30'),
					);
					echo form_input($element);
					?>
					
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
						<label><?php echo get_languageword('Categories');?> </label>
						<div class="dark-picker dark-picker-bright">
							<?php 
							$val = '';
							if( isset($_POST['submitbutt']) )
							{
								$val = $this->input->post( 'category_id' );
							}
							elseif( isset($profile->category) && !isset($_POST['submitbutt']))
							{
								$val = $profile->category;
							}
							echo form_dropdown('category_id', $categories, $val, 'class="select-picker"');
							?>
							<?php echo form_error('category_id');?>
						</div>
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Payment Type');?></label>
					<div class="dark-picker dark-picker-bright">
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post('budget_type');
					}
					elseif( isset($profile->budget_type) && !isset($_POST['submitbutt']))
					{
						$val = $profile->budget_type;
					}
					echo form_dropdown('budget_type', array('One Time' => get_languageword('One Time')), $val, 'class="select-picker"');
					?>
					<?php echo form_error('budget_type');?>
					</div>
				</div>
			</div>

			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('title_of_requirement');?>:</label>
					<?php
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'title_of_requirement' );
					}
					elseif( isset($profile->title_of_requirement) && !isset($_POST['submitbutt']))
					{
						$val = $profile->title_of_requirement;
					}
					$element = array(
						'name'	=>	'title_of_requirement',
						'id'	=>	'title_of_requirement',
						'value'	=>	$val,
						//'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('title_of_requirement'),
					);
					echo form_input($element);
					?>
					
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Minimum Bid in Credits:');?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'budget' );
					}
					elseif( isset($profile->budget) && !isset($_POST['submitbutt']))
					{
						$val = $profile->budget;
					}
					$element = array(
						'name'	=>	'budget',
						'id'	=>	'budget',
						'value'	=>	$val,
						//'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('credits'),
					);			
					echo form_input($element);
					?>
					<?php echo form_error('budget');?>
				</div>
			</div>
			
			<div class="col-sm-6 " id="iq_date_div">
				<div class="input-group ">
					<label><?php echo get_languageword('Preferred Slot: Start Date');?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'iq_date' );
					}
					elseif( isset($profile->iq_date) && !isset($_POST['submitbutt']))
					{
						$val = $profile->iq_date;
					}
					$element = array(
						'name'	=>	'iq_date',
						'id'	=>	'iq_date',
						'value'	=>	$val,
						'class' => 'form-control',
						'placeholder' => get_languageword('2019-03-01'),
					);
					echo form_input($element);
					?>
					<?php echo form_error('iq_date');?>
				</div>
			</div>
			
			<div class="col-sm-6 " id="iq_time_slot_div">
				<div class="input-group ">
					<label><?php echo get_languageword('Preferred Slot: Start Time');?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'iq_time_slot' );
					}
					elseif( isset($profile->iq_time_slot) && !isset($_POST['submitbutt']))
					{
						$val = $profile->iq_time_slot;
					}
					$element = array(
						'name'	=>	'iq_time_slot',
						'id'	=>	'iq_time_slot',
						'value'	=>	$val,
						'class' => 'form-control',
						'placeholder' => get_languageword('Example format 6-7,13-14,14-16,20.30-21.30')
					);
					echo form_input($element);
					?>
					<?php echo form_error('iq_time_slot');?>
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Attach');?></label>
					<input type="file" name="attach[]" style="background: none;" multiple>
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Tutor Rating');?></label>
					<div class="dark-picker dark-picker-bright">
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post('tutor_rating');
					}
					elseif( isset($profile->tutor_rating) && !isset($_POST['submitbutt']))
					{
						$val = $profile->tutor_rating;
					}
					echo form_dropdown('tutor_rating', array('' => get_languageword('Any'),'5' => get_languageword('5 Star'),'4' => get_languageword('4 Star'),'3' => get_languageword('3 Star'),'2' => get_languageword('2 Star'),'1' => get_languageword('1 Star'),), $val, 'class="select-picker"');
					?>
					<?php echo form_error('tutor_rating');?>
					</div>
				</div>
			</div>

			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Requirement Details');?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'requirement_details' );
					}
					elseif( isset($profile->requirement_details) && !isset($_POST['submitbutt']))
					{
						$val = $profile->requirement_details;
					}
					$element = array(
						'name'	=>	'requirement_details',
						'id'	=>	'requirement_details',
						'value'	=>	$val,
						'maxlength'	=> 200,
						'rows'	=>5,
						//'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('requirement_details'),
					);
					echo form_textarea($element);
					?>
					<?php echo form_error('requirement_details');?>
				</div>
			</div>
			
			

			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>

		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
	function priority_sel(){
		if($('#priority').val() == "Immediately"){
			$('#priority_date').css('display','none');
			$('#priority_time').css('display','none');
		} else{
			$('#priority_date').css('display','block');
			$('#priority_time').css('display','block');
		}
	}
	function teaching_type_sel(){
		if($('#teaching_type').val() == "Record"){
			$('#iq_date_div').css('display','none');
			$('#iq_time_slot_div').css('display','none');
		} else{
			$('#iq_date_div').css('display','block');
			$('#iq_time_slot_div').css('display','block');
		}
	}
	$(function() {
        $("#iq_date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
       $("#date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
   });
	
</script>
<!-- Dashboard panel ends -->