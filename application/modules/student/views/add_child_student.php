
<!-- Dashboard panel -->
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'add_child_student', 'id' => 'add_child_student', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('student/add_child_student',$attributes);?>
			<?php foreach($child_list as $key => $value){ ?>
				<div class="col-sm-4 ">
					<div class="input-group ">
						<label style="margin-top: 10px;">Child Student Email ID <?php echo $key+1;?>:</label>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="input-group ">
						<input type="text" name="child_email<?php echo $key;?>" value="<?php echo $value->email;?>" class="form-control" >
					</div>
				</div>
			<?php } ?>
			<div class="col-sm-4 ">
				<div class="input-group ">
					<label style="margin-top: 10px;">Child Student Email ID <?php echo count($child_list)+1;?>:</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="input-group ">
					<input type="text" name="child_email<?php echo count($child_list);?>" value="" class="form-control" >
					<input type="hidden" name="new_add_child" value="<?php echo count($child_list);?>">
				</div>
			</div>
			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>

		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
</script>
<!-- Dashboard panel ends -->