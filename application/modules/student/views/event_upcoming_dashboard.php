<!-- Event Dashboard panel -->
<div class="dashboard-panel">
	<form method="post" class="comment-form dark-fields">
		<div class="row">
			<label style="font-size:25px;">Upcoming Events:</label>
		</div>
		<?php
		foreach($upcoming_events as $key => $value){ 
		if(!empty($value->student_date)){?>
		<div class="row" style="border:1px solid #14bdee;margin-bottom:10px;">
			<?php if($value->tution != 'Leave bank'){ ?>
				<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Course Title:<?php echo $value->course_name;?></div>
				<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Tutor Name:<a href="<?php echo site_url('user-profile/'.$value->slug);?>"><?php echo $value->username;?></a></div>
			<?php } else { ?>
				<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Collab Request:<?php echo $value->content;?></div>
				<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Student Name:<a href="<?php echo site_url('user-profile/'.$value->slug);?>"><?php echo $value->username;?></a></div>
			<?php } ?>
			
			<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Date:<?php echo date($date_format,strtotime($value->student_date));?></div>
			<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Time Slot:<?php echo $value->student_time_slot;?></div>
			
			<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">
				<?php if($my_profile->whiteboard == 'Yes'){ ?>
				<a href="<?php echo $value->whiteboard_url?>" target="_blank">
					<span class="nav-btn" style="padding: 0px 24px;"><img src="<?php echo base_url();?>assets/front/images/whiteboard.png" style="width:17px;"/> &nbsp; Whiteboard</span>
                </a>
                <?php } ?>
                
                <?php if($my_profile->zoom_id == 'Yes' && $value->tution != 'Leave bank'){ 
                if(!empty($value->tutor_zoom_link)){ ?>
				<a href="<?php  echo $value->tutor_zoom_link;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span></a>
				<?php }else if(!empty($value->zoom_pim_url)){ ?>
				<a href="<?php  echo $value->zoom_pim_url;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span></a>
				<?php } else{?>
					<span class="nav-btn" style="padding: 0px 18px;cursor: pointer;" onclick="no_zoom()"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span>
				<?php } }?>
				
				<?php if($my_profile->fluidmath == 'Yes' && $value->tution != 'Leave bank'){
				 if(!empty($value->fluidmath_link)){ ?>
				<a href="<?php  echo $value->fluidmath_link;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span></a>
				<?php }else if(!empty($value->fluidmath_url)){ ?>
				<a href="<?php  echo $value->fluidmath_url;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span></a>
				<?php } else{?>
					<span class="nav-btn" style="padding: 0px 18px;cursor: pointer;" onclick="no_fluid()"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span>
				<?php } }?>
			
			</div>
			<?php if($value->tution != 'Leave bank'){ ?>
			<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">ZOOM Joining Details:</div>
			<div class="col-sm-12"><textarea style="width:100%;height: 150px;" readonly><?php if(!empty($value->description)){echo strip_tags($value->description);}else{echo strip_tags($value->zoom_joining_details);}?></textarea></div>
			<?php } ?>
		</div>
		<?php }} ?>
		
	</form>
</div>
<!-- Event Dashboard panel ends --> 