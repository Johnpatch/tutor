<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require __DIR__.'../../../../libraries/jwt/autoload.php';
use \Firebase\JWT\JWT;
class Student extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->library(array('session'));
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->model('home_model');
		$this->load->model('base_model');
		$this->data['my_profile'] = getUserRec();
		
		
	}
	/**
	 * Generate the index page
	 *
	 * @access	public
	 * @return	string
	 */
	function index($child_id  = 0)
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		if($child_id != 0){
			$this->session->set_userdata('child_id',$child_id);
			$this->session->set_userdata('first_login',0);
			$student_details = $this->base_model->get_user_info($child_id);
			$this->session->set_userdata('child_info',$student_details);
			redirect('student/index', 'refresh');
		} else if($this->session->userdata('child_id') == 0 && $this->session->userdata('user_belongs_group') == 5){
			//redirect('auth/logout', 'refresh');
		}
		
		$user_id = $this->ion_auth->get_user_id();
		$my_relation_status = $this->base_model->get_parent_status($user_id);
		$this->session->set_userdata('my_relation_status',$my_relation_status);

		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5){
			if(empty($this->session->userdata('child_id'))){
				$user_id = $this->ion_auth->get_user_id();
				$email = $this->session->userdata('email');
				$child_list = $this->home_model->get_child_list($email);
				if(count($child_list) == 1){
					$this->session->set_userdata('child_id',$child_list[0]->id);
					$this->session->set_userdata('first_login',0);
					$this->session->set_userdata('child_info',$child_list[0]);
					$this->data['child_details'] = getUserRec($user_id);
				}
				else if(count($child_list) > 1){
					$this->data['child_list'] = $child_list;
				}
			}
			else{
				$user_id = $this->session->userdata('child_id');
				$this->data['child_details'] = getUserRec($user_id);
			}
		}
		else
			$user_id = $this->ion_auth->get_user_id();
		
		$this->load->model('student_model');
		$student_dashboard_data = $this->student_model->get_student_dashboard_data($user_id);
		$this->data['student_dashboard_data']	= $student_dashboard_data;	
		
		$profile = getUserRec();

		$this->data['pagetitle'] 	= get_languageword('dashboard');
		$this->data['activemenu'] 	= "dashboard";
		$this->data['content'] 		= 'index';
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to upload gallery pictures
	 *
	 * @access	public
	 * @return	string
	 */
	function my_gallery()
	{
			
		$this->data['activemenu'] 	= "account";
		$this->data['pagetitle'] = get_languageword('My Gallery');
		

		$this->load->library('Image_crud');

		$image_crud = new image_CRUD();
		$image_crud->unset_jquery(); //As we are using our jQuery bundle we need to unset default jQuery
		
		$image_crud->set_table($this->db->dbprefix('gallery'));
		$image_crud->set_relation_field('user_id');
		$image_crud->set_ordering_field('image_order');
		
		$image_crud->set_title_field('image_title');
		$image_crud->set_primary_key_field('image_id');
		$image_crud->set_url_field('image_name');
		$image_crud->set_image_path('assets/uploads/gallery');
		$output = $image_crud->render();
		$output->grocery = TRUE;
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->data['activesubmenu'] 	= "gallery";
		$this->grocery_output($this->data);

	}

	/**
	 * Fecilitates to update student leads
	 * @access	public
	 * @return	string
	 */
	function student_leads()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['profile'] = getUserRec();
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table($this->db->dbprefix('student_leads'));
		$crud->where('user_id', $user_id);
		$crud->set_relation('teaching_type_id','teaching_types','teaching_type');
		$crud->set_relation('location_id','locations','location_name');
		$crud->set_relation('course_id','categories','name');
		$crud->set_subject( get_languageword('student_leads') );
		
		$crud->unset_add();
		$crud->unset_delete();
		 
    			
		$crud->columns('course_id','teaching_type_id','location_id','title_of_requirement','updated_at','duration_needed', 'no_of_views','status');

		

		//########Eidt fields only#######
		$crud->edit_fields('status', 'updated_at');

		$crud->field_type('updated_at', 'hidden', date('Y-m-d H:i:s'));

		//####### Changing column names #######
		$crud->display_as('updated_at','Last Updated');
		$crud->display_as('course_id','Course Name');
		$crud->display_as('teaching_type_id','Teaching Type');
		$crud->display_as('location_id','Location');
		$crud->display_as('duration_needed','Duration');


		//#### Invisible fileds in reading ####
		if ($crud->getState() == 'read') {
		    $crud->field_type('user_id', 'hidden');
		    $crud->field_type('updated_at', 'visible');
		}


		$output = $crud->render();
		
		$this->data['activemenu'] 	= "myleads";
		$this->data['activesubmenu'] 	= "student_leads";
		$this->data['content'] 		= 'student_leads';
		$this->data['pagetitle'] = get_languageword('Student Leads');
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}

	
	/**
	 * Fecilitates to update personal information
	 *
	 * @access	public
	 * @return	string
	 */
	function post_requirement()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');

		if($this->session->userdata('user_belongs_group') == 5)
			$student_id = $this->session->userdata('child_id');
		else
			$student_id = $this->ion_auth->get_user_id();

		if(isset($_POST['submitbutt']))
		{
			$this->form_validation->set_rules('location_id', get_languageword('location'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('teaching_type_id', get_languageword('teaching_type'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('priority_of_requirement', get_languageword('priority_of_requirement'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('duration_needed', get_languageword('duration_needed'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('title_of_requirement', get_languageword('title_of_requirement'),'trim|required|xss_clean');
			$this->form_validation->set_rules('course_id', get_languageword('course'), 'trim|required|xss_clean');
						
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{	
				

				$inputdata['user_id']=$student_id;
				$inputdata['location_id'] = $this->input->post('location_id');
				$inputdata['course_id'] = $this->input->post('course_id');
				$inputdata['teaching_type_id'] = $this->input->post('teaching_type_id');
				$inputdata['present_status']=$this->input->post('present_status');
				$inputdata['priority_of_requirement']=$this->input->post('priority_of_requirement');
				$inputdata['duration_needed']=$this->input->post('duration_needed');
				$inputdata['budget']=$this->input->post('budget');
				$inputdata['budget_type']=$this->input->post('budget_type');
				$inputdata['requirement_details']=$this->input->post('requirement_details');
				$inputdata['title_of_requirement']=$this->input->post('title_of_requirement' );
				$inputdata['created_at']=date("Y-m-d H:i:s");
				$inputdata['updated_at']=$inputdata['created_at'];

				$is_duplicate = $this->base_model->fetch_records_from('student_leads', array('location_id' => $inputdata['location_id'], 'course_id' => $inputdata['course_id'], 'teaching_type_id' => $inputdata['teaching_type_id'], 'budget' => $inputdata['budget'], 'budget_type' => $inputdata['budget_type'], 'priority_of_requirement' => $inputdata['priority_of_requirement'], 'status' => 'Opened'));

				if(count($is_duplicate) > 0) {

					$this->prepare_flashmessage(get_languageword('you_have_already_posted_the_same_requirement'), 2);
					redirect(URL_STUDENT_LEADS);
				}


				//05-12-2018 admin notification start
				$lead_id = $this->base_model->insert_operation_id($inputdata, 'student_leads');

				$data = array();
				$data['user_id'] 	= $student_id;
				$data['title'] 		= get_languageword('student_posted_new_lead');
				$data['content'] 	= "Student has been Posted a Lead "." ".$this->input->post('title_of_requirement');
				$data['datetime']   = date('Y-m-d H:i:s');
				$data['admin_read'] = 0;
				$data['page_link']  = SITEURL."admin/all_leads";
				$data['table_name'] = "student_leads";
				$data['primary_key_column'] = "id";
				$data['primary_key_value']  = $lead_id;

				$this->base_model->insert_operation($data,'notifications');	
				unset($data);
				//admin notification end



				$this->prepare_flashmessage(get_languageword('your requirement posted successfully'), 0);
				redirect('student/student-leads');	
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}			
		
		}	
		$this->data['profile'] = getUserRec($student_id);

		//location options
		$locations = $this->home_model->get_locations($params = array('child' => 1));
		$location_opts[''] = get_languageword('select_location');
		foreach ($locations as $key => $value) {
			$location_opts[$value->id] = $value->location_name;
		}
		$this->data['location_opts'] = $location_opts;
		
		//Course Options
		$courses = $this->home_model->get_courses();
		$course_opts[''] = get_languageword('type_of_course');
		foreach ($courses as $key => $value) {
			$course_opts[$value->id] = $value->name;
		}
		$this->data['course_opts'] = $course_opts;
		//Teaching type Options
		$teaching_types = $this->base_model->fetch_records_from('teaching_types');
		$teaching_types_options = array();
		foreach ($teaching_types as $key => $value) {
			$teaching_types_options[$value->id] = $value->teaching_type;
		}
		$this->data['teaching_types_options'] = $teaching_types_options;

		$this->data['activemenu'] 	= "myleads";
		$this->data['activesubmenu'] = "post_requirement";
		$this->data['content'] 		= 'post_requirement';
		$this->data['pagetitle'] = get_languageword('Post Requirement');
		$this->_render_page('template/site/student-template', $this->data);


	}



	/**
	 * Fecilitates to update personal information
	 *
	 * @access	public
	 * @return	string
	 */
	function personal_info()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$this->data['message'] = $this->session->flashdata('message');

		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();


		if(isset($_POST['submitbutt']))
		{
			$this->form_validation->set_rules('first_name', get_languageword('first_name'), 'trim|required|xss_clean');
			//$this->form_validation->set_rules('dob', get_languageword('date_of_birth'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('education_type', get_languageword('education_type'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('grade_year', get_languageword('grade_year'), 'trim|required|xss_clean');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{
				$first_name = ucfirst(strtolower($this->input->post('first_name')));
				$last_name = ucfirst(strtolower($this->input->post('last_name')));
				$username =  $first_name.' '.$last_name;

				$prev_username = $this->base_model->fetch_value('users', 'username', array('id' => $user_id));

				//If user updates the username
				if($prev_username != $username) {
					$slug = prepare_slug($username, 'slug', 'users');
					$inputdata['slug'] =  $slug;
				}

				$inputdata['first_name'] = $first_name;
				$inputdata['last_name'] = $last_name;
				$inputdata['username'] = $username;
				$inputdata['gender'] = $this->input->post('gender');
				$inputdata['dob'] = $this->input->post('dob');
				$inputdata['website'] = $this->input->post('website');
				$inputdata['facebook'] = $this->input->post('facebook');
				$inputdata['twitter'] = $this->input->post('twitter');
				$inputdata['linkedin'] = $this->input->post('linkedin');
				$inputdata['skype'] = $this->input->post('skype');
				$inputdata['country'] = $this->input->post('country');
				$inputdata['time_zone'] = $this->input->post('time_zone');
				$inputdata['grade_year'] = $this->input->post('grade_year');
				$inputdata['education_type'] = $this->input->post('education_type');
				
				$language_of_teaching = $this->input->post('language_of_teaching');
				if(!empty($language_of_teaching))
				$inputdata['language_of_teaching'] = implode(',', $language_of_teaching);

				
				$this->base_model->update_operation($inputdata, 'users', array('id' => $user_id));
				
				$this->prepare_flashmessage(get_languageword('profile updated successfully'), 0);
				redirect('student/personal-info');				
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}
		}	
		$this->data['profile'] = getUserRec($user_id);
		
		//Preparing Language options
		$lng_opts = $this->db->get_where('languages',array('status' => 'Active'))->result();
		$options = array();
		if(!empty($lng_opts))
		{
			foreach($lng_opts as $row):
				$options[$row->name] = $row->name;
			endforeach;
		}
		
		$countries = $this->base_model->fetch_records_from('country');
		$country_list = array();
		foreach ($countries as $key => $value) {
			$country_list[$key.'_'.$value->phonecode] = $value->nicename;
		}
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$this->data['timezone_list'] = $time_zone_list;
		$this->data['countries'] = $country_list;
		
		$education_type = array();
		$education_type[0] = "School";
		$education_type[1] = "College-UG";
		$education_type[2] = "College-Masters";
		$this->data['education_type'] = $education_type;
		
		$grade_year = array();
		$grade_year[''] = '';
		for($i=1;$i<=12;$i++){
			$grade_year[$i] = $i."st Grade";
		}
		$this->data['grade_year1'] = $grade_year;
		$grade_year = array();
		$grade_year[''] = '';
		$grade_year[1] = "Freshman Year";
		$grade_year[2] = "Sophomore Year";
		$grade_year[3] = "Junior Year";
		$grade_year[4] = "Senior Year";
		$this->data['grade_year2'] = $grade_year;

		$this->data['language_options'] = $options;
		$this->data['activemenu'] 	= "account";
		$this->data['activesubmenu'] = "personal_info";
		$this->data['content'] 		= 'personal_info';
		$this->data['pagetitle'] = get_languageword('Personal Information');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to update profile information includes profile picture
	 *
	 * @access	public
	 * @return	string
	 */
	function profile_information()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['message'] = $this->session->flashdata('message');
		
		if(isset($_POST['submitbutt']))
		{

			$this->form_validation->set_rules('profile', get_languageword('profile_description'), 'trim|required|max_length[500]|xss_clean');
			$this->form_validation->set_rules('seo_keywords',get_languageword('seo_keywords'), 'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('meta_desc',get_languageword('meta_description'),'trim|max_length[100]|xss_clean');
			$this->form_validation->set_rules('profile_page_title', get_languageword('profile_page_title'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('willing_to_travel', get_languageword('How far are you willing to travel'), 'trim|required|xss_clean');
			if($_FILES['photo']['name'] != '')
			{
				$this->form_validation->set_rules('photo', get_languageword('Profile Image'), 'trim|callback__image_check');
			}

			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{
				$inputdata['profile'] = $this->input->post('profile');
				$inputdata['profile_page_title'] = $this->input->post('profile_page_title');
				$inputdata['seo_keywords'] = $this->input->post('seo_keywords');
				$inputdata['meta_desc'] = $this->input->post('meta_desc');
				$inputdata['willing_to_travel'] = $this->input->post('willing_to_travel');
				$inputdata['qualification'] = $this->input->post('qualification');
				$inputdata['own_vehicle'] = $this->input->post('own_vehicle');
				
				$image 	= $_FILES['photo']['name'];
				//Upload User Photo
				if (!empty($image)) 
				{
					$ext = pathinfo($image, PATHINFO_EXTENSION);
					$file_name = $user_id.'.'.$ext;
					$config['upload_path'] 		= 'assets/uploads/profiles/';
					$config['allowed_types'] 	= 'jpg|jpeg|png';
					$config['overwrite'] 		= true;
					$config['file_name']        = $file_name;
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('photo'))
					{
						$inputdata['photo']		= $file_name;
						$this->create_thumbnail($config['upload_path'].$config['file_name'],'assets/uploads/profiles/thumbs/'.$config['file_name'], 200, 200);		
					}
				}				
				$this->base_model->update_operation($inputdata, 'users', array('id' => $user_id));
				
				$this->prepare_flashmessage(get_languageword('profile updated successfully'), 0);
				redirect('student/profile-information');				
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}
		}	
		$this->data['profile'] = getUserRec($user_id);
		$degrees = array();
		$records = $this->base_model->fetch_records_from('terms_data', array('term_type' => 'degree', 'term_status' => 'Active'));
		if(!empty($records))
		{
			foreach($records as $record)
			{
				$degrees[$record->term_id] = $record->term_title;
			}
		}
		$this->data['degrees'] = $degrees;
		
		$years = array();
		for($y = 0; $y < 100; $y++)
		{
			$year = date('Y');
			$years[$year-$y] = $year-$y;
		}
		$this->data['years'] = $years;
		$this->data['activemenu'] 	= "account";
		$this->data['activesubmenu'] = "profile_information";
		$this->data['content'] 		= 'profile_information';
		$this->data['pagetitle'] = get_languageword('Profile Information');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	public function _image_check()
	{
		$image = $_FILES['photo']['name'];
		$name = explode('.',$image);
		
		if(count($name)>2 || count($name)<= 0) {
           $this->form_validation->set_message('_image_check', 'Only jpg / jpeg / png images are accepted');
            return FALSE;
        }
		
		$ext = $name[1];
		
		$allowed_types = array('jpg','jpeg','png');
		
		if (!in_array($ext, $allowed_types))
		{			
			
			$this->form_validation->set_message('_image_check', 'Only jpg / jpeg / png images are accepted.');
			
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	/**
	 * [manage_courses description]
	 * @return [type] [description]
	 */
	function manage_courses()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->model('student_model');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$studnentPrefferedCourseIds	= $this->student_model->get_student_preffered_course_ids($user_id); //Getting locaiton ids
		

		if ($this->input->post()) 
		{		
			if ($this->input->post('student_courses')) {
				
				if ($this->input->post('student_courses') != $studnentPrefferedCourseIds) {
					$student_courses 	= $this->input->post('student_courses');
					if ($this->base_model->delete_record_new('student_preffered_courses', array('student_id'=> $user_id))) {
						$data['student_id'] 	= $user_id;
						$data['created_at'] = date('Y-m-d H:i:s');
						foreach($student_courses as $courses) {
							if (is_numeric($courses)) {
								$data['course_id'] = $courses;
								$this->base_model->insert_operation($data, 'student_preffered_courses');
							}
						}

						$is_profile_updated = $this->ion_auth->user($user_id)->row()->is_profile_update;

						if($is_profile_updated != 1) {

							$stu_pref_teaching_types = $this->base_model->fetch_records_from('student_prefferd_teaching_types', array('student_id' => $user_id, 'status' => 1));
							if(count($stu_pref_teaching_types) > 0)
								$this->base_model->update_operation(array('is_profile_update' => 1), 'users', array('id' => $user_id));
						}

						$this->prepare_flashmessage(get_languageword('courses') . " " . get_languageword('updated successfully'), 0);
					}
					else
					{
						$this->prepare_flashmessage(get_languageword('courses') . " " . get_languageword('failed to updated'), 1);
					}						
				}
				else {
					$this->prepare_flashmessage(get_languageword('You have not done any changes'), 2);
				}
			}
			else {
				$this->prepare_flashmessage(
				get_languageword('please_select_atleast_one_preferred_course'), 1);
			}
			redirect('student/manage-courses');
		}
		
		$this->data['courses'] 				  =   $this->home_model->get_popular_courses('','',false);
		$this->data['studnentPrefferedCourseIds'] =   $studnentPrefferedCourseIds;
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "manage_courses";
		$this->data['pagetitle'] = get_languageword('courses');		
		$this->data['content'] 		= 'manage_courses';

		$this->_render_page('template/site/student-template', $this->data);
	}
	
	
	/**
	 * Fecilitates to update contact information
	 *
	 * @access	public
	 * @return	string
	 */
	function update_contact_information()
	{
		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		if(isset($_POST['submitbutt']))
		{
			$this->form_validation->set_rules('city', get_languageword('City'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('land_mark', get_languageword('land_mark'), 'trim|required|xss_clean');			
			$this->form_validation->set_rules('country', get_languageword('country'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('pin_code', get_languageword('pin_code'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('phone', get_languageword('phone'), 'trim|required|xss_clean');
			
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{
				$inputdata['city'] = $this->input->post('city');
				$inputdata['land_mark'] = $this->input->post('land_mark');
				$code_country = explode('_', $this->input->post('country'));

				$inputdata['country'] = $code_country[1];
				$inputdata['phone_code'] = $code_country[0];

				$inputdata['pin_code'] = $this->input->post('pin_code');
				$inputdata['phone'] = $this->input->post('phone');				
				$inputdata['academic_class'] = isset($_POST['academic_class']) ? 'yes' : 'no';
				$inputdata['non_academic_class'] = isset($_POST['non_academic_class']) ? 'yes' : 'no';
				$inputdata['share_phone_number'] = isset($_POST['share_phone_number']) ? 'yes' : 'no';				
				$this->base_model->update_operation($inputdata, 'users', array('id' => $user_id));

				$this->prepare_flashmessage(get_languageword('record updated successfully'), 0);								
				redirect('student/update-contact-information');				
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}
		}
		
		$this->data['profile'] = getUserRec($user_id);
		$countries = $this->base_model->fetch_records_from('country');
		$countries_opts = array('' => get_languageword('please select country'));
		if(!empty($countries))
		{
			foreach($countries as $country)
			{
				$countries_opts[$country->phonecode.'_'.$country->nicename]  = $country->nicename." +".$country->phonecode;
			}
		}
		$this->data['countries'] 	 = $countries_opts;
		$this->data['activemenu'] 	 = "account";
		$this->data['activesubmenu'] = "update_contact_info";
		$this->data['content'] 		 = 'update_contact_information';
		$this->data['pagetitle'] 	 = get_languageword('update_contact_information');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to view contact information
	 *
	 * @access	public
	 * @return	string
	 */
	function contact_information()
	{
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['profile'] = getUserRec($user_id);
		$this->data['activemenu'] 	= "account";
		$this->data['activesubmenu'] = "update_contact_info";	
		$this->data['content'] 		= 'contact_information';
		$this->data['pagetitle'] = get_languageword('My Address');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to add or update student studenting subjects
	 *
	 * @access	public
	 * @return	string
	 */
	function manage_subjects()
	{
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->model('student_model');
		$studentSubjectIds 	= $this->student_model->get_student_subject_ids(
		$this->ion_auth->get_user_id()); //Getting student selected subject ids
		
		if ($this->input->post()) {	
		
			if ($this->input->post('student_subjects')) {
				if ($this->input->post('student_subjects') != $studentSubjectIds) {
					$student_subjects 	= $this->input->post('student_subjects');
					if ($this->base_model->delete_record_new('student_subjects', array(
						'user_id' 	=> $this->ion_auth->get_user_id()))) {
						$data['user_id'] 		= $this->ion_auth->get_user_id();
						foreach($student_subjects as $subject) {
							if (is_numeric($subject)) {
								$data['subject_id'] = $subject;
								$this->base_model->insert_operation($data, 'student_subjects');
							}
						}
						$this->prepare_flashmessage(get_languageword('subjects')." " . 
						get_languageword('updated_successfully'), 0);
					}
					else
					{
						$this->prepare_flashmessage(get_languageword('subjects').' '.get_languageword('failed to update'), 1);
					}
				}
				else {
					$this->prepare_flashmessage(get_languageword('You have not done any changes'), 2);
				}
			}
			else {
				$this->prepare_flashmessage(get_languageword('Please select at least on subject'), 1);
			}
			redirect('student/manage-subjects', 'refresh');
		}
		
		$this->data['subjects'] 	= $this->student_model->get_subjects();
		$this->data['studentSubjectIds'] 	= $studentSubjectIds;
		
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "manage_subjects";	
		$this->data['content'] 		= 'manage_subjects';
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to add or update student locations, where he is studenting
	 *
	 * @access	public
	 * @return	string
	 */
	function manage_locations()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->model('student_model');
		$studentLocationIds 	= $this->student_model->get_student_location_ids($user_id); //Getting locaiton ids
		
		if ($this->input->post()) 
		{	
			if ($this->input->post('student_locations')) {
				if ($this->input->post('student_locations') != $studentLocationIds) {
					$student_locations 	= $this->input->post('student_locations');
					if ($this->base_model->delete_record_new('student_locations', array('student_id'=> $user_id))) {
						$data['student_id'] 	= $user_id;
						$data['created_at'] = date('Y-m-d H:i:s');
						foreach($student_locations as $location) {
							if (is_numeric($location)) {
								$data['location_id'] = $location;
								$this->base_model->insert_operation($data, 'student_locations');
							}
						}

						$this->prepare_flashmessage(get_languageword('Locations') . " " . get_languageword('updated successfully'), 0);
					}
					else
					{
						$this->prepare_flashmessage(get_languageword('Locations') . " " . get_languageword('failed to updated'), 1);
					}						
				}
				else {
					$this->prepare_flashmessage(get_languageword('You have not done any changes'), 2);
				}
			}
			else {
				$this->prepare_flashmessage(
				get_languageword('please_select_atleast_one_preferred_location'), 1);
			}
			redirect('student/manage-locations');
		}
		
		$this->data['locations'] 				= $this->student_model->get_locations();
		$this->data['studentLocationIds'] 		= $studentLocationIds;
		
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "manage_locations";
		$this->data['pagetitle'] = get_languageword('Locations');		
		$this->data['content'] 		= 'manage_locations';
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	/**
	 * Fecilitates to add or update student teaching types
	 *
	 * @access	public
	 * @return	string
	 */
	function manage_teaching_types()
	{

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->model('student_model');
		$studentSelectedTypeIds 	= $this->student_model->get_student_selected_teachingtype_ids($user_id);
		
		if ($this->input->post()) 
		{
		
			if ($this->input->post('student_selected_types')) {
				if ($this->input->post('student_selected_types') != $studentSelectedTypeIds) {
					$student_selected_types 	= $this->input->post('student_selected_types');
					 
					if ($this->base_model->delete_record_new('student_prefferd_teaching_types', array('student_id' => $user_id))) {
						$data['student_id'] 		= $user_id;
						$data['created_at'] 	= date('Y-m-d H:i:s');
						foreach($student_selected_types as $student_type) {
							if (is_numeric($student_type)) {
								$data['teaching_type_id'] = $student_type;
								$this->base_model->insert_operation($data, 'student_prefferd_teaching_types');
							}
						}

						$is_profile_updated = $this->ion_auth->user($user_id)->row()->is_profile_update;

						if($is_profile_updated != 1) {

							$stu_pref_courses = $this->base_model->fetch_records_from('student_preffered_courses', array('student_id' => $user_id, 'status' => 1));
							if(count($stu_pref_courses) > 0)
								$this->base_model->update_operation(array('is_profile_update' => 1), 'users', array('id' => $user_id));
						}

						$this->prepare_flashmessage(get_languageword('Teaching Types'). " " . get_languageword('updated_successfully'), 0);
					}
					else
					{
						$this->prepare_flashmessage(get_languageword('Teaching Types'). " " . get_languageword('failed to update'), 1);
					}
				}
				else {
					$this->prepare_flashmessage(get_languageword('you_have_not_done_any_changes') , 2);
				}
			}
			else {
				$this->prepare_flashmessage(
				get_languageword('please_select_atleast_one_preferred_teaching_type') , 1);
			}
			redirect('student/manage-teaching-types', 'refresh');
		}
		
		$this->data['student_types'] 				= $this->student_model->get_teachingtypes();
		$this->data['studentSelectedTypeIds']	 	= $studentSelectedTypeIds;
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "manage_teaching_types";	
		$this->data['pagetitle']	= get_languageword('preffered_teaching_types');
		$this->data['content'] 		= 'manage_teaching_types';
		$this->_render_page('template/site/student-template', $this->data);

	}


	/**
	 * Fecilitates to upload certificates
	 *
	 * @access	public
	 * @return	string
	 */
	function certificates()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['message'] = $this->session->flashdata('message');
		
		if(isset($_POST['submitbutt']))
		{

			if(count($_FILES['certificate']['name']) > 0)
			{
				foreach ($_FILES['certificate']['name'] as $i => $value)
				{
					if($_FILES['certificate']['name'][$i] != '')
					{
					
					$tmpFilePath = $_FILES['certificate']['tmp_name'][$i];
					$ext = pathinfo($_FILES['certificate']['name'][$i], PATHINFO_EXTENSION);
					$new_name = $user_id.'_'.$i.'.'.$ext;
					$filePath = './assets/uploads/certificates/'.$new_name;
						if(move_uploaded_file($tmpFilePath, $filePath))
						{
							if(in_array(strtolower($ext), array('jpg', 'png', 'gif', 'jpeg')))
							{
							$this->create_thumbnail($filePath,'./assets/uploads/certificates/thumbs/','40','40');
							}
							$user_image['admin_certificate_id'] = $i;
							$user_image['user_id']				= $user_id;
							if(isset($this->config->item('site_settings')->need_admin_for_student) && $this->config->item('site_settings')->need_admin_for_student == 'yes')
							$user_image['admin_status']			= 'Pending';
							else
							$user_image['admin_status']			= 'Approved';
							$user_image['certificate_type']		= 'admin';
							$user_image['certificate_name']		= $new_name;
							$user_image['file_type']		= $ext;
							
							$existed = $this->base_model->fetch_records_from('users_certificates',
														array('admin_certificate_id'=>$i,
														'user_id'=>$user_id,'certificate_type'=>'admin'));
							if(count($existed)>0)
							{
								$whr['user_certificate_id']			= $existed[0]->user_certificate_id;
								$this->base_model->update_operation($user_image,'users_certificates',$whr);
							}
							else
							{
							$this->base_model->insert_operation($user_image,'users_certificates');	
							}
						}
					}
				}

				if(count($_FILES['other']['name']) > 0)
				{
					$n=0;
					if(count($_FILES['other']['name']) > 0)
					{
						$n=0;
						for($i=0; $i<count($_FILES['other']['name']); $i++) 
						{					
							$n++;
							 //Get the temp file path
							$tmpFilePath = $_FILES['other']['tmp_name'][$i];			
							
							 //Make sure we have a filepath
							if($tmpFilePath != "")
							{
								//save the filename
								$shortname = $user_id.'_'.str_replace(' ','_',rand().'_'.$_FILES['other']['name'][$i]);
								$ext = pathinfo($_FILES['other']['name'][$i], PATHINFO_EXTENSION);

								//save the url and the file
								$filePath = './assets/uploads/certificates/'.$shortname;
								//Upload the file into the temp dir
								if(move_uploaded_file($tmpFilePath, $filePath)) 
								{								
									$user_image['user_id']				= $user_id;
									$user_image['admin_certificate_id'] = 0;
									if(isset($this->config->item('site_settings')->need_admin_for_student) && $this->config->item('site_settings')->need_admin_for_student == 'yes')
									$user_image['admin_status']			= 'Pending';
									else
									$user_image['admin_status']			= 'Approved';
									$user_image['certificate_type']		= 'other';
									$user_image['certificate_name']		= $shortname;
									$user_image['file_type']		= $ext;									
									$this->base_model->insert_operation($user_image,'users_certificates');
								}
							}
						}
					}
				}
			}
			$this->prepare_flashmessage(get_languageword('Certificates uploaded successfully'), 0);
			redirect('student/certificates');
		}
		
		$certificates = $this->base_model->fetch_records_from('certificates', array('certificate_for' => 'students', 'status' => 'Active'));		
		$this->data['certificates'] 	= $certificates;
		
		$user_uploads = $this->base_model->fetch_records_from('users_certificates', array('user_id' => $user_id));
		$user_uploads_arr = array();
		if(!empty($user_uploads))
		{
			foreach($user_uploads as $up)
			{
				$user_uploads_arr[$up->admin_certificate_id] = $up->certificate_name;
			}
		}
		$this->data['user_uploads_arr'] 	= $user_uploads_arr;
		
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "certificates";
		$this->data['pagetitle']	= get_languageword('certificates');
		$this->data['content'] 		= 'certificates';
		$this->_render_page('template/site/student-template', $this->data);
	}
		
	/**
	 * Fecilitates to set privacy
	 *
	 * @access	public
	 * @return	string
	 */
	function manage_privacy()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['profile'] = getUserRec();	

		if(isset($_POST['submitbutt']))
		{
			$inputdata['free_demo'] = $this->input->post('free_demo');
			$inputdata['visibility_in_search'] = $this->input->post('visibility_in_search');
			$inputdata['show_contact'] = $this->input->post('show_contact');
			$inputdata['availability_status'] = $this->input->post('availability_status');
			$inputdata['name_privacy'] = $this->input->post('name_privacy');
			$this->base_model->update_operation($inputdata, 'users', array('id' => $user_id));
			$this->prepare_flashmessage(get_languageword('privacy updated successfully'), 0);								
			redirect('student/manage-privacy');			
		}
		
		$this->data['pagetitle'] = get_languageword('Manage Privacy');
		$this->data['activemenu'] 	= "manage";
		$this->data['activesubmenu'] = "manage_privacy";	
		$this->data['content'] 		= 'manage_privacy';

		$this->_render_page('template/site/student-template', $this->data);
	}

	/**
	 * Facilitates to display packages for student.
	 *
	 * @access	public
	 * @param	string (Optional)
	 * @return	string
	 */	
	function list_packages($param1 = '')
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['pagetitle'] = get_languageword('packages');
		$this->load->model('student_model');
		$this->data['package_data'] = $this->student_model->list_student_packages();
		$this->data['payment_gateways'] = $this->base_model->get_payment_gateways('', 'Active');

		$this->data['activemenu'] 	= "packages";
		$this->data['activesubmenu'] = "list_packages";	
		$this->data['content'] 		= 'list_packages';
		$this->_render_page('template/site/student-template', $this->data);
	}

	/**
	 * [mysubscriptions description]
	 * @return [type] [description]
	 */
	function mysubscriptions()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table($this->db->dbprefix('subscriptions'));
		$crud->where('user_id', $user_id);
		$crud->set_subject( get_languageword('subscriptions') );
		
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_read();
		
		$crud->columns('subscribe_date','package_name','transaction_no', 'payment_type','credits','amount_paid');
		$crud->callback_column('subscribe_date',array($this,'callback_subscribe_date'));
		$crud->callback_column('amount_paid',array($this,'callback_amount_paid'));
		$output = $crud->render();
		
		$this->data['pagetitle'] = get_languageword('My Subscriptions');
		$this->data['activemenu'] 	= "packages";
		$this->data['activesubmenu'] 	= "mysubscriptions";
		$this->data['pagetitle'] = get_languageword('Subscriptions');		
		$this->data['content'] 		= 'mysubscriptions';
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;

		$this->grocery_output($this->data);
	}
	
	function callback_subscribe_date($value, $row)
	{
		return date($this->session->userdata('date_format'), strtotime($value));
	}
	function callback_amount_paid( $value, $row ) {
		if ( $row->payment_received == 1 ) {
			$value .= '&nbsp;<img src="'.URL_FRONT_IMAGES . 'checked.png">';
		} else {
			if ( $row->payment_type == 'manual' ) {
				$value .= '&nbsp;<a href="'.URL_STUDENT_MANUAL.'/'.$row->id.'"><img src="'.URL_FRONT_IMAGES . 'error.png"></a>';
			} else {
				$value .= '&nbsp;<img src="'.URL_FRONT_IMAGES . 'error.png">';
			}
		}
		return $value;
	}
	
	/**
	 * [book_tutor description]
	 * @return [type] [description]
	 */
	function book_tutor()
	{
		if (!$this->ion_auth->logged_in()) {
			$this->prepare_flashmessage(get_languageword('please_login_to_book_tutor'), 2);
			redirect('auth/login', 'refresh');
		}

		if(!$this->ion_auth->is_student() && !$this->ion_auth->is_parent()) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth', 'refresh');
		}


		if(!$this->input->post()) {

			$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
			redirect(URL_HOME_SEARCH_TUTOR, 'refresh');
		}

		if($this->session->userdata('user_belongs_group') == 5){
			$student_id 		= $this->session->userdata('child_id');
			
		}
		else{
			$student_id 		= $this->ion_auth->get_user_id();
			
		}
		$tutor_id   		= $this->input->post('tutor_id');
		$tutor_slug   		= $this->input->post('tutor_slug');
		$course_slug		= $this->input->post('course_slug');
		$tution 			= $this->input->post('tution');


		//Check Whether student is premium user or not
		/*if(!is_premium($student_id)) {

			$this->prepare_flashmessage(get_languageword('please_become_premium_member_to_book_tutor'), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}*/

		$course_details = $this->home_model->get_tutor_course_details($course_slug, $tutor_id);

		//Check whether Tutor teaches the course or not
		if(empty($course_details)) {

			$this->prepare_flashmessage(get_languageword('no_course_details_found'), 2);
			redirect(URL_HOME_TUTOR_PROFILE.'/'.$tutor_slug, 'refresh');
		}

		$course_id 				= $course_details->course_id;
		$fee 					= $course_details->fee;

		//Check If student has sufficient credits to book tutor
		/*if(!is_eligible_to_make_booking($student_id, $fee)) {

			$this->prepare_flashmessage(get_languageword("you_do_not_have_enough_credits_to_book_the_tutor_Please_get_required_credits_here"), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}*/

		$start_date  			= date('Y-m-d', strtotime($this->input->post('start_date')));
		$time_slot   			= $this->input->post('time_slot');
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$student_details = $this->base_model->get_user_info($student_id);
		if(!empty($student_details->time_zone))
			$time_zone = $time_zone_list[$student_details->time_zone];
		else
			$time_zone = "UTC";
		
		$t = strtotime($this->input->post('start_date'));
		$time_slot = explode("-",$this->input->post('time_slot'));
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
		
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->sub($myInterval);
		$myDateTime_end->sub($myInterval);
		$start_date = $myDateTime_start->format("Y-m-d");
		$time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");

		//Check If student already booked the tutor on the same slot and it is not yet approved by tutor
		if($this->home_model->is_already_booked_the_tutor($student_id, $tutor_id, $course_id, $start_date, $time_slot)) {

			$this->prepare_flashmessage(get_languageword('you_already_booked_this_tutor_and_your_course_not_yet_completed'), 2);
			redirect(URL_HOME_TUTOR_PROFILE.'/'.$tutor_slug, 'refresh');
		}

		//Check If selected time-slot is available
		if(empty($course_details->time_slots) || !$this->home_model->is_time_slot_avail($tutor_id, $course_id, $start_date, $time_slot,$tution)) {

			$this->prepare_flashmessage(get_languageword('time_slot_not_available'), 2);
			redirect(URL_HOME_TUTOR_PROFILE.'/'.$tutor_slug, 'refresh');
		}


		$content 				= $course_details->content;
		$duration_value 		= $course_details->duration_value;
		$duration_type 			= $course_details->duration_type;
		$per_credit_value 		= $course_details->per_credit_value;
		$days_off 				= $course_details->days_off;

		$preferred_location 	= ($this->input->post('teaching_type') == "willing-to-travel") ? $this->input->post('location_slug') : $this->input->post('teaching_type');
		$message   				= $this->input->post('message');

		if($duration_type == "hours") {

			$formatted  = str_replace(':', '.', $time_slot);
			$time 	    = explode('-', str_replace(' ', '', $formatted));

			$start_time = number_format($time[0],2);
			$end_time   = number_format($time[1],2);
			if($end_time < $start_time)
				$end_time = (int)$end_time + 24;

			$total_time = $end_time - $start_time;

			if($total_time >= 1) {

				$days = round($duration_value / $total_time);

			} else {

				$total_time = (int)(explode('.', number_format($total_time,2))[1]);
				$days = round($duration_value / ($total_time/60));
			}
			$count_day = 0;
			$days_off_list = array();
			$end_date = $start_date;
			if(!empty($days_off)){
				$days_off_list = explode(",",$days_off);
				while($count_day < $days){
					$temp_time = strtotime($end_date.'+1 days');
					$week_day = strtoupper(date("D",$temp_time));
					if(!in_array($week_day,$days_off_list)){
						$count_day++;	
					}
					$end_date = date("Y-m-d", strtotime($end_date.'+1 days'));
				}	
			}
			else{
				$end_date = date("Y-m-d", strtotime($start_date.'+'.$days.' days'));
			}

		} else {

			$end_date = date("Y-m-d", strtotime($start_date.'+'.$duration_value.' '.$duration_type));
		}

		$end_date = date("Y-m-d", strtotime($end_date.'-1 days'));

		$admin_commission   	= get_system_settings('admin_commission_for_a_booking');
		$admin_commission_val   = round($fee * ($admin_commission / 100));

		$created_at   		= date('Y-m-d H:i:s');
		$updated_at   		= $created_at;
		$updated_by   		= $student_id;
		$created_by 		= $student_id;

		$inputdata	=	array(
								'student_id'			=> $student_id,
								'tutor_id'				=> $tutor_id,
								'course_id'				=> $course_id,
								'content'				=> $content,
								'duration_value'		=> $duration_value,
								'duration_type'			=> $duration_type,
								'fee'					=> $fee,
								'per_credit_value'		=> $per_credit_value,
								'start_date'			=> $start_date,
								'end_date'				=> $end_date,
								'time_slot'				=> $time_slot,
								'days_off'				=> $days_off,
								'preferred_location'	=> $preferred_location,
								'message'				=> $message,
								'admin_commission'		=> $admin_commission,
								'admin_commission_val'	=> $admin_commission_val,
								'created_at'			=> $created_at,
								'updated_at'			=> $updated_at,
								'updated_by'			=> $updated_by,
								'created_by'			=> $created_by,
								'tution'				=> $tution
							);

		$ref = $this->base_model->insert_operation($inputdata, 'bookings');
		

		if($ref > 0) {

			//Log Credits transaction data & update user net credits - Start
			$log_data = array(
							'user_id' => $student_id,
							'credits' => $fee,
							'per_credit_value' => $per_credit_value,
							'action'  => 'debited',
							'purpose' => 'Slot booked with the Tutor "'.$tutor_slug.'" and Booking Id is '.$ref,
							'date_of_action	' => date('Y-m-d H:i:s'),
							'reference_table' => 'bookings',
							'reference_id' => $ref,
						);

			log_user_credits_transaction($log_data);

			update_user_credits($student_id, $fee, 'debit');
			//Log Credits transaction data & update user net credits - End


			//Email Alert to Tutor - Start
				//Get Tutor Booking Success Email Template
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '5'));

				if(!empty($email_tpl)) {

					$email_tpl = $email_tpl[0];

					$student_rec = getUserRec($student_id);
					$tutor_rec 	 = getUserRec($tutor_id);


					if(!empty($email_tpl->from_email)) {

						$from = $email_tpl->from_email;

					} else {

						$from 	= get_system_settings('Portal_Email');
					}

					$to 	= $tutor_rec->email;

					if(!empty($email_tpl->template_subject)) {

						$sub = $email_tpl->template_subject;

					} else {

						$sub = get_languageword("Booking Request From Student");
					}

					if(!empty($email_tpl->template_content)) {

						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

						$site_title = $this->config->item('site_settings')->site_title;


						$original_vars  = array($logo_img, $site_title, $tutor_rec->username, $student_rec->username, $course_slug, $start_date." & ".$time_slot, $preferred_location, '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);


						$temp_vars		= array('__SITE_LOGO__', '__SITE_TITLE__', '__TUTOR_NAME__', '__STUDENT_NAME__', '__COURSE_NAME__', '__DATE_TIME__', '__LOCATION__', '__LOGIN_LINK__', '__SITE_TITLE__');
						$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

					} else {

						$msg = get_languageword('please')." <a href='".URL_AUTH_LOGIN."'> ".get_languageword('Login Here')."</a> ".get_languageword('to view the booking details');
						$msg .= "<p>".get_languageword('Thank you')."</p>";
					}

					sendEmail($from, $to, $sub, $msg);
				}
			//Email Alert to Tutor - End
			


			//05-12-2018 admin notification start
			$data = array();
			$data['user_id'] 	= $student_id;
			$data['title'] 		= get_languageword('new_course_booking');
			$data['content'] 	= "Student sent Booking Request to Tutor";
			$data['datetime']   = date('Y-m-d H:i:s');
			$data['admin_read'] = 0;
			$data['page_link']  = SITEURL.'admin/student-bookings/read/'.$ref;
			$data['table_name'] = "bookings";
			$data['primary_key_column'] = "booking_id";
			$data['primary_key_value']  = $ref;

			$this->base_model->insert_operation($data,'notifications');	
			unset($data);
			//05-12-2018 admin notification start
			

			$this->prepare_flashmessage(get_languageword('your_slot_with_the_tutor_booked_successfully_Once_tutor_approved_your_booking and_initiated_the_session_you_can_start_the_course_on_the_booked_date'), 0);
			redirect(URL_HOME_TUTOR_PROFILE.'/'.$tutor_slug);

		} else {

			$this->prepare_flashmessage(get_languageword('your_slot_with_the_tutor_not_booked_you_can_send_message_to_the_tutor'), 2);
			redirect(URL_HOME_TUTOR_PROFILE.'/'.$tutor_slug);
		}


	}


	/**
	 * [enroll_in_institute description]
	 * @return [type] [description]
	 */
	function enroll_in_institute()
	{
		if (!$this->ion_auth->logged_in()) {
			$this->prepare_flashmessage(get_languageword('please_login_to_enroll_in_institute'), 2);
			redirect('auth/login', 'refresh');
		}

		if(!$this->ion_auth->is_student()) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth', 'refresh');
		}


		if(!$this->input->post()) {

			$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
			redirect(URL_HOME_SEARCH_INSTITUTE, 'refresh');
		}


		$student_id 		= $this->ion_auth->get_user_id();
		$inst_id	   		= $this->input->post('inst_id');
		$inst_slug   		= $this->input->post('inst_slug');
		$batch_id			= $this->input->post('batch_id');


		//Check Whether student is premium user or not
		if(!is_premium($student_id)) {

			$this->prepare_flashmessage(get_languageword('please_become_premium_member_to_enroll_in_institute'), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}

		$batch_details = $this->home_model->get_inst_batch_details($batch_id);

		//Check whether Institute offering the course-batch or not
		if(empty($batch_details)) {

			$this->prepare_flashmessage(get_languageword('no_batch_details_found'), 2);
			redirect(URL_HOME_INSTITUTE_PROFILE.'/'.$inst_slug, 'refresh');
		}

		$fee 					= $batch_details['fee'];
		$batch_max_strength 	= $batch_details['batch_max_strength'];

		//Check If student has sufficient credits to book tutor
		if(!is_eligible_to_make_booking($student_id, $fee)) {

			$this->prepare_flashmessage(get_languageword("you_do_not_have_enough_credits_to_enroll_in_the_institute_Please_get_required_credits_here"), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}


		//Check If student already booked the tutor on the same slot and it is not yet approved by tutor
		if($this->home_model->is_already_enrolled_in_the_batch($student_id, $batch_id)) {

			$this->prepare_flashmessage(get_languageword('you_already_enrolled_in_this_batch_and_your_course_not_yet_completed'), 2);
			redirect(URL_HOME_INSTITUTE_PROFILE.'/'.$inst_slug, 'refresh');
		}

		//Check If slot is available in the selected batch
		if(!($batch_max_strength > $this->home_model->total_enrolled_students_in_batch($batch_id))) {

			$this->prepare_flashmessage(get_languageword('slot_not_available_in_the_batch_Please_select_other_batch'), 2);
			redirect(URL_HOME_INSTITUTE_PROFILE.'/'.$inst_slug, 'refresh');
		}


		$inputdata = $batch_details;


		$message   				= $this->input->post('message');

		$admin_commission   	= get_system_settings('admin_commission_for_a_booking');
		$admin_commission_val   = round($fee * ($admin_commission / 100));

		$created_at   		= date('Y-m-d H:i:s');
		$updated_at   		= $created_at;
		$updated_by   		= $student_id;


		unset($inputdata['batch_max_strength']);
		unset($inputdata['status']);
		unset($inputdata['sort_order']);
		unset($inputdata['created_at']);
		unset($inputdata['updated_at']);

		$inputdata['student_id']			=	$student_id;
		$inputdata['message']				=	$message;
		$inputdata['admin_commission']		=	$admin_commission;
		$inputdata['admin_commission_val']	=	$admin_commission_val;
		$inputdata['created_at']			=	$created_at;
		$inputdata['updated_at']			=	$updated_at;
		$inputdata['updated_by']			=	$updated_by;

		$ref = $this->base_model->insert_operation($inputdata, 'inst_enrolled_students');

		if($ref > 0) {

			$course_name = $this->base_model->fetch_value('categories', 'name', array('id' => $inputdata['course_id']));

			//Log Credits transaction data & update user net credits - Start
			$log_data = array(
							'user_id' => $student_id,
							'credits' => $fee,
							'per_credit_value' => $inputdata['per_credit_value'],
							'action'  => 'debited',
							'purpose' => 'Enrolled in the batch "'.$inputdata['batch_name'].' - '.$inputdata['batch_code'].'" for the course "'.$course_name.'" offered by the isntitute "'.$inst_slug.'"',
							'date_of_action	' => date('Y-m-d H:i:s'),
							'reference_table' => 'inst_enrolled_students',
							'reference_id' => $ref,
						);

			log_user_credits_transaction($log_data);

			update_user_credits($student_id, $fee, 'debit');
			//Log Credits transaction data & update user net credits - End


			//admin notification
			$not_data = array();
			$not_data['user_id'] 	= $student_id;
			$not_data['title'] 		= get_languageword('student_enrolled_in_batch').' '.$batch_details['batch_name'];
			$not_data['content'] 	= "Student has enrolled in batch ".' '.$batch_details['batch_name'];
			$not_data['datetime']   = date('Y-m-d H:i:s');
			$not_data['admin_read'] = 0;
			$not_data['page_link']  = SITEURL.'admin/inst-batche-enrolled-students/'.$batch_details['batch_id'].'/read/'.$ref;
			$not_data['table_name'] = "inst_enrolled_students";
			$not_data['primary_key_column'] = "enroll_id";
			$not_data['primary_key_value']  = $ref;

			$this->base_model->insert_operation($not_data,'notifications');	
			unset($not_data);



			//Email Alert to Institute & Batch Tutor - Start
				//Get Institute Enroll Success Email Template
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '8'));

				if(!empty($email_tpl)) {

					$email_tpl = $email_tpl[0];

					$student_rec = getUserRec($student_id);
					$tutor_rec 	 = getUserRec($inputdata['tutor_id']);
					$inst_rec 	 = getUserRec($inputdata['inst_id']);


					if(!empty($email_tpl->from_email)) {

						$from = $email_tpl->from_email;

					} else {

						$from 	= get_system_settings('Portal_Email');
					}

					$to 	= $inst_rec->email.",".$tutor_rec->email;

					if(!empty($email_tpl->template_subject)) {

						$sub = $email_tpl->template_subject;

					} else {

						$sub = get_languageword("Enrollment Request From Student");
					}

					if(!empty($email_tpl->template_content)) {

						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

						$site_title = $this->config->item('site_settings')->site_title;


						$original_vars  = array($logo_img, $site_title, $student_rec->username, $inputdata['batch_name'].' - '.$inputdata['batch_code'], $course_name, $inst_slug, '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);


						$temp_vars		= array('__SITE_LOGO__', '__SITE_TITLE__', '__STUDENT_NAME__', '__BATCH_NAME__', '__COURSE_NAME__', '__INSTITUTE_NAME__', '__LOGIN_LINK__', '__SITE_TITLE__');

						$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

					} else {

						$msg = get_languageword('please')." <a href='".URL_AUTH_LOGIN."'>".get_languageword('Login Here')."</a> ".get_languageword('to view the enrollment details');
						$msg .= "<p>".get_languageword('Thank you')."</p>";
					}

					sendEmail($from, $to, $sub, $msg);
				}
			//Email Alert to Institute & Batch Tutor - End

			$this->prepare_flashmessage(get_languageword('your_slot_booked_successfully_Once_isntitute_approved_your_booking and_tutor_initiated_the_session_you_can_start_the_course_on_course_starting_date'), 0);
			redirect(URL_HOME_INSTITUTE_PROFILE.'/'.$inst_slug);

		} else {

			$this->prepare_flashmessage(get_languageword('your_slot_with_the_tutor_not_booked_You_can_send_message_to_the_tutor'), 2);
			redirect(URL_HOME_INSTITUTE_PROFILE.'/'.$inst_slug);
		}


	}


	function _callback_course_duration($primary_key, $row)
	{
		return $row->duration_value.' '. $row->duration_type;
	}

	function _callback_course_id($primary_key , $row)
	{

	  $course_name = $this->base_model->fetch_value('categories', 'name', array('id' => $row->course_id));
	   return $course_name;
	}
	
	function _callback_category_id($primary_key , $row)
	{
	   $teaching_name = $this->base_model->fetch_value('categories', 'name', array('id' => $row->category));
	   return $teaching_name;
	}
	
	function _callback_tutor_name($primary_key, $row){
		$teaching_name = $this->base_model->fetch_value('users', 'username', array('id' => $row->tutor_id));
	   	return $teaching_name;
	}

	/**
	 * [enquiries description]
	 * @param  string $param [description]
	 * @return [type]        [description]
	 */
	function enquiries($param = "")
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$user_id = $this->ion_auth->get_user_id();
		$my_relation_status = $this->base_model->get_parent_status($user_id);

		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table(TBL_BOOKINGS);
		$crud->set_relation('tutor_id',TBL_USERS, 'username');
		$crud->set_relation('updated_by',TBL_USERS, 'username');
		$crud->set_relation('student_id',TBL_USERS, 'time_zone');
		
		$crud->where(TBL_BOOKINGS.'.student_id', $user_id);


		$status_arr = array('pending', 'approved', 'cancelled_before_course_started', 'cancelled_when_course_running', 'cancelled_after_course_completed', 'session_initiated', 'running', 'completed', 'called_for_admin_intervention', 'closed');
		if(in_array($param, $status_arr)) {

			$crud->where(TBL_BOOKINGS.'.status', $param);
		}
		if($param == 'session_initiated')
			$crud->where(TBL_BOOKINGS.'.preferred_location','online-zoom');
		if($param != "session_initiated"){
			$crud->unset_add();
			$crud->unset_delete();	
		} else{
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_read();
		}
		
		if($my_relation_status > 0  || ($this->session->userdata('user_belongs_group') == 2 && $this->session->userdata('book_edit_status') == 'Yes'))
			$crud->unset_edit();

		$crud->set_subject( get_languageword('booking_status') );
		$crud->set_enable_initiate_session_option_before_mins($this->config->item('site_settings')->enable_initiate_session_option_before_mins);
		//Unset Actions
		//$crud->unset_add();
		//$crud->unset_delete();

		//List Table Columns
		$crud->columns('course_id', 'tutor_id', 'course_duration', 'fee','content', 'start_date', 'time_slot', 'preferred_location', 'tution', 'status','student_id');

		/*******05-12-2018*************/
		if( $param == 'session_initiated' || $param == 'running' ) {
			$crud->add_action(get_languageword('join'), '', '', 'fa fa-mixcloud', array($this, 'join_link') );
		}
		/*******05-12-2018*************/


		$crud->callback_column('course_duration',array($this,'_callback_course_duration'));
		$crud->callback_column('course_id',array($this,'_callback_course_id'));

		//Display Alias Names
		$crud->display_as('course_id',get_languageword('course_booked'));
		$crud->display_as('tutor_id',get_languageword('tutor_name'));
		$crud->display_as('fee',get_languageword('fee').' ('.get_languageword('in_credits').')');
		$crud->display_as('admin_commission',get_languageword('admin_commission_percentage_in_credits'));
		$crud->display_as('per_credit_value',get_languageword('per_credit_value')." (".get_system_settings('currency_symbol').")");
		$crud->display_as('start_date',get_languageword('preferred_commence_date'));
		$crud->display_as('tution',get_languageword('Tution Mode/Type'));

		if($param == "closed") {

			$crud->add_action(get_languageword('rate_this_tutor'), URL_FRONT_IMAGES.'/star.png', URL_STUDENT_RATE_TUTOR.'/');

			$crud->add_action(get_languageword('get_certificate'), URL_FRONT_IMAGES.'print.png', '', 'stu-certificate', array($this,'callback_student_certificate'));


		}

		//Form fields for Edit Record
		$crud->edit_fields('status', 'status_desc', 'updated_at', 'prev_status');

		//Hidden Fields
		$crud->field_type('updated_at', 'hidden', date('Y-m-d H:i:s'));

		//Unset Fields
		$crud->unset_fields('student_id', 'admin_commission_val');


		//Authenticate whether Tutor editing/viewing his records only
		if($crud_state == "edit" || $crud_state == "read") {

			if($param != "" && $param != "add" && $param != "edit" && $param != "read" && $param != "success")
				$p_key = $this->uri->segment(4);
			else
				$p_key = $this->uri->segment(3);

			$booking_det = $this->base_model->fetch_records_from('bookings', array('booking_id' => $p_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];

				if($booking_det->student_id != $user_id) {

					$this->prepare_flashmessage(get_languageword('not_authorized'), 1);
	    			redirect(URL_STUDENT_ENQUIRIES);
				}

				if($crud_state == "edit") {

					$booking_status = $booking_det->status;

					$updated_by = getUserType($booking_det->updated_by);

					$crud->field_type('prev_status', 'hidden', $booking_status);

					$crud->display_as('status', get_languageword('change_status'));

					if(in_array($booking_status, array('pending', 'approved', 'session_initiated', 'running', 'completed'))) {
						$crud->required_fields(array('status'));
					}

					if($booking_status == "pending") {

						$crud->field_type('status', 'dropdown', array('closed' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention')));
					}

					if($booking_status == "approved") {
						if($booking_det->preferred_location != 'record'){
							$status = array('closed' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
						}		
						else
							$status = array('completed' => get_languageword('completed'),'closed' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

						$crud->field_type('status', 'dropdown', $status);

					}

					if($booking_status == "session_initiated") {

						$status = array('running' => get_languageword('start_course'), 'closed' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
						$crud->field_type('status', 'dropdown', $status);
					}

					if($booking_status == "running") {

						$status = array('completed' => get_languageword('completed'),'closed' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

						$crud->field_type('status', 'dropdown', $status);

					}

					if($booking_status == "completed") {

						$status = array('closed' => get_languageword('close'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

						$crud->field_type('status', 'dropdown', $status);

					}

					if($booking_status == "called_for_admin_intervention" && $updated_by == "tutor") {

						$crud->required_fields(array('status'));

						$status = array('closed' => get_languageword('close'));

						$crud->field_type('status', 'dropdown', $status);

					} else if($booking_status == "called_for_admin_intervention" && ($updated_by == "student" || $updated_by == "admin")) {

						$crud->edit_fields('status_desc', 'updated_at');
					}


					if($booking_status == "cancelled_when_course_running" && $updated_by == "tutor") {

						$crud->required_fields(array('status'));

						$status = array('called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));

						$crud->field_type('status', 'dropdown', $status);
					}

					if($booking_status == "closed" || $booking_status == "cancelled_before_course_started" || ($booking_status == "cancelled_when_course_running" && $updated_by == "student") || $booking_status == "cancelled_after_course_completed") {

						$crud->edit_fields('status_desc', 'updated_at');

					}

				}

			} else {

				$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
	    		redirect(URL_STUDENT_ENQUIRIES);
			}

		}


		if($crud_state == "read") {

			$crud->field_type('updated_at', 'visibile');
			$crud->set_relation('course_id', 'categories', 'name');
		}



		//05-12-2018 admin notification start
		$methd = $this->uri->segment(3);
		$stus = $this->input->post('status');
		if($stus == 'completed'){
			$this->load->model('student_model');
			$booking_info = array();
			$current_method = "";
			if($methd != "update" && $methd != "update_validation"){
				$booking_info = (array)$this->student_model->get_booking_detail1($this->uri->segment(3));
				$current_method = $this->uri->segment(2);
			}
			else{
				$booking_info = (array)$this->student_model->get_booking_detail1($this->uri->segment(4));
				$current_method = $this->uri->segment(3);
			}
			if($current_method == "update" && !empty($booking_info['iq_id'])){
				$this->student_model->remove_new_pending_iq($booking_info);
			}
		}
		if ($this->input->post('status')!="" && $this->input->post('prev_status')!="" && $methd=="update_validation") {
			 
			 $stus = ucwords(str_replace("_", " ", $this->input->post('status')));

			 $p_key = $this->uri->segment(4);
			 
			 $booking_details = $this->base_model->fetch_records_from('bookings', array('booking_id' => $p_key));

			if(!empty($booking_details)) {

				$booking_details = $booking_details[0];

				$data = array();
				$data['user_id'] 	= $user_id;
				$data['title'] 		= get_languageword('student_changed_course_status').' to '. $stus;
				$data['content'] 	= "Student has changed course status to "." ".$stus;
				$data['datetime']   = date('Y-m-d H:i:s');
				$data['admin_read'] = 0;
				$data['page_link']  = SITEURL.'admin/student-bookings/read/'.$booking_details->booking_id;
				$data['table_name'] = "bookings";
				$data['primary_key_column'] = "booking_id";
				$data['primary_key_value']  = $booking_details->booking_id;

				
				$this->base_model->insert_operation($data,'notifications');	
				unset($data);
			}
		}
		/*if($stus == "closed" || $stus == 'completed'){
			$this->load->model('student_model');
			$booking_info = array();
			if($methd != "update" && $methd != "update_validation"){
				$booking_info = (array)$this->student_model->get_booking_detail1($this->uri->segment(3));
			}
			else{
				$booking_info = (array)$this->student_model->get_booking_detail1($this->uri->segment(4));
			}
			$this->delete_zoom_meeting($booking_info['booking_id']);
			
		}*/
		//05-12-2018 admin notification end

		$crud->callback_column('preferred_location', array($this, 'callback_column_preferred_location'));
		$crud->callback_column('status', array($this, 'callback_column_booking_status'));

		$crud->callback_update(array($this,'callback_send_email'));
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$crud->set_timezone($time_zone_list);

		$output = $crud->render();
		$param = get_languageword($param);
		$this->data['pagetitle'] = get_languageword('bookings');
		$this->data['activemenu'] 	= "enquiries";
		$this->data['activesubmenu'] = $param;
		$this->data['is_ttuor'] = false;
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}
	
	function delete_zoom_meeting($booking_id){
		$zoom_info = $this->base_model->delete_zoom_session1($booking_id);
		if(!empty($zoom_info)){
			$zoom = $this->base_model->get_zoom_info($zoom_info['zoom_id']);
			$zoom_user_info = $this->getUsers($zoom['zoom_api_key'],$zoom['zoom_secret_key']);
			$zoom_user_token = $zoom_user_info['token'];
				
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.zoom.us/v2/meetings/".$zoom_info['zoom_meeting_id'],
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "DELETE",
			  CURLOPT_POSTFIELDS => "",
			  CURLOPT_HTTPHEADER => array(
			    "Authorization: Bearer ".$zoom_user_token,
			    "Content-Type: application/x-www-form-urlencoded",
			    "cache-control: no-cache"
			  ),
			));
			$zoom_response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
		}
	}

	function callback_student_certificate($primary_key , $row)
	{
		if ($row->is_certificate_issued=="Yes") {
			return URL_STUDENT_GET_CERTIFICATE.'/'.$row->booking_id;
		} 
		return '#';
	}

	/**
	 * [join_link description]
	 * @param  [type] $primary_key [description]
	 * @param  [type] $row         [description]
	 * @return [type]              [description]
	 */
	function join_link( $primary_key , $row ) {
		$user_id = $this->ion_auth->get_user_id();
		$now = date('Y-m-d');
		
		$query = 'SELECT b.*, course.name course_name FROM ' . TBL_BOOKINGS . ' b 
			INNER JOIN ' . TBL_USERS . ' st ON b.student_id = st.id ' . 
			'INNER JOIN ' . TBL_USERS . ' tu ON b.tutor_id = tu.id ' . 
			'INNER JOIN ' . TBL_CATEGORIES . ' course ON b.course_id = course.id ' .
			'WHERE (b.status = "session_initiated" OR b.status="running") AND b.student_id = ' . $user_id .' AND booking_id = ' . $primary_key;
		$booking_details = $this->db->query( $query )->result();
		// echo $query;
		$link = '#';
		if( count( $booking_details ) > 0 && $booking_details[0]->preferred_location == 'online-bbb' ) {
			$link = URL_VIRTUAL_CLASS . '/init/'.$primary_key;
		}
		if( count( $booking_details ) > 0  && $booking_details[0]->preferred_location == 'online-zoom' ) {
			$query1 = "SELECT * FROM ".TBL_ZOOM_SESSION." WHERE booking_id = ".$primary_key;
			$zoom_details = $this->db->query( $query1 )->row_array();
			$link = $zoom_details['student_zoom_link'];
		}
		return $link;
	}
	

	function callback_column_booking_status($primary_key , $row)
	{
		
	    return humanize($row->status);
	}
	
	function callback_column_collab_id($primary_key , $row)
	{	
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$my_details = $this->base_model->get_user_info($user_id);
		if($my_details->collab_id != $row->open_collab_id)
	    	return $row->grade_year.'-'.$row->open_collab_id;
	    else
	    	return $row->grade_year.'-'.$row->collab_id;
	}

	function callback_column_preferred_location($primary_key , $row)
	{

	    return humanize($row->preferred_location);
	}


	function callback_send_email($post_array, $primary_key)
	{
		$post_array['updated_by'] = $this->ion_auth->get_user_id();

		if($this->base_model->update_operation($post_array, 'bookings', array('booking_id' => $primary_key))) {

			$booking_det = $this->base_model->fetch_records_from('bookings', array('booking_id' => $primary_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];

				$student_rec = getUserRec($booking_det->student_id);
				$tutor_rec 	 = getUserRec($booking_det->tutor_id);

				//If Student Cancelled booking before session gets started, refund Student's Credits
				if($post_array['status'] == "cancelled_before_course_started") {

					//Log Credits transaction data & update user net credits - Start
					$log_data = array(
									'user_id' => $booking_det->student_id,
									'credits' => $booking_det->fee,
									'per_credit_value' => $booking_det->per_credit_value,
									'action'  => 'credited',
									'purpose' => 'Slot booked with the Tutor "'.$tutor_rec->username.'" has cancelled before course started',
									'date_of_action	' => date('Y-m-d H:i:s'),
									'reference_table' => 'bookings',
									'reference_id' => $primary_key,
								);

					log_user_credits_transaction($log_data);

					update_user_credits($booking_det->student_id, $booking_det->fee, 'credit');
					//Log Credits transaction data & update user net credits - End
				}


				//If Student Cancelled booking before session gets started, refund Student's Credits
				if($post_array['status'] == "closed") {

					$tutor_acquired_credits = $booking_det->fee - $booking_det->admin_commission_val;

					//Log Credits transaction data & update user net credits - Start
					$log_data = array(
									'user_id' => $booking_det->tutor_id,
									'credits' => $tutor_acquired_credits,
									'per_credit_value' => $booking_det->per_credit_value,
									'action'  => 'credited',
									'purpose' => 'Credits added for the booking "'.$primary_key.'" ',
									'date_of_action	' => date('Y-m-d H:i:s'),
									'reference_table' => 'bookings',
									'reference_id' => $primary_key,
								);

					log_user_credits_transaction($log_data);

					update_user_credits($booking_det->tutor_id, $tutor_acquired_credits, 'credit');
					//Log Credits transaction data & update user net credits - End
				}


				//If Student updates the status as "start course", and if preferred teaching type is online, Email student's skype id to Tutor
				//Email Alert to Tutor - Start
				//Get Send Student's Skype Email Template
				if($post_array['status'] == "running" && $booking_det->preferred_location == "online") {

					$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '7'));

					if(!empty($email_tpl)) {

						$email_tpl = $email_tpl[0];

						if(!empty($email_tpl->from_email)) {

							$from = $email_tpl->from_email;

						} else {

							$from 	= $student_rec->email;
						}

						$to 	= $tutor_rec->email;

						if(!empty($email_tpl->template_subject)) {

							$sub = $email_tpl->template_subject;

						} else {

							$sub = get_languageword("Student Skype Id");
						}

						$student_addr = $student_rec->skype.", <br/>Phone: ".$student_rec->phone;

						$course_name = $this->base_model->fetch_value('categories', 'name', array('id' => $booking_det->course_id));

						if(!empty($email_tpl->template_content)) {


							$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

							$site_title = $this->config->item('site_settings')->site_title;



							$original_vars  = array($logo_img, $site_title, $tutor_rec->username, $student_rec->username, $course_name, $booking_det->start_date." & ".$booking_det->time_slot, $booking_det->preferred_location, $student_addr, $site_title);


							$temp_vars		= array('__SITE_LOGO__', '__SITE_TITLE__', '__TUTOR__NAME__', '__STUDENT_NAME__', '__COURSE_NAME__', '__DATE_TIME__', '__LOCATION__', '__STUDENT_ADDRESS__', '__SITE_TITLE__');

							$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

						} else {

							$msg = "<p>
										".get_languageword('hello')." ".$tutor_rec->username.",</p>
									<p>
										".get_languageword('Student ')." &quot;".$student_rec->username."&quot; ".get_languageword('started the course')."  &quot;".$course_name."&quot;</p>
									<p>
										".get_languageword('for the timeslot')." &quot;".$booking_det->start_date." & ".$booking_det->time_slot."&quot; and &quot; ".$booking_det->preferred_location."&quot; ".get_languageword('as preferred location for sessions')."</p>
									<p>
										".get_languageword('Below is the Skype id of the Student')."</p>
									<p>
										".$student_addr."</p>";

							$msg .= "<p>".get_languageword('Thank you')."</p>";
						}

						sendEmail($from, $to, $sub, $msg);
					}
					//Email Alert to Tutor - End
				}


				//If Student updates the status as "called_for_admin_intervention", Send email alert to Admin
				//Email Alert to Admin - Start
				//Get Claim By Student Email Template
				if($post_array['status'] == "called_for_admin_intervention") {

					$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '9'));

					if(!empty($email_tpl)) {

						$email_tpl = $email_tpl[0];

						if(!empty($email_tpl->from_email)) {

							$from = $email_tpl->from_email;

						} else {

							$from 	= $student_rec->email;
						}

						$to 	= get_system_settings('Portal_Email');

						if(!empty($email_tpl->template_subject)) {

							$sub = $email_tpl->template_subject;

						} else {

							$sub = get_languageword("Claim By Student");
						}


						if(!empty($email_tpl->template_content)) {

							$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

							$site_title = $this->config->item('site_settings')->site_title;


							$original_vars  = array($logo_img, $site_title, $student_rec->username, get_languageword('booking').' "'.$booking_det->booking_id.'"', '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);

							$temp_vars		= array('__SITE_LOGO__', '__SITE_TITLE__', '__STUDENT_NAME__', '__BOOKING_ID__', '__LOGIN_LINK__', '__SITE_TITLE__');

							$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

						} else {

							$msg = "<p>
										".get_languageword('Hi').",</p>
									<p>
										".get_languageword('Student ')." &quot;".$student_rec->username."&quot; ".get_languageword('claimed for your intervention for the booking')." &quot;".$booking_det->booking_id."&quot;</p>
									<p>
										".get_languageword('please')." <a href='".URL_AUTH_LOGIN."'>".get_languageword('Login Here')."</a> ".get_languageword('to view the details').".</p>";

							$msg .= "<p>".get_languageword('Thank you')."</p>";
						}

						sendEmail($from, $to, $sub, $msg);
					}
					//Email Alert to Admin - End
				}

			}

			return TRUE;

		} else return FALSE;
	}
	
	function callback_send_email_iq($post_array, $primary_key)
	{
		$post_array['updated_by'] = $this->ion_auth->get_user_id();

		if($this->base_model->update_operation($post_array, 'instant_bids', array('id' => $primary_key))) {

			$booking_det = $this->base_model->fetch_records_from('instant_bids', array('id' => $primary_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];
			}

			return TRUE;

		} else return FALSE;
	}
	
	function callback_send_email_collab($post_array, $primary_key)
	{
		if($this->base_model->update_operation($post_array, 'collabs', array('id' => $primary_key))) {

			$booking_det = $this->base_model->fetch_records_from('collabs', array('id' => $primary_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];
			}

			return TRUE;

		} else return FALSE;
	}

	//Give Rating to Tutor
	function rate_tutor($booking_id = "")
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$user_id = $this->ion_auth->get_user_id();
		$booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : $booking_id;

		if(empty($booking_id)) {

			$this->prepare_flashmessage(get_languageword('Invalid Request'), 1);
			redirect(URL_STUDENT_ENQUIRIES);
		}

		$booking_det = $this->base_model->fetch_records_from('bookings', array('booking_id' => $booking_id, 'status' => 'closed'));

		if(empty($booking_det)) {

			$this->prepare_flashmessage(get_languageword('No Booking Deatils Found'), 2);
			redirect(URL_STUDENT_ENQUIRIES);
		}

		$booking_det = $booking_det[0];

		//Check whether related student is rating the tutor
		if($booking_det->student_id != $user_id) {

			$this->prepare_flashmessage(get_languageword('Not Authorized'), 1);
			redirect(URL_STUDENT_INDEX);
		}

		if($this->input->post()) {

			$inputdata['booking_id']	= $booking_id;
			$inputdata['student_id']	= $user_id;
			$inputdata['tutor_id']		= $booking_det->tutor_id;
			$inputdata['course_id']		= $booking_det->course_id;
			$inputdata['rating']		= $this->input->post('score');
			$inputdata['comments']		= $this->input->post('comments');


			if($this->input->post('review_id')) {

				$inputdata['updated_at']	= date('Y-m-d H:i:s');

				if($this->base_model->update_operation($inputdata, 'tutor_reviews', array('id' => $this->input->post('review_id')))) {


					
					$this->prepare_flashmessage(get_languageword('Thanks for rating the tutor Your review successfully updated to the Tutor'), 0);

				} else {

					$this->prepare_flashmessage(get_languageword('Review not updated due to some technical issue Please retry again Thank you'), 2);
				}

			} else {

				$inputdata['created_at']	= date('Y-m-d H:i:s');
				$inputdata['updated_at']	= $inputdata['created_at'];

				//05-12-2018 admin notification 
				$review_id = $this->base_model->insert_operation_id($inputdata, 'tutor_reviews');

				if ($review_id) {

					$this->prepare_flashmessage(get_languageword('Thanks for rating the tutor Your review successfully sent to the Tutor'), 0);

				} else {

					$this->prepare_flashmessage(get_languageword('Review not sent due to some technical issue Please retry again Thank you'), 2);
				}
			}

			redirect(URL_STUDENT_ENQUIRIES.'/closed');

		}

		$review_det = $this->base_model->fetch_records_from('tutor_reviews', array('booking_id' => $booking_id));

		$this->data['review_det'] 	= (!empty($review_det)) ? $review_det[0] : array();
		$this->data['booking_id'] 	= $booking_id;
		$this->data['pagetitle'] 	= get_languageword('Rate Tutor')." (".getUserRec($booking_det->tutor_id)->username.")";
		$this->data['activemenu'] 	= "enquiries";
		$this->data['content'] 		= 'rate_tutor';
		$this->_render_page('template/site/student-template', $this->data);

	}



	/**
	 * [enrolled_courses description]
	 * @return [type] [description]
	 */
	function enrolled_courses()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$this->data['message'] = $this->session->flashdata('message');

		$user_id = $this->ion_auth->get_user_id();
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table($this->db->dbprefix('inst_enrolled_students'));
		$crud->where('student_id', $user_id);
		$crud->set_relation('course_id','categories','name');
		$crud->set_relation('inst_id','users','username');
		$crud->set_relation('tutor_id','users','username');
		$crud->set_subject( get_languageword('student_enrolled_courses') );

		$crud->columns('course_id','inst_id','batch_code','batch_name', 'time_slot','course_offering_location','duration_type', 'duration_value','batch_start_date', 'status');

		$crud->unset_add();
		$crud->unset_delete();

		$crud->edit_fields('status', 'status_desc', 'updated_at', 'prev_status', 'updated_by');

		//Hidden Fields
		$crud->field_type('updated_at', 'hidden', date('Y-m-d H:i:s'));
		$crud->field_type('updated_by', 'hidden', $user_id);

		$crud->display_as('course_id',get_languageword('course_Name'));
		$crud->display_as('inst_id',get_languageword('institute_Name'));
		$crud->display_as('tutor_id',get_languageword('Tutor Name'));

		if($crud_state == "edit") {

			$p_key = $this->uri->segment(4);

			$enroll_det = $this->base_model->fetch_records_from('inst_enrolled_students', array('enroll_id' => $p_key));

			if(!empty($enroll_det)) {

				$enroll_det = $enroll_det[0];

				if($enroll_det->student_id != $user_id) {

					$this->prepare_flashmessage(get_languageword('not_authorized'), 1);
	    			redirect(URL_STUDENT_ENROLLED_COURSES);
				}

				$booking_status = $enroll_det->status;

				$crud->field_type('prev_status', 'hidden', $booking_status);

				$crud->display_as('status', get_languageword('change_status'));

				if($booking_status == "pending")
					$crud->field_type('status', 'dropdown', array('cancelled_before_course_started' => get_languageword('cancel')));
				else if($booking_status == "closed" || $booking_status == "approved")
					$crud->field_type('status', 'dropdown', array('called_for_admin_intervention' => get_languageword('claim_for_admin_intervention')));
				else {
					$crud->field_type('prev_status', 'hidden', $enroll_det->prev_status);
					$crud->field_type('status', 'hidden', $booking_status);
				}

			} else {

				$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
	    		redirect(URL_STUDENT_ENROLLED_COURSES);
			}
		}

		if($crud_state == "read") {
			$crud->field_type('updated_at', 'visible');
		}


		$crud->callback_after_update(array($this, 'callback_after_enroll_status_update'));

		$output = $crud->render();

		$this->data['pagetitle'] = get_languageword('enrolled_courses');
		$this->data['grocery_output'] = $output;
		$this->data['activemenu'] = 'enrolled_courses';
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);

	}


	function callback_after_enroll_status_update($post_array,$primary_key)
	{

		$rec_det = $this->base_model->fetch_records_from('inst_enrolled_students', array('enroll_id' => $primary_key));

		if(empty($rec_det))
			return FALSE;

		$rec_det = $rec_det[0];

		//If Student Cancelled booking before session gets started, refund Student's Credits
		if($post_array['status'] == "cancelled_before_course_started") {

			$inst_rec = getUserRec($rec_det->inst_id);

			//Log Credits transaction data & update user net credits - Start
			$log_data = array(
							'user_id' => $rec_det->student_id,
							'credits' => $rec_det->fee,
							'per_credit_value' => $rec_det->per_credit_value,
							'action'  => 'credited',
							'purpose' => 'Slot booked with the Institute "'.$inst_rec->username.'" has cancelled by you before course started',
							'date_of_action	' => date('Y-m-d H:i:s'),
							'reference_table' => 'inst_enrolled_students',
							'reference_id' => $primary_key,
						);

			log_user_credits_transaction($log_data);

			update_user_credits($rec_det->student_id, $rec_det->fee, 'credit');
			//Log Credits transaction data & update user net credits - End
		}


		//If Student updates the status as "called_for_admin_intervention", Send email alert to Admin
		//Email Alert to Admin - Start
		//Get Claim By Student Email Template
		if($post_array['status'] == "called_for_admin_intervention") {

			$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '9'));

			if(!empty($email_tpl)) {

				$email_tpl = $email_tpl[0];


				if(!empty($email_tpl->from_email)) {

					$from = $email_tpl->from_email;

				} else {

					$from 	= $this->session->userdata('email');
				}

				$to 	= get_system_settings('Portal_Email');

				if(!empty($email_tpl->template_subject)) {

					$sub = $email_tpl->template_subject;

				} else {

					$sub = get_languageword("Claim By Student About Batch");
				}


				if(!empty($email_tpl->template_content)) {

					$original_vars  = array($this->session->userdata('username'), get_languageword('batch').' "'.$rec_det->batch_id.'"', '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>');
					$temp_vars		= array('___STUDENT_NAME___', '___BOOKING_ID___', '___LOGINLINK___');
					$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

				} else {

					$msg = "<p>
								".get_languageword('Hi').",</p>
							<p>
								".get_languageword('Student ')." &quot;".$this->session->userdata('username')."&quot; ".get_languageword('claimed for your intervention for the batch')." &quot;".$rec_det->batch_id."&quot;</p>
							<p>
								".get_languageword('please')." <a href='".URL_AUTH_LOGIN."'>".get_languageword('Login Here')."</a> ".get_languageword('to view the details')."</p>";

					$msg .= "<p>".get_languageword('Thank you')."</p>";
				}

				sendEmail($from, $to, $sub, $msg);
			}
			//Email Alert to Admin - End
		}
 
		return true;
	}


	/**
	 * [credits_transactions_history description]
	 * @return [type] [description]
	 */
	function credits_transactions_history()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		if($this->session->userdata('user_belongs_group') == 5)
			$student_id = $this->session->userdata('child_id');
		else
			$student_id = $this->ion_auth->get_user_id();
		
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table($this->db->dbprefix('user_credit_transactions'));
		$crud->where('user_id', $student_id);

		$crud->set_subject( get_languageword('user_credit_transactions') );

		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();

		$crud->columns('credits','action','purpose','date_of_action');

		$crud->unset_read_fields('user_id', 'reference_table', 'reference_id', 'per_credit_value');

		$output = $crud->render();

		$this->data['activemenu'] 	= "user_credit_transactions";
		$this->data['pagetitle'] = get_languageword('credits_transactions_history');
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}	
		
	/**
	 * [manual_payment_status description]
	 * @param  [type] $payment_id [description]
	 * @return [type]             [description]
	 */
	function manual_payment_status( $payment_id ) {
		$this->data['message'] = $this->session->flashdata('message');
		if ( ! empty( $payment_id ) ) {
			$check = $this->db->query( 'SELECT * FROM `'.$this->db->dbprefix('subscriptions').'` s INNER JOIN `'.$this->db->dbprefix('users').'` u ON s.user_id = u.id AND u.active = 1 AND s.payment_received = 0 WHERE s.id = ' . $payment_id .' AND u.id = ' . $this->ion_auth->get_user_id())->result();
			if ( empty( $check ) ) {
				safe_redirect( $this->ion_auth->get_user_id() );
			} else {
				if(isset($_POST['submitbutt']))
				{
					$this->form_validation->set_rules('payment_updated_user_message', get_languageword('Enter you comments'), 'trim|required|xss_clean');
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');			
					if ($this->form_validation->run() == TRUE)
					{
						$inputdata = array();						
						$inputdata['payment_updated_user'] = 'yes';
						$inputdata['payment_updated_user_date'] = date('Y-m-d H:i:s');
						$inputdata['payment_updated_user_message'] = $this->input->post('payment_updated_user_message');
						$this->base_model->update_operation($inputdata, 'subscriptions', array('id' => $payment_id));
						$this->prepare_flashmessage(get_languageword('record updated successfully'), 0);
						redirect('student/mysubscriptions');	
					}
				}
				$this->data['activemenu'] 	 = "packages";
				$this->data['activesubmenu'] = "mysubscriptions";
				$this->data['content'] 		 = 'manual_payment_status';
				$this->data['pagetitle'] 	 = get_languageword('manual_payment_status');
				$this->data['profile'] = $check[0];
				$this->_render_page('template/site/student-template', $this->data);
			}
		} else {
			$this->safe_redirect( site_url( 'student/mysubscriptions', 'Wrong operation' ) );
		}
	}

	/**
	 * [paywith_razorpay description]
	 * @return [type] [description]
	 */
	function paywith_razorpay() {
		$params = $this->uri->uri_to_assoc();
		print_r($params);
		if ( ! isset( $params['package'] ) || ! isset( $params['gateway'] ) ) {
			$this->safe_redirect();
		}
		$package_id = $params['package'];
		$gateway_id = $params['gateway'];
		
		$package_info 	= $this->db->get_where('packages',array('id' => $package_id))->result();
		
		$gateway_details = $this->base_model->get_payment_gateways(' AND st2.type_id = '.$gateway_id);
		
		if ( empty( $package_info ) || empty( $gateway_details ) ) {
			$this->safe_redirect();
		}
		
		$this->load->model('student_model');
		$this->data['package_data'] = $package_info;
		$this->data['payment_gateways'] = $gateway_details;
		$user_info = $this->base_model->get_user_details( $this->ion_auth->get_user_id() );
		$this->data['user_info'] = $user_info[0];
		$user_info = $user_info[0];
		$field_values = $this->db->get_where('system_settings_fields',array('type_id' => $gateway_id))->result();
		$razorpay_key_id = 'rzp_test_tjwMzd8bqhZkMr';
		$razorpay_key_secret = 'EWI9VQiMH43p6LDCbpsgvvHZ';
		$razorpay_payment_action = 'capture';
		$razorpay_mode = 'sandbox';
		
		foreach($field_values as $value) {
			if( $value->field_key == 'razorpay_key_id' ) {
				$razorpay_key_id = $value->field_output_value;
			}
			if( $value->field_key == 'razorpay_key_secret' ) {
				$razorpay_key_secret = $value->field_output_value;
			}
			if( $value->field_key == 'razorpay_payment_action' ) {
				$razorpay_payment_action = $value->field_output_value;
			}
			if( $value->field_key == 'razorpay_mode' ) {
				$razorpay_mode = $value->field_output_value;
			}
		}
		$package_info 	= $this->db->get_where('packages',array('id' => $package_id))->result();
		
		$total_amount 	= $package_info[0]->package_cost;
		if(isset($package_info[0]->discount) && ($package_info[0]->discount != 0))
		{
			if($package_info[0]->discount_type == 'Value')
			{
				$total_amount = $package_info[0]->package_cost - $package_info[0]->discount;				
			}
			else
			{
				$discount = ($package_info[0]->discount/100)*$package_info[0]->package_cost;						
				$total_amount = $package_info[0]->package_cost - $discount;
			}
		}
		$config = array(
			'razorpay_key_id' => $razorpay_key_id,
			'razorpay_key_secret' => $razorpay_key_secret,
			'razorpay_payment_action' => $razorpay_payment_action,
			'razorpay_mode' => $razorpay_mode,
			'total_amount' => $total_amount * 100,
			
			'product_name' => $package_info[0]->package_name,
			
			'firstname' => $user_info->first_name,
			'lastname' => $user_info->last_name,
			'email' => $user_info->email,
			'phone' => $user_info->phone,
			
			'success_url' => base_url() . 'payment/razorpay_success',
			'cancel_url' => base_url() . 'payment/razorpay_failed',
			'failed_url' => base_url() . 'payment/razorpay_success',
		);
		$this->data['razorpay'] = $config;
		
		$this->data['activemenu'] 	= "packages";
		$this->data['activesubmenu'] = "paywith_razorpay";	
		$this->data['content'] 		= 'paywith_razorpay';
		if ( $this->ion_auth->is_student() ) {
			$this->_render_page('template/site/student-template', $this->data);
		} elseif( $this->ion_auth->is_tutor() ) {
			$this->_render_page('template/site/tutor-template', $this->data);
		} elseif( $this->ion_auth->is_institute() ) {
			$this->_render_page('template/site/institute-template', $this->data);
		} else {
			$this->_render_page('template/site/admin-template', $this->data);
		}
	}



	/**
	 * [course_purchases description]
	 * @return [type] [description]
	 */
	function course_purchases()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$student_id = $this->ion_auth->get_user_id();

		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table(TBL_PREFIX.'course_purchases');
		$crud->set_relation('sc_id',TBL_PREFIX.'tutor_selling_courses','course_title');
		$crud->set_relation('tutor_id',TBL_PREFIX.'users','username');
		$crud->where('user_id', $student_id);
		$crud->where('payment_status', 'Completed');

		$crud->set_subject( get_languageword('My_Course_Purchases') );

		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_read();

		$crud->columns('sc_id','tutor_id','max_downloads','total_downloads','paid_date');

		$crud->display_as('sc_id', get_languageword('Course_Title'));
		$crud->display_as('tutor_id', get_languageword('tutor_name'));
		$crud->display_as('paid_date', get_languageword('Purchased_On'));

		$crud->add_action(get_languageword('View_Download_History'), URL_FRONT_IMAGES.'magnifier-grocery.png', URL_STUDENT_COURSE_DOWNLOAD_HISTORY.'/');
		$crud->add_action(get_languageword('Download_Course_Curriculum'), URL_FRONT_IMAGES.'download.png', URL_STUDENT_DOWNLOAD_COURSE.'/');

		$output = $crud->render();

		$this->data['activemenu'] 		= "my_course_purchases";
		$this->data['pagetitle'] 		= get_languageword('My_Course_Purchases');
		$this->data['grocery_output'] 	= $output;
		$this->data['grocery'] 			= TRUE;
		$this->grocery_output($this->data);

	}


	/**
	 * [download_course description]
	 * @param  string $purchase_id [description]
	 * @return [type]              [description]
	 */
	function download_course($purchase_id = '')
	{

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$student_id = $this->ion_auth->get_user_id();

		if(empty($purchase_id)) {

			$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
			redirect(URL_STUDENT_COURSE_PURCHASES);
		}


		$purchase_det = $this->base_model->fetch_records_from('course_purchases', array('user_id' => $student_id, 'payment_status' => 'Completed', 'purchase_id' => $purchase_id));


		if(empty($purchase_det)) {

			$this->prepare_flashmessage(get_languageword('You_have_not_purchased_the_course'), 1);
			redirect(URL_STUDENT_COURSE_PURCHASES);

		} else {

			$purchase_det = $purchase_det[0];
		}


		if(!empty($purchase_det)) {

			if(!($purchase_det->total_downloads < $purchase_det->max_downloads)) {

				$this->prepare_flashmessage(get_languageword('You_have_reached_maximum_limit_of_downloads_Please_purchase_the_course_again_to_download_the_fiels_Thank_you'), 2);
				redirect(URL_STUDENT_COURSE_PURCHASES);
			}
		}



		$dir 		= $purchase_id.'_'.$student_id;
		$zip_file_ph= URL_PUBLIC_UPLOADS.'course_curriculum_files/'.$dir.'.zip';


		$this->load->library('user_agent');
		$inputdata = array(
			'sc_id' 		=> $purchase_det->sc_id,
			'purchase_id' 	=> $purchase_id,
			'tutor_id' 		=> $purchase_det->tutor_id,
			'user_id' 		=> $purchase_det->user_id,
			'ip_address' 	=> $this->input->ip_address(),
			'browser' 		=> $this->agent->browser(),
			'browser_version' => $this->agent->version(),
			'platform' 		=> $this->agent->platform(),
			'mobile_device' => $this->agent->mobile(),
			'robot' 		=> $this->agent->robot(),
			'is_download_success' => 'No', 
			'downloaded_date' => date('Y-m-d H:i:s')
		);


		$zip = new ZipArchive;
		if ($zip->open($zip_file_ph) === TRUE) {

		    $inputdata['is_download_success'] = 'Yes';
		    $zip->close();

		    if($this->base_model->insert_operation($inputdata, 'course_downloads')) {

		    	$this->load->model('student_model');
		    	$this->student_model->update_student_course_downloads($student_id, $purchase_id);
		    	$this->student_model->update_tutor_course_downloads($purchase_det->tutor_id, $purchase_det->sc_id);
			}

			$course_title = $this->base_model->fetch_value('tutor_selling_courses', 'course_title', array('sc_id' => $purchase_det->sc_id));

			$this->load->library('zip');
			$this->zip->read_file($zip_file_ph);
			$this->zip->download( $course_title );

		} else {

			$this->prepare_flashmessage(get_languageword('Course_Curriculum_could_not_be_downloaded_due_to_some_technical_issue_Please_download_after_some_time'), 2);
			redirect(URL_STUDENT_COURSE_PURCHASES);
		}


	}


	/**
	 * [course_download_history description]
	 * @param  string $purchase_id [description]
	 * @return [type]              [description]
	 */
	function course_download_history($purchase_id = "")
	{

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$student_id = $this->ion_auth->get_user_id();

		if(empty($purchase_id)) {

			$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
			redirect(URL_STUDENT_COURSE_PURCHASES);
		}


		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table(TBL_PREFIX.'course_downloads');
		$crud->set_relation('sc_id',TBL_PREFIX.'tutor_selling_courses','course_title');
		$crud->set_relation('tutor_id',TBL_PREFIX.'users','username');
		$crud->where('user_id', $student_id);
		$crud->where('purchase_id', $purchase_id);

		$crud->set_subject( get_languageword('Course_Download_History') );

		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_read();

		$crud->columns('sc_id','tutor_id','ip_address','browser','browser_version', 'platform', 'downloaded_date');

		$crud->display_as('sc_id', get_languageword('Course_Title'));
		$crud->display_as('tutor_id', get_languageword('tutor_name'));

		$output = $crud->render();

		$this->data['activemenu'] 		= "my_course_purchases";
		$this->data['pagetitle'] 		= get_languageword('Course_Download_History');
		$this->data['grocery_output'] 	= $output;
		$this->data['grocery'] 			= TRUE;
		$this->grocery_output($this->data);

	}
	

	/******************************
	06-12-2018
	*******************************/
	/**
	 * [get_certificate description]
	 * @param  string $booking_id [description]
	 * @return [type]             [description]
	 */
	function get_certificate($booking_id = "")
	{

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : $booking_id;

		if(empty($booking_id)) {

			$this->prepare_flashmessage(get_languageword('Please complete your course to get certificate'), 2);
			redirect(URL_STUDENT_ENQUIRIES.'/closed');
		}

		$user_id = $this->ion_auth->get_user_id();

		//Check whether booking exists
		$booking_det = $this->base_model->get_query_result("SELECT b.*, c.name AS course_name FROM ".$this->db->dbprefix('bookings')." b INNER JOIN ".$this->db->dbprefix('categories')." c ON c.id=b.course_id WHERE b.booking_id=".$booking_id." AND b.student_id=".$user_id." AND b.status='closed' ");

		if(empty($booking_det)) {

			$this->prepare_flashmessage(get_languageword('Invalid request'), 1);
			redirect(URL_STUDENT_ENQUIRIES.'/closed');
		}

		$booking_det = $booking_det[0];

		if($booking_det->is_certificate_issued != "Yes") {

			$this->prepare_flashmessage(get_languageword('Certificate_not_issued_by_Tutor'), 2);
			redirect(URL_STUDENT_ENQUIRIES.'/closed');
		}

		$user_rec 	 = getUserRec($user_id);
		$tutor_rec 	 = getUserRec($booking_det->tutor_id);

		// Load all views as normal
		$this->load->view('course_completion_certificate', array('booking_det' => $booking_det, 'user_rec' => $user_rec, '$tutor_rec' => $tutor_rec));
		// Get output html
		$html = $this->output->get_output();
		echo $html;die();
	}
	
	function new_instant_bids()
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$student_id = $this->session->userdata('child_id');
		else
			$student_id = $this->ion_auth->get_user_id();
		
		$student_details = $this->base_model->get_user_info($student_id);
		if(!is_premium($student_id)) {
			$this->prepare_flashmessage(get_languageword('please_become_premium_member_to_book_tutor'), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}

		if(isset($_POST['submitbutt']))
		{
			$this->form_validation->set_rules('teaching_type_id', get_languageword('teaching_type'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('priority_of_requirement', get_languageword('priority_of_requirement'), 'trim|required|xss_clean');
			
						
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{	
				$inputdata['student_id']=$student_id;
				$inputdata['teaching_type_id'] = $this->input->post('teaching_type_id');
				$inputdata['priority_of_requirement']=$this->input->post('priority_of_requirement');
				$inputdata['priority_date_time']=$this->input->post('date')." ".$this->input->post('time');
				$inputdata['category'] = $this->input->post('category_id');
				$inputdata['tutor_rating']=$this->input->post('tutor_rating');
				$inputdata['budget']=$this->input->post('budget');
				$inputdata['budget_type']=$this->input->post('budget_type');
				$inputdata['requirement_details']=$this->input->post('requirement_details');
				$inputdata['title_of_requirement']=$this->input->post('title_of_requirement' );
				
				$time_zone_list = $this->base_model->get_timezone_list();
				$time_zone_list = explode(",",$time_zone_list->field_type_values);
				$time_zone = $time_zone_list[$student_details->time_zone];
				
				$t = strtotime($this->input->post('iq_date'));
				$time_slot = explode("-",$this->input->post('iq_time_slot'));
				$userTimezone = new DateTimeZone($time_zone);
				$gmtTimezone = date_create("now",timezone_open("UTC"));
				$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
				$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
				
				$offset = timezone_offset_get($userTimezone,$gmtTimezone);
				
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->sub($myInterval);
				$myDateTime_end->sub($myInterval);
				
				$inputdata['iq_time_slot']=$myDateTime_start->format('H')."-".$myDateTime_end->format('H');
				$inputdata['iq_date']=$myDateTime_start->format("Y-m-d");
				$inputdata['created_at']=date("Y-m-d H:i:s");
				$inputdata['updated_at']=$inputdata['created_at'];
				$inputdata['status'] = "new";
				$inputdata['created_by'] = $student_id;
				$inputdata['updated_by'] = $student_id;
				if(isset($_FILES['attach']) && !empty($_FILES['attach'])){
					$myFile = $_FILES['attach'];
                	$fileCount = count($myFile["name"]);
                	$insert_id_list = "";
                	for($i = 0;$i < $fileCount;$i++){
						$ext = pathinfo($myFile['name'][$i], PATHINFO_EXTENSION);
						$fileName = $student_id."_".date("YmdHis").".".$ext;
						$filePath = './assets/uploads/instant_bids/'.$fileName;
						move_uploaded_file($myFile['tmp_name'][$i], $filePath);	
						if($i > 0)
							$insert_id_list .= ",";
						$insert_id_list .= $this->base_model->update_attach_name($fileName,$student_id);
					}
					$inputdata['attach'] = $insert_id_list;
				}
				$tutor_list = $this->base_model->get_tutor_list_from_category($inputdata['category']);
				foreach($tutor_list as $key => $value){
					$inputdata['tutor_id'] = $value->id;
					$instant_bids_id = $this->base_model->insert_operation_id($inputdata, 'instant_bids');	
				}

				//05-12-2018 admin notification start
				/*$data = array();
				$data['user_id'] 	= $student_id;
				$data['title'] 		= get_languageword('student_posted_new_instant_question');
				$data['content'] 	= "Student has been Posted a Instant Question "." ".$this->input->post('title_of_requirement');
				$data['datetime']   = date('Y-m-d H:i:s');
				$data['admin_read'] = 0;
				$data['page_link']  = SITEURL."admin/all_iqs";
				$data['table_name'] = "instant_bids";
				$data['primary_key_column'] = "id";
				$data['primary_key_value']  = $instant_bids_id;

				$this->base_model->insert_operation($data,'notifications');	
				unset($data);*/
				//admin notification end
				
				if(!empty($inputdata['category'])){
					
					$device_ids = array();
					$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '26'));
					$email_tpl = $email_tpl[0];
					$sub = $msg = "";
					if(!empty($email_tpl)){
						$sub = $email_tpl->template_subject;
						$msg = $email_tpl->template_content;
					}
					
					foreach($tutor_list as $k => $v){
						if(!empty($v->device_id)){
							array_push($device_ids,$v->device_id);
						}
						if(!empty($v->email)){
							if(!empty($msg)){
								$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
								$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
								$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$v->username, $this->ion_auth->get_user_name(), $inputdata['iq_time_slot'], $inputdata['teaching_type_id'], $login_link,$this->config->item('site_title', 'ion_auth'));
								$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR_NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','TITLE');
								$content = str_replace($temp_vars, $original_vars, $msg);
								$email_settings = (array)$this->config->item('email_settings');
								sendEmail($email_settings['webmail']['User Name'], $v->email, $sub, $content);
							}
						}
					}
					if(count($device_ids) > 0){
						$this->load->library('OneSignalPush');
						$message = array(
						  "en" => "New IQ bid is added",
						  "title" => "New Instant Question Bid",
						  "icon" => "myicon",
						  "sound"=>"default"
						);
						
						$data = array(
							"body" => "New IQ bid is added",
							"title" => "New Instant Question Bid",
						);
						$gcpm = new OneSignalPush();
						$gcpm->setDevices($device_ids);
						$res = $gcpm->send($message,$data);
					}					
				}


				$this->prepare_flashmessage(get_languageword('Your Instant Question posted successfully'), 0);
				redirect(URL_STUDENT_INSTANT_BIDS);	
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}			
		
		}	
		$this->data['profile'] = getUserRec();

		
		$categories = get_categories();
		$categories_option = array();
		foreach($categories as $key => $value){
			$categories_option[$value->id] = $value->name;
		}
		$this->data['categories'] = $categories_option;

		$this->data['activemenu'] 	= "instant";
		$this->data['activesubmenu'] = "new";
		$this->data['content'] 		= 'new_instant_bids';
		$this->data['pagetitle'] = get_languageword('New Instant Bids');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function instant_bids($param = "")
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table(TBL_INSTANT_BIDS);
		$crud->set_relation('student_id',TBL_USERS, 'time_zone');
		$crud->where(TBL_INSTANT_BIDS.'.student_id', $user_id);

		$status_arr = array('pending', 'new', 'approved');
		if(in_array($param, $status_arr)) {
			$crud->where(TBL_INSTANT_BIDS.'.status', $param);
		}
		
		$crud->unset_add();
		if($param == "approved"){
			$crud->unset_edit();
			$crud->unset_delete();
		}
		$crud->set_subject( get_languageword('Instant Question') );
		$crud->set_enable_initiate_session_option_before_mins($this->config->item('site_settings')->enable_initiate_session_option_before_mins);

		//List Table Columns
		$crud->columns('teaching_type_id', 'tutor_id', 'priority_of_requirement', 'category', 'budget_type', 'budget','title_of_requirement', 'iq_date', 'iq_time_slot', 'status', 'student_id');

		$crud->callback_column('category',array($this,'_callback_category_id'));
		$crud->callback_column('tutor_id',array($this,'_callback_tutor_name'));

		//Display Alias Names
		$crud->display_as('teaching_type_id',get_languageword('teaching_type'));
		$crud->display_as('budget',get_languageword('credits'));
		$crud->display_as('tutor_id',get_languageword('tutor'));
		$crud->display_as('iq_date',get_languageword('Available Date'));
		$crud->display_as('iq_time_slot',get_languageword('Available time'));


		if($param == "closed") {
			$crud->add_action(get_languageword('rate_this_tutor'), URL_FRONT_IMAGES.'/star.png', URL_STUDENT_RATE_TUTOR.'/');
			$crud->add_action(get_languageword('get_certificate'), URL_FRONT_IMAGES.'print.png', '', 'stu-certificate', array($this,'callback_student_certificate'));
		}

		//Form fields for Edit Record
		$crud->edit_fields('status', 'updated_at', 'updated_by');

		//Hidden Fields
		$crud->field_type('updated_at', 'hidden', date('Y-m-d H:i:s'));
		$crud->field_type('updated_by', 'hidden', $user_id);

		//Unset Fields
		$crud->unset_fields('student_id');


		//Authenticate whether Tutor editing/viewing his records only
		if($crud_state == "edit" || $crud_state == "read") {

			if($param != "" && $param != "add" && $param != "edit" && $param != "read" && $param != "success")
				$p_key = $this->uri->segment(4);
			else
				$p_key = $this->uri->segment(3);

			$booking_det = $this->base_model->fetch_records_from('instant_bids', array('id' => $p_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];

				if($booking_det->student_id != $user_id) {

					$this->prepare_flashmessage(get_languageword('not_authorized'), 1);
	    			redirect(URL_STUDENT_ENQUIRIES);
				}

				if($crud_state == "edit") {

					$booking_status = $booking_det->status;

					$updated_by = getUserType($booking_det->updated_by);

					$crud->display_as('status', get_languageword('change_status'));

					if(in_array($booking_status, array('pending', 'approved', 'session_initiated', 'running', 'completed'))) {
						$crud->required_fields(array('status'));
					}
					
					if($booking_status == "new") {
						$crud->edit_fields();
					}

					if($booking_status == "pending") {
						$crud->field_type('status', 'dropdown', array('approved' => get_languageword('approved'),'cancel' => get_languageword('cancel')));
					}

					if($booking_status == "approved") {
						//$status = array('cancelled_before_course_started' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
						//$crud->field_type('status', 'dropdown', $status);
					}
				}

			} else {
				$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
	    		redirect(URL_STUDENT_ENQUIRIES);
			}
		}


		if($crud_state == "read") {
			$crud->field_type('updated_at','visible');
		}

		//05-12-2018 admin notification start
		$methd = $this->uri->segment(3);
		$stus = $this->input->post('status');
		if($stus == 'approved' ){
			$this->load->model('student_model');
			$booking_info = array();
			$current_method = "";
			if($methd != "update" && $methd != "update_validation"){
				$booking_info = (array)$this->student_model->get_booking_detail($this->uri->segment(3));
				$current_method = $this->uri->segment(2);
			} else{
				$booking_info = (array)$this->student_model->get_booking_detail($this->uri->segment(4));
				$current_method = $this->uri->segment(3);
			}
			if($current_method == "update"){
				$data['student_id'] = $booking_info['student_id'];
				$data['tutor_id'] = $booking_info['tutor_id'];
				$data['course_id'] = $this->student_model->get_course_iq_id($booking_info['category']);
				$data['content'] = $booking_info['title_of_requirement'];
				$data['duration_value'] = 1;
				$data['duration_type'] = 'hours';
				$data['fee'] = $booking_info['budget'];
				$data['per_credit_value'] = get_system_settings('per_credit_value');
				$data['start_date'] = $booking_info['iq_date'];
				$data['end_date'] = $booking_info['iq_date'];
				if($booking_info['teaching_type_id'] == 'Online ZOOM'){
					$data['time_slot'] = $booking_info['iq_time_slot'];
					$data['preferred_location'] = 'online-zoom';
				}
				if($booking_info['teaching_type_id'] == 'Record')
					$data['preferred_location'] = 'record';
				$data['prev_status'] = $data['status'] = 'pending';
				$data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $user_id;
				$data['created_by'] = $booking_info['created_by'];
				$data['admin_commission'] = get_system_settings('admin_commission_for_a_booking');
				$data['admin_commission_val']   = round($data['fee'] * ($data['admin_commission'] / 100));
				$data['tution'] = 'One on One';
				$data['iq_id'] = $booking_info['id'];
				$this->student_model->insert_iq_to_booking($data);
				
				$device_ids = array();
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '28'));
				$email_tpl = $email_tpl[0];
				$sub = $msg = "";
				if(!empty($email_tpl)){
					$sub = $email_tpl->template_subject;
					$msg = $email_tpl->template_content;
				}
				$tutor_info = (array)$this->base_model->get_tutor_info($booking_info['tutor_id']);
				if(!empty($tutor_info['device_id'])){
					array_push($device_ids,$tutor_info['device_id']);
				}
				if(!empty($tutor_info['email'])){
					if(!empty($msg)){
						$login_link = "<a href='".SITEURL."auth/login"."'>Login</a>";
						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
						$original_vars  = array($logo_img,$this->config->item('site_settings')->site_title,$tutor_info['username'], $this->ion_auth->get_user_name(), $booking_info['iq_time_slot'], $booking_info['teaching_type_id'], $login_link,$this->config->item('site_title', 'ion_auth'));
						$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_TUTOR__NAME_', '_STUDENT_NAME_', '_DATE_TIME_', '_IQ_PREFERRED_TEACHING_TYPE_', '_LOGIN_LINK_','IQ_TITLE');
						$content = str_replace($temp_vars, $original_vars, $msg);
						$email_settings = (array)$this->config->item('email_settings');
						sendEmail($email_settings['webmail']['User Name'], $tutor_info['email'], $sub, $content);
					}
				}
				if(count($device_ids) > 0){
					$this->load->library('OneSignalPush');
					$message = array(
					  "en" => "Student Accept Bid",
					  "title" => "Student Accept Bid",
					  "icon" => "myicon",
					  "sound"=>"default"
					);
					$data = array(
						"body" => "Student Accept Bid",
						"title" => "Student Accept Bid",
					);
					$gcpm = new OneSignalPush();
					$gcpm->setDevices($device_ids);
					$res = $gcpm->send($message,$data);
				}
			}
		}
		
		$crud->callback_column('status', array($this, 'callback_column_booking_status'));

		$crud->callback_update(array($this,'callback_send_email_iq'));
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$crud->set_timezone($time_zone_list);
		$output = $crud->render();
		if($param == '')
			$param = 'all';
		$param = get_languageword($param);
		$this->data['pagetitle'] = get_languageword('Instant Questions');
		$this->data['activemenu'] 	= "instant";
		$this->data['activesubmenu'] = $param;
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}
	
	function change_book_running(){
		$this->load->model('student_model');
		$user_id = $this->ion_auth->get_user_id();
		$booking_id = $_POST['booking_id'];
		$booking_info = $this->student_model->get_course_booking_detail($booking_id);
		
		$tutor_info = (array)$this->base_model->get_user_info($booking_info->tutor_id);
		$course_name = (array)$this->base_model->get_course_name($booking_info->course_id);
		$cur_date = date("Ymd");
		$url = str_replace(' ','',"https://bitpaper.io/go/".$course_name['name']."_".$cur_date."_".$booking_info->time_slot."hrs/".$tutor_info['username']);
		$data['url'] = $url;
		if($tutor_info['whiteboard'] == 'Yes' )
			$data['is_open'] = 1;
		else if(($tutor_info['gcal_hangouts'] == 'Yes' || $tutor_info['gcal_zoom'] == 'Yes') && $booking_info->tution == 'One on One'){
			if($user_id == $booking_info['tutor_id'])
				$data['is_open'] = 1;
			else
				$data['is_open'] = 0;
		}
		else if($tutor_info['gcal_zoom'] == 'Yes' && $booking_info->tution == 'Share'){
			if($user_id == $booking_info->tutor_id)
				$data['is_open'] = 1;
			else
				$data['is_open'] = 0;
		} else{
			$data['is_open'] = 0;
		}
		echo json_encode($data);
	}
	
	public function getUsers ($api_key, $api_secret) {
	    //list users endpoint GET https://api.zoom.us/v2/users
		$ch = curl_init('https://api.zoom.us/v2/users');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  
	    // add token to the authorization header
	    $token = $this->generateJWT($api_key, $api_secret);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $token
		));
		$response = curl_exec($ch);
		$response = json_decode($response);
		$data['response'] = $response;
		$data['token'] = $token;
		return $data;
	}
	
	public function generateJWT ($api_key, $api_secret) {
		$key = $api_key;
		$secret = $api_secret;
		$token = array(
			"iss" => $key,
	        // The benefit of JWT is expiry tokens, we'll set this one to expire in 1 minute
			"exp" => time() + 36000
		);
		$token = JWT::encode($token, $secret);
		return $token;
	} 
	
	function get_attach_list_web(){
		$data = $_POST['attach'];
		$data = explode(",",$data);
		$html = "";
		if(!empty($data)){
			foreach($data as $key => $value){
				$result = $this->base_model->get_iq_attach_list($value);
				$html .= '<div style="margin-bottom:10px;"><a href="'.base_url().'assets/uploads/instant_bids/'.$result->filename.'" download>'.$result->filename.'</a></div>';
			}	
		}
		$result = array();
		$result['html'] = $html;
		echo json_encode($result);
	}
	
	public function google_calendar(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		$this->data['message'] = $this->session->flashdata('message');
		$profile = getUserRec();
		$email = $profile->email;
		$email = explode("@",$email);
		$this->data['email'] = $email[0];
		$this->data['email1'] = $email[1];
		$this->data['activemenu'] 	= "enquiries";
		$this->data['activesubmenu'] = get_languageword('g_sessions');
		$this->data['content'] 		= 'google_calendar';
		$this->data['pagetitle'] = get_languageword('g_Sessions');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	public function iq_live($course_slug = '', $location_slug = '', $teaching_type_slug = ''){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$course_slug = (!empty($course_slug)) ? array($course_slug) : $this->input->post('course_slug');

		$location_slug = (!empty($location_slug)) ? array($location_slug) : $this->input->post('location_slug');

		$teaching_type_slug = (!empty($teaching_type_slug)) ? array($teaching_type_slug) : $this->input->post('teaching_type_slug');


		if(!empty($course_slug[0]) && $course_slug[0] == "by_location")
			$course_slug = '';
		if(!empty($course_slug[0]) && $course_slug[0] == "by_teaching_type") {
			$teaching_type_slug = $location_slug;
			$course_slug   = '';
			$location_slug = '';
		}
		$course_slug = str_replace('_', '-', $course_slug);
		$location_slug = str_replace('_', '-', $location_slug);
		$teaching_type_slug = str_replace('_', '-', $teaching_type_slug);

		$params = array(
			'limit' 	  	=> LIMIT_PROFILES_LIST, 
			'course_slug' 	=> $course_slug, 
			'location_slug' => $location_slug, 
			'teaching_type_slug' => $teaching_type_slug,
			'iq_live_indicator' 		=> 'ONLINE'
		);

		$tutor_list = $this->home_model->get_tutors($params);
		$cur_date = date("Y-m-d");
		foreach($tutor_list as $key => $value){
			$session_list = $this->base_model->get_zoom_seesion_list($value->id,$cur_date);
			if(empty($session_list))
				$tutor_list[$key]->cur_status = 'AVAILABLE';
			else{
				$cur_time = date("H");
				$is_exist = 0;
				foreach($session_list as $k => $v){
					$time_slot = $v->time_slot;
					$time_slot = explode("-",$time_slot);
					if($time_slot[0] <= $cur_time & $time_slot[1] >= $cur_time){
						$tutor_list[$key]->cur_status = 'BUSY';
						$is_exist = 1;
						break;
					}
				}
				if($is_exist == 0)
					$tutor_list[$key]->cur_status = 'AVAILABLE';
			}
		}
		
		$this->data['tutor_list'] = $tutor_list;
		
			
		//total rows count
		unset($params['limit']);
        $total_records = count($this->home_model->get_tutors($params));


		$this->data['total_records'] = $total_records;
		$this->data['course_slug'] 	 = $course_slug;
		$this->data['location_slug'] = $location_slug;
		$this->data['teaching_type_slug'] = $teaching_type_slug;


		/*** Drop-down Options - Start ***/
		$show_records_count_in_search_filters = strip_tags($this->config->item('site_settings')->show_records_count_in_search_filters);
		$avail_records_cnt = "";
		//Course Options
		$courses = $this->home_model->get_courses();
		$course_opts[''] = get_languageword('select');
		if(!empty($courses)) {
			foreach ($courses as $key => $value) {
				if($show_records_count_in_search_filters == "Yes") {

					$avail_records_cnt = " (".count($this->home_model->get_tutors(array('course_slug'=>$value->slug, 'location_slug'=>$location_slug, 'teaching_type_slug'=>$teaching_type_slug))).")";
				}
				$course_opts[$value->slug] = $value->name.$avail_records_cnt;
			}
		}
		$this->data['course_opts'] = $course_opts;


		//Location Options
		$locations = $this->home_model->get_locations(array('child' => true));
		$location_opts[''] = get_languageword('select');
		if(!empty($locations)) {
			foreach ($locations as $key => $value) {
				if($show_records_count_in_search_filters == "Yes") {

					$avail_records_cnt = " (".count($this->home_model->get_tutors(array('location_slug'=>$value->slug, 'course_slug'=>$course_slug, 'teaching_type_slug'=>$teaching_type_slug))).")";
				}
				$location_opts[$value->slug] = $value->location_name.$avail_records_cnt;
			}
		}
		$this->data['location_opts'] = $location_opts;


		//Teaching type Options
		$teaching_types = $this->home_model->get_teaching_types();
		$teaching_type_opts[''] = get_languageword('select');
		foreach ($teaching_types as $key => $value) {
			if($show_records_count_in_search_filters == "Yes") {

				$avail_records_cnt = " (".count($this->home_model->get_tutors(array('teaching_type_slug'=>$value->slug, 'course_slug'=>$course_slug, 'location_slug'=>$location_slug))).")";
			}
			$teaching_type_opts[$value->slug] = $value->teaching_type.$avail_records_cnt;
		}
		$this->data['teaching_type_opts'] = $teaching_type_opts;
		
		
		$this->data['message'] = $this->session->flashdata('message');

		$this->data['activemenu'] 	= "iq_live";
		$this->data['content'] 		= 'iq_live';
		$this->data['pagetitle'] = get_languageword('IQ LIVE');
		$this->_render_page('template/site/student-template', $this->data);		
	}
	
	function event_today_dashboard(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$user_id = $this->ion_auth->get_user_id();
		//$my_relation_status = $this->base_model->get_parent_status($user_id);
		//$this->session->set_userdata('my_relation_status',$my_relation_status);

		$this->data['message'] = $this->session->flashdata('message');
		$user_id = $this->ion_auth->get_user_id();
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$student_details = $this->base_model->get_user_info($user_id);
		if(!empty($student_details->time_zone))
			$time_zone = $time_zone_list[$student_details->time_zone];
		else
			$time_zone = "UTC";
		
		$cur_date = date("Y-m-d");
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$cur_date_time = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone($time_zone));
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$cur_date_time->add($myInterval);
		
		$today_events = $this->base_model->get_today_events($user_id,$cur_date,'Student');
		if(!empty($today_events)){
			$add_events = array();
			$cnt_add_events = 0;
			foreach($today_events as $key => $value){
				$time_slot = explode("-",$value->time_slot);
				$myDateTime_start = new DateTime(date('Y-m-d',strtotime($value->start_date))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
				$myDateTime_end = new DateTime(date('Y-m-d',strtotime($value->end_date))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));		
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->add($myInterval);
				$myDateTime_end->add($myInterval);
				if($cur_date_time->format("Y-m-d") >= $myDateTime_start->format("Y-m-d") && $cur_date_time->format("Y-m-d") <= $myDateTime_end->format("Y-m-d")){
					$change_event = $this->base_model->get_change_event($value->booking_id,$value->time_slot,date("Y-m-d"));
					if(empty($change_event)){
						$today_events[$key]->student_date = $cur_date_time->format("Y-m-d");
						$today_events[$key]->student_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
						$today_events[$key]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$cur_date_time->format("Ymd")."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$value->username)."_".$cur_date_time->format("Ymd");
					}else if($change_event->type == 'change' && $change_event->date == $change_event->org_date){
						$time_slot = explode("-",$change_event->time_slot);
						$myDateTime_start = new DateTime(date('Y-m-d',strtotime($change_event->date))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
						$myDateTime_end = new DateTime(date('Y-m-d',strtotime($change_event->date))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
						$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
						$myDateTime_start->add($myInterval);
						$myDateTime_end->add($myInterval);
						
						$today_events[$key]->student_date = $myDateTime_start->format("Y-m-d");
						$today_events[$key]->student_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
						$today_events[$key]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$myDateTime_start->format("Ymd")."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$value->username)."_".$myDateTime_start->format("Ymd");
						$today_events[$key]->org_date = $change_event->org_date;
						$today_events[$key]->org_time_slot = $change_event->org_time_slot;
					}
					$add_change_event = $this->base_model->get_add_change_event($value->booking_id,$cur_date_time->format("Y-m-d"));
					if(!empty($add_change_event)){
						foreach($add_change_event as $k => $v){
							$temp = $this->base_model->get_specific_event($user_id,$cur_date,'Tutor',$value->booking_id);
							$add_events[$cnt_add_events] = $temp;
							$time_slot1 = explode("-",$v->time_slot);
							
							$myDateTime_start1 = new DateTime(date('Y-m-d',strtotime($v->date))." ".$time_slot1[0].":00:00", new DateTimeZone($time_zone));
							$myDateTime_end1 = new DateTime(date('Y-m-d',strtotime($v->date))." ".$time_slot1[1].":00:00", new DateTimeZone($time_zone));	
							$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
							$myDateTime_start1->add($myInterval);
							$myDateTime_end1->add($myInterval);
							
							$add_events[$cnt_add_events]->tutor_date = $myDateTime_start1->format("Y-m-d");
							$add_events[$cnt_add_events]->tutor_time_slot = $myDateTime_start1->format("H")."-".$myDateTime_end1->format("H");
							$add_events[$cnt_add_events]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$myDateTime_start1->format('Ymd')."_".$myDateTime_start1->format("H").$myDateTime_end1->format("H")."/".str_replace(" ","",$tutor_details->username)."_".$myDateTime_start1->format('Ymd');
							$add_events[$cnt_add_events]->org_date = $v->org_date;
							$add_events[$cnt_add_events]->org_time_slot = $v->org_time_slot;
							$cnt_add_events++;
						}
					}
				}
			}
			if(!empty($add_events)){
				$upcoming_cnt = count($upcoming_events);
				foreach($add_events as $key => $value){
					$upcoming_events[$upcoming_cnt+$key] = $value;
				}
			}
		}
		$this->data['date_format'] = $this->base_model->get_date_format();
		$this->data['today_events'] = $today_events;
		$this->data['pagetitle'] 	= get_languageword('Event Dashboard');
		$this->data['activemenu'] 	= "event_dashboard";
		$this->data['activesubmenu'] 	= "today";
		$this->data['content'] 		= 'event_today_dashboard';
		$this->data['is_student']   = $this->ion_auth->is_student();
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function event_upcoming_dashboard(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$user_id = $this->ion_auth->get_user_id();
		$my_relation_status = $this->base_model->get_parent_status($user_id);
		$this->session->set_userdata('my_relation_status',$my_relation_status);

		$this->data['message'] = $this->session->flashdata('message');
		$user_id = $this->ion_auth->get_user_id();
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$student_details = $this->base_model->get_user_info($user_id);
		if(!empty($student_details->time_zone))
			$time_zone = $time_zone_list[$student_details->time_zone];
		else
			$time_zone = "UTC";
		
		$datetime = new DateTime('tomorrow');
		$tomorrow_date = $datetime->format('Y-m-d');
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));
		$tomorrow_date_time = new DateTime(date('Y-m-d', strtotime($datetime->format('Y-m-d'))), new DateTimeZone($time_zone));
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		
		$cur_date_time = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone($time_zone));
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$cur_date_time->add($myInterval);
		
		if($cur_date_time->format('Y-m-d') != $tomorrow_date_time->format('Y-m-d'))
			$tomorrow_date = $tomorrow_date_time->format('Y-m-d');
		
		$upcoming_events = $this->base_model->get_today_events($user_id,$tomorrow_date,'Student');
		if(!empty($upcoming_events)){
			$add_events = array();
			$cnt_add_events = 0;
			foreach($upcoming_events as $key => $value){
				$time_slot = explode("-",$value->time_slot);
				$myDateTime_start = new DateTime(date('Y-m-d',strtotime($value->start_date))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
				$myDateTime_end = new DateTime(date('Y-m-d',strtotime($value->end_date))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->add($myInterval);
				$myDateTime_end->add($myInterval);
				if($tomorrow_date_time->format("Y-m-d") >= $myDateTime_start->format("Y-m-d") && $tomorrow_date_time->format("Y-m-d") <= $myDateTime_end->format("Y-m-d")){
					$change_event = $this->base_model->get_change_event($value->booking_id,$value->time_slot,$tomorrow_date_time->format("Y-m-d"));
					if(empty($change_event) && ($tomorrow_date == $myDateTime_start->format("Y-m-d") || $tomorrow_date <= $value->end_date)){
						$upcoming_events[$key]->student_date = $tomorrow_date_time->format("Y-m-d");
						$upcoming_events[$key]->student_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
						$upcoming_events[$key]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$tomorrow_date_time->format('Ymd')."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$value->username)."_".$tomorrow_date_time->format('Ymd');
					}else if($change_event->type == 'change' && $change_event->date == $change_event->org_date){
						$time_slot = explode("-",$change_event->time_slot);
						$myDateTime_start = new DateTime(date('Y-m-d',strtotime($change_event->date))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
						$myDateTime_end = new DateTime(date('Y-m-d',strtotime($change_event->date))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
						$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
						$myDateTime_start->add($myInterval);
						$myDateTime_end->add($myInterval);
						
						$upcoming_events[$key]->student_date = $tomorrow_date_time->format("Y-m-d");
						$upcoming_events[$key]->student_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
						$upcoming_events[$key]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$tomorrow_date_time->format('Ymd')."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$value->username)."_".$tomorrow_date_time->format('Ymd');
						$upcoming_events[$key]->org_date = $change_event->org_date;
						$upcoming_events[$key]->org_time_slot = $change_event->org_time_slot;
					}
					$add_change_event = $this->base_model->get_add_change_event($value->booking_id,$tomorrow_date_time->format("Y-m-d"));
					if(!empty($add_change_event)){
						foreach($add_change_event as $k => $v){
							$temp = $this->base_model->get_specific_event($user_id,$tomorrow_date,'Tutor',$value->booking_id);
							$add_events[$cnt_add_events] = $temp;
							$time_slot1 = explode("-",$v->time_slot);
							
							$myDateTime_start1 = new DateTime(date('Y-m-d',strtotime($v->date))." ".$time_slot1[0].":00:00", new DateTimeZone($time_zone));
							$myDateTime_end1 = new DateTime(date('Y-m-d',strtotime($v->date))." ".$time_slot1[1].":00:00", new DateTimeZone($time_zone));	
							$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
							$myDateTime_start1->add($myInterval);
							$myDateTime_end1->add($myInterval);
							
							$add_events[$cnt_add_events]->tutor_date = $myDateTime_start1->format("Y-m-d");
							$add_events[$cnt_add_events]->tutor_time_slot = $myDateTime_start1->format("H")."-".$myDateTime_end1->format("H");
							$add_events[$cnt_add_events]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->course_name)."_".$myDateTime_start1->format('Ymd')."_".$myDateTime_start1->format("H").$myDateTime_end1->format("H")."/".str_replace(" ","",$tutor_details->username)."_".$myDateTime_start1->format('Ymd');
							$add_events[$cnt_add_events]->org_date = $v->org_date;
							$add_events[$cnt_add_events]->org_time_slot = $v->org_time_slot;
							$cnt_add_events++;
						}
					}
				}
			}
			if(!empty($add_events)){
				$upcoming_cnt = count($upcoming_events);
				foreach($add_events as $key => $value){
					$upcoming_events[$upcoming_cnt+$key] = $value;
				}
			}
		}
		$this->data['date_format'] = $this->base_model->get_date_format();
		$this->data['upcoming_events'] = $upcoming_events;
		$this->data['pagetitle'] 	= get_languageword('Event Dashboard');
		$this->data['activemenu'] 	= "event_dashboard";
		$this->data['activesubmenu'] 	= "upcoming";
		$this->data['content'] 		= 'event_upcoming_dashboard';
		$this->data['is_student']   = $this->ion_auth->is_student();
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function event_previous_dashboard(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$user_id = $this->ion_auth->get_user_id();
		$my_relation_status = $this->base_model->get_parent_status($user_id);
		$this->session->set_userdata('my_relation_status',$my_relation_status);

		$this->data['message'] = $this->session->flashdata('message');
		$user_id = $this->ion_auth->get_user_id();
		
		$this->data['pagetitle'] 	= get_languageword('Event Dashboard');
		$this->data['activemenu'] 	= "event_dashboard";
		$this->data['activesubmenu'] 	= "previous";
		$this->data['content'] 		= 'event_previous_dashboard';
		$this->data['is_student']   = $this->ion_auth->is_student();
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function select_child(){
		$result = array();
		$result['status'] = false;
		if($_SESSION['user_belongs_group'] == 5){
			$email = $this->session->userdata('email');
			$child_list = $this->home_model->get_child_list($email);
			if(count($child_list) == 1)
				$this->session->set_userdata('child_id',$child_list[0]->id);
			else if(count($child_list) > 1){
				$result['list'] = $child_list;
				$result['status'] = true;
			}
		}
		
		echo json_encode($result);
	}
	
	function select_child1(){
		$result = array();
		$email = $_POST['email'];
		$child_list = $this->home_model->get_child_list($email);
		$result['list'] = $child_list;
		echo json_encode($result);
	}

	function get_parent(){
		$result = array();
		$email = $_POST['email'];
		$parent_info = $this->home_model->get_parent_details($email);
		$result['info'] = $parent_info;
		echo json_encode($result);
	}
	
	function add_child_student(){
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_parent()) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		
		$user_id = $this->ion_auth->get_user_id();

		if(isset($_POST['submitbutt']))
		{
			$is_exist = 0;
			
			foreach($_POST as $key => $value){
				if(strpos($key, 'child_email') !== false){
					if($is_exist == 0)
						$is_exist = 1;
				}
				if($is_exist == 1){
					$is_exist = 2;
					$this->base_model->format_child_list($this->session->userdata('email'));
				}
				if(strpos($key, 'child_email') !== false && !empty($value)){
					$this->base_model->register_child($this->session->userdata('email'),$value);
					
				}
			}
			$last_email = $_POST['child_email'.$_POST['new_add_child']];
			if(!empty($last_email)){
				$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '31'));
				if(!empty($email_tpl)) {
					$email_tpl = $email_tpl[0];
					$student_rec = $this->base_model->get_user_info_by_email($last_email);
					$tutor_rec 	 = getUserRec($user_id);

					if(!empty($email_tpl->from_email)) {
						$from = $email_tpl->from_email;
					} else {
						$from 	= get_system_settings('Portal_Email');
					}

					$to 	= $last_email;

					if(!empty($email_tpl->template_subject)) {
						$sub = $email_tpl->template_subject;
					} else {
						$sub = get_languageword("Booking Request From Student");
					}

					if(!empty($email_tpl->template_content)) {
						$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

						$site_title = $this->config->item('site_settings')->site_title;


						$original_vars  = array($logo_img, $site_title, $student_rec->username, $tutor_rec->username, '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);


						$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_STUDENT_NAME_', '_PARENT_NAME_', '_LOGIN_LINK_', '_SITE_TITLE_');
						$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

					} else {

						$msg = get_languageword('please')." <a href='".URL_AUTH_LOGIN."'> ".get_languageword('Login Here')."</a> ".get_languageword('to view the booking details');
						$msg .= "<p>".get_languageword('Thank you')."</p>";
					}

					sendEmail($from, $to, $sub, $msg);
				}
			}
			$this->prepare_flashmessage(get_languageword('Your child student changed successfully'), 0);
			redirect(URL_STUDENT_ADD_CHILD_STUDENT);
		}	
		$this->data['profile'] = getUserRec();

		$child_list = $this->home_model->get_child_list($this->session->userdata('email'));
		
		$this->data['child_list'] = $child_list;

		$this->data['activemenu'] 	= "account";
		$this->data['activesubmenu'] = "add_child_student";
		$this->data['content'] 		= 'add_child_student';
		$this->data['pagetitle'] = get_languageword('Add Child Student');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function privileges(){
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_parent()) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		$user_id = $this->ion_auth->get_user_id();
		$student_id = $this->session->userdata('child_id');
		if(isset($_POST['submitbutt']))
		{
			$inputdata = array();
			$inputdata['course_sell_purch'] = $_POST['course_sell_purch'];
			$inputdata['iq_live'] = $_POST['iq_live'];
			$inputdata['book_edit_status'] = $_POST['book_edit_status'];
			$inputdata['g_sessions'] = $_POST['g_sessions'];
			$inputdata['admin_claims'] = $_POST['admin_claims'];
			$inputdata['my_leads'] = $_POST['my_leads'];
			$inputdata['iq_post'] = $_POST['iq_post'];
			$inputdata['packages'] = $_POST['packages'];
			$inputdata['manage'] = $_POST['manage'];
			$inputdata['account'] = $_POST['account'];
			$inputdata['enrolled_courses'] = $_POST['enrolled_courses'];
			$inputdata['collabs'] = $_POST['collabs'];
			$this->base_model->upate_privileges_chage($inputdata,$student_id);
			
			$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '32'));
			if(!empty($email_tpl)) {
				$email_tpl = $email_tpl[0];
				$student_rec = getUserRec($student_id);
				$tutor_rec 	 = getUserRec($user_id);

				if(!empty($email_tpl->from_email)) {
					$from = $email_tpl->from_email;
				} else {
					$from 	= get_system_settings('Portal_Email');
				}

				$to 	= $student_rec->email;

				if(!empty($email_tpl->template_subject)) {
					$sub = $email_tpl->template_subject;
				} else {
					$sub = get_languageword("Booking Request From Student");
				}

				if(!empty($email_tpl->template_content)) {
					$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';

					$site_title = $this->config->item('site_settings')->site_title;


					$original_vars  = array($logo_img, $site_title, $student_rec->username, $tutor_rec->username, '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);


					$temp_vars		= array('_SITE_LOGO_', '_SITE_TITLE_', '_STUDENT_NAME_', '_PARENT_NAME_', '_LOGIN_LINK_', '_SITE_TITLE_');
					$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);

				} else {

					$msg = get_languageword('please')." <a href='".URL_AUTH_LOGIN."'> ".get_languageword('Login Here')."</a> ".get_languageword('to view the booking details');
					$msg .= "<p>".get_languageword('Thank you')."</p>";
				}

				sendEmail($from, $to, $sub, $msg);
			}
			$this->prepare_flashmessage(get_languageword('Your child student changed successfully'), 0);
			redirect(URL_STUDENT_PRIVILEGES);
		}	

		$student_details = $this->base_model->get_user_info($student_id);
		
		$this->data['profile'] = $student_details;

		$this->data['activemenu'] 	= "account";
		$this->data['activesubmenu'] = "privileges";
		$this->data['content'] 		= 'privileges';
		$this->data['pagetitle'] = get_languageword('Privileges');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function my_tutors(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		//$user_id = $this->ion_auth->get_user_id();
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table($this->db->dbprefix('tutor_student'));
		$crud->where('student_id1', $user_id);
		$crud->set_subject("Bookings");


		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_read();
		
		$crud->edit_fields('student_gdrive_url');

		$crud->columns('tutor_id1','student_gdrive_url', 'tutor_gdrive_url');
		$crud->callback_column('tutor_id1',array($this,'callback_my_student_parent_id'));
		$crud->display_as('tutor_gdrive_url','Tutor GDrive Url');
		$crud->display_as('student_gdrive_url','My GDrive Url');
		$crud->display_as('tutor_id1','Tutor Name');
		
		$output = $crud->render();
		
		$this->data['pagetitle'] = "Bookings";
		$this->data['activemenu'] 	= "my_tutors";
		$this->data['activesubmenu'] = "my_tutors";
		//$this->data['content'] 		= 'my_students';
		$this->data['is_ttuor'] = true;
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}
	
	function callback_my_student_parent_id($value, $row)
	{
		$result = $this->base_model->get_user_info($value);
		return $result->username;
	}
	
	function regenerate_collab(){
		$data = $_POST;
		$unique = $this->base_model->get_unique_id();
		$collab = $this->base_model->save_collab_id($_POST['id'],$unique);
		$result['collab_id'] = $collab;
		echo json_encode($result);
	}
	
	function new_collab(){
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}
		
		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$student_id = $this->session->userdata('child_id');
		else
			$student_id = $this->ion_auth->get_user_id();
		
		$student_details = $this->base_model->get_user_info($student_id);
		if(!is_premium($student_id)) {
			$this->prepare_flashmessage(get_languageword('please_become_premium_member_to_book_tutor'), 2);
			redirect(URL_STUDENT_LIST_PACKAGES, 'refresh');
		}
		
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		
		
		if(isset($_POST['submitbutt']))
		{
			$this->form_validation->set_rules('reason', get_languageword('reason'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('date', get_languageword('date'), 'trim|required|xss_clean');
			$this->form_validation->set_rules('time_slot', get_languageword('time_slot'), 'trim|required|xss_clean');
						
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			
			if ($this->form_validation->run() == TRUE)
			{	
				$inputdata = array();
				$inputdata['reason'] = $_POST['reason'];
				$inputdata['credits'] = 0;
				if(!empty($student_details->time_zone))
					$time_zone = $time_zone_list[$student_details->time_zone];
				else
					$time_zone = "UTC";
				
				$t = strtotime($_POST['date']);
				$time_slot = explode("-",$_POST['time_slot']);
				$userTimezone = new DateTimeZone($time_zone);
				$gmtTimezone = date_create("now",timezone_open("UTC"));
				$myDateTime_start = new DateTime(date('Y-m-d',$t)." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
				$myDateTime_end = new DateTime(date('Y-m-d',$t)." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));
				
				$offset = timezone_offset_get($userTimezone,$gmtTimezone);
				
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->sub($myInterval);
				$myDateTime_end->sub($myInterval);
		
				$inputdata['date'] = $myDateTime_start->format("Y-m-d");
				$inputdata['time_slot'] = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
				$inputdata['grade_year'] = $this->session->userdata('grade_year');
				if(!empty($_POST['collabs_list'])){
					$email_tpl = $this->base_model->fetch_records_from('email_templates', array('template_status' => 'Active', 'email_template_id' => '36'));
					$from = "";
					if(!empty($email_tpl)) {
						$email_tpl = $email_tpl[0];
						$from 	= get_system_settings('Portal_Email');
						if(!empty($email_tpl->template_subject)) {
							$sub = $email_tpl->template_subject;
						} else {
							$sub = get_languageword("Booking Request From Student");
						}
					}
					foreach($_POST['collabs_list'] as $key => $value){
						$inputdata['collab_id'] = $value;
						$inputdata['user_id'] = $this->base_model->get_id_from_collab_id($value);
						$inputdata['open_collab_id'] = $student_details->collab_id;
						$inputdata['status'] = 'pending';
						$this->base_model->insert_operation($inputdata,'collabs');
						
						$student_rec 	 = getUserRec($inputdata['user_id']);
						$to = $student_rec->email;
						if(!empty($email_tpl->template_content)) {
							$logo_img='<img src="'.get_site_logo().'" class="img-responsive" width="120px" height="50px">';
							$site_title = $this->config->item('site_settings')->site_title;
							$original_vars  = array($logo_img, $site_title,$student_rec->username, $student_details->username, $_POST['date']." ".$_POST['time_slot'], 'Collab Request', '<a href="'.URL_AUTH_LOGIN.'">'.get_languageword('Login Here').'</a>', $site_title);
							$temp_vars		= array('__SITE_LOGO__', '__SITE_TITLE__', '__STUDENT_NAME1__', '__STUDENT_NAME2__', '__DATE_TIME__', '__LOCATION__', '__LOGIN_LINK__', '__SITE_TITLE__');
							$msg = str_replace($temp_vars, $original_vars, $email_tpl->template_content);
						} 
						sendEmail($from, $to, $sub, $msg);
					}
					/*$inputdata['collab_id'] = $student_details->collab_id;
					$inputdata['user_id'] = $student_id;
					$inputdata['open_collab_id'] = $student_details->collab_id;
					$inputdata['status'] = 'pending';
					$this->base_model->insert_operation($inputdata,'collabs');*/
				}
				$this->prepare_flashmessage(get_languageword('New Collabs posted successfully'), 0);
				redirect(URL_STUDENT_POST_MY_COLLAB);	
			}
			else
			{
				$this->data['message'] = $this->prepare_message(validation_errors(), 1);
			}			
		
		} else{
			if(!empty($student_details->time_zone))
				$time_zone = $time_zone_list[$student_details->time_zone];
			else
				$time_zone = "UTC";
			
			$t = strtotime(date("Y-m-d H:i:s"));
			
			$userTimezone = new DateTimeZone($time_zone);
			$gmtTimezone = date_create("now",timezone_open("UTC"));
			$myDateTime_start = new DateTime(date('Y-m-d H:i:s',$t), new DateTimeZone($time_zone));
			$offset = timezone_offset_get($userTimezone,$gmtTimezone);
			
			$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
			$myDateTime_start->add($myInterval);
			$a = $myDateTime_start->format("Y-m-d H:i:s");
			$this->data['default_date'] = $myDateTime_start->format("Y-m-d");
		}
		if(empty($student_details->grade_year))
			$collab_list = array();
		else
			$collab_list = $this->base_model->get_collab_list($student_details->grade_year,$student_details->education_type,$student_id);
		
		if(!empty($collab_list)){
			foreach($collab_list as $key => $value){
				$cnt = $this->base_model->get_collab_count($value->collab_id);
				$collab_list[$key]->collab_count = $cnt;
				if($value->is_online == "no")
					$collab_list[$key]->collab_status = "OFFLINE";
				else{
					$cur_date = date("Y-m-d");
					$student_collab_list = $this->base_model->get_student_collab_list($value->collab_id,$cur_date);
					if(empty($student_collab_list))
						$collab_list[$key]->collab_status = "AVAILABLE";
					else{
						$cur_time = date("H");
						$is_exist = 0;
						foreach($student_collab_list as $k => $v){
							$time_slot = $v->time_slot;
							$time_slot = explode("-",$time_slot);
							if($time_slot[0] <= $cur_time & $time_slot[1] >= $cur_time){
								$collab_list[$key]->collab_status = 'BUSY';
								$is_exist = 1;
								break;
							}
						}
						if($is_exist == 0)
							$collab_list[$key]->collab_status = 'AVAILABLE';
					}
				}
			}
		}
			
		$this->data['profile'] = getUserRec($student_id);
		$this->data['collab_list'] = $collab_list;

		$this->data['activemenu'] 	= "my_collabs";
		$this->data['activesubmenu'] = "new";
		$this->data['content'] 		= 'my_new_collabs';
		$this->data['pagetitle'] = get_languageword('My Collabs');
		$this->_render_page('template/site/student-template', $this->data);
	}
	
	function student_collabs($param = "")
	{
		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parent())) {
			$this->prepare_flashmessage(get_languageword('You dont have permission to access this page'), 1);
			redirect('auth/login', 'refresh');
		}

		$this->data['message'] = $this->session->flashdata('message');
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$my_details = $this->base_model->get_user_info($user_id);
		$this->load->library(array('grocery_CRUD'));
		$crud = new grocery_CRUD();
		$crud_state = $crud->getState();
		$crud->set_table('pre_collabs');
		$crud->set_relation('user_id',TBL_USERS, 'time_zone');
		$crud->where('pre_collabs'.'.user_id', $user_id);
		$crud->or_where('pre_collabs.open_collab_id',$my_details->collab_id);
		

		$status_arr = array('pending', 'new', 'approved');
		if(in_array($param, $status_arr)) {
			$crud->where('pre_collabs'.'.status', $param);
		}
		
		$crud->unset_add();
		if($param == "approved"){
			$crud->unset_edit();
			$crud->unset_delete();
		}
		$crud->set_subject( get_languageword('Collabs') );
		$crud->set_enable_initiate_session_option_before_mins($this->config->item('site_settings')->enable_initiate_session_option_before_mins);

		//List Table Columns
		$crud->columns('collab_id','reason', 'date', 'time_slot', 'status');

		
		//Form fields for Edit Record
		$crud->edit_fields('status');

		//Hidden Fields
		//$crud->field_type('updated_at', 'hidden', date('Y-m-d H:i:s'));
		//$crud->field_type('grade_year', 'hidden', $user_id);

		//Unset Fields
		//$crud->unset_fields('student_id');


		//Authenticate whether Tutor editing/viewing his records only
		if($crud_state == "edit" || $crud_state == "read") {

			if($param != "" && $param != "add" && $param != "edit" && $param != "read" && $param != "success")
				$p_key = $this->uri->segment(4);
			else
				$p_key = $this->uri->segment(3);

			$booking_det = $this->base_model->fetch_records_from('collabs', array('id' => $p_key));

			if(!empty($booking_det)) {

				$booking_det = $booking_det[0];

				if($booking_det->user_id != $user_id) {
					$this->prepare_flashmessage(get_languageword('not_authorized'), 1);
	    			redirect(URL_STUDENT_ENQUIRIES);
				}

				if($crud_state == "edit") {

					$booking_status = $booking_det->status;

					$crud->display_as('status', get_languageword('change_status'));

					if(in_array($booking_status, array('pending', 'approved'))) {
						$crud->required_fields(array('status'));
					}
					
					if($booking_status == "new") {
						$crud->edit_fields();
					}

					if($booking_status == "pending") {
						$crud->field_type('status', 'dropdown', array('approved' => get_languageword('approved'),'cancel' => get_languageword('cancel')));
					}

					if($booking_status == "approved") {
						//$status = array('cancelled_before_course_started' => get_languageword('cancel'), 'called_for_admin_intervention' => get_languageword('claim_for_admin_intervention'));
						//$crud->field_type('status', 'dropdown', $status);
					}
				}

			} else {
				$this->prepare_flashmessage(get_languageword('invalid_request'), 1);
	    		redirect(URL_STUDENT_ENQUIRIES);
			}
		}


		if($crud_state == "read") {
			$crud->field_type('updated_at','visible');
		}

		//05-12-2018 admin notification start
		$methd = $this->uri->segment(3);
		$stus = $this->input->post('status');
		if($stus == 'approved' ){
			$booking_info = array();
			$current_method = "";
			if($methd != "update" && $methd != "update_validation"){
				$booking_info = (array)$this->base_model->get_collab_detail($this->uri->segment(3));
				$current_method = $this->uri->segment(2);
			} else{
				$booking_info = (array)$this->base_model->get_collab_detail($this->uri->segment(4));
				$current_method = $this->uri->segment(3);
			}
			if($current_method == "update"){
				$this->load->model('student_model');
				$data['course_id'] = 173;
				$data['tutor_id'] = $this->base_model->get_id_from_collab_id($booking_info['open_collab_id']);
				$data['student_id'] = $booking_info['user_id'];
				$data['time_slot'] = $booking_info['time_slot'];
				$data['fee'] = 0;
				$data['content'] = $booking_info['reason'];
				$data['start_date'] = $data['end_date'] = $booking_info['date'];
				$data['duration_type'] = 'hours';
				$collab_time_slot = explode('-',$booking_info['time_slot']);
				$data['duration_value'] = $collab_time_slot[1] - $collab_time_slot[0];
				$data['preferred_location'] = 'online-zoom';
				$data['tution'] = 'Leave bank';
				
				
				$data['per_credit_value'] = get_system_settings('per_credit_value');
				$data['prev_status'] = $data['status'] = 'session_initiated';
				$data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $user_id;
				$data['created_by'] = $user_id;
				$data['admin_commission'] = get_system_settings('admin_commission_for_a_booking');
				$data['admin_commission_val']   = round($data['fee'] * ($data['admin_commission'] / 100));
				
				$data['iq_id'] = $booking_info['id'];
				$this->student_model->insert_iq_to_booking($data);
				
				
			}
		}
		
		$crud->callback_column('status', array($this, 'callback_column_booking_status'));
		$crud->callback_column('collab_id', array($this, 'callback_column_collab_id'));

		$crud->callback_update(array($this,'callback_send_email_collab'));
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$crud->set_timezone($time_zone_list);
		$output = $crud->render();
		
		$param = get_languageword($param);
		$this->data['pagetitle'] = get_languageword('Collabs');
		$this->data['activemenu'] 	= "my_collabs";
		$this->data['activesubmenu'] = $param;
		$this->data['grocery_output'] = $output;
		$this->data['grocery'] = TRUE;
		$this->grocery_output($this->data);
	}
	
	function get_previous_events(){
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];
		if($this->session->userdata('user_belongs_group') == 5)
			$user_id = $this->session->userdata('child_id');
		else
			$user_id = $this->ion_auth->get_user_id();
		$time_zone_list = $this->base_model->get_timezone_list();
		$time_zone_list = explode(",",$time_zone_list->field_type_values);
		$student_details = $this->base_model->get_user_info($user_id);
		if(!empty($student_details->time_zone))
			$time_zone = $time_zone_list[$student_details->time_zone];
		else
			$time_zone = "UTC";
		$userTimezone = new DateTimeZone($time_zone);
		$gmtTimezone = date_create("now",timezone_open("UTC"));				
		$myDateTime_start = new DateTime(date('Y-m-d H:i:s',strtotime($from_date." 00:00:00")), new DateTimeZone($time_zone));
		$myDateTime_end = new DateTime(date('Y-m-d H:i:s',strtotime($to_date." 23:59:59")), new DateTimeZone($time_zone));
		$offset = timezone_offset_get($userTimezone,$gmtTimezone);
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime_start->add($myInterval);
		$myDateTime_end->sub($myInterval);
		$from_date = $myDateTime_start->format("Y-m-d");		
		$to_date = $myDateTime_end->format("Y-m-d");		
		$previous_list = $this->base_model->get_previous_events1($from_date,$to_date,$user_id);
			
		foreach($previous_list as $key => $value){
			if(!empty($value->time_slot)){
				$time_slot = explode("-",$value->time_slot);
				if(!empty($value->start_date)){
					$myDateTime_start = new DateTime(date('Y-m-d',strtotime($value->start_date))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
					$myDateTime_end = new DateTime(date('Y-m-d',strtotime($value->start_date))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
				}else{
					if($value->tution != "Leave bank"){
						if($value->start_date1 == $value->end_date){
							$myDateTime_start = new DateTime(date('Y-m-d',strtotime($value->start_date1))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
							$myDateTime_end = new DateTime(date('Y-m-d',strtotime($value->start_date1))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
						}else{
							$myDateTime_start = new DateTime(date('Y-m-d')." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
							$myDateTime_end = new DateTime(date('Y-m-d')." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
						}
					}else{
						$myDateTime_start = new DateTime(date('Y-m-d',strtotime($value->start_date1))." ".$time_slot[0].":00:00", new DateTimeZone($time_zone));
						$myDateTime_end = new DateTime(date('Y-m-d',strtotime($value->start_date1))." ".$time_slot[1].":00:00", new DateTimeZone($time_zone));	
					}
				}
					
				$offset = timezone_offset_get($userTimezone,$gmtTimezone);
				
				$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
				$myDateTime_start->add($myInterval);
				$myDateTime_end->add($myInterval);
				if($myDateTime_start->format("Y-m-d") >= $from_date && $myDateTime_start->format("Y-m-d") <= $to_date){
					$previous_list[$key]->student_date = $myDateTime_start->format("Y-m-d");
					$previous_list[$key]->student_time_slot = $myDateTime_start->format("H")."-".$myDateTime_end->format("H");
					$previous_list[$key]->whiteboard_url = "https://bitpaper.io/go/".str_replace(" ","",$value->name)."_".$myDateTime_start->format("Ymd")."_".$myDateTime_start->format("H").$myDateTime_end->format("H")."/".str_replace(" ","",$value->username)."_".$myDateTime_start->format("Ymd");
				}
			}
		}
		$data['previous_list'] = $previous_list;
		echo json_encode($data);
	}
}
?>