    <!-- User Profile Details -->
    <?php  if(!empty($tutor_details)) {
            foreach ($tutor_details as $row) {
     ?>
    <div class="container">
        <div class="row-margin ">

            <?php echo $this->session->flashdata('message'); ?>

            <div class="box-border">
                <div class="row ">
                    <!-- User Profile -->
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="user-profile-pic">
                            <img src="<?php echo get_tutor_img($row->photo, $row->gender); ?>" alt="<?php echo $row->username; ?>" class="img-responsive img-circle">
                        </div>
                        <p style="text-align: center;margin-left:24px; <?php if($row->cur_status != 'BUSY') echo 'font-weight: 600;' ?>" class="user-status <?php if($row->cur_status == 'BUSY') echo 'offline' ?>"><?php echo $row->cur_status; ?></p>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                        <div class="user-profile-content">
                            <ul class="user-badges">
                                <?php
                                      if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'yes') == 0) {

                                        $title = get_languageword('not_yet_verified');
                                        $last_verified_date = "";
                                        if(!empty($row->admin_approved_date)) {
                                            $title = get_languageword('last_verified:');
                                            $last_verified_date = date('jS F, Y', strtotime($row->admin_approved_date));
                                        }
                                ?>
                                <li>
                                    <a href="#" title="<?php echo $title; ?>" data-content="<?php echo $last_verified_date; ?>" class="red-popover" data-toggle="popover" data-placement="top" data-trigger="hover"><i class="fa fa-heart"></i></a>
                                </li>
                                <?php } ?>
                            </ul>
                            <h4 class="title"> <?php echo ucwords($row->username); ?></h4>
                            <p class="sub-title"><u><?php echo $row->gender.", ".calcAge($row->dob)." ".get_languageword('years');  ?></u></p>
                            <?php if(!empty($tutor_raing)) { ?>
                            <ul class="user-info">
                                <?php if(!empty($tutor_raing->avg_rating)) { ?>
                                <li>
                                    <div class="avg_rating" <?php echo 'data-score='.$tutor_raing->avg_rating; ?> ></div>
                                </li>
                                <?php } ?>
                                <?php if(!empty($tutor_raing->no_of_ratings)) { ?>
                                <li><?php  echo $tutor_raing->no_of_ratings." ".get_languageword('Ratings'); ?></li>
                                <?php } ?>
                                <?php if(!empty($row->city) || !empty($row->country)) { ?>
                                <li><i class="fa fa-map-marker"></i> <?php echo $row->city.", ".$row->country; ?></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                            <p> <?php echo $row->profile; ?> </p>
                            <hr>
                            <?php 
                                if($row->show_contact!='None'){
                                    if($row->show_contact=='All' || $row->show_contact=='Email'){?>
                                    <h4><strong><?php echo get_languageword('email'); ?>: </strong> <?php echo $row->email; ?></h4>
                                    <?php }

                                     if($row->show_contact=='All' || $row->show_contact=='Mobile'){?>
                                    <h4><strong><?php echo get_languageword('phone'); ?>: </strong> <?php echo $row->phone; ?></h4>
                                    <?php }
                            }?>
                            <h4><strong><?php echo get_languageword('experience'); ?>: </strong> <?php echo $row->teaching_experience." ".get_languageword('years'); ?></h4>
                            <h4><strong><?php echo get_languageword('qualification'); ?>:</strong>  <?php echo $row->qualification; ?></h4>
                            <h4><strong><?php echo get_languageword('language_of_teaching'); ?>:</strong>  <?php echo $row->language_of_teaching; ?></h4>
                             <?php if($row->academic_class != 'no' || $row->non_academic_class !='no'){?>
                            <h4><strong><?php echo get_languageword('Teaching_Class_Types'); ?>: </strong> 
                            <?php if($row->academic_class != 'no')
                                     echo get_languageword('Academic'); 

                                  if($row->non_academic_class !='no')
                                   echo ', '. get_languageword('Non_Academic'); ?></h4><?php } ?>                                
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                        <div class="send-quote-block">
                            <h2 class="heading-line"><?php echo get_languageword('send_me_your_message'); ?>:</h2>
                            <?php $this->load->view('send_message', array('user_course_opts' => $tutor_course_opts, 'to_user_type' => 'tutor', 'to_user_id' => $row->id)); ?>
                        </div>
                    </div>
                </div>
            </div>

            <!--  More about Me -->
            <div class="row mtop7">
                <div class="col-sm-12">
                    <h2 class="heading-line"><?php echo get_languageword('more_about_me'); ?></h2>
                    <ul class="user-more-details">

                        <?php if(!empty($row->tutoring_courses)) { ?>
                        <li>
                            <div class="media-left "><?php echo get_languageword('tutoring_courses'); ?>:</div>

                            <div class="media-body"><?php echo $row->tutoring_courses; ?></div>
                        </li>
                        <?php } ?>
                        <?php if(!empty($row->tutoring_locations)) { ?>
                        <li>
                            <div class="media-left "><?php echo get_languageword('tutoring_locations'); ?>:</div>

                            <div class="media-body"><?php echo $row->tutoring_locations; ?></div>
                        </li>
                        <?php } ?>
                        <?php if(!empty($row->experience_desc)) { ?>
                        <li>
                            <div class="media-left "><?php echo get_languageword('career_experience'); ?>:</div>
                            <div class="media-body"><?php echo $row->experience_desc; ?></div>
                        </li>
                        <?php } ?>
                        <?php if(!empty($row->i_love_tutoring_because)) { ?>
                        <li>
                            <div class="media-left "><?php echo get_languageword('i_love_tutoring_because'); ?> :</div>
                            <div class="media-body"><?php echo $row->i_love_tutoring_because; ?></div>
                        </li>
                        <?php } ?>
                        <?php if(!empty($row->other_interests)) { ?>
                        <li>
                            <div class="media-left "><?php echo get_languageword('other_interests'); ?>:</div>
                            <div class="media-body"><?php echo $row->other_interests; ?></div>
                        </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>

            <!-- Gallery -->
            <?php if(!empty($row->tutor_gallery)) { ?>
            <div class="row mtop7">
                <div class="col-sm-12">
                    <h2 class="heading-line"><?php echo get_languageword('gallery'); ?></h2>
                </div>
                <div class="col-sm-8">
                    <div class="tab-content tabpill-content">

                        <?php $i=1; foreach ($row->tutor_gallery as $gallery) { ?>
                        <div id="vid<?php echo $i; ?>" class="tab-pane fade <?php if($i++ == 1) echo "active in"; ?> ">
                            <div class="my-images popup-gallery">
                                <a href="<?php echo URL_UPLOADS_GALLERY.'/'.$gallery->image_name; ?>" title="<?php echo $gallery->image_title; ?>">
                                    <img src="<?php echo URL_UPLOADS_GALLERY.'/'.$gallery->image_name; ?>" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="NavPillTabs">
                        <ul class=" video-tabs video-thumbs">
                            <?php $i=1; foreach ($row->tutor_gallery as $gallery_thumbs) { ?>
                            <li class="<?php if($i == 1) echo 'active'; ?>">
                                <a data-toggle="pill" href="#vid<?php echo $i++; ?>">
                                    <img src="<?php echo URL_UPLOADS_GALLERY.'/thumb__'.$gallery_thumbs->image_name; ?>" alt="" class="img-responsive">
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>

            <!--  More about Me -->
            <?php if(!empty($row->tutor_experience)) { ?>
            <div class="row mtop7">
                <div class="col-sm-12">
                    <h2 class="heading-line"><?php echo get_languageword('my_experience'); ?></h2>
                    <ul class="user-more-details">
                        <?php foreach ($row->tutor_experience as $exp) { ?>
                        <li>
                            <div class="media-left"><?php echo $exp->from_date." - ".$exp->to_date; ?>:</div>
                            <div class="media-body">
                                <h4><strong><?php echo $exp->company; ?></strong> - <?php echo $exp->role; ?></h4> 
                                <?php echo $exp->description; ?>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } ?>
			
			<div class="row mtop7">
				<?php 
					$attributes = array('name' => 'iq_live_book', 'id' => 'iq_live_book', 'class' => 'comment-form dark-fields');
					echo form_open_multipart('home/iq_live_book',$attributes);?>
						<input type="hidden" value="<?php echo $tutor_details[0]->id;?>" id="tutor_id" name="tutor_id"/>
						<input type="hidden" value="<?php echo $tutor_details[0]->slug;?>" id="tutor_slug" name="tutor_slug"/>
						<div class="col-sm-6 ">
							<div class="input-group ">
									<label><?php echo get_languageword('Preferred Teaching type');?> <sup>*</sup>:</label>
									<div class="dark-picker dark-picker-bright">
										<?php 
										$val = '';
										if( isset($_POST['submitbutt']) )
										{
											$val = $this->input->post( 'teaching_type_id' );
										}
										elseif( isset($profile->teaching_type_id) && !isset($_POST['submitbutt']))
										{
											$val = $profile->teaching_type_id;
										}
										echo form_dropdown('teaching_type_id', array('Online ZOOM'=>get_languageword('online ZOOM'),'Record'=>get_languageword('recorded')), $val, 'class="select-picker"  onchange="teaching_type_sel()" id="teaching_type"');
										?>
										<?php echo form_error('teaching_type_id');?>
									</div>
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Priority of Requirement');?><?php echo required_symbol();?>:</label>
								<div class="dark-picker dark-picker-bright">
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'priority_of_requirement' );
								}
								elseif( isset($profile->priority_of_requirement) && !isset($_POST['submitbutt']))
								{
									$val = $profile->priority_of_requirement;
								}
								echo form_dropdown('priority_of_requirement', array('Immediately' => get_languageword('Immediately'), 'By Date & Time' => get_languageword('By Date & Time')), $val, 'class="select-picker" onchange="priority_sel();" id="priority"');
								?>
								<?php echo form_error('priority_of_requirement');?>
							   </div>
							</div>
						</div>
						
						<div class="col-sm-6 " id="priority_date" style="display: none;">
							<div class="input-group ">
								<label><?php echo get_languageword('Date');?>:</label>
								<?php
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'date' );
								}
								elseif( isset($profile->date) && !isset($_POST['submitbutt']))
								{
									$val = $profile->date;
								}
								$element = array(
									'name'	=>	'date',
									'id'	=>	'date',
									'value'	=>	$val,
									//'required' => 'required',
									'class' => 'form-control',
									'placeholder' => get_languageword('2019-03-01'),
								);
								echo form_input($element);
								?>
								
							</div>
						</div>
						
						<div class="col-sm-6 " id="priority_time" style="display: none;">
							<div class="input-group ">
								<label><?php echo get_languageword('Time');?>:</label>
								<?php
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'time' );
								}
								elseif( isset($profile->time) && !isset($_POST['submitbutt']))
								{
									$val = $profile->time;
								}
								$element = array(
									'name'	=>	'time',
									'id'	=>	'time',
									'value'	=>	$val,
									//'required' => 'required',
									'class' => 'form-control',
									'placeholder' => get_languageword('15:30'),
								);
								echo form_input($element);
								?>
								
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
									<label><?php echo get_languageword('Categories');?> </label>
									<div class="dark-picker dark-picker-bright">
										<?php 
										$val = '';
										if( isset($_POST['submitbutt']) )
										{
											$val = $this->input->post( 'category_id' );
										}
										elseif( isset($profile->category) && !isset($_POST['submitbutt']))
										{
											$val = $profile->category;
										}
										echo form_dropdown('category_id', $categories, $val, 'class="select-picker"');
										?>
										<?php echo form_error('category_id');?>
									</div>
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Payment Type');?></label>
								<div class="dark-picker dark-picker-bright">
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post('budget_type');
								}
								elseif( isset($profile->budget_type) && !isset($_POST['submitbutt']))
								{
									$val = $profile->budget_type;
								}
								echo form_dropdown('budget_type', array('One Time' => get_languageword('One Time')), $val, 'class="select-picker"');
								?>
								<?php echo form_error('budget_type');?>
								</div>
							</div>
						</div>

						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('title_of_requirement');?>:</label>
								<?php
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'title_of_requirement' );
								}
								elseif( isset($profile->title_of_requirement) && !isset($_POST['submitbutt']))
								{
									$val = $profile->title_of_requirement;
								}
								$element = array(
									'name'	=>	'title_of_requirement',
									'id'	=>	'title_of_requirement',
									'value'	=>	$val,
									//'required' => 'required',
									'class' => 'form-control',
									'placeholder' => get_languageword('title_of_requirement'),
								);
								echo form_input($element);
								?>
								
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Minimum Bid in Credits:');?></label>
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'budget' );
								}
								elseif( isset($profile->budget) && !isset($_POST['submitbutt']))
								{
									$val = $profile->budget;
								}
								$element = array(
									'name'	=>	'budget',
									'id'	=>	'budget',
									'value'	=>	$val,
									//'required' => 'required',
									'class' => 'form-control',
									'placeholder' => get_languageword('credits'),
								);			
								echo form_input($element);
								?>
								<?php echo form_error('budget');?>
							</div>
						</div>
						
						<div class="col-sm-6 " id="iq_date_div">
							<div class="input-group ">
								<label><?php echo get_languageword('Preferred Slot: Start Date');?></label>
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'iq_date' );
								}
								elseif( isset($profile->iq_date) && !isset($_POST['submitbutt']))
								{
									$val = $profile->iq_date;
								}
								$element = array(
									'name'	=>	'iq_date',
									'id'	=>	'iq_date',
									'value'	=>	$val,
									'class' => 'form-control',
									'placeholder' => get_languageword('2019-03-01'),
								);
								echo form_input($element);
								?>
								<?php echo form_error('iq_date');?>
							</div>
						</div>
						
						<div class="col-sm-6 " id="iq_time_slot_div">
							<div class="input-group ">
								<label><?php echo get_languageword('Preferred Slot: Start Time');?></label>
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'iq_time_slot' );
								}
								elseif( isset($profile->iq_time_slot) && !isset($_POST['submitbutt']))
								{
									$val = $profile->iq_time_slot;
								}
								$element = array(
									'name'	=>	'iq_time_slot',
									'id'	=>	'iq_time_slot',
									'value'	=>	$val,
									'class' => 'form-control',
									'placeholder' => get_languageword('Example format 6-7,13-14,14-16,20.30-21.30')
								);
								echo form_input($element);
								?>
								<?php echo form_error('iq_time_slot');?>
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Attach');?></label>
								<input type="file" name="attach[]" style="background: none;" multiple>
							</div>
						</div>
						
						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Tutor Rating');?></label>
								<div class="dark-picker dark-picker-bright">
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post('tutor_rating');
								}
								elseif( isset($profile->tutor_rating) && !isset($_POST['submitbutt']))
								{
									$val = $profile->tutor_rating;
								}
								echo form_dropdown('tutor_rating', array('' => get_languageword('Any'),'5' => get_languageword('5 Star'),'4' => get_languageword('4 Star'),'3' => get_languageword('3 Star'),'2' => get_languageword('2 Star'),'1' => get_languageword('1 Star'),), $val, 'class="select-picker"');
								?>
								<?php echo form_error('tutor_rating');?>
								</div>
							</div>
						</div>

						<div class="col-sm-6 ">
							<div class="input-group ">
								<label><?php echo get_languageword('Requirement Details');?></label>
								<?php			   
								$val = '';
								if( isset($_POST['submitbutt']) )
								{
									$val = $this->input->post( 'requirement_details' );
								}
								elseif( isset($profile->requirement_details) && !isset($_POST['submitbutt']))
								{
									$val = $profile->requirement_details;
								}
								$element = array(
									'name'	=>	'requirement_details',
									'id'	=>	'requirement_details',
									'value'	=>	$val,
									'maxlength'	=> 200,
									'rows'	=>5,
									//'required' => 'required',
									'class' => 'form-control',
									'placeholder' => get_languageword('requirement_details'),
								);
								echo form_textarea($element);
								?>
								<?php echo form_error('requirement_details');?>
							</div>
						</div>
						
						

						<div class="col-sm-12 ">
							<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
						</div>

					<?php echo form_close();?>
			</div>
            
        </div>
    </div>

    <script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
    <script>
        function get_tutor_course_details()
        {
            course_slug     = $('#course_slug option:selected').val();
            selected_date   = $('#start_date').val();
			tution_mode 	= $("input[name='tution']:checked").val();
			
            if(!course_slug || !selected_date || !tution_mode) {
                $('#fee').text('');
                $('#duration').text('');
                $('#days_off').text('');
                $('#content_li').remove();
                $('#time_slot_div').text('<?php echo get_languageword("Please Select Course, Tution Mode And Date First"); ?>');
                return;
            }

            $.ajax({
                    type: "POST",
                    url: "<?php echo URL_HOME_AJAX_GET_TUTOR_COURSE_DETAILS; ?>",
                    data: { "course_slug" : course_slug, "tutor_id" : <?php echo $row->id; ?>, "selected_date" : selected_date, "tution":tution_mode },
                    cache: false,
                    beforeSend: function() {
                        $('#time_slot_div').html('<font color="#5bc0de" size="6"> Loading...</font>');
                    },
                    success: function(response) {

                        if(response == "") {
                            $('#fee').text('');
                            $('#duration').text('');
                            $('#days_off').text('');
                            $('#content_li').remove();
                            $('#time_slot_div').html('<?php echo get_languageword("no_slots_available."); ?> <a href="#"><?php echo get_languageword("click_here_to_send_me_your_message"); ?></a>');
                            $('#request_tutor_btn').slideUp();
                        } else {
                            var fee_duration = response.split('~');
                            var fee          = fee_duration[0];
                            var duration     = fee_duration[1];
                            var content      = fee_duration[2];
                            var time_slots   = fee_duration[3];
                            var days_off     = fee_duration[4];

                            $('#fee').text(fee);
                            $('#duration').text('credits/'+duration);
                            if(days_off)
                                $('#days_off').text('Days off: '+days_off);

                            if(content) {
                                $('#content_li').remove();
                                $('#course_li').after('<li id="content_li"><?php echo get_languageword("course_content"); ?><p>'+content+'</p></li>');
                            }

                            time_slot_html = "";
                            if(time_slots != "")
                                time_slots = time_slots.split(',');

                            total_available_timeslots = time_slots.length;

                            if(total_available_timeslots > 0) {

                                for(i=0;i<total_available_timeslots;i++) {

                                    check_radio = "";
                                    if(i == 0)
                                        check_radio = 'checked = "checked"'; 
                                    time_slot_html += '<li><div><input id="radio1'+i+'" type="radio" name="time_slot" value="'+time_slots[i]+'" '+check_radio+' ><label for="radio1'+i+'"><span><span></span></span>'+time_slots[i]+'</label></div></li>';
                                }

                                $('#time_slot_div').html(time_slot_html);
                                $('#request_tutor_btn').slideDown();

                            } else {

                                $('#time_slot_div').html('<?php echo get_languageword("no_slots_available."); ?> <a href="#"><?php echo get_languageword("click_here_to_send_me_your_message"); ?></a>');
                                 $('#request_tutor_btn').slideUp();
                            }
                        }
                    }
            });

        }


        function toggle_location_chkbx()
        {
            $('input[name="teaching_type"]').removeAttr('checked');
            $('input[value="willing-to-travel"]').prop('checked',true);
        }


    </script>

    <script src="<?php echo URL_FRONT_JS;?>jquery.validate.min.js"></script>
    <script type="text/javascript"> 
      (function($,W,D)
       {
          var JQUERY4U = {};
       
          JQUERY4U.UTIL =
          {
              setupFormValidation: function()
              {

                  //form validation rules
                  $("#book_tutor_form").validate({
                      rules: {
                            course_slug: {
                                required: true
                            },
                            location_slug: {
                                required: function(){
                                            return ($('input[name="teaching_type"]:checked').val() == "willing-to-travel");
                                          }
                            },
                            start_date: {
                                required: true
                            }
                      },

                      messages: {
                            course_slug: {
                                required: "<?php echo get_languageword('please_select_course'); ?>"
                            },
                            location_slug: {
                                required: "<?php echo get_languageword('please_select_location'); ?>"
                            },
                            start_date: {
                                required: "<?php echo get_languageword('please_select_date,on_which_you_want_to_start_the_course'); ?>"
                            }
                      },

                      submitHandler: function(form) {
                          form.submit();
                      }
                  });
              }
          }
             //when the dom has loaded setup form validation rules
         $(D).ready(function($) {
             JQUERY4U.UTIL.setupFormValidation();
         });
     })(jQuery, window, document);


     $(function() {

       $( "#start_date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
              get_tutor_course_details();
           }
       });

     });
     
     function priority_sel(){
		if($('#priority').val() == "Immediately"){
			$('#priority_date').css('display','none');
			$('#priority_time').css('display','none');
		} else{
			$('#priority_date').css('display','block');
			$('#priority_time').css('display','block');
		}
	}
	function teaching_type_sel(){
		if($('#teaching_type').val() == "Record"){
			$('#iq_date_div').css('display','none');
			$('#iq_time_slot_div').css('display','none');
		} else{
			$('#iq_date_div').css('display','block');
			$('#iq_time_slot_div').css('display','block');
		}
	}
	$(function() {
        $("#iq_date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
       $("#date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
   });


    </script>

    <link rel="stylesheet" href="<?php echo URL_FRONT_CSS;?>jquery.raty.css">
    <script src="<?php echo URL_FRONT_JS;?>jquery.raty.js"></script>
    <script>

        /****** Tutor Avg. Rating  ******/
       $('div.avg_rating, span.avg_rating').raty({

        path: '<?php echo RESOURCES_FRONT;?>raty_images',
        score: function() {
          return $(this).attr('data-score');
        },
        readOnly: true
       });

       
    </script>

    <?php } } ?>
    <!-- User Profile Details  -->