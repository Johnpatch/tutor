    <!-- User Profile Details -->
    <?php  if(!empty($tutor_details)) {
            foreach ($tutor_details as $row) {
     ?>
    <div class="container">
        <div class="row-margin ">

            <?php echo $this->session->flashdata('message'); ?>

            <div class="box-border">
                <div class="row ">
                    <!-- User Profile -->
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="user-profile-pic">
                            <img src="<?php echo get_tutor_img($row->photo, $row->gender); ?>" alt="<?php echo $row->username; ?>" class="img-responsive img-circle">
                        </div>
                        <?php echo get_user_online_status($row->is_online); ?>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                        <div class="user-profile-content">
                            <ul class="user-badges">
                                <?php
                                      if(strcasecmp(get_system_settings('need_admin_for_tutor'), 'yes') == 0) {

                                        $title = get_languageword('not_yet_verified');
                                        $last_verified_date = "";
                                        if(!empty($row->admin_approved_date)) {
                                            $title = get_languageword('last_verified:');
                                            $last_verified_date = date('jS F, Y', strtotime($row->admin_approved_date));
                                        }
                                ?>
                                <li>
                                    <a href="#" title="<?php echo $title; ?>" data-content="<?php echo $last_verified_date; ?>" class="red-popover" data-toggle="popover" data-placement="top" data-trigger="hover"><i class="fa fa-heart"></i></a>
                                </li>
                                <?php } ?>
                            </ul>
                            <h4 class="title"> <?php echo ucwords($row->username); ?></h4>
                            <p class="sub-title"><u><?php echo $row->gender.", ".calcAge($row->dob)." ".get_languageword('years');  ?></u></p>
                                                      
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                        <div class="send-quote-block">
                            <h2 class="heading-line"><?php echo get_languageword('send_me_your_message'); ?>:</h2>
                            <?php $this->load->view('send_message', array('to_user_type' => 'tutor', 'to_user_id' => $row->id)); ?>
                        </div>
                    </div>
                </div>
            </div>


            
        </div>
    </div>

    <script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
    <script>
        
    </script>

    <script src="<?php echo URL_FRONT_JS;?>jquery.validate.min.js"></script>
    <script type="text/javascript"> 
      (function($,W,D)
       {
          var JQUERY4U = {};
       
          JQUERY4U.UTIL =
          {
              setupFormValidation: function()
              {

                  //form validation rules
                  $("#book_tutor_form").validate({
                      rules: {
                            course_slug: {
                                required: true
                            },
                            location_slug: {
                                required: function(){
                                            return ($('input[name="teaching_type"]:checked').val() == "willing-to-travel");
                                          }
                            },
                            start_date: {
                                required: true
                            }
                      },

                      messages: {
                            course_slug: {
                                required: "<?php echo get_languageword('please_select_course'); ?>"
                            },
                            location_slug: {
                                required: "<?php echo get_languageword('please_select_location'); ?>"
                            },
                            start_date: {
                                required: "<?php echo get_languageword('please_select_date,on_which_you_want_to_start_the_course'); ?>"
                            }
                      },

                      submitHandler: function(form) {
                          form.submit();
                      }
                  });
              }
          }
             //when the dom has loaded setup form validation rules
         $(D).ready(function($) {
             JQUERY4U.UTIL.setupFormValidation();
         });
     })(jQuery, window, document);


     $(function() {

     });


    </script>

    <link rel="stylesheet" href="<?php echo URL_FRONT_CSS;?>jquery.raty.css">
    <script src="<?php echo URL_FRONT_JS;?>jquery.raty.js"></script>

    <?php } } ?>
    <!-- User Profile Details  -->