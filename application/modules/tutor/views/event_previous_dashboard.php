<!-- Event Dashboard panel -->
<div class="dashboard-panel">
	<form action="<?php echo site_url('tutor/save_zoom_details');?>" method="post" class="comment-form dark-fields" enctype="multipart/form-data">
		<div class="row">
			<label style="font-size:25px;">Previous Events:</label>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="input-group ">
					<label>From Date:</label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'from_date' );
					}
					$element = array(
						'name'	=>	'from_date',
						'id'	=>	'from_date',
						'value'	=>	$val,
						'class' => 'form-control',
						'placeholder' => get_languageword('2019-03-01'),
					);
					echo form_input($element);
					?>
					<?php echo form_error('from_date');?>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group ">
					<label>To Date:</label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'to_date' );
					}else{
						$val = date("Y-m-d",strtotime("-1 days"));
					}
					$element = array(
						'name'	=>	'to_date',
						'id'	=>	'to_date',
						'value'	=>	$val,
						'class' => 'form-control',
						'placeholder' => get_languageword('2019-03-01'),
					);
					echo form_input($element);
					?>
					<?php echo form_error('to_date');?>
				</div>
			</div>
			<div class="col-sm-4">
				 <div class="input-group">
				 	<label style="height: 24px;"></label><br>
				 	<span class="nav-btn" onclick="search_previous()" style="cursor: pointer;">Search</span>
				 </div>
			</div>
		</div>
		
		<div class="previous_section">
			
		</div>
		
		<div class="row">
			<div class="col-sm-12">
				<input type="hidden" name="previous"/>
				
			</div>
		</div>
	</form>
</div>
<!-- Event Dashboard panel ends --> 
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
	function search_previous(){
		if($('#to_date').val() != "" && $('#from_date').val() != ""){
			$.ajax({
                type: "POST",
                url: "<?php echo site_url('tutor/get_previous_events'); ?>",
                data: {to_date:$('#to_date').val(),from_date:$('#from_date').val()},
                success: function(response) {
                	var obj = JSON.parse(response);
                	if(obj.previous_list != null){
                		var html = "";
						for(var i =0;i<obj.previous_list.length;i++){
							html += '<div class="row" style="border:1px solid #14bdee;margin-bottom:10px;">';
							html += '<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Course Title:'+obj.previous_list[i].name+'</div>';
							html += '<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Student Name:<a href="user-profile/'+obj.previous_list[i].slug+'">'+obj.previous_list[i].username+'</a></div>';
							html += '<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Date:'+obj.previous_list[i].tutor_date+'</div>';
							html += '<div class="col-sm-6" style="margin-bottom: 5px;font-size:15px;">Time Slot:'+obj.previous_list[i].tutor_time_slot+'</div>';
							html += '<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">';
							
							<?php if($my_profile->fluidmath == 'Yes'){ ?>
							if(obj.previous_list[i].fluidmath_link != null && obj.previous_list[i].fluidmath_link != "null" && obj.previous_list[i].fluidmath_link != "")
								html += '<a href="'+obj.previous_list[i].fluidmath_link+'" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span></a>';
							else
								html += '<span class="nav-btn" style="padding: 0px 18px;cursor:pointer;" onclick="no_fluid()"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span>';
							
							html += '<a href="tutor/edit_event_fluid_link/3/'+obj.previous_list[i].booking_id+'/'+obj.previous_list[i].tutor_date+'" style="color:blue;text-decoration: none;"><span class="edit-icon"><img src="<?php echo base_url();?>assets/grocery_crud/themes/flexigrid/css/images/edit.png" style="width:17px;"/></span></a>';
							<?php } ?>
							
							<?php if($my_profile->zoom_id == 'Yes'){ ?>
							if(obj.previous_list[i].tutor_zoom_link != null && obj.previous_list[i].tutor_zoom_link != "null" &&  obj.previous_list[i].tutor_zoom_link != "")
								html += '<a href="'+obj.previous_list[i].tutor_zoom_link+'" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span></a>';
							else
								html += '<span class="nav-btn" style="padding: 0px 18px;cursor:pointer;" onclick="no_zoom()"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span>';
							
							html += '<a href="tutor/edit_event_zoom_link/3/'+obj.previous_list[i].booking_id+'/'+obj.previous_list[i].tutor_date+'" style="color:blue;text-decoration: none;"><span class="edit-icon"><img src="<?php echo base_url();?>assets/grocery_crud/themes/flexigrid/css/images/edit.png" style="width:17px;"/></span></a>';
							<?php } ?>
							
							<?php if($my_profile->whiteboard == 'Yes'){ ?>
							html += '<a href="'+obj.previous_list[i].whiteboard_url+'" target="_blank"><span class="nav-btn" style="padding: 0px 24px;"><img src="<?php echo base_url();?>assets/front/images/whiteboard.png" style="width:17px;"/> &nbsp; Whiteboard</span></a>';
							<?php } ?>
				                
				            html += '<a href="https://calendar.google.com/calendar/r/day/'+obj.previous_list[i].tutor_date_y+'/'+obj.previous_list[i].tutor_date_m+'/'+obj.previous_list[i].tutor_date_d+'" target="_blank"><span class="nav-btn" style="padding: 2px 24px;"><img src="<?php echo base_url();?>assets/front/images/google-calendar.png" style="width:24px;"/></span></a></div></div>';
						}
						$('.previous_section').html(html);
					}
                }
        	});
		}
	}
	$(function() {
        $("#from_date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           onSelect: function() {
           }
       });
       $("#to_date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           onSelect: function() {}
       });
   });
	
</script>