<!-- Event Dashboard panel -->
<div class="dashboard-panel">
	<form action="<?php echo site_url('tutor/edit_event_fluid_link/'.$type.'/'.$booking_id);?>" method="post" class="comment-form dark-fields" enctype="multipart/form-data">
		<div class="row">
			<label style="font-size:25px;">Fluidmath Link:</label>
		</div>
		<?php echo $message;?>
		
		<div class="row" style="border:1px solid #14bdee;margin-bottom:10px;">
			<div class="col-sm-12">
				<input name="fluidmath_link" style="width:100%;" value="<?php if(!empty($result)){echo $result->fluidmath_link;}?>"></input>
				<input type="hidden" name="date" value="<?php echo $cur_date;?>">
				<input type="hidden" name="booking_id" value="<?php echo $booking_id;?>">
				<input type="hidden" name="student_id" value="<?php echo $student_id;?>">
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-sm-12">
				<button class="btn-link-dark dash-btn n-b" name="submitbtn" type="Submit">Save</button>
			</div>
		</div>
	</form>
</div>
<!-- Event Dashboard panel ends --> 