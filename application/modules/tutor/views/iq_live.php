
<!-- Dashboard panel -->
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'iq_live', 'id' => 'iq_live', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('tutor/iq_live',$attributes);?>
			<div class="col-sm-6 " id="priority_date">
				<div class="input-group ">
					<label><?php echo get_languageword('IQ LIVE INDICATOR');?></label>
					<div class="radio">
						<label>
							<input type="radio" value="ONLINE" name="iq_live_indicator" <?php if(isset($profile->iq_live_indicator) && $profile->iq_live_indicator == 'ONLINE') echo 'checked';?>>
							<span class="radio-content">
								<span class="item-content">ONLINE</span>
								<i aria-hidden="true" class="fa uncheck fa-circle-thin"></i>
								<i aria-hidden="true" class="fa check fa-dot-circle-o"></i>
							</span>
						</label>
						<label>
							<input type="radio" value="OFFLINE" name="iq_live_indicator" <?php if(isset($profile->iq_live_indicator) && $profile->iq_live_indicator != 'ONLINE') echo 'checked';?>>
							<span class="radio-content">
								<span class="item-content"><?php echo get_languageword('OFFLINE');?></span>
								<i aria-hidden="true" class="fa uncheck fa-circle-thin"></i>
								<i aria-hidden="true" class="fa check fa-dot-circle-o"></i>
							</span>
						</label>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Minimum Bid in Credits:');?> (Per 30 Minute Session)</label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'iq_live_credit' );
					}
					elseif( isset($profile->iq_live_credit) && !isset($_POST['submitbutt']))
					{
						$val = $profile->iq_live_credit;
					}
					$element = array(
						'name'	=>	'iq_live_credit',
						'id'	=>	'iq_live_credit',
						'value'	=>	$val,
						'class' => 'form-control',
						'placeholder' => get_languageword('credits'),
					);			
					echo form_input($element);
					?>
				</div>
			</div>
			
			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>
		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
	
	$(function() {
   });
	
</script>
<!-- Dashboard panel ends -->