<!-- Dashboard panel -->
<style>
	.collab_table{
		text-align: center;
	}
	.collab_table tr td,
	.collab_table tr th{
		border:1px solid black;
	}
</style>
<div class="dashboard-panel">
	<?php echo $message;?>
	<div class="row">
		
		<?php 
		$attributes = array('name' => 'edit_event', 'id' => 'edit_event', 'class' => 'comment-form dark-fields');
		echo form_open_multipart('tutor/change_event',$attributes);?>
			<div class="col-sm-6 ">
				<div class="input-group ">
					<label><?php echo get_languageword('Date:');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post( 'date' );
					}else{
						$val = $date;
					}
					
					$element = array(
						'name'	=>	'date',
						'id'	=>	'date',
						'value'	=>	$val,
						'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword(''),
					);			
					echo form_input($element);
					?>
					<?php echo form_error('date');?>
				</div>
			</div>
			
			<div class="col-sm-6 " id="time_slot">
				<div class="input-group ">
					<label><?php echo get_languageword('Time Slot');?><?php echo required_symbol();?></label>
					<?php			   
					$val = '';
					if( isset($_POST['submitbutt']) )
					{
						$val = $this->input->post('time_slot' );
					}else{
						$val = $time_slot;
					}
					
					$element = array(
						'name'	=>	'time_slot',
						'id'	=>	'time_slot',
						'value'	=>	$val,
						'required' => 'required',
						'class' => 'form-control',
						'placeholder' => get_languageword('Example format 6-7,13-14,14-16,20.30-21.30'),
					);
					echo form_input($element);
					?>
					<?php echo form_error('time_slot');?>
				</div>
			</div>
			<input type="hidden" name="type" value="<?php echo $type;?>"/>
			<input type="hidden" name="time_slot_old" value="<?php echo $time_slot;?>"/>
			<input type="hidden" name="date_old" value="<?php echo $date;?>"/>
			<input type="hidden" name="booking_id" value="<?php echo $booking_id;?>"/>
			<div class="col-sm-12 ">
				<button class="btn-link-dark dash-btn" name="submitbutt" type="Submit"><?php echo get_languageword('SAVE');?></button>
			</div>

		<?php echo form_close();?>
	</div>

</div>
<script src="<?php echo URL_FRONT_JS;?>jquery.js"></script>
<script>
	$(function() {
       $("#date").datepicker({
           dateFormat: 'yy-mm-dd',
           defaultDate: "+1w",
           changeMonth: true,
           minDate: 0,
           onSelect: function() {
           }
       });
   });
	
</script>
<!-- Dashboard panel ends -->