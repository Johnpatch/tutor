<!-- Event Dashboard panel -->
<div class="dashboard-panel">
	<form action="<?php echo site_url('tutor/save_zoom_details');?>" method="post" class="comment-form dark-fields" enctype="multipart/form-data">
		
		<div class="row">
			<label style="font-size:25px;">Upcoming Events:</label>
		</div>
		<?php
		foreach($upcoming_events as $key => $value){ 
		if(!empty($value->tutor_date)){?>
		<div class="row" style="border:1px solid #14bdee;margin-bottom:10px;">
			<div class="col-sm-5">
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">Course Title:<?php echo $value->course_name;?></div>
				</div>
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">Date:<?php echo date($date_format,strtotime($value->tutor_date));?></div>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">Student Name:<a href="<?php echo site_url('user-profile/'.$value->slug);?>"><?php echo $value->username;?></a></div>
				</div>
				<div class="row">
					<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">Time Slot:<?php echo $value->tutor_time_slot;?></div>
				</div>
			</div>
			<div class="col-sm-2">
				<a href="#" onclick="remove_event(<?php echo $value->booking_id;?>,1,'<?php echo $value->course_name;?>','<?php echo $value->tutor_date;?>','<?php echo $value->tutor_time_slot;?>','<?php if(isset($value->org_date)){echo $value->org_date;}?>','<?php if(isset($value->org_time_slot)){echo $value->org_time_slot;}?>')" style="float: right;margin-left: 5px;">
					<img src="<?php echo base_url();?>assets/front/images/delete.png" style="width: 16px;"/>
				</a>
				<a href="#" onclick="change_event(<?php echo $value->booking_id;?>,1,'<?php echo $value->tutor_date;?>','<?php echo $value->tutor_time_slot;?>','<?php if(isset($value->org_date)){echo $value->org_date;}?>','<?php if(isset($value->org_time_slot)){echo $value->org_time_slot;}?>')" style="float:right;">
					<img src="<?php echo base_url();?>assets/front/images/pencil.png" style="width:16px;"/>
				</a>
			</div>
			<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">
			<?php if($my_profile->fluidmath == 'Yes'){
			if(!empty($value->fluidmath_link)){ ?>
			<a href="<?php  echo $value->fluidmath_link;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span></a>
			<?php }else if(!empty($value->fluidmath_url)){ ?>
			<a href="<?php  echo $value->fluidmath_url;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span></a>
			<?php } else{?>
				<span class="nav-btn" style="padding: 0px 18px;cursor:pointer;" onclick="no_fluid()"><img src="<?php echo base_url();?>assets/front/images/fluid.png" style="width:20px;margin-top:-4px;"/> Fluidmath</span>
			<?php } ?>
			<a href="<?php echo site_url('tutor/edit_event_fluid_link/2/'.$value->booking_id);?>" style="color:blue;text-decoration: none;"><span class="edit-icon"><img src="<?php echo base_url();?>assets/grocery_crud/themes/flexigrid/css/images/edit.png" style="width:17px;"/></span></a>
			<?php } ?>
			
			<?php if($my_profile->zoom_id == 'Yes'){
			if(!empty($value->tutor_zoom_link)){ ?>
			<a href="<?php  echo $value->tutor_zoom_link;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span></a>
			<?php }if(!empty($value->zoom_pim_url)){ ?>
			<a href="<?php  echo $value->zoom_pim_url;?>" target="_blank"><span class="nav-btn" style="padding: 0px 18px;"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span></a>
			<?php } else{?>
				<span class="nav-btn" style="padding: 0px 18px;cursor:pointer;" onclick="no_zoom()"><img src="<?php echo base_url();?>assets/front/images/zoom.png" style="width:20px;margin-top:-4px;"/>JOIN ZOOM</span>
			<?php } ?>
			<a href="<?php echo site_url('tutor/edit_event_zoom_link/2/'.$value->booking_id);?>" style="color:blue;text-decoration: none;"><span class="edit-icon"><img src="<?php echo base_url();?>assets/grocery_crud/themes/flexigrid/css/images/edit.png" style="width:17px;"/></span></a>
			<?php } ?>
			
			
				<?php if($my_profile->whiteboard == 'Yes'){ ?>
				<a href="<?php echo $value->whiteboard_url?>" target="_blank">
					<span class="nav-btn" style="padding: 0px 24px;"><img src="<?php echo base_url();?>assets/front/images/whiteboard.png" style="width:17px;"/> &nbsp; Whiteboard</span>
                </a>
                <?php } ?>
                <a href="https://calendar.google.com/calendar/r/day/<?php echo date('Y',strtotime($value->tutor_date))."/".date('m',strtotime($value->tutor_date))."/".date('d',strtotime($value->tutor_date)) ?>" target="_blank">
					<span class="nav-btn" style="padding: 2px 24px;"><img src="<?php echo base_url();?>assets/front/images/google-calendar.png" style="width:24px;"/></span>
                </a>
			</div>
			<div class="col-sm-12" style="margin-bottom: 5px;font-size:15px;">ZOOM Joining Details:</div>
			<div class="col-sm-12"><textarea name="<?php echo $value->booking_id."_".$tomorrow_date;?>" style="width:100%;height: 150px;" <?php if(empty($value->description)){ echo 'readonly';}?>><?php if(!empty($value->description)){echo strip_tags($value->description);}else{echo strip_tags($value->zoom_joining_details);}?></textarea></div>
		</div>
		<?php }} ?>
		
		
		
		<div class="row">
			<div class="col-sm-12">
				<input type="hidden" name="upcoming"/>
				<button class="btn-link-dark dash-btn n-b" type="Submit">Save</button>
			</div>
		</div>
	</form>
</div>
<!-- Event Dashboard panel ends --> 
