<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Tutor_model extends Base_Model  
{
	var $numrows;
	function __construct()
	{
		parent::__construct();
	}
	
	/****** GET SUBJECTS	
	* Author @
	* Adi
	******/
	function get_subjects()
	{		
		$subjects = array();		
		$parentSubjectDetails = $this->db->select('id AS parentSubject_id, subject_name AS parentSubject_name')->get_where($this->db->dbprefix( 'subjects' ), array('subject_parent_id' => 0, 'status' => 'Active'))->result();
		
		foreach($parentSubjectDetails as $p) {		
			$query = "SELECT s.*, (SELECT count(*) FROM ".$this->db->dbprefix( 'tutor_subjects' )." ts WHERE ts.subject_id = s.id AND ts.status = 'Active') AS no_of_tutors FROM ".$this->db->dbprefix( 'subjects' )." s 	WHERE s.subject_parent_id = ".$p->parentSubject_id." AND s.status = 'active'";			
			$childSubjects = $this->db->query($query)->result();			
			$subjects[$p->parentSubject_name] = $childSubjects;		
		}
		return $subjects;	
	}
	
	/****** GET TUTOR SUBJECT IDs
	* Author @
	* Adi
	******/
	function get_tutor_subject_ids($tutor_id = null)
	{
	
		$tutorSubjectIds = array();
		
		if($tutor_id != null && is_numeric($tutor_id)) {
		
			$tutorSubjectsRec = $this->db->select('subject_id')->get_where($this->db->dbprefix( 'tutor_subjects' ), array('user_id' => $tutor_id, 'status' => 'Active'))->result();
				
			foreach($tutorSubjectsRec as $t)
				array_push($tutorSubjectIds, $t->subject_id);
		}
		
		return $tutorSubjectIds;
	
	}
	
	/****** GET TUTOR SUBJECTS	
	* Author @
	* Adi
	******/
	function getTutorSubjects($tutor_id = null)
	{		
		$tutorSubjects = array();
		$tutorSubjectsArr = array();
		
		if($tutor_id != null && is_numeric($tutor_id)) {
		
			$tutorSubjectsRec = $this->db->select('subject_id')->get_where($this->db->dbprefix( 'tutor_subjects' ), array('user_id' => $tutor_id, 'status' => 'Active'))->result();
			
			foreach($tutorSubjectsRec as $t)
				array_push($tutorSubjectsArr, $t->subject_id);
		
			$parentSubjectDetails = $this->db->select('id AS parentSubject_id, subject_name AS parentSubject_name')->get_where($this->db->dbprefix( 'subjects' ), array('subject_parent_id' => 0, 'status' => 'Active'))->result();
			
			foreach($parentSubjectDetails as $p) {
			
				$childSubjects = $this->db->query("SELECT * FROM ".$this->db->dbprefix( 'subjects' )." WHERE subject_parent_id = ".$p->parentSubject_id." AND id IN (".implode(',', $tutorSubjectsArr).") AND status='Active'")->result();
				
				if(count($childSubjects) > 0)
					$tutorSubjects[$p->parentSubject_name] = $childSubjects;		
			}
		}

		return $tutorSubjects;
	
	}
	
	/****** GET LOCATIONS	
	* Author @
	* Adi
	******/
	function get_locations()
	{
		
		$locations = array();
		
		$parentLocationDetails = $this->db->select('id AS parentLocation_id, location_name AS parentLocation_name')->get_where($this->db->dbprefix( 'locations' ), array('parent_location_id' => 0, 'status' => 'Active'))->result();
		
		foreach($parentLocationDetails as $p) {
		
			$query = "SELECT l . * , (

					SELECT count( * )
					FROM ".$this->db->dbprefix( 'tutor_locations' )." tl,
					 ".$this->db->dbprefix( 'users' )." u,
					 ".$this->db->dbprefix( 'users_groups' )." ug
					WHERE (tl.location_id = l.id OR 
					u.location_id = l.id) 
					AND ug.group_id = 3
					AND ug.user_id = u.id
					AND u.id = tl.tutor_id
					AND u.active = 1
					AND tl.status = '1'
					) AS no_of_tutors
					FROM ".$this->db->dbprefix( 'locations' )." l
					WHERE l.parent_location_id = ".$p->parentLocation_id."
					AND l.status = 'active'";
			
			$childLocations = $this->db->query($query)->result();
			
			$locations[$p->parentLocation_name] = $childLocations;		
		}

		return $locations;
	
	}
	
	/****** GET TUTOR LOCATION IDs
	* Author @
	*Raghu
	******/
	function get_tutor_location_ids($tutor_id = null)
	{
	
		$tutorLocationIds = array();
		
		if($tutor_id != null && is_numeric($tutor_id)) {
		
			$tutorLocationsRec = $this->db->select('location_id')->get_where($this->db->dbprefix( 'tutor_locations' ), array('tutor_id' => $tutor_id, 'status' => '1'))->result();
				
			foreach($tutorLocationsRec as $l)
				array_push($tutorLocationIds, $l->location_id);
		}
		
		return $tutorLocationIds;
	
	}
	
	/****** GET TUTOR LOCATIONS	
	* Author @
	* Adi
	******/
	function get_tutor_locations($tutor_id = null)
	{		
		$tutorLocations = array();
		$tutorLocationsArr = array();
		
		if($tutor_id != null && is_numeric($tutor_id)) {
		
			$tutorLocationsRec = $this->db->select('location_id')->get_where($this->db->dbprefix( 'tutor_locations' ), array('tutor_id' => $tutor_id, 'status' => '1'))->result();
			
			foreach($tutorLocationsRec as $l)
				array_push($tutorLocationsArr, $l->location_id);
		
			$parentLocationDetails = $this->db->select('id AS parentLocation_id, location_name AS parentLocation_name')->get_where($this->db->dbprefix( 'locations' ), array('parent_location_id' => 0, 'status' => 'Active'))->result();
			
			foreach($parentLocationDetails as $p) {
			
				$childLocations = $this->db->query("SELECT * FROM ".$this->db->dbprefix( 'locations' )." WHERE parent_location_id = ".$p->parentLocation_id." AND id IN (".implode(',', $tutorLocationsArr).") AND status='Active'")->result();
				
				if(count($childLocations) > 0)
					$tutorLocations[$p->parentLocation_name] = $childLocations;		
			}
		}

		return $tutorLocations;
	
	}
	
	/****** GET TUTOR Teaching type IDs
	* Author @
	* Adi
	******/
	function get_tutor_selected_teachingtype_ids($tutor_id = null)
	{
	
		$tutorSelectedTypeIds = array();
		
		if($tutor_id != null && is_numeric($tutor_id)) {
		
			$tutorSelectedTypesRec = $this->db->select('teaching_type_id')->get_where($this->db->dbprefix( 'tutor_teaching_types' ), array('tutor_id' => $tutor_id, 'status' => '1'))->result();
				
			foreach($tutorSelectedTypesRec as $t)
				array_push($tutorSelectedTypeIds, $t->teaching_type_id);
		}
		
		return $tutorSelectedTypeIds;
	
	}
	
	/**
	 * Get tutor types
	 *
	 * @access	public
	 * @param	void
	 * @return	mixed
	 */
	function get_tutor_teachingtypes()
	{		
		$tutorTypes = $this->db->get_where($this->db->dbprefix( 'teaching_types' ), array('status' => '1'))->result();		
		return $tutorTypes;	
	}
	
	/**
	 * Get tutor types
	 *
	 * @access	public
	 * @param	void
	 * @return	mixed
	 */
	function list_tutor_packages()
	{		
		$query = "select * from " . $this->db->dbprefix('packages') . " 
		where status = 'Active' AND (package_for='All' OR package_for='Tutor')";
		$packages = $this->db->query($query)->result();
		return $packages;	
	}

	

	function get_tutor_assigned_course($user_id = null, $inst_id=null)
	{
		
		$user_id = (!empty($user_id)) ? $user_id : $this->ion_auth->get_user_id();
		$inst_id = (!empty($inst_id)) ? $inst_id : is_inst_tutor();

		if(empty($user_id) || empty($inst_id))
			return array();

		$tutor_assigned_courses_opts = array('' => get_languageword('no_courses_available'));

		$query = "SELECT ib.course_id, c.name FROM ".$this->db->dbprefix('inst_batches')." ib INNER JOIN ".$this->db->dbprefix('categories')." c ON c.id=ib.course_id WHERE ib.inst_id=".$inst_id." AND ib.tutor_id=".$user_id." GROUP BY ib.course_id ORDER BY c.name";

		$tutor_assigned_courses = $this->db->query($query)->result();

		if(!empty($tutor_assigned_courses)) {

			$tutor_assigned_courses_opts = array('' => get_languageword('select_course'));

			foreach ($tutor_assigned_courses as $key => $value)
				$tutor_assigned_courses_opts[$value->course_id] = $value->name;
		}

		return $tutor_assigned_courses_opts;
	}


	function get_tutor_dashboard_data($tutor_id = "")
	{

		 $tutor_dashboard_data = array();

		$query = "select count(*) total_bookings from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id;
		$tutor_dashboard_data['total_bookings'] = $this->db->query($query)->row()->total_bookings;

		$query = "select count(*) pending_bookings from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id." And status='pending'";
		$tutor_dashboard_data['pending_bookings'] = $this->db->query($query)->row()->pending_bookings;

		$query = "select count(*) completed_bookings from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id." And status='completed'";
		$tutor_dashboard_data['completed_bookings'] = $this->db->query($query)->row()->completed_bookings;

		$query = "select count(*) running_bookings from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id." And status='running'";
		$tutor_dashboard_data['running_bookings'] = $this->db->query($query)->row()->running_bookings;

		$query = "select count(*) courses from ".$this->db->dbprefix('tutor_courses')." where tutor_id=".$tutor_id;
		$tutor_dashboard_data['courses'] = $this->db->query($query)->row()->courses;

		$query = "select count(*) messages from ".$this->db->dbprefix('chat')." where to_id=".$tutor_id." AND tutor_status=0";
		$tutor_dashboard_data['messages'] = $this->db->query($query)->row()->messages;

		$query = "SELECT COUNT(question_id) as questions FROM ".$this->db->dbprefix('booking_questions')." WHERE booking_id IN (SELECT booking_id from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id.") AND question_id NOT IN (SELECT question_id from ".$this->db->dbprefix('booking_answers')." where tutor_status='0')";
		
		$questions_count = $this->db->query($query)->row()->questions;
		

		$query = "SELECT COUNT(question_id) as questions FROM ".$this->db->dbprefix('booking_questions')." WHERE booking_id IN (SELECT booking_id from ".$this->db->dbprefix('bookings')." where tutor_id=".$tutor_id.") AND question_id IN (SELECT question_id from ".$this->db->dbprefix('booking_answers')." where tutor_status='0')";


		$questions_conversion_count = $this->db->query($query)->row()->questions;

		$tutor_dashboard_data['questions'] = $questions_count+$questions_conversion_count;
		
		$query = "SELECT COUNT(*) as blogs FROM ".$this->db->dbprefix('tutor_blogs')." WHERE blog_status = 'Active' AND admin_approved = 'Yes'";
		$tutor_dashboard_data['blogs'] = $this->db->query($query)->row()->blogs;
		
		$query = "SELECT COUNT(*) as leads FROM ".TBL_USERS." u INNER JOIN ".TBL_USERS_GROUPS." ug ON ug.user_id=u.id INNER JOIN ".TBL_STUDENT_LEADS." sl ON sl.user_id=u.id WHERE u.active=1 AND u.visibility_in_search='1' AND u.availability_status='1' AND u.is_profile_update=1 AND ug.group_id=2 AND sl.status='opened'";
		$tutor_dashboard_data['leads'] = $this->db->query($query)->row()->leads;
		
		$query = "SELECT count(*) iqs FROM pre_instant_bids where tutor_id = ".$tutor_id." and status != 'complete'";
		$tutor_dashboard_data['iqs'] = $this->db->query($query)->row()->iqs;

		return $tutor_dashboard_data;
	}
	
	function get_inst_tutor_dashboard($tutor_id=" ")
	{
		$query = "SELECT u.username as inst_name, c.name courses ,count(ib.course_id) batches FROM ".$this->db->dbprefix('inst_batches')." ib join ".$this->db->dbprefix('users')." u on ib.inst_id=id join ".$this->db->dbprefix('categories')." c on ib.course_id= c.id  where tutor_id=".$tutor_id." group by ib.course_id,ib.inst_id";
		
		$get_inst_tutor_dashboard = $this->db->query($query)->result();

		return $get_inst_tutor_dashboard;
	}
	
	function get_blogs($tutor_id='')
	{
		$cond="";
		if(!empty($tutor_id)) {
			$cond .= " WHERE b.tutor_id=".$tutor_id." ";
		} else {
			$cond .= " WHERE u.active=1 ";
		}
		
		$blogs = array();
		
		$query = "SELECT b.*,u.username FROM ".$this->db->dbprefix('tutor_blogs')." b INNER JOIN ".$this->db->dbprefix('users')." u ON b.tutor_id=u.id  ";

		$blogs = $this->base_model->get_query_result($query);
		
		return $blogs;
	}
	
	function get_booking_detail($booking_id){
		if(!empty($booking_id)){
			$sql = "SELECT * FROM ".$this->db->dbprefix('bookings')." WHERE booking_id = ".$booking_id;
			return $this->db->query($sql)->row();	
		} else{
			return 1;
		}	
	}

	function get_instant_booking_detail($instant_id){
		if(!empty($instant_id)){
			$sql = "SELECT * FROM ".$this->db->dbprefix('instant_bids')." WHERE id = ".$instant_id;
			return $this->db->query($sql)->row();	
		} else{
			return 1;
		}
	}
	
	function get_session_exist($time_slot, $start_date,$end_date,$tutor_id,$tution_type,$course_id,$booking_id,$preferred_location){
		$sql = "";
		$temp['cnt'] = 0;
		if($tution_type == "One on One"){
			$sql = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' AND booking_id != ".$booking_id;
			return $this->db->query($sql)->row();
		}
		else if($tution_type == "Share"){
			$sql = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='One on One' and course_id = '".$course_id."'";
			$result = (array)$this->db->query($sql)->row();
			if($result['cnt'] > 0)
				return $result;
			else{
				$sql1 = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='Share' and course_id = '".$course_id."' and preferred_location != '".$preferred_location."'";
				$result1 = (array)$this->db->query($sql1)->row();
				
				$sql3 = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='Share' and course_id = '".$course_id."' and preferred_location = '".$preferred_location."'";
				$result3 = (array)$this->db->query($sql3)->row();
				
				$sql2 = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='Share' and course_id != '".$course_id."'";
				$result2 = (array)$this->db->query($sql2)->row();
				if($result1['cnt'] > 0){
					return $result1;
				} else if($result3['cnt'] > 0){
					return $temp;
				} else{
					if($result2['cnt'] > 0)
						return $result2;
					else
						return $temp;
				}
			}
		}
		$temp['cnt'] = 1;
		return $temp;
	}
	
	function get_same_book_session_exist($time_slot, $start_date,$end_date,$tutor_id,$tution_type,$course_id,$booking_id,$preferred_location){
		$sql = "";
		if($tution_type == "One on One"){
			$sql = "SELECT * FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' AND booking_id != ".$booking_id."";
			return (array)$this->db->query($sql)->result();
		}
		else if($tution_type == "Share"){
			$sql = "SELECT * FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='One on One' and course_id = '".$course_id."'";
			$result = (array)$this->db->query($sql)->result();
			if(count($result) > 0)
				return $result;
			else{
				$sql1 = "SELECT * FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='Share' and course_id = '".$course_id."' AND booking_id != ".$booking_id."";
				$result1 = (array)$this->db->query($sql1)->result();
				
				$sql2 = "SELECT * FROM ".$this->db->dbprefix('bookings')." WHERE time_slot = '".$time_slot."' and (end_date >= '".$start_date."' AND start_date <= '".$end_date."') and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."' and tution='Share' and course_id != '".$course_id."' AND booking_id != ".$booking_id."";
				$result2 = (array)$this->db->query($sql2)->result();
				if(count($result1) > 0){
					return $result1;
				}  else{
					if(count($result2) > 0)
						return $result2;
					else
						return NULL;
				}
			}
		}
		return NULL;
	}
	
	function get_zoom_session_exist($booking_id){
		$sql = "SELECT * FROM ".$this->db->dbprefix('zoom_session')." WHERE booking_id = ".$booking_id." AND (delete_indicator != 1 OR delete_indicator IS NULL)";
		return $this->db->query($sql)->row();
	}

	function get_iq_session_exist($date,$time,$tutor_id){
		$sql = "SELECT COUNT(*) as cnt FROM ".$this->db->dbprefix('instant_bids')." WHERE iq_date = '".$date."' iq_time_slot = '".$time."' and (status = 'approved' OR status='running' OR status='session_initiated') and tutor_id = '".$tutor_id."'";
		return $this->db->query($sql)->row();
	}
	
	function get_zoom_info_list(){
		$sql = "SELECT * FROM ".$this->db->dbprefix('zoom')." WHERE active_flag = 1";
		return $this->db->query($sql)->result_array();
	}
	
	function get_zoom_meeting_list($start_time,$start_date,$duration){
		$end_date = date("Y-m-d",strtotime($start_date.' +'.$duration));
		$sql = "SELECT * FROM ".$this->db->dbprefix('zoom_session')." WHERE start_time = '".$start_time."' and (start_date >= '".$start_date."' AND start_date <= '".$end_date."') AND (delete_indicator != 1 OR delete_indicator IS NULL)";
		return $this->db->query($sql)->result_array();
	}
	
	function insert_zoom_session($data){
		return $this->db->insert($this->db->dbprefix('zoom_session'), $data);
	}
	
	function get_tutor_info($user_id){
		$sql = "SELECT * FROM ".$this->db->dbprefix('users')." WHERE id = ".$user_id;
		return $this->db->query($sql)->row();
	}
	
	function get_student_iqs($params = array(),$user_id)
    {
        $query 				= "";
        $limit_cond 		= "";
        $teaching_type_cond = "";
        if(!empty($params['start']) && !empty($params['limit']) && $params['start'] >= 0 && $params['limit'] >= 0) {
            $limit_cond = ' LIMIT '.$params['start'].', '.$params['limit'];
        } elseif(empty($params['start']) && !empty($params['limit']) && $params['limit'] >= 0){
            $limit_cond = ' LIMIT '.$params['limit'];
        }
        if(!empty($params['teaching_type_slug'])) {
        	$param_string = "";
        	if(is_array($params['teaching_type_slug'])){
				foreach($params['teaching_type_slug'] as $k => $v){
	        		if($k != 0)
	        			$param_string .= ",";
					$param_string .= "'".$v."'";
				}
	        	$teaching_type_cond = " AND sl.teaching_type_id IN (".$param_string.") ";	
			} else{
				$teaching_type_cond = " AND sl.teaching_type_id IN ('".$params['teaching_type_slug']."') ";	
			}
        }
    	$query = "SELECT u.*, sl.*, sl.id AS lead_id FROM pre_instant_bids sl INNER JOIN pre_users u ON sl.student_id = u.id WHERE sl. STATUS = 'new'
AND sl.tutor_id =  ".$user_id." ".$teaching_type_cond." ORDER BY sl.id DESC ".$limit_cond." ";
        $result_set = $this->db->query($query);
        return ($result_set->num_rows() > 0) ? $result_set->result() : array();
    }
    
    function is_already_viewed_the_lead($user_id = "", $reference_table = "", $reference_id = "")
    {
        if(empty($user_id) || empty($reference_table) || empty($reference_id))
            return FALSE;

        $is_exist = $this->db->select('id')->get_where(TBL_USER_CREDIT_TRANSACTIONS, array('user_id' => $user_id, 'reference_table' => $reference_table, 'reference_id' => $reference_id))->row();
        if(count($is_exist) > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    function get_student_profile($student_slug = "",$student_lead_id= "")
    {
        if(empty($student_slug))
            return NULL;

        $CI =& get_instance();
		
		
		if(!$this->ion_auth->logged_in()) {
			$CI->prepare_flashmessage(get_languageword('please_login_to_continue.'), 2);
			return redirect(URL_AUTH_LOGIN);
		}
		

        $student_id = $this->get_uid_by_slug($student_slug);

        if(empty($student_id))
            return NULL;

        $student_info_query = "SELECT * FROM ".TBL_USERS." WHERE id=".$student_id." AND active=1 AND visibility_in_search='1' AND availability_status='1' ";

        $student_details = $this->db->query($student_info_query)->result();

        if(!empty($student_details)) {

            if($student_lead_id > 0) {

                $lead_info_query = "SELECT * FROM pre_instant_bids left join pre_categories on pre_categories.id = pre_instant_bids.category WHERE pre_instant_bids.id = ".$student_lead_id;

                $lead_details = $this->db->query($lead_info_query)->result();

                /*if(!empty($lead_details) && !$this->ion_auth->is_admin()) {

                    $credits_required_for_viewing_lead = $lead_details[0]->budget;

                    if($credits_required_for_viewing_lead > 0) {

                        $viewer_id = $this->ion_auth->get_user_id();

                        if(!$this->is_already_viewed_the_lead($viewer_id, 'pre_instant_bids', $student_lead_id)) {

                            $viewer_credits = get_user_credits($viewer_id);

                            if($viewer_credits >= $credits_required_for_viewing_lead) {

                                //Log Credits transaction data & update user net credits - Start
                                $log_data = array(
                                                'user_id' => $viewer_id,
                                                'credits' => $credits_required_for_viewing_lead,
                                                'per_credit_value' => get_system_settings('per_credit_value'),
                                                'action'  => 'debited',
                                                'purpose' => get_languageword('For Instant Bid ').' "'.$lead_details[0]->title_of_requirement.'" '.get_languageword('of Student').' "'.$student_details[0]->username.'"',
                                                'date_of_action ' => date('Y-m-d H:i:s'),
                                                'reference_table' => 'student_leads',
                                                'reference_id' => $student_lead_id,
                                            );

                                log_user_credits_transaction($log_data);

                                update_user_credits($viewer_id, $credits_required_for_viewing_lead, 'debit');
                                //Log Credits transaction data & update user net credits - End

                                //Update Lead View Count
                                $this->update_lead_view_count($student_lead_id);

                            } else {

								
									$hlink = '#';
									if($this->ion_auth->is_tutor())
										$hlink = URL_TUTOR_LIST_PACKAGES;
									else if($this->ion_auth->is_institute())
										$hlink = URL_INSTITUTE_LIST_PACKAGES;

									$CI->prepare_flashmessage(get_languageword('you_don\'t_have_enough_credits_to_view_the_lead_details. Please')." <a href='".$hlink."'><strong>".get_languageword('_get_credits_here.')."</strong></a> ", 2);
									return redirect(URL_HOME_SEARCH_STUDENT_LEADS);
								
								
                            }
                        }
                    }
                }*/
                $student_details[0]->lead_details = $lead_details;
            }

             //Student's Gallery
            $student_gallery_query = "SELECT image_title, image_name FROM ".$this->db->dbprefix('gallery')." WHERE user_id=".$student_id." AND image_status='Active' ORDER BY image_order ASC";
            $student_details[0]->student_gallery = $this->db->query($student_gallery_query)->result();

            return $student_details;

        } else return array();

    }
    
    function get_uid_by_slug($uslug = "")
    {
        if(empty($uslug))
            return NULL;

        $row = $this->db->select('id')->get_where($this->db->dbprefix('users'), array('slug' => $uslug))->row();

        return (!empty($row)) ? $row->id : '';
    }
    
    function update_update_iq_bids($data,$userid){
		$sql = "UPDATE pre_instant_bids SET ";
		$sql .= " updated_at = '".date("Y-m-d H:i:s")."', updated_by = ".$userid.", status = 'pending' ";
		if(!empty($data['tutor_iq_date']))
			$sql .= " ,iq_date = '".$data['tutor_iq_date']."', iq_time_slot = '".$data['tutor_iq_time']."'";
		if(!empty($data['tutor_bid_credit']))
			$sql .= " ,budget = ".$data['tutor_bid_credit'];
		$sql .= " where id = ".$data['iq_id'];
		$this->db->query($sql);
		$sql = "SELECT * FROM pre_instant_bids WHERE id = ".$data['iq_id'];
		return $this->db->query($sql)->row();
	}
	
	function get_user_email($userid){
		$sql = "SELECT email FROM pre_users where id=".$userid;
		$result = $this->db->query($sql)->row();
		return $result->email;
	}
	
	function update_record_attach($instant_id,$record_url){
		$sql = "UPDATE pre_instant_bids SET tutor_attach_link = '".$record_url."' WHERE id = ".$instant_id;
		$this->db->query($sql);
	}
	
	function get_user_info($student_id){
		$sql = "SELECT * FROM pre_users where id = ".$student_id;
		return $this->db->query($sql)->row();
	}
	
	public function get_credit_value($tutor_id, $course_id){
		$sql = "SELECT * FROM pre_tutor_courses WHERE tutor_id = ".$tutor_id." and course_id = ".$course_id;
		$result = $this->db->query($sql)->row();
		return $result->fee;
	}
	
	public function get_same_course_time_cnt($booking_info){
		$sql = "SELECT COUNT(*) as cnt FROM pre_bookings WHERE tutor_id = ".$booking_info['tutor_id']." and course_id = ".$booking_info['course_id']." and time_slot = '".$booking_info['time_slot']."' and tution='Share' and start_date='".$booking_info['start_date']."'";
		$cnt = $this->db->query($sql)->row();
		return $cnt->cnt;
	}
	
	public function update_credits($booking_info,$credit){
		$sql = "UPDATE pre_bookings SET fee = ".$credit." WHERE booking_id = ".$booking_info['booking_id'];
		$this->db->query($sql);
		$sql = "UPDATE pre_bookings SET fee = ".$credit." WHERE tutor_id = ".$booking_info['tutor_id']." and course_id = ".$booking_info['course_id']." and time_slot = '".$booking_info['time_slot']."' and (status = 'session_initiated' OR status='running') and tution='Share' and start_date='".$booking_info['start_date']."'";
		$this->db->query($sql);
		
	}
	
	public function set_whiteboard_url($booking_id, $url){
		$sql = "UPDATE pre_zoom_session set whiteboard_url = '".$url."' WHERE booking_id = ".$booking_id;
		$this->db->query($sql);
	}
}

?>