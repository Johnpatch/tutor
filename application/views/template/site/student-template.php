<?php $this->load->view('template/site/header'); ?>
<!-- Page Title Wrap  -->
<div class="page-title-wrap">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			</div>
		</div>
	</div>
</div>
<!-- Page Title Wrap  -->

<!-- News Scroller  -->
<?php $this->load->view('template/site/scroller'); ?>
<!-- News Scroller  -->

<!-- Dashboard section  -->
<?php 
if($this->session->userdata('first_login') == 1 && $this->session->userdata('user_belongs_group') == 5){ ?>
<section class="dashboard-section">
	<div class="container" style="margin-bottom: 20px;">
		<div class="row text-center">
			<label style="font-size:30px;">Choose a student login:</label>
		</div>
		<div class="row text-center" id="student_choose_section">
			<?php foreach($child_list as $key => $value){ ?>
				<div class="col-sm-6" style="margin-top: 30px;">
					<a href="<?php echo site_url('student/index/'.$value->id);?>" class="btn-link-dark"><?php echo $value->first_name.' '.$value->last_name; ?></a>
				</div>	
			<?php } ?>
		</div>
	</div>
</section>
<?php } else{
?>
<section class="dashboard-section">
	<div class="container">
		<div class="row offcanvas offcanvas-right row-margin">
		   <div class="col-xs-8 col-sm-4 sidebar-offcanvas" id="sidebar">
				<?php $this->load->view('template/site/student-template-leftmenu'); ?>
				<!-- /.panel-group -->
			</div>
			<div class="col-xs-12 col-sm-8 dashboard-content ">
				<!-- breadcrumb -->
				<ol class="breadcrumb dashcrumb">
					<li><a href="<?php echo base_url();?>student/index"><?php echo get_languageword('Home');?></a></li>
					<li class="active"><?php if(isset($pagetitle)) echo $pagetitle?></li>
				</ol>
				<!-- breadcrumb ends -->
				<?php $this->load->view($content); ?>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<!-- Dashboard section  -->

<?php $this->load->view('template/site/footer'); ?>